from simplesmsforms.smsform_exceptions import SMSFieldException
import const
import db_lookups

class GenericSMSFieldException(SMSFieldException):
	def __str__(self):
		return db_lookups.lookup_messages_text("GENERIC_SMS_FIELD_ERROR").format(field=self.field)

class FutureDateException(SMSFieldException):
    pass

class PastThreeMonthsException(SMSFieldException):
    pass

class InvalidBeneficiaryIDException(SMSFieldException):
	def __str__(self):
		return db_lookups.lookup_messages_text("INVALID_BENEFICIARY_ID_ERROR").format(field=self.field)

class PersonIsVoidException(SMSFieldException):
	def __str__(self):
		return db_lookups.lookup_messages_text("PERSON_VOID_ERROR").format(field=self.field)

class AuthenticationException(SMSFieldException):
    def __str__(self):
        return self.field

class InvalidOrgUnitSelectionException(SMSFieldException):
	def __str__(self):
		return db_lookups.lookup_messages_text("SELECTION_ERROR_INVALID_ORG_UNIT").format(field=self.field)

class NonExistantOrgUnitException(SMSFieldException):
	def __str__(self):
		return  db_lookups.lookup_messages_text("SELECTION_ERROR_NON_EXISTANT_ORG_UNIT").format(field=self.field)

class InvalidBeneficiaryWardException(SMSFieldException):
	def __str__(self):
		return db_lookups.lookup_messages_text("GEO_ERROR_INVALID_WARD").format(field=self.field)

class InvalidWorkerGeoException(SMSFieldException):
	def __str__(self):
		return db_lookups.lookup_messages_text("GEO_ERROR_INVALID_GEO_WORKER")

class InvalidWorkerIDException(SMSFieldException):
	def __str__(self):
		return db_lookups.lookup_messages_text("INVALID_WORKER_ERROR")


class NoPrimaryOrgUnit(SMSFieldException):
	def __str__(self):
		return db_lookups.lookup_messages_text("NO_PRIMARY_ORG_UNIT")

class NoServicesOrConditionsRecordedException(SMSFieldException):
	def __str__(self):
		return db_lookups.lookup_messages_text("NO_SERVICES_PROVIDED")

class NoMessagesToUndoException(SMSFieldException):
	def __str__(self):
		return db_lookups.lookup_messages_text("NO_MESSAGES_TO_UNDO_ERROR")

class UserNotAuthorisedServiceProvider(SMSFieldException):
	def __str__(self):
		return db_lookups.lookup_messages_text("MISSING_WORKFORCE_ID_ERROR")

class MissingPasswordException(SMSFieldException):
    def __str__(self):
        return db_lookups.lookup_messages_text("PASSWORD_IS_MISSING_ERROR")

class NoAdverseConditionOnGuardian(SMSFieldException):
    def __str__(self):
        return db_lookups.lookup_messages_text("NO_ADVERSE_ON_GUARDIAN_ERROR")

class MissingSearchTextException(SMSFieldException):
    def __str__(self):
        return db_lookups.lookup_messages_text("MISSING_SEARCH_TEXT_ERROR")
