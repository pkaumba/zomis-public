from rapidsms.apps.base import AppBase
from sms_forms import RECSForm, FINWForm, FINBForm, FINUForm
import const
import db_lookups


def construct_verbose_response(bound_fields):
    response_list = []
    for bound_field in bound_fields:
        field_name = bound_field[0]
        field_val = bound_field[1][1]
        if field_name in ["services_provided", "referral_provided", "adverse_conditions", "encounter_type"]:
            field_val = ", ".join(db_lookups.lookup_db_values(field_val))

        field_response_string = "{field_name}={field_val}".format(
            field_name=bound_field[0],
            field_val=field_val)
        response_list.append(field_response_string)

    return "---".join(response_list)

recast_messages = {
    "You must specify a password within your message":
    db_lookups.lookup_messages_text("PASSWORD_IS_MISSING_ERROR"),
    "You must specify a encounter_type within your message":db_lookups.lookup_messages_text("ENCOUNTER_TYPE_MISSING_ERROR")
}

def recast_error_texts(text):
    """Looks up the passed in text for alternative custom messages"""
    try:
        return recast_messages[text]
    except KeyError:
        return text

def handle_RECS(msg):
    recs_form = RECSForm()
    valid, bound_fields, errors = recs_form.process_form(
        msg.text)
    if valid:
        fields_dict = recs_form.bound_fields_to_bound_dict(bound_fields)
        saved, errors = db_lookups.save(msg, msg.connection.identity, fields_dict)
        if saved:
            msg.respond(db_lookups.lookup_messages_text(u"RECS_SUCCESS_MESSAGE"))

    if not valid or not saved:
        #Some errors from the SMSFORM app may need to be recast
        #to custom error messages. TODO: This is brittle
        errors = [recast_error_texts(str(error)) for error in errors]
        msg.respond(u", ".join([error for error in errors]))
    return True

def handle_FINU(msg):
    finu_form = FINUForm()
    valid, bound_fields, errors = finu_form.process_form(
        msg.text)
    if valid:
        fields_dict = finu_form.bound_fields_to_bound_dict(bound_fields)
        results, errors = db_lookups.process_finu(msg, msg.connection.identity,
                                        fields_dict)
        if results:
            results_string = db_lookups.construct_finu_results_response(results)
            if len(results) > 5:
                results_string = results_string + "There are more matches"
            msg.respond(results_string)
        if not results:
            msg.respond(db_lookups.lookup_messages_text("NO_ORG_UNITS_FOUND"))

    if not valid:
        #Some errors from the SMSFORM app may need to be recast
        #to custom error messages. TODO: This is brittle
        errors = [recast_error_texts(str(error)) for error in errors]
        msg.respond(u", ".join([error for error in errors]))
    return True

def handle_FINW(msg):
    finw_form = FINWForm()
    valid, bound_fields, errors = finw_form.process_form(
        msg.text)
    if valid and not errors:
        fields_dict = finw_form.bound_fields_to_bound_dict(bound_fields)
        results, errors = db_lookups.process_finw(msg, msg.connection.identity,
                                        fields_dict)
        if results:
            results_string = db_lookups.construct_finw_results_response(results)
            if len(results) > 5:
                results_string = results_string + "There are more matches"
            msg.respond(results_string)
        else:
            msg.respond(db_lookups.lookup_messages_text("NO_WORKFORCE_MEMBERS_FOUND"))

    if errors:
        #Some errors from the SMSFORM app may need to be recast
        #to custom error messages. TODO: This is brittle
        errors = [recast_error_texts(str(error)) for error in errors]
        msg.respond(u", ".join([error for error in errors]))
    return True

def handle_FINB(msg):
    finb_form = FINBForm()
    valid, bound_fields, errors = finb_form.process_form(
        msg.text)
    if valid:
        fields_dict = finb_form.bound_fields_to_bound_dict(bound_fields)
        results, errors = db_lookups.process_finb(msg, msg.connection.identity,
                                        fields_dict)
        if results:
            results_string = db_lookups.construct_finb_results_response(results)
            if len(results) > 5:
                results_string = results_string + "There are more matches"
            msg.respond(results_string)
            return True

    if errors:
        #Some errors from the SMSFORM app may need to be recast
        #to custom error messages. TODO: This is brittle
        errors = [recast_error_texts(str(error)) for error in errors]
        msg.respond(u", ".join([error for error in errors]))
    else:
        msg.respond(db_lookups.lookup_messages_text("NO_BENEFICIARIES_FOUND"))
    return True

class SMSApp(AppBase):
    def handle(self, msg):
        message_text = msg.text.strip().lower()
        if message_text == 'recs':
            msg.respond(db_lookups.lookup_messages_text("RECS_EXPECTED_STRING"))
            return True
        elif message_text.startswith("recs"):
            recs_return = handle_RECS(msg)
            if recs_return:
                return True
            else:
                return False
        elif message_text.startswith("finu"):
            finu_return = handle_FINU(msg)
            if finu_return:
                return True
            else:
                return False
        elif message_text.startswith("finw"):
            finw_return = handle_FINW(msg)
            if finw_return:
                return True
            else:
                return False
        elif message_text.startswith("finb"):
            finb_return = handle_FINB(msg)
            if finb_return:
                return True
            else:
                return False
        elif message_text == "undo" or message_text == "recs undo":
            undone_encounter_details, errors = db_lookups.undo_last_action(msg.connection.identity)
            if not undone_encounter_details:
                msg.respond(u", ".join([str(error) for error in errors]))
            else:
                beneficiary_id = undone_encounter_details['beneficiary_id']
                encounter_date = undone_encounter_details['encounter_date']

                response = db_lookups.lookup_messages_text("UNDO_SUCCESS_MSG").format(
                    beneficiary_id=beneficiary_id,
                    encounter_date=encounter_date.strftime("%d %b %Y")
                    )
                msg.respond(response.decode('utf-8'))
        else:
            msg.respond(db_lookups.lookup_messages_text("UNRECOGNISED_KEYWORD_ERROR"))
        return False
