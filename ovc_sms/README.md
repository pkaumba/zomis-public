OVC SMS Installation and Usage
==============================
##Overview
The ovc_sms module works as a standard Django application with an extra app.py
that RapidSMS expects.
We are currently using the CeleryRouter and Database backend for testing during
developement. With the CeleryRouter, one needs to run the celery process as a
separate process, the command to accomplish that is given under the celery
section.

####Installation
Step one is installing all the required  libraries that will be using. The entire
list of reuqired libraries, their versions and dependancies can then be installed
with
Always better to remove the old smsforms app

>`pip install -r requirements.txt`

Ensure any database sync is done

>`python manage.py syncdb`
>`python manage.py migrate`


####Celery
With the CeleryRouter in use, the **system will not receive or send messages if
the celery process is not up**
The main configuration for celery is in OVC_IMS/celery_app.py. To run the celery
process, run the following command:

>`celery worker -E -A OVC_IMS.celery_app:app`

It would be adviseable to run celery with something like supervisord.

**If all is succesful, the sms tester will be available on /sms/httptester**