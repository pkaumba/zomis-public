import json
from django.core.exceptions import ObjectDoesNotExist
from django.db import transaction
from django.core import serializers
import operator
from models import Document, DocumentTag, DocumentGeo, DocumentOrgUnit
from ovc_auth.models import OVCUserRoleGeoOrg
from ovc_main.models import SetupList, SetupGeorgraphy, RegOrgUnit, RegPersonsOrgUnits
from django.db.models import Q
from django.template.defaultfilters import filesizeformat
from OVC_IMS import settings
from os.path import splitext, join


def check_duplicates(title):
    existsing_title = None
    if title:
        existsing_title = Document.objects.filter(document_title__iexact=title)

    if existsing_title:
        return True
    else:
        return False


def get_document_type_list():
    return SetupList.objects.filter(item_category__exact='Document type').order_by('the_order').values_list('id',
                                                                                                            'item_description', )


def get_search_document_type_list():
    list_data = [('-1', 'All document types')]
    return tuple(
        list_data + [(l[0], l[1]) for l in
                     get_document_type_list()])


def get_service_area_list():
    return SetupList.objects.filter(item_category__exact='Topic').order_by('the_order').values_list('id',
                                                                                                    'item_description', )


def get_search_service_area_list():
    list_data = [('-1', 'All service areas')]
    return tuple(
        list_data + [(l[0], l[1]) for l in
                     get_service_area_list()])


def get_geographic_area_list():
    return SetupGeorgraphy.objects.all().values_list('area_id',
                                                     'area_name', )


def get_geographical_level_list():
    list_data = [('-1', '-----')]
    return tuple(
        list_data + [(l.id, l.item_description) for l in
                     SetupList.objects.filter(item_category='Geographic relevancy').order_by('the_order')])


def get_document_stage_list():
    list_data = [('-1', '-----')]
    return tuple(
        list_data + [(l.id, l.item_description) for l in
                     SetupList.objects.filter(item_category='Document stage').order_by('the_order')])


def get_document_availability_list():
    list_data = [('-1', '-----')]
    return tuple(
        list_data + [(l.id, l.item_description) for l in
                     SetupList.objects.filter(item_category='Document availability').order_by('the_order')])


def get_document_role_list():
    list_data = [('-1', '-----')]
    return tuple(
        list_data + [(l.id, l.item_description) for l in
                     SetupList.objects.filter(item_category='Document role').order_by('the_order')])


def get_item_id(record_id):
    select_item = None
    try:
        item = SetupList.objects.get(id=record_id).item_id

        if (item == 'GRPR'):
            select_item = 'GPRV'
        elif (item == 'GRDS'):
            select_item = 'GDIS'
        elif (item == 'GRWD'):
            select_item = 'GWRD'
        else:
            select_item = item

    except ObjectDoesNotExist:
        select_item = None

    return select_item


def get_item_description(item_id):
    description = None
    try:
        item = SetupList.objects.get(item_id=item_id).item_description
        description = item

    except ObjectDoesNotExist:
        description = None

    return description


def populate_select():
    optionstring = ''

    for id, item_description in get_geographical_level_list():
        optionstring += '<option value="%s">%s</option>\n' % (
            id, item_description)

    return optionstring


def get_library(documentId, user):
    document = Document.objects.filter(pk=documentId)

    return document


def get_library_view(documentid, user):
    doc = Document.objects.get(pk=documentid)
    docs_json = []
    doc_dict = {}
    doc_dict['document_id'] = doc.document_id
    doc_dict['document_title'] = doc.document_title
    doc_dict['document_keywords'] = doc.document_keywords
    doc_dict['document_types'] = get_document_types_view_list(doc)
    doc_dict['document_service_area'] = get_service_areas_view_list(doc)
    doc_dict['geographic_relevancy'] = get_item_description(doc.geographic_relevancy_id)
    doc_dict['geographic_area'] = get_geo_areas_view_list(doc)
    doc_dict['stage'] = get_item_description(doc.stage_id)
    doc_dict['stage_notes'] = doc.stage_notes
    doc_dict['availability'] = get_item_description(doc.availability_id)
    docs_json.append(doc_dict)

    return doc_dict


def get_all_library(user):
    orgs = []
    if (user.is_anonymous()):
        doc_list = Document.objects.filter(
            (Q(availability_id__exact='DAPB'))
        )
    else:
        reg_person = user.reg_person
        person_orgs = RegPersonsOrgUnits.objects.filter(
            (Q(person=reg_person)) &
            (Q(primary=True))
        )

        for o in person_orgs:
            orgs.append(o.parent_org_unit.id)

        ######
        doc_list = Document.objects.all()

    docs_json = []
    is_contributing = False
    for doc in doc_list:
        is_contributing = False
        for doc_org_id in doc.documentorgunit_set.values_list('org_unit_id', flat=True):
            if doc_org_id in orgs:
                is_contributing = True

        doc_dict = {}
        doc_dict['document_id'] = doc.document_id
        doc_dict['document_title'] = doc.document_title
        doc_dict['document_types'] = get_document_types(doc)
        doc_dict['document_keywords'] = doc.document_keywords
        doc_dict['document_service_area'] = get_service_areas(doc)
        doc_dict['document_geo_links'] = get_geo_areas(doc)
        doc_dict['document_file'] = doc.document_file

        if doc.availability_id == 'DACO':
            if is_contributing:
                docs_json.append(doc_dict)
        else:
            docs_json.append(doc_dict)

    return docs_json


def get_document_types(document):
    all_document_types = SetupList.objects.filter(
        (Q(item_category__exact='Document type'))
        & (Q(item_id__in=DocumentTag.objects.filter(document=document).values_list('tag_id')))
    ).values_list('item_description', flat=True)

    return get_list(all_document_types)


def get_document_types_view_list(document):
    all_document_types = SetupList.objects.filter(
        (Q(item_category__exact='Document type'))
        & (Q(item_id__in=DocumentTag.objects.filter(document=document).values_list('tag_id')))
    ).values_list('item_description', flat=True)

    return all_document_types


def get_service_areas_view_list(document):
    all_document_types = SetupList.objects.filter(
        (Q(item_category__exact='Topic'))
        & (Q(item_id__in=DocumentTag.objects.filter(document=document).values_list('tag_id')))
    ).values_list('item_description', flat=True)

    return all_document_types


def get_service_areas(document):
    all_document_types = SetupList.objects.filter(
        (Q(item_category__exact='Topic'))
        & (Q(item_id__in=DocumentTag.objects.filter(document=document).values_list('tag_id')))
    ).values_list('item_description', flat=True)

    return get_list(all_document_types)


def get_geo_areas(document):
    all_areas = SetupGeorgraphy.objects.filter(
        (Q(area_id__in=DocumentGeo.objects.filter(document=document).values_list('area_id')))
    ).values_list('area_name', flat=True)

    if all_areas:
        display_list = all_areas
    else:
        display_list = SetupList.objects.filter(
            (Q(item_id=document.geographic_relevancy_id)) &
            (Q(item_id='GRIN') | Q(item_id='GRNT'))
        ).values_list('item_description', flat=True)

    return get_list(display_list)


def get_geo_areas_view_list(document):
    all_areas = SetupGeorgraphy.objects.filter(
        (Q(area_id__in=DocumentGeo.objects.filter(document=document).values_list('area_id')))
    ).values_list('area_name', flat=True)

    return all_areas


def get_list(items):
    return ', '.join([str(item) for item in items])


def delete_document(document_id, user):
    document = Document.objects.get(pk=document_id)
    document.delete()


def search_library_json(librarySearch, documentType, serviceArea, user):
    

    orgs = []
    if not user.is_anonymous():
        reg_person = user.reg_person
        person_orgs = RegPersonsOrgUnits.objects.filter(
            (Q(person=reg_person)) &
            (Q(primary=True))
        )
        
        for o in person_orgs:
            orgs.append(o.parent_org_unit.id)

    doc_tag_list_type = DocumentTag.objects.filter(
        (Q(tag_id=get_item_id(documentType)))
    )
    
    doc_tag_list_service = DocumentTag.objects.filter(
        (Q(tag_id=get_item_id(serviceArea)))
    )
    
    doc_id_list = []
    if documentType and documentType != "-1":
        doc_id_list.append(-2)
        for doc in doc_tag_list_type:
            doc_id_list.append(doc.document.document_id)
        docs_list = get_document_from_db(librarySearch, user, doc_id_list) 
        
    elif serviceArea and serviceArea != "-1":
        doc_id_list.append(-2)
        for doc in doc_tag_list_service:
            doc_id_list.append(doc.document.document_id)
        docs_list = get_document_from_db(librarySearch, user, doc_id_list)
        
    elif (serviceArea and serviceArea != "-1") and (documentType and documentType != "-1"):
        doc_id_list.append(-2)
        for doc in doc_tag_list_type:
            doc_id_list.append(doc.document.document_id)
            
        doc_id_list_serv = []
        doc_id_list_serv.append(-2)
        for docs in doc_tag_list_service:
            doc_id_list_serv.append(docs.document.document_id)
        docs_list = get_document_from_db(librarySearch, user, doc_id_list,doc_id_list_serv)
        
    else:
        docs_list = get_document_from_db(librarySearch, user)
    

    docs_json = []
    for doc in docs_list:

        is_contributing = False
        for doc_org_id in doc.documentorgunit_set.values_list('org_unit_id', flat=True):
            if doc_org_id in orgs:
                is_contributing = True

        doc_dict = {}

        doc_dict['document_id'] = doc.document_id
        doc_dict['document_title'] = doc.document_title
        doc_dict['document_types'] = get_document_types(doc)
        doc_dict['document_keywords'] = doc.document_keywords
        doc_dict['document_service_area'] = get_service_areas(doc)
        doc_dict['document_geo_links'] = get_geo_areas(doc)
        doc_dict['document_file'] = doc.document_file.name
        doc_dict['document_url'] = doc.document_file.url

        if doc.availability_id == 'DACO':
            if is_contributing:
                docs_json.append(doc_dict)
        else:
            docs_json.append(doc_dict)

    # docs_list_json = json.dumps(docs_json)

    return list(docs_json)

def get_document_from_db(librarySearch,user,doc_id_list=None,doc_list_ser=None):
    to_return = None
    if doc_id_list:
        if user.is_anonymous():
            to_return = Document.objects.filter(Q(availability_id__exact='DAPB') & Q(pk__in=doc_id_list) & (Q(document_title__icontains=librarySearch) | Q(document_keywords__icontains=librarySearch))) if librarySearch != "" else Document.objects.filter(Q(availability_id__exact='DAPB') & Q(pk__in=doc_id_list))
        else:
            to_return = Document.objects.filter(Q(pk__in=doc_id_list) & (Q(document_title__icontains=librarySearch) | Q(document_keywords__icontains=librarySearch))) if librarySearch != "" else Document.objects.filter(Q(pk__in=doc_id_list))
    elif doc_id_list and doc_list_ser:
        if user.is_anonymous():
            to_return = Document.objects.filter(Q(availability_id__exact='DAPB') & Q(pk__in=doc_id_list) & Q(pk__in=doc_list_ser) & (Q(document_title__icontains=librarySearch) | Q(document_keywords__icontains=librarySearch))) if librarySearch != "" else Document.objects.filter(Q(availability_id__exact='DAPB') & Q(pk__in=doc_id_list) & Q(pk__in=doc_list_ser))
        else:
            to_return = Document.objects.filter(Q(pk__in=doc_id_list) & Q(pk__in=doc_list_ser) & (Q(document_title__icontains=librarySearch) | Q(document_keywords__icontains=librarySearch))) if librarySearch != "" else Document.objects.filter(Q(pk__in=doc_id_list) & Q(pk__in=doc_list_ser))
    else:
        if user.is_anonymous():
            to_return = Document.objects.filter(Q(availability_id__exact='DAPB') & (Q(document_title__icontains=librarySearch) | Q(document_keywords__icontains=librarySearch))) if librarySearch != "" else Document.objects.filter(Q(availability_id__exact='DAPB'))
        else:
            to_return = Document.objects.filter((Q(document_title__icontains=librarySearch) | Q(document_keywords__icontains=librarySearch))) if librarySearch != "" else Document.objects.all()
    return to_return

def get_document_view(documentid, user):
    document_view = get_library_view(documentid, user)
    return document_view


def get_document(fields, user):
    document_title = None
    document_keywords = None
    geographic_relevancy = None
    stage = None
    stage_notes = None
    availability = None
    document_file = None
    ext = None

    if fields.has_key('document_title'):
        document_title = fields['document_title']
    if fields.has_key('document_keywords'):
        document_keywords = fields['document_keywords']
    if fields.has_key('geographic_relevancy'):
        geographic_relevancy = fields['geographic_relevancy']
    if fields.has_key('stage'):
        stage = fields['stage']
    if fields.has_key('stage_notes'):
        stage_notes = fields['stage_notes']
    if fields.has_key('availability'):
        availability = fields['availability']
    if fields.has_key('document_file'):
        document_file = fields['document_file']
        ext = get_file_extention(fields)

    document = Document(
        document_title=document_title,
        document_keywords=document_keywords,
        person_id_created=user,
        geographic_relevancy_id=get_item_id(geographic_relevancy),
        stage_id=get_item_id(stage),
        stage_notes=stage_notes,
        availability_id=get_item_id(availability),
        extension=ext,
        document_file=document_file,
    )

    return document


@transaction.commit_on_success
def save_document(fields, user):
    document = get_document(fields, user)

    try:
        if document:
            document.save()
            save_document_types(fields, document, user)
            save_document_topics(fields, document, user)
            save_document_geo(fields, document, user)
            save_document_orgs(fields, document, user)

        else:
            raise Exception('incomplete file information')

    except Exception as e:
        print e
        raise Exception('file upload has invalid data. upload failed.')


@transaction.commit_on_success
def save_document_types(fields, doc, user):
    document_types = []
    if fields.has_key('document_types_tags'):
        document_types = fields['document_types_tags']

    for document_type in document_types:
        DocumentTag.objects.create(document=doc, tag_id=get_item_id(document_type))


@transaction.commit_on_success
def save_document_topics(fields, doc, user):
    document_topics = []
    if fields.has_key('document_topics_tags'):
        document_topics = fields['document_topics_tags']

    for document_topic in document_topics:
        DocumentTag.objects.create(document=doc, tag_id=get_item_id(document_topic))


@transaction.commit_on_success
def save_document_geo(fields, doc, user):
    document_geos = []
    if fields.has_key('geographic_area'):
        document_geos = fields['geographic_area']

    for document_geo in document_geos:
        DocumentGeo.objects.create(document=doc, area_id=document_geo)


def save_document_orgs(fields, doc, user):
    document_orgs = []
    document_orgs_json = None
    if fields.has_key('orgTable_data'):
        document_orgs = fields['orgTable_data']
        document_orgs_json = json.loads(document_orgs)
        for key, value in document_orgs_json.iteritems():

            val = value
            src = None
            org = None

            if val['tbl_source_id'] == '-1':
                src = val['tbl_source_name']
            else:
                org = val['tbl_source_id']

            DocumentOrgUnit.objects.create(
                document=doc,
                document_role_id=get_item_id(val['tbl_role_id']),
                source_name=src,
                org_unit_id=org
            )


def get_area_json(level_id):
    item_id = get_item_id(level_id)

    area_list = SetupGeorgraphy.objects.filter(area_type_id=item_id)
    area_json = []
    for area in area_list:
        area_dict = {}
        area_dict['area_id'] = area.id
        area_dict['area_name'] = area.area_name
        area_json.append(area_dict)

    area_list_json = json.dumps(area_json)

    return area_list_json


def get_org_json(user):
    upload_orgs = OVCUserRoleGeoOrg.objects.filter(
        Q(user=user)
    )

    orgs = []
    for upload_org in upload_orgs:
        orgs.append(upload_org.org_unit.id)

    org_list = RegOrgUnit.objects.filter(
        Q(pk__in=orgs)
        & Q(is_void=False)
        & Q(date_closed=None)
    )

    area_json = []
    if (org_list.count() == 1):
        for area in org_list:
            area_dict = {}
            area_dict['id'] = area.id
            area_dict['name'] = area.org_unit_id_vis + ' ' + area.org_unit_name
            area_json.append(area_dict)

    area_list_json = json.dumps(area_json)

    return area_list_json


def search_org_json_all(term):
    org_list = RegOrgUnit.objects.filter(
        (Q(org_unit_name__icontains=term) | Q(org_unit_id_vis__icontains=term))
        & Q(is_void=False)
        & Q(date_closed=None)
    )
    area_json = []
    for area in org_list:
        area_dict = {}
        area_dict['id'] = area.id
        area_dict['name'] = area.org_unit_id_vis + ' ' + area.org_unit_name
        area_json.append(area_dict)

    area_list_json = json.dumps(area_json)

    return area_list_json


def search_org_json(term, user):
    reg_person = user.reg_person
    person_orgs = RegPersonsOrgUnits.objects.filter(person=reg_person)

    orgs = []
    for o in person_orgs:
        orgs.append(o.parent_org_unit.id)

    if (user.is_superuser):
        org_list = RegOrgUnit.objects.filter(
            (Q(org_unit_name__icontains=term) | Q(org_unit_id_vis__icontains=term))
            & Q(is_void=False)
            & Q(date_closed=None)
        )
    else:
        org_list = RegOrgUnit.objects.filter(
            Q(pk__in=orgs)
            & (Q(org_unit_name__icontains=term) | Q(org_unit_id_vis__icontains=term))
            & Q(is_void=False)
            & Q(date_closed=None)
        )

    area_json = []
    for area in org_list:
        area_dict = {}
        area_dict['id'] = area.id
        area_dict['name'] = area.org_unit_id_vis + ' ' + area.org_unit_name
        area_json.append(area_dict)

    area_list_json = json.dumps(area_json)

    return area_list_json


def get_file_extention(fields):
    content = fields['document_file']
    return get_file_extention_from_filename(content.name)


def get_file_extention_from_filename(file_name):
    ext = splitext(file_name)[1][1:].lower()

    return ext


# only accessible to users with upload roles.
def get_orgs_with_upload_json(user):
    upload_orgs = OVCUserRoleGeoOrg.objects.filter(
        Q(user=user)
    )
    upload_orgs_json = []
    for upload_org in upload_orgs:
        upload_orgs_dict = {}
        upload_orgs_dict['id'] = upload_org.org_unit.id
        upload_orgs_json.append(upload_orgs_dict)

    return json.dumps(upload_orgs_json)






