'''
Created on Sep 2, 2014

@author: MLumpa
'''
from django import forms
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Fieldset, ButtonHolder, \
    Submit, Button, HTML, MultiField, Div, Field
from crispy_forms.bootstrap import FormActions, AppendedText
from ovc_main.models import SetupList, SetupGeorgraphy, RegOrgUnit
from ovc_main.utils.fields_list_provider import get_org_list,geo_control_select_list, \
        community_control_populate, get_user_related_organisation
from functools import partial
import organisation_unit_form_text as form_label
import re
from ovc_main.utils import validators, lookup_field_dictionary

from ovc_main.utils.geo_location import get_communities_in_ward
from ovc_main.utils.auth_access_control_util import del_form_field, fields_to_show, \
        filter_fields_for_display
import datetime
from django.conf import settings

DateInput = partial(forms.DateInput, {'class': 'datepicker'})

locationcontroltohide={lookup_field_dictionary.org_type_cwac:
                                            ['communities'],
                                       lookup_field_dictionary.org_type_district_level_committee:
                                            ['wards', 'communities'],
                                        lookup_field_dictionary.org_type_district_office_gov:
                                            ['wards', 'communities'],
                                        lookup_field_dictionary.org_type_ngo_private_multiple_district:
                                            ['wards', 'communities'],
                                        lookup_field_dictionary.org_type_provincial_level_committee:
                                            ['wards', 'communities'],
                                        lookup_field_dictionary.org_type_provincial_office_gov:
                                            ['wards', 'communities'],
                                        lookup_field_dictionary.org_type_ngo_private_national_hq:
                                            ['wards', 'communities'],
                                        lookup_field_dictionary.org_type_national_level_gov:
                                            ['wards', 'communities']
                                            }

districts_multi_select = {
        lookup_field_dictionary.org_type_community_based_org : False,
        lookup_field_dictionary.org_type_community_level_committee : False,
        lookup_field_dictionary.org_type_cwac : False,
        lookup_field_dictionary.org_type_district_level_committee : False,
        lookup_field_dictionary.org_type_district_office_gov : False,
        lookup_field_dictionary.org_type_national_level_gov : False,
        lookup_field_dictionary.org_type_ngo_private_national_hq : False,
        lookup_field_dictionary.org_type_ngo_private_single_district :False,
        lookup_field_dictionary.org_type_ngo_private_multiple_district : True,
        lookup_field_dictionary.org_type_provincial_level_committee : True,
        lookup_field_dictionary.org_type_provincial_office_gov :True,
        lookup_field_dictionary.org_type_residential_children : False,
        lookup_field_dictionary.org_type_sub_district_gov :False
    }
        
wards_multi_select = {
        lookup_field_dictionary.org_type_community_based_org : False,
        lookup_field_dictionary.org_type_community_level_committee : True,
        lookup_field_dictionary.org_type_cwac : False,
        lookup_field_dictionary.org_type_district_level_committee : False,
        lookup_field_dictionary.org_type_district_office_gov : False,
        lookup_field_dictionary.org_type_national_level_gov : False,
        lookup_field_dictionary.org_type_ngo_private_national_hq : False,
        lookup_field_dictionary.org_type_ngo_private_single_district :True,
        lookup_field_dictionary.org_type_ngo_private_multiple_district : True,
        lookup_field_dictionary.org_type_provincial_level_committee : False,
        lookup_field_dictionary.org_type_provincial_office_gov :False,
        lookup_field_dictionary.org_type_residential_children : False,
        lookup_field_dictionary.org_type_sub_district_gov :True
    }
roles_and_fields_to_show = {}
roles_and_fields_to_show['Organisation Update'] ={'auth.update org contact':
                                                                ['land_phone_number', 'mobile_phone_number', 'email_address', 
                                                                 'postal_address', 'physical_address','org_system_id'],
                                                          'auth.update org contact rel':
                                                                ['land_phone_number', 'mobile_phone_number', 'email_address', 
                                                                 'postal_address', 'physical_address','org_system_id'],
                                                          'auth.update org general':
                                                                ['districts','communities', 'wards', 'org_closed_not_functional', 
                                                                 'org_never_existed', 'dontskip','date_closed','org_system_id', 'organisation_type'],                                                     
                                                        'auth.update org general rel':
                                                                ['districts','communities', 'wards', 'org_closed_not_functional', 
                                                                'org_never_existed', 'dontskip', 'date_closed','org_system_id','organisation_type'],
                                                          'auth.update org special':
                                                                ['org_name', 'date_org_setup', 'legal_registration_type', 
                                                                 'legal_registratiom_number','org_system_id']
                                                            }

permission_type_fields = {
                            'rel_org':{'auth.update org contact rel':
                                        ['land_phone_number', 'mobile_phone_number', 'email_address', 
                                         'postal_address', 'physical_address','org_system_id'],
                                       'auth.update org general rel':
                                        ['districts','communities', 'wards', 'org_closed_not_functional', 
                                         'org_never_existed', 'dontskip', 'date_closed','org_system_id','organisation_type']},
                            'other':{'auth.update org special':
                                        ['org_name', 'date_org_setup', 'legal_registration_type', 
                                         'legal_registratiom_number','org_system_id'],
                                    'auth.update org contact':
                                        ['land_phone_number', 'mobile_phone_number', 'email_address', 
                                         'postal_address', 'physical_address','org_system_id'],
                                    'auth.update org general':
                                        ['districts','communities', 'wards', 'org_closed_not_functional', 
                                         'org_never_existed', 'dontskip','date_closed','org_system_id', 'organisation_type'],                                                     
                                     
                                    }
                            }


def getlistoforganisations():
    org_drop_list = [('','-----')]
    return tuple(org_drop_list + [(l.item_id, l.item_description) for l in SetupList.objects.filter(item_category='Organisational unit type')])

def getlistoflegalregtype():
    org_drop_list = [('','-----')]
    return tuple(org_drop_list + [(l.item_id, l.item_description) for l in SetupList.objects.filter(item_category='Organisational unit ID - external')])

def getlistofdistricts():
    dsts = [('','-----')]
    return tuple([(location.area_id, SetupGeorgraphy.objects.get(area_id=location.parent_area_id).area_name+'-%s' % location.area_name)for location in SetupGeorgraphy.objects.filter(area_type_id=lookup_field_dictionary.geo_type_district_code)])


def getlistofprovincedistricts():
    optionstring = ''
    province_district = [(location.area_id, SetupGeorgraphy.objects.get(area_id=location.parent_area_id).area_name+'-%s' % location.area_name)for location in SetupGeorgraphy.objects.filter(area_type_id=lookup_field_dictionary.geo_type_district_code)]
    
    for prov_dist in province_district:
        optionstring += '<option value="%s">%s</option>\n' % (prov_dist[0], prov_dist[1])
        
    return optionstring

        

class OrganisationRegistrationForm(forms.Form):
    org_name = forms.CharField(label=form_label.label_org_name)
    org_system_id =  forms.CharField(required=False, widget=forms.HiddenInput())
    date_org_setup = forms.DateField(widget=DateInput(format='%d-%B-%Y'), input_formats=validators.date_input_formats_tpl, label=form_label.label_date_org_setup, required=False)#http://stackoverflow.com/questions/20700185/how-to-use-datepicker-in-djangoflow.com/questions/20700185/how-to-use-datepicker-in-django
    date_closed = forms.DateField(widget=DateInput(format='%d-%B-%Y'), input_formats=validators.date_input_formats_tpl, required=False, label=form_label.label_date_closed)
    
    organisationslist = getlistoforganisations()
    organisation_type = forms.ChoiceField(choices=organisationslist, label=form_label.label_organisation_type)
    
    reg_type_choices=[]
    reg_type_choices = getlistoflegalregtype()
    legal_registration_type = forms.ChoiceField(label=form_label.label_legal_registration_type, choices=reg_type_choices, required=False)
    legal_registratiom_number = forms.CharField(label=form_label.label_legal_registratiom_number, required=False)
    
    parent_org_choices=[]
    parent_org_choices = get_org_list()
    parent_unit = forms.ChoiceField(label=form_label.label_parent_unit, required=False)
    
    land_phone_number = forms.RegexField(label=form_label.label_land_phone_number, required=False, regex=validators.land_phone_number_regex, error_message = (form_label.error_msg_land_number_wrong_format))
    #mobile_phone_number = forms.CharField(label='Mobile phone number')
    mobile_phone_number = forms.RegexField(regex=validators.mobile_phone_number_regex, error_message = (form_label.error_msg_mobile_number_wrong_format), required=False)
    
    email_address = forms.EmailField(label=form_label.label_email_address, required=False)
    postal_address = forms.CharField(widget=forms.Textarea(attrs={'rows': 4}), label=form_label.label_postal_address, required=False)
    physical_address = forms.CharField(widget=forms.Textarea(attrs={'rows': 4}), label=form_label.label_physical_address, required=False)
    
    org_closed_not_functional = forms.BooleanField(label='', required=False, widget=forms.CheckboxInput(attrs={'id':'orgclosedcheckbox', }));
    org_never_existed = forms.BooleanField(label='', required=False);

    
    
    def __init__(self, user, *args, **kwargs):
        self.user = user
        self.custom_errors = {'district':'', 'ward':'', 'community':''}
        self.is_update_page = False
        do_display_status = 'display:None'
        do_display_org_type = ''
        actionurl = '/organisation/new/'
        header_text = """<div class="note note-info"><h4><strong>Register New Organisation Unit("""+settings.APP_NAME+""" form 1a)</strong></h4><h5><p>This form is used to register organisation unit within """+settings.APP_NAME+""" and obtain a """+settings.APP_NAME+""" ID number</p>Note that for large organisations, each branch or office should be identified separately as separate organisational units</h5></div>"""
        submitButtonText = form_label.label_button_save
        if 'is_update_page' in kwargs:
            self.is_update_page = kwargs.pop('is_update_page')
            if self.is_update_page:
                do_display_status = ''
                header_text = """<div class="note note-info"><h4><strong>Organisation Unit File Update("""+settings.APP_NAME+""" form 1b)</strong></h4><h5><p>This form is used to update organisation unit</h5></div>"""
                actionurl = '/organisation/update_org/'
                submitButtonText = form_label.label_button_update
                do_display_org_type = 'display:None'
        
        
        _organisation_type = None
        if args and 'organisation_type' in args[0]:
            _organisation_type = args[0]['organisation_type']
            
        print _organisation_type, 'our organisation type'
        
        districtsoptionlist = ''
        districts = []
        wards = []
        communities = []

        if 'districts' in kwargs:
            districts = kwargs.pop('districts')
            districts = self.convert_to_int_array(districts)
            setoptionsdisabled = False
            if _organisation_type and _organisation_type in districts_multi_select:
                setoptionsdisabled = not districts_multi_select[_organisation_type]
            districtsoptionlist = geo_control_select_list(lookup_field_dictionary.geo_type_district_code, districts, setdisabled=setoptionsdisabled)
        elif args and 'districts' in args[0]:
            districts = args[0].getlist('districts')
            districts = self.convert_to_int_array(districts)
            setoptionsdisabled = False
            if _organisation_type and _organisation_type in districts_multi_select:
                setoptionsdisabled = not districts_multi_select[_organisation_type]
            districtsoptionlist = geo_control_select_list(lookup_field_dictionary.geo_type_district_code, districts, setdisabled=setoptionsdisabled)
        else:
            districtsoptionlist = geo_control_select_list(lookup_field_dictionary.geo_type_district_code)   
        
        wardsoptionlist = ''
        if 'wards' in kwargs:
            wards = kwargs.pop('wards')
            wards = self.convert_to_int_array(wards)
            if districts:
                wardsoptionlist = geo_control_select_list(lookup_field_dictionary.geo_type_ward_code, wards, grandparentfilter=districts)
        elif args and 'wards' in args[0]:
            wards = args[0].getlist('wards')
            wards = self.convert_to_int_array(wards)
            if districts:
                setoptionsdisabled = False
                if _organisation_type and _organisation_type in districts_multi_select:
                    setoptionsdisabled = not wards_multi_select[_organisation_type]
                wardsoptionlist = geo_control_select_list(lookup_field_dictionary.geo_type_ward_code, wards, grandparentfilter=districts, setdisabled=setoptionsdisabled) 
        elif districts:
            wardsoptionlist = geo_control_select_list(lookup_field_dictionary.geo_type_ward_code, grandparentfilter=districts)  
        
        #print wards, '1in communities wards to print'
        communitiesoptionlist = ''
        if 'communities' in kwargs:
            communities = kwargs.pop('communities')
            communities = self.convert_to_int_array(communities)
            if communities:
                communitiesoptionlist = community_control_populate(communities, wards)
        elif args and 'communities' in args[0]:
            communities = args[0].getlist('communities')
            communities = self.convert_to_int_array(communities)
            if communities:
                communitiesoptionlist = community_control_populate(communities, wards)
        elif wards:
            communitiesoptionlist = community_control_populate(parent_filter=wards)
        
        print districts, wards, communities, 'in forms, districts, wards, communities'   
        self._districts = districts
        self._wards = wards
        self._communities = communities
        
        print args, args, 'in form, args'
        print kwargs, 'in forms, kwargs'
        
        
        
        district_has_error = ''
        ward_has_error = ''
        community_has_error = ''
        self.custom_validation(_organisation_type, self._districts, self._wards, self._communities)
        if self.custom_errors['district']:
            district_has_error = ' has-error'
        if self.custom_errors['ward']:
            ward_has_error = ' has-error'
        if self.custom_errors['community']:
            community_has_error = ' has-error'
        
        super(OrganisationRegistrationForm, self).__init__(*args, **kwargs)

        org_system_id = None
        tmp_parent_unit = None
        if len(args) > 0:
            if 'org_system_id' in args[0]:
                org_system_id = args[0]['org_system_id']
            if 'parent_unit' in args[0]:
                tmp_parent_unit = args[0]['parent_unit']
        
        exclude_list = [org_system_id]
        include_list = []
        if tmp_parent_unit:
            include_list = self.convert_to_int_array(tmp_parent_unit)
        if not org_system_id:
            exclude_list = []
        tmp_org_choices = get_user_related_organisation(user, exclude_list, include_list)
        self.fields['parent_unit'] = forms.ChoiceField(
            choices=tmp_org_choices, required=False, label=form_label.label_parent_unit )
        #if self.date_closed:
        #    self.instance.org_closed_not_functional = True
        #    self.fields['instance.org_closed_not_functional'].attr()
        display_date_closed_control = 'none'
        _organisation_type = None
        if len(args) > 0:
            if 'date_closed' in args[0]:
                date_closed = args[0]['date_closed']
                if date_closed:
                    display_date_closed_control = 'visible'
                    self.fields['org_closed_not_functional'].widget.attrs['checked'] = 'checked'
            
            if 'organisation_type' in args[0]:
                _organisation_type = args[0]['organisation_type']
        

        
        self.helper = FormHelper()
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-md-3'
        self.helper.field_class = 'col-md-6'
        self.helper.form_action = actionurl
        self.helper.layout = Layout(
            MultiField(
                       
                        #header_text,
                        '',#I have no idea, but this empty string is needed for the follwing div to show
                        Div(
                                Div(HTML("""
                                        <div class="panel-heading"></div>
                                        <div class="panel-body pan mandatory" ><p/> 
                                                <div class='col-md-12'>
                                                    <dl class='dl-horizontal col-md-5'>
                                                      <dt>"""+form_label.label_organisation_id_short+"""</dt>
                                                      <dd>{{organisation.org_id}}</dd>
                                                     </dl>
                                                     <dl class='dl-horizontal col-md-7'>
                                                      <dt>"""+form_label.label_org_name_short+"""</dt>
                                                      <dd>{{organisation.name}}</dd>
                                                    </dl>
                                                </div>
                                                <div  class='col-md-12'>
                                                    <dl class='dl-horizontal col-md-5'>
                                                      <dt>"""+form_label.label_organisation_type+"""</dt>
                                                      <dd>{{organisation.unit_type_name}}</dd>
                                                    </dl>
                                                    <dl class='dl-horizontal col-md-7'>
                                                      <dt>"""+form_label.label_parent_unit_short+"""</dt>
                                                      <dd>{{organisation.parent_organisation_name}}</dd>
                                                    </dl>
                                                </div>
                                            </div>
                                        """
                                         ),
                                    css_class='panel panel-primary mandatory',
                                    style='%s' % do_display_status),
                                Div(HTML('<div class=" headerspace ">'+form_label.label_section_heading_about_organisation+'</div>'),
                                    css_class = 'panel-heading'
                                    ),
                                
                                Div(
                                    HTML('<div class=" headerspace "></div><p/>'),
                                    HTML("""<div id="div_id_org_name" class="form-group" style=\""""+do_display_status+"""\">
                                            <label for="id_org_name" class="control-label col-md-3">"""+form_label.label_organisation_id+"""</label>
                                            <div>
                                              <p class="form-control-static col-md-6"><strong>{{organisation.org_id}} </strong></p>
                                            </div>
                                            
                                        </div>"""),
                                    AppendedText('org_name','<span data-toggle="tooltip" title="'+form_label.label_tooltip_org_name+'" data-placement="bottom"><i class="fa fa-info-circle"></i></span>'),
                                    AppendedText('date_org_setup','<span data-toggle="tooltip" title="'+form_label.label_tooltip_date_org_setup+'" data-placement="bottom"><i class="fa fa-info-circle"></i></span>'),
                                    'org_system_id',
                                    css_class = 'panel-body pan'
                                    ),
                                css_class='panel panel-primary',
                            ),
                        Div(
                                Div(HTML(form_label.label_section_heading_organisation_type),
                                    css_class = 'panel-heading'
                                    ),
                                
                                Div(
                                    HTML('<p/>'),
                                    Div('organisation_type', style='%s' % do_display_org_type),
                                    'legal_registration_type',
                                    AppendedText('legal_registratiom_number', '<span data-toggle="tooltip" title="'+form_label.label_tooltip_legal_reg_type+'"><i class="fa fa-info-circle"></i></span>'),
                                    css_class = 'panel-body pan'
                                    ),
                                css_class='panel panel-primary',
                        
                            ),
                        Div(
                                Div(
                                    HTML(form_label.label_section_heading_services_location),
                                    css_class = 'panel-heading'
                                    ),
                                
                                Div(
                                    HTML('<p/>'),
                                    Div(
                                        HTML("""<!--multiselect control from http://loudev.com/ -->
                                             
                                             <label class="control-label col-md-3">"""+form_label.label_districts+"""</label>
                                             <div class="col-md-6" id="multipledistricts">
                                                 <select name="districts" id="districts" multiple="multiple">"""+
                                                    districtsoptionlist+"""
                                                </select>
                                            </div><span data-toggle="tooltip" title='"""+form_label.label_tooltip_district+"""'><i class="fa fa-info-circle"></i></span>
                                            <span id="error_1_id_district" class="help-block"><strong>"""+self.custom_errors['district']+"""</strong></span>
                                             
                                             """),
                                        css_class = 'form-group form-horizontal form-bordered%s' % district_has_error,
                                        css_id = 'district_location'
                                        ),
                                     Div(
                                        HTML("""
                                             
                                             <label class="control-label col-md-3">"""+form_label.label_wards+"""</label>
                                             <div class="col-md-6">
                                                 <select name="wards" id="wards" multiple="multiple">
                                                    """+wardsoptionlist+"""
                                                </select>
                                            </div><span data-toggle="tooltip" title='"""+form_label.label_tooltip_ward+"""'><i class="fa fa-info-circle"></i></span>
                                            <span id="error_1_id_district" class="help-block"><strong>"""+self.custom_errors['ward']+"""</strong></span>
                                             
                                             """),
                                        css_class = 'form-group form-horizontal form-bordered%s' % ward_has_error,
                                        css_id = 'ward_location'
                                        ),
                                    Div(
                                        HTML("""
                                             
                                             <label class="control-label col-md-3">"""+form_label.label_communities+"""</label>
                                             <div class="col-md-6">
                                                 <select name="communities" id="communities" multiple="multiple">
                                                    """+communitiesoptionlist+"""
                                                </select>
                                            </div><span data-toggle="tooltip" title='"""+form_label.label_tooltip_community+"""'><i class="fa fa-info-circle"></i></span>
                                            <span id="error_1_id_district" class="help-block"><strong>"""+self.custom_errors['community']+"""</strong></span>
                                             
                                             """),
                                        css_class = 'form-group form-horizontal form-bordered%s' % community_has_error,
                                        css_id = 'community_location'
                                        ),
                                    css_class = 'panel-body pan'
                                    ),
                                css_class='panel panel-primary'
                            ),
                        Div(
                                Div(
                                    HTML(form_label.label_section_heading_parent_unit),
                                    css_class = 'panel-heading'
                                    ),
                                
                                Div(
                                    HTML('<p/>'),
                                    AppendedText('parent_unit','<span data-toggle="tooltip" data-html="true" title="'+form_label.label_tooltip_parent_unit+'"><i class="fa fa-info-circle"></i></span>'),
                                    css_class = 'panel-body pan'
                                    ),
                                css_class='panel panel-primary'
                            ),
                       Div(
                                Div(
                                    HTML(form_label.label_section_heading_contact_details),
                                    css_class = 'panel-heading'
                                    ),
                                
                                Div(
                                    HTML('<p/>'),
                                    'land_phone_number',
                                    'mobile_phone_number',
                                    'email_address',
                                    'postal_address',
                                    'physical_address',
                                    css_class = 'panel-body pan'
                                    ),
                                css_class='panel panel-primary'
                            ),
                       Div(
                                Div(
                                    HTML(form_label.label_section_heading_organisation_status),
                                    css_class = 'panel-heading'
                                    ),
                                
                                Div(
                                    HTML('<p/>'),
                                    Div(
                                        HTML("<label class='col-md-6 dontskip' style='margin-right:-6%'>"+form_label.label_tick_if_closed+"</label>"),
                                        Div('org_closed_not_functional', css_class='col-md-3 orgclosed', style='margin-top:0.1%; padding-right:35%'), css_class='form-group col-md-8', style='margin-left:15%'),
                                    Div('date_closed', css_class='col-md-5 date_closed_control', style='margin-left:35%%;, margin-top:-1%%; margin-top: -2%%;display:%s' % display_date_closed_control),
                                    Div(
                                        HTML("<label class='col-md-6 dontskip' style='margin-right:-3.5%;margin-left:-2%'>"+form_label.label_tick_if_void+"</label>"),
                                        Div('org_never_existed', css_class='col-md-3 orgneverexisted', style='margin-top:-0.5%'), css_class='form-group col-md-6', style='margin-left:22%'),
                                    
                                    css_class = 'panel-body pan'
                                    ),
                                css_class='panel panel-primary',
                                style='%s' % do_display_status
                            ),
                       Div(

                            #Submit('save', 'Save changes'),
                            #Button('cancel', 'Cancel'),
                            HTML("<Button type=\"submit\" style=\"margin-right:4px\" class=\" mandatory btn btn-primary\"><i class=\"fa fa-floppy-o\"></i> "+submitButtonText+"</Button>"),
                            HTML("<a type=\"button\" href=\"/organisation/\" class=\" mandatory btn btn-success\"><i class=\"fa fa-undo\"></i> "+form_label.label_button_cancel+"</a>"),
                            style='margin-left:38%'
                           )

            )
        )

 
        
                                           

        #if self.is_update_page:
        #    print 'this is an update page'
        #    layout = self.helper.layout
        #    searchitems = fields_to_show(self.user, roles_and_fields_to_show['Organisation Update'])
        #    
        #    if _organisation_type and _organisation_type in locationcontroltohide:
        #        for _control in locationcontroltohide[_organisation_type]:
        #            if _control in searchitems:
        #                searchitems.remove(_control)
        #        
        #    if searchitems:
        #        del_form_field(layout, searchitems)
        #        self.helper.layout = layout
        
         
        if self.is_update_page:
            print 'this is an update page'
            layout = self.helper.layout
            searchitems = filter_fields_for_display(self.user, permission_type_fields, organisation_id=org_system_id)
            print searchitems, 'these are the fields returned'
            
            #we need this field for purposes of displaying the geo controls and for their associated logic in the UI
            if _organisation_type and _organisation_type in locationcontroltohide:
                for _control in locationcontroltohide[_organisation_type]:
                    if _control in searchitems:
                        searchitems.remove(_control)
                
            if searchitems:
                del_form_field(layout, searchitems)
                self.helper.layout = layout
        
    def clean_org_name(self):
        print 'this is a clean page'
        org_name = self.cleaned_data.get('org_name')
        print self.is_update_page, 'is update page'
        print org_name, 'in form, orgname'       
        numberoforgswithsamename = RegOrgUnit.objects.filter(org_unit_name=org_name, is_void=False).count()
        
        if self.is_update_page==False and numberoforgswithsamename >= 1:
            raise forms.ValidationError(form_label.error_msg_org_name_name_exists)
        
        if len(org_name) >= 250:
            raise forms.ValidationError(form_label.error_msg_org_name_over_max_chars)
        
        return org_name       
    def clean_parent_unit(self):
        parent_unit = self.cleaned_data.get('parent_unit')
        #print parent_unit, 'our parent unit'
        if RegOrgUnit.objects.filter(is_void=False).count() > 0 and not parent_unit and not self.is_update_page:
            raise forms.ValidationError(form_label.error_msg_parent_unit_is_required)
        if not parent_unit and len(get_user_related_organisation(self.user)) > 1 and not self.is_update_page:
            raise forms.ValidationError(form_label.error_msg_parent_unit_is_required)
        return parent_unit
        
    def clean_date_org_setup(self):

        date_org_setup = self.cleaned_data.get('date_org_setup')
        
        if not ((date_org_setup and date_org_setup <= datetime.datetime.now().date()) or not date_org_setup):
            raise forms.ValidationError(form_label.error_msg_date_org_setup_in_future)
        return date_org_setup
        

   
    def clean_date_closed(self):
        date_closed = self.cleaned_data.get('date_closed')
        if date_closed and date_closed > datetime.datetime.now().date():
            raise forms.ValidationError(form_label.error_msg_date_closed_in_future)
        return date_closed
    
    def clean(self):
        cleaned_data = super(OrganisationRegistrationForm, self).clean()
        date_closed = cleaned_data.get('date_closed')
        date_org_setup = cleaned_data.get('date_org_setup')

        #date closed and date org setup validation.
        if (date_closed and date_org_setup) and (date_closed < date_org_setup):

            msg_date_setup = u""+form_label.error_msg_date_org_setup_date_after_date_closed
            msg_date_closed = u""+form_label.error_msg_date_closed_before_date_org_setup
            
            self._errors["date_closed"] = self.error_class([msg_date_closed])
            self._errors["date_org_setup"] = self.error_class([msg_date_setup])
            
        
        #date closed validation.
        org_closed_not_functional = cleaned_data.get('org_closed_not_functional')
        if org_closed_not_functional and not date_closed:
            msg_date_closed = u""+form_label.error_msg_date_closed_is_empty
            self._errors["date_closed"] = self.error_class([msg_date_closed])
            
        legal_registration_type = cleaned_data.get('legal_registration_type')
        legal_registratiom_number = cleaned_data.get('legal_registratiom_number')
        
        if legal_registration_type and legal_registration_type == lookup_field_dictionary.legal_org_registration_type_ngo_number:
            if not legal_registratiom_number:
                self._errors["legal_registratiom_number"] = self.error_class([u""+form_label.error_msg_legal_registration_number_required])
            elif legal_registratiom_number:
                pat = '^RNGO/\d{3}/\d{4}/\d{2}$'
                match = re.search(pat, legal_registratiom_number)
                if not match:
                    self._errors["legal_registratiom_number"] = self.error_class([u""+form_label.error_msg_legal_registration_number_ngo_wrong_format])
                
        if self._errors or not self.is_custom_validation():
            print 'mush', self._errors
            self._errors["__all__"] = self.error_class([u""+form_label.error_msg_overall_page_not_saved])
        #wards validation   
        
 
        return cleaned_data
    
    def is_custom_validation(self):
        for value in self.custom_errors.values():
            if value:
                return False
        return True
    
    def custom_validation(self,organisation_type=None, districts=None, wards=None, communities=None):
        print 'org: ',organisation_type,'district ', districts, 'wards ', wards, 'communities ', communities
        self.custom_errors.clear()
        self.custom_errors = {'district':'', 'ward':'', 'community':''}
        if organisation_type:
            if organisation_type == lookup_field_dictionary.org_type_cwac:
                if not districts or len(districts) > 1:
                    self.custom_errors['district'] = form_label.error_msg_cwac_district
                if not wards or len(wards) > 1:
                    self.custom_errors['ward'] = form_label.error_msg_cwac_ward
            
            if organisation_type in [lookup_field_dictionary.org_type_residential_children, lookup_field_dictionary.org_type_community_based_org]:
                if not districts or len(districts) > 1:
                    self.custom_errors['district'] = form_label.error_msg_residential_children_district
                if not wards or len(wards) > 1:
                    self.custom_errors['ward'] = form_label.error_msg_residential_children_ward
                if not communities or len(communities) > 1:
                    self.custom_errors['community'] = form_label.error_msg_residential_children_community

            if organisation_type in [lookup_field_dictionary.org_type_community_level_committee, lookup_field_dictionary.org_type_sub_district_gov, lookup_field_dictionary.org_type_ngo_private_single_district]:
                if not districts or len(districts) > 1:
                    self.custom_errors['district'] = form_label.error_msg_community_level_committee_district
            
            if organisation_type in [lookup_field_dictionary.org_type_district_level_committee, lookup_field_dictionary.org_type_district_office_gov]:
                if not districts or len(districts) > 1:
                    self.custom_errors['district'] = form_label.error_msg_district_level_committee_district
                if wards:
                    self.custom_errors['ward'] = form_label.error_msg_district_level_committee_ward
                if communities:
                    self.custom_errors['community'] = form_label.error_msg_district_level_committee_community
            
            if organisation_type in [lookup_field_dictionary.org_type_ngo_private_multiple_district, lookup_field_dictionary.org_type_provincial_level_committee, lookup_field_dictionary.org_type_provincial_office_gov]:
                if wards:
                    self.custom_errors['ward'] = form_label.error_msg_ngo_private_multiple_district_ward
                if communities:
                    self.custom_errors['community'] = form_label.error_msg_ngo_private_multiple_district_community

            if organisation_type in [lookup_field_dictionary.org_type_national_level_gov]:
                if districts:
                    self.custom_errors['district'] = form_label.error_msg_no_district_required
                if wards:
                    self.custom_errors['ward'] = form_label.error_msg_no_ward_required
                if communities:
                    self.custom_errors['community'] = form_label.error_msg_no_community_required

    def convert_to_int_array(self, valuetoconvert):
        if str(valuetoconvert).isdigit():
            return [int(valuetoconvert)]
        elif valuetoconvert:
            tmp = map(int, valuetoconvert)
            return tmp
        else:
            return None