'''
Created on Sep 2, 2014

@author: MLumpa
'''
from django.conf.urls import patterns, url
from django.views.generic import TemplateView
from ovc_main.views import reg_views as views, \
    service_views, api_views as api, upload_download_views as capture_views

urlpatterns = patterns('',
    url(r'^$', views.index, name='index'),
    url(r'^new/', views.new_organisation, name='new-organisation'),
    url(r'^wards/', views.listofwards, name='listofwards'),
    url(r'^orglists/', views.org_search_results, name='org_search_results'),
    url(r'^view_org/', views.organisation_view, name='organisation_view'),
    url(r'^update_org/', views.update_organisation, name='organisation_update'),
    url(r'^wfcsearch/', views.workforce_index, name='wfc_search_index'),
    url(r'^wfclists/', views.wfc_search_results, name='wfc_search_index'),
    url(r'^get_workforce_for_res_institution/', views.get_workforce_for_res_institution, name='get_workforce_for_res_institution'),
    url(r'^new_workforce/', views.new_workforce, name='workforce'),
    url(r'^orgautocomp/', views.org_autocompletion_list, name='orgautocomplete'),
    url(r'^res_wfc_autocomplete/', views.res_wfc_autocomplete, name='res_institute_wfc_autocomplete'),
    url(r'^workforce_training_autocomplete/', views.all_workforce_autocomplete, name='workforce_training_autocomplete'),
    url(r'^res_child_autocomplete/', views.res_child_autocomplete, name='res_child_autocomplete'), 
    url(r'^public_sens_ben_autocomp/', views.public_sens_ben_autocomp, name='public_sens_ben_autocomp'),      
    url(r'^view_wfc/', views.workforce_view, name='workforce_view'),
    url(r'^update_wfc/', views.workforce_update, name='workforce_update'),
    url(r'^communities/', views.list_of_cwacs_in_ward, name='list_of_cwacs_in_ward'),
    url(r'^ben_search/', views.search_beneficiary, name='ben_search'),
    url(r'^guardian/', views.new_guardian_reg, name='guardian'),
    url(r'^child/', views.new_child_reg, name='child'),
    url(r'^ben_autocomplete/', views.ben_autocompletion_list, name='benautocomplete'),
    url(r'^ben_wfc_autocomplete/', views.ben_wfc_autocompletion_list, name='benwfcautocomplete'),
    url(r'^ben_res_autocomplete/', views.ben_res_autocompletion_list, name='benresautocomplete'),
    url(r'^benlists/', views.ben_search_results, name='ben_search_results'),
    url(r'^formlists/', views.form_search_results, name='form_search_results'),
    url(r'^view_ben/', views.beneficiary_view, name='beneficiary_view'),
    url(r'^update_ben/', views.update_beneficiary, name='beneficiary_update'),
    url(r'^guardian_contact/', views.get_guardian_contacts, name='guard_contact'),
    url(r'^capture_admin/', capture_views.capture_admin, name='capture_administration'),
    url(r'^begin_upload/', capture_views.start_upload, name='start_upload'),
    url(r'^begin_download/', capture_views.start_download, name='start_download'),
    url(r'^stop_download_upload/', capture_views.stop_download_upload, name='stop_download_upload'),
    url(r'^upload/', api.process_upload, name='api_capture_upload'),
    url(r'^download/', api.download, name='download_sections'),
    #url(r'^data_dictionary/', api.data_dictionary, name='data_dictionary'),
    url(r'^check_names/', views.check_person_names, name='check_names'),
    url(r'^check_form_titles/', views.check_form_titles, name='check_form_titles'),
    url(r'^refresh_capture_site/', capture_views.refresh_capture_site, name='refresh_capture_site'),
    url(r'^forms_home/', views.forms_home_page, name='forms_home'),
    url(r'^subject/', views.get_subject, name='subject'),
    url(r'^add_form/', views.load_form, name='add_form'),
    #url(r'^user_can_add_form/', views.user_can_add_form, name='user_can_add_form'),
    url(r'^get_wfc_object/', views.get_wfc, name='get_wfc_object'),
    url(r'^delete/', views.delete_form, name='delete'),
    url(r'^reports_and_analysis/', views.reports_and_analysis, name='reports_and_analysis'),
    url(r'^rep_utils/', views.report_utils, name='rep_utils'),
    url(r'^report_params/', views.report_paramenters, name='report_params'),
    url(r'^run_report/', views.get_report, name='run_report'),
    #url(r'^persons/', views.get_persons_list, name='api_persons_list'),
    url(r'^get_reported_by_options/', views.get_reported_by_options, name='get_field_options'),
    url(r'^help/', views.view_help, name='help'),
    url(r'^form-search/', service_views.forms_search, name='form_search'),
    url(r'^delrecord/', service_views.delete_form, name='delete_form'),
    #url(r'^help/', TemplateView.as_view(template_name='ovc_main/ZOMIS_help_html/HTML/index.html')),
    
    url(r'^basic_service_referral/', service_views.basic_service_referral_vw, name='basic_service_referral'),
    url(r'^residential_institution/', service_views.residential_institution_vw, name='residential_institution'),
    url(r'^workforce_training/', service_views.workforce_training_vw, name='workforce_training'),
    url(r'^public_sensitisation/', service_views.public_sensitisation_vw, name='public_sensitisation'),
    url(r'^all_ben_autocomplete/', service_views.beneficiary_search_result, name='beneficiary_search_result'), 
)
