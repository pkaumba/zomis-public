$(document).ready( function() {
		$("#searchbtn").click(function () {
		                //alert($("#searchorgtxt").val());
		                $.ajax({
		                    dataType: "html",
		                    url: "/organisation/orglists?searchterm=" + $("#searchorgtxt").val(),
		                    contentType: "application/json; charset=utf-8",
		                    //data: {"districtid" : values},
		                    success: function (results) {
		                        if (results.length == 0) {
		                            $('#orgresults tbody').html("<tr><td colspan='5'>No results for search term supplied</td></tr>");
		                        }
		                        else {
		                            $('#orgresults tbody').empty().append(results);
		                        }
		                    },
		                    error: function () {
		                        $('#orgresults tbody').html("<tr><td span='5'>No results for search term supplied</td></tr>");
		                        //alert($("#searchorgtxt").val())
		                    }
                });
            });

            $(".gw-next").click(function () {
                var pagenumber = $(".gw-page").val();
                orgpagination(++pagenumber);
            });
            
            $(".gw-prev").click(function () {
                var pagenumber = $(".gw-page").val();
                orgpagination(--pagenumber);
            });
            
            orgpagination = function (pagenumber)
            {
                $(".totalrecords").empty().append("<i class='fa fa-spinner fa-spin'></i> Searching...");
                $(".searching").show();
                $(".totalrecords").hide();
                $.ajax({
                    dataType: "json",
                    url: "/organisation/orglists?searchterm=" + $("#searchorgtxt").val() + "&pagenumber=" + pagenumber + "&itemsperpage=" + $(".gw-pageSize").val() + "&orgtype=" + $("#orgtype").val() + "&search_all=" + $(".search_closed_org").is(':checked'),
                    success: function (results) {
                        if (results.length == 0) {
                            $('#orgresults tbody').html("<tr><td colspan='5'>No results for search term supplied</td></tr>");
                        }
                        else {
                            $('#orgresults tbody').empty().html("");
                            $.each(results, function (index) {

                                if (results[index].pageinfo == 'pageinfo')
                                {
                                    $(".countpages").empty().html(results[index].pagescount);
                                    $(".totalrecords").empty().append("Found " + results[index].total_records + " matching organisation(s)");

                                    //set the control number
                                    $(".gw-page").val(results[index].pagenumber);
                                    if (results[index].pagenumber >= results[index].pagescount)
                                    {
                                        if (!$(".gw-next").hasClass("disabled"))
                                        {
                                            $(".gw-next").addClass("disabled");
                                        }
                                        if ($(".gw-prev").hasClass("disabled"))
                                        {
                                            $(".gw-prev").removeClass("disabled");
                                        }

                                    }
                                    if (results[index].pagescount == 1) {
                                        if (!$(".gw-next").hasClass("disabled"))
                                        {
                                            $(".gw-next").addClass("disabled");
                                        }
                                        if (!$(".gw-prev").hasClass("disabled"))
                                        {
                                            $(".gw-prev").addClass("disabled");
                                        }
                                    }
                                    if (results[index].pagenumber <= 1) {
                                        if (!$(".gw-prev").hasClass("disabled"))
                                        {
                                            $(".gw-prev").addClass("disabled");
                                        }
                                    }
                                    if (results[index].pagenumber < results[index].pagescount) {
                                        if ($(".gw-next").hasClass("disabled"))
                                        {
                                            $(".gw-next").removeClass("disabled");
                                        }
                                    }
                                    if (results[index].pagenumber < results[index].pagescount && results[index].pagenumber > 1) {
                                        if ($(".gw-prevt").hasClass("disabled"))
                                        {
                                            $(".gw-prev").removeClass("disabled");
                                        }
                                    }

                                }
                                else {

                                    var tmp_span = "<span class='badge label-danger'>closed</span>";
                                    var is_active = false;
                                    if (results[index].is_active === true)
                                    {
                                        tmp_span = "";
                                    }
                                    var action_html = "";
                                    var row_html = "<tr>";
                                    if (!results[index].is_capture_app) {
                                        row_html = "<tr class=='table_row_clickable' " +
                                                "href='/organisation/view_org?organisation_id=" + results[index].org_system_id + "' style='cursor:pointer'> ";
                                    }
                                    if (!results[index].is_capture_app) {
                                        action_html = "<td><a href='/organisation/update_org?organisation_id=" + results[index].org_system_id + "' class='btn btn-primary btn-xs'><i class='fa fa-edit'></i>&nbsp;Update</a></td> </tr>";
                                    }
                                    
                                    $("#orgresults > tbody").append(
                                            "<tr><td>" + results[index].org_id + " " + tmp_span + "</td> " +
                                            "<td>" + results[index].org_name + "</td> " +
                                            "<td>" + results[index].unit_type_name + "</td> " +
                                            "<td>" + results[index].parent_organisation_name + "</td> " +
                                            "<td>" + results[index].location + "</td> </tr>" );
                                }
                            });
                        }
                    },
                    error: function () {
                        $('#orgresults tbody').html("<tr><td span='5'>No results for search term supplied</td></tr>");
                        //alert($("#searchorgtxt").val())
                    }
                });

                $(".searching").fadeOut('slow', function () {
                    $(".totalrecords").fadeIn('slow');
                });

                //$(".totalrecords").show();
            };
            
            $(".gw-pageSize").change(function () {
                $(".gw-pageSize").val($(".gw-pageSize").val());
                orgpagination(1);
            });
            $("#searchbtn2").click(function () {
                orgpagination(1);
            });

            $("#searchorgtxt").keypress(function (event) {
                var keycode = (event.keyCode ? event.keyCode : event.which);
                if (keycode == '13') {
                    orgpagination(1);
                    return false;
                }
            });
                });
          
   