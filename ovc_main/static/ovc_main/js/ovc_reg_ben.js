$(document).ready(function () {     
									function modifyChangeDateProp()
                                    {
                                        if($("#id_is_update").val() == "Yes")
                                        {
                                            $("#id_guardian_change_date").prop('required', true);
                                        }
                                    }
                                    addGuardian = function() {
                                    		var enteredGuardian = $('input[name="guardian"]').val();
                                            var spVals = enteredGuardian.split(',');
                                            var len = spVals.length;
                                            
                                            if(enteredDataInvalid())
                                            {
                                                return;
                                            }
                                            
                                            if(len != 3)
                                            {
                                                return;
                                            }
                                            var ident = spVals[0];
                                            var guardian = spVals[2];
                                            var notes = $('input[name=notes_relationship]').val();
                                            
                                            if(enteredGuardianAlreadyExists(ident))
                                            {
                                                return;
                                            }
                                            
                                            var guard_id = '"'+ident+'"';
                                            if($("#id_is_update").val() == "Yes")
                                            {
                                                $("#guardianTable").find('tbody')
                                                .append($('<tr>')
                                                    .append($('<td id="1">')
                                                        .append($('<label>')
                                                            .text(ident)
                                                        )
                                                    )
                                                    .append($('<td id="2">')
                                                        .append($('<label>')
                                                            .text(guardian)
                                                        )
                                                    )
                                                    .append($('<td id="3">')
                                                        .append($('<label>')
                                                            .text(notes)
                                                        )
                                                    )
                                                    .append($("<td id='4' > </td>"))
                                                    .append($("<td id='5'> <button onclick='myDeleteFunction()' class='removebutton'><i class='fa fa-trash fa-2'></i></button></td>"))
                                                );
                                            }
                                            else
                                            {
                                                $("#guardianTable").find('tbody')
                                                .append($('<tr>')
                                                    .append($('<td id="1">')
                                                        .append($('<label>')
                                                            .text(ident)
                                                        )
                                                    )
                                                    .append($('<td id="2">')
                                                        .append($('<label>')
                                                            .text(guardian)
                                                        )
                                                    )
                                                    .append($('<td id="3">')
                                                        .append($('<label>')
                                                            .text(notes)
                                                        )
                                                    )
                                                    .append($("<td id='4' > <button type='button' onclick='useContacts("+guard_id+")' class='use-address'><i class='fa fa-check-square-o'></i></button></td>"))
                                                    .append($("<td id='5'> <button onclick='myDeleteFunction()' class='removebutton'><i class='fa fa-trash fa-2'></i></button></td>"))
                                                );
                                            }
                                            
                                            modifyChangeDateProp();
                                            updateTableData();   
                                            clearForm();
                                        }
                                        function clearForm(){
                                            $('input[name="guardian"]').val('');
                                            $('input[name="notes_relationship"]').val('');
                                            $('input[id=ben_id_hidden]').val('');;
                                            $('input[id=ben_name_hidden]').val('');
                                            $("#guardian_error_display").hide();
                                        }
                                        
                                        function enteredGuardianAlreadyExists(enteredGuardianId)
                                        {
                                            var alreadyEntered = false;
                                        
                                            $("#guardianTable tr").each(function(i, v){
                                                $(this).children('td').each(function(ii, vv){
                                                    if ($(this).attr("id") == '1'){
                                                        if($(this).text() == enteredGuardianId)
                                                        {
                                                            $("#guardian_error_display").text("This guardian has already been added");
                                                            $("#guardian_error_display").show();
                                                            alreadyEntered = true;
                                                        }                                          
                                                    }
                                                });
                                            });
                                            
                                            if(alreadyEntered)
                                            {
                                                 return true;
                                            }
                                            else
                                            {
                                                 return false;
                                            }
                                        }
                                        
                                        function enteredDataInvalid()
                                        {
                                            var enteredGuardian = $('input[name="guardian"]').val();
                                            var spVals = enteredGuardian.split(',');
                                            var len = spVals.length;
                                            if(len == 3)
                                            {
                                                if(spVals[0]!="" || spVals[0]=="")
                                                {
                                                    if(spVals[0]!="" && testbenId(spVals[0]))
                                                    {
                                                         return false;
                                                    }
                                                    $("#guardian_error_display").text("The entered guardian beneficiary Id is invalid.");
                                                    $("#guardian_error_display").show();
                                                    return true;
                                                    
                                                }
                                                else if(spVals[1]!=""||spVals[1]=="")
                                                {
                                                    if(spVals[1]!="" && testNrc(spVals[1]))
                                                    {
                                                        return false;
                                                    }
                                                    
                                                    $("#guardian_error_display").text("The entered guardian National Id is invalid.");
                                                    $("#guardian_error_display").show();
                                                    return true;
                                                }
                                                else if(spVals[2]=="")
                                                {
                                                    $("#guardian_error_display").text("Guardian Name cannot be empty.");
                                                     $("#guardian_error_display").show();
                                                     return true;
                                                }
                                            }
                                            else
                                            {
                                                $("#guardian_error_display").text("Please enter valid guardian details in the format beneficiary Id, National Id, Guardian Full Name.");
                                                 $("#guardian_error_display").show();
                                                 return true;
                                            }
                                            
                                            if($('input[name=notes_relationship]').val()=="")
                                            {
                                                $("#guardian_error_display").text("The notes on relationship field cannot be empty");
                                                 $("#guardian_error_display").show();
                                                 return true;
                                            }
                                        
                                        return false;
                                        }
                                        
                                        function testNrc(nrcNum) { 
                                          // http://stackoverflow.com/a/46181/11236
                                                      
                                            var re = /^\b\d{6}\/\b\d{2}\/\b[1-3]{1}$/;
                                            return re.test(benId);
                                        }
                                        
                                        function testbenId(nrcNum) { 
                                            // http://stackoverflow.com/a/46181/11236
                                            
                                            var re = /^B\d{8}$/;
                                            return re.test(nrcNum);
                                        }
                                        
                                        useContacts = function (guard_id){
                                             $.ajax({
                                                        dataType: "json",
                                                        url: "/beneficiary/guardian_contact?guardian_id=" + guard_id,
                                                        contentType: "application/json; charset=utf-8",
                                                        success: function (reg_guardians) {
                                                            $.each(reg_guardians, function (key, value) {
                                                                switch(key)
                                                                {
                                                                    case 'Phone number - mobile':
                                                                            $( "#id_mobile_phone_number" ).val(value);
                                                                            break;
                                                                    case 'Email address':
                                                                            $( "#id_email_address" ).val(value);
                                                                            break;
                                                                    case 'Physical address':
                                                                            $( "#id_physical_address" ).val(value);
                                                                            break;
                                                                    default:
                                                                            break;
                                                                }
                                                            });
                                                        }
                                                    });
                                        }
                                        
                                        myDeleteFunction = function(){
                                            $(document).on('click', 'button.removebutton', function () { 
                                                $(this).closest('tr').remove();
                                                return false;
                                            });
                                            modifyChangeDateProp();
                                        }
                                        
                                        function updateTableData(){
                                        
                                        var row_count = $('#guardianTable tbody').children().length;
                                        
                                        if(row_count <= 0)
                                            {
                                                return;
                                            }                                            
                                                                                        
                                            var data = Array();
                                            $("#guardianTable tr").each(function(i, v){
                                                data[i] = Array();
                                                $(this).children('td').each(function(ii, vv){
                                                    if ( $(this).attr("id") == '4' || $(this).attr("id") == '5' || $(this).text() == ''){
                                                        //do nothing really                                                   
                                                    }
                                                    else{   
                                                        data[i][ii] = $(this).text();
                                                    }
                                                    
                                                    if($(this).attr("id") == '5')
                                                    {
                                                        data[i][ii] = '#';
                                                    }
                                                }); 
                                            });
                                            
                                            $( "#id_guardians_hidden" ).val('');
                                            
                                            $( "#id_guardians_hidden" ).val(data);
                                            
                                            //$( "#res_id_hidden" ).val($("#id_ben_res_id_hidden").val());
                                        }
                                        
                                        updateFinale = function(e){
                                            FinalizeGuardianData();
                                            if($("input[id='id_no_adult_guardians']").is( ":checked" ))
                                            {
                                                $("#guardianTable").find('tbody').empty();
                                                $('input[name="guardians_hidden"]').val('');
                                                clearForm();
                                            }
                                            
                                            checkNrc();
                                            checkDateofDeath();
                                            validateWfcField();
                                            checkUpdateMode(e);
                                        }
                                        
                                        function checkDateofDeath(){
                                        	if(!$("#id_date_of_death").prop('required'))
                                        	{
                                        		$("#id_date_of_death").val('');
                                        	}
                                        }

                                        function checkNames(){
                                        	var tmpperson;
                                            $.ajax({
                                                    dataType: "json",
                                                    url: "/beneficiary/check_names?first_name=" + $( "#id_first_name" ).val() + "&last_name=" + $( "#id_last_name" ).val() + "&pk_id=" + $( "#id_pk_id" ).val(),
                                                    contentType: "application/json; charset=utf-8",
                                                    async: false,
                                                    success: function (person) {
                                                    	tmpperson = person;
                                                    }
                                                });
                                            return tmpperson;
                                        }
                                        
                                        function checkNrc()
                                        {
                                            var entered_date = $("#id_date_of_birth").val();
                                              var selDate = new Date(entered_date);
                                              var today = new Date();
                                              var val = today - selDate;
                                              var diff = val / (24*60*60*1000);
                                              
                                              if(diff >= 5843.88)
                                              {
                                                  //Save the nrc
                                              }
                                              else
                                              {
                                                  $("#id_nrc").val('');
                                              }
                                        }
                                        
                                        function FinalizeGuardianData(){
                                            if($('input[name="guardians_hidden"]').val() == "first_time")
                                            {
                                                updateTableData();
                                                if($('input[name="guardians_hidden"]').val() == "first_time")
                                                {
                                                    $('input[name="guardians_hidden"]').val('');
                                                }
                                            }
                                        }
                                        
                                        function invalidWorkforce(){
                                        	var enteredWorkforce = $('input[name="workforce_member"]').val();
                                            var spVals = enteredWorkforce.split(',');
                                            var len = spVals.length;
                                            var re = /^W\d{7}$/;
                                            var toReturn;
                                            
                                        	if(len == 4)
                                            {
                                                if(spVals[1]!="" || spVals[1]=="")
                                                {
                                                    if(spVals[1]!="" && re.test(spVals[1]))
                                                    {
                                                    	toReturn = false;
                                                    }
                                                    else{
                                                    	toReturn = true;
                                                    }
                                                }
                                                else if(spVals[2]!=""||spVals[2]=="")
                                                {
                                                    if(spVals[2]!="" && testNrc(spVals[2]))
                                                    {
                                                    	toReturn = false;
                                                    }
                                                    else{
                                                    	toReturn = true;
                                                    }
                                                }
                                                else if(spVals[3]=="")
                                                {
                                                	toReturn = true;
                                                }
                                                
                                                if(spVals[0]){
                                                	$.ajax({
                                                        dataType: "json",
                                                        url: "/beneficiary/get_wfc_object?wfc_id=" + spVals[0] + "&wfc_obj='Yes'",
                                                        contentType: "application/json; charset=utf-8",
                                                        async: false,
                                                        success: function (person) {
                                                			if(person == null || person == undefined)
                                                			{
                                                				toReturn = true;
                                                			}
                                                			else
                                                			{
                                                				toReturn = false;
                                                			}
                                                        }
                                                    });
                                                }
                                                return toReturn;
                                                
                                            }
                                            else if($('input[name="workforce_member"]').val() != null && $('input[name="workforce_member"]').val() != "")
                                            {
                                            	return true;
                                            }
                                        }
                                        
                                        
                                        function validateWfcField()
                                        {
                                        	if (invalidWorkforce()) {
                                        		$('input[name="workforce_member"]')[0].setCustomValidity('Enter a valid workforce');
                                		    } 
                                        	else {
                                        		$('input[name="workforce_member"]')[0].setCustomValidity('');
                                		    }
                                        }
                                        
                                        function checkUpdateMode(e){
                                            var edit_mode = $('input:radio[name=edit_mode]:checked').val();
                                            $( "#id_edit_mode_hidden" ).val(edit_mode);
                                            if(edit_mode == 3)
	                                  		  {
	                                  			  e.preventDefault();
	                                  			  $("#duplicate_dialog_box").click();
	                                  		  }
	                                  		  else
											  {
												  var person = checkNames();
										            if(person != undefined)
										            {
										            	e.preventDefault();
														$("#confirm_dialog_box").click();
										            }
											  }
                                        }
                                        
                                        $("#id_cancel_submit").on('click',function(){
                                        	toggleSubmitButton();
                                        });
                                        
                                        function toggleSubmitButton(){
                                        	if($(":submit").attr('disabled') == 'disabled'){
                                        		$(":submit").removeAttr('disabled');
                                        	}
                                        	else{
                                        		$(":submit").attr('disabled', 'disabled');
                                        	}
                                        }
                                        
                                        $("form").submit(function(){
                                        	toggleSubmitButton();
                                            return true;
                                        });
                                        
});