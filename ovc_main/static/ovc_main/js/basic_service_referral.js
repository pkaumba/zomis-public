$(document).ready(function () {
	
	var bendateofbirth;
	$("#div_id_new_adverse_condition").hide();
	
	 $('.datepicker').datepicker(
     {
         //minDate: "-18Y",
         dateFormat: 'd-MM-yy',
         yearRange: "1904:2050",
         maxDate: 0,
         changeMonth: true,
         changeYear: true,
         onSelect: function(dateselected, dateobj){
        	 handleAdverseCondVisibility();
         }
     });
	
	function handleAdverseCondVisibility(){
		$("#div_id_new_adverse_condition").hide();
		var dateofservice = $("#id_date_of_service").val();
		if(bendateofbirth !== undefined && dateofservice !== ""){
			var age  = _calculateAge(bendateofbirth, dateofservice);
			if(age < 18)
				$("#div_id_new_adverse_condition").show();
			
		}
		
	}
	
	//http://stackoverflow.com/questions/4060004/calculate-age-in-javascript
	function _calculateAge(birthdaystring, todatestring) { // birthday is a date
	    var birthday = new Date(birthdaystring);
	    var todate = new Date(todatestring);
		var ageDifMs = todate.getTime() - birthday.getTime();
	    var ageDate = new Date(ageDifMs); // miliseconds from epoch
	    return Math.abs(ageDate.getUTCFullYear() - 1970);
	}

	var fill_orgs_control = function(){
		org_unit_control = $("#id_organisation_unit").empty();
		org_unit_control.append('<option value="">-----</option>');
		$.each(workforce_orgs, function(orgid, orgname){
			if(orgid !=='-1')
			{
				org_unit_control.append('<option value="'+orgid+'">'+orgname+'</option')
			}
		});
	};
	var ben_area_id = '';
	var subjectid  = $("#id_subject").val().trim();
	
	var servicesprovided_dict = {},
	referralsmade_dict = {},
	referralscompleted_dict = {}, 
	adverseconditions_dict = {};
	
	var tablerows = [];
	if(subjectid !== '')
	{
		$("#id_workforce_system_id").val(subjectid);
	}

	$("#searchbtn-basic-service").click(function(e){
		
		
		tablerows = [];
	
		searchrecords();
		$("#searchtext").parent().removeClass('has-error');
		$("#id_searchtext_error").remove();	
		
		
		return false;
	});
	
	var searchrecords = function(formid){
		var searchtext = $("#searchtext").val();
		var tmpformid = formid;
		var whattosearch = 'past-records';
		console.log(formid);
		if(formid === undefined)
		{
			tmpformid = '';
		}
		else{
			whattosearch = "byformid";
		}
		console.log(whattosearch);
		$.ajax({
			url: '/forms/form-search/',
			datatype: 'json',
			data: {
					formtype:'FT3e', 
					what: whattosearch,
					subject: subjectid,
					form_id: tmpformid,
					dateofsearch: $('#datetosearch').val(),
					dateattr: $('#datesearchattribute').val(),
					token: searchtext
				}
		})
		.done(function( result ) {
			$("#service-referral-records tbody").empty().html();
			add_to_table($.parseJSON(result));
		  });
	};
	var add_to_table = function(data){
		var rmemptyrow = true;
		//console.log(typeof(data));
		//console.log(data);
		$.each(data, function(key, jsonstring){
			//console.log(key);
			if(rmemptyrow)
			{
				$(".noresultsrow").hide();
				rmemptyrow=false;
			}
			tablerows.push(jsonstring);
			var workforceid = jsonstring.workforceid,
			
			servicesprovided = jsonstring.servicesprovided,
			
			referralscompleted = jsonstring.referralscompleted,
			referralsmade = jsonstring.referralsmade,
			adverseconditions = jsonstring.adverseconditions,
			
			formid = jsonstring.formid,
			
			beneficiaryid = jsonstring.beneficiaryid,
			benfirstname = jsonstring.benfirstname,
			bensurname = jsonstring.bensurname,
			bendisplayid = jsonstring.bendisplayid,
			
			orgunitid = jsonstring.orgunitid,
			areaid = jsonstring.areaid,
			areaname = jsonstring.areaname,
			encountertypeid = jsonstring.encountertypeid,
			encountertypetext = jsonstring.encountertypetext,
			encounterdate = jsonstring.encounterdate,
			encounterid = jsonstring.encounterid;
			var message = '';
			if(jsonstring.message === 'new' || jsonstring.message === 'update')
			{
				message = '<span class="badge">'+jsonstring.message+'</span> ';
			}
			//console.log(servicesprovided);
			$("#service-referral-records tbody")
				.append($("<tr>").attr('id', (tablerows.length-1))
							.append($("<td>").append(message+encounterdate),
									$("<td>").append(bendisplayid+'-'+benfirstname+' '+bensurname),
									$("<td>").append(encountertypetext),
									$("<td>").append(jsonarraykeystostring(servicesprovided)),
									$("<td>").append(jsonarraykeystostring(referralsmade)),
									$("<td>").append(jsonarraykeystostring(referralscompleted)),
									$("<td>").append(jsonarraykeystostring(adverseconditions)),
									$("<td>").append('<span>	\
															<a href="view" class="btn btn-primary btn-xs record-action">	\
																<i class="fa fa-file-o"></i> View	\
															</a>	\
														</span>	\
														<span>\
															<a href="update" class="btn btn-warning btn-xs record-action">\
																<i class="fa fa-pencil-square-o"></i> Edit	\
															</a>	\
														</span>\
														<span>	\
															<a href="del" class="btn btn-danger btn-xs record-action">\
																<i class="fa fa-trash"></i> Delete	\
															</a>	\
														</span>'
													)
									)
						);
		});
	};
	
	$("#service-referral-records tbody").on('click','.record-action', function(e){
		e.preventDefault();
		//alert($(this).attr('href'));
		$("#add_record").removeClass("disabled");
		var action = $(this).attr('href');
		var rowindex = $(this).parent().parent().parent().attr('id');
		var rowdetails = tablerows[parseInt(rowindex)];
		if(action === 'del')
		{
			$(this).parent().parent().parent().hide();
			post_del(rowdetails.formid, rowdetails.beneficiaryid, rowdetails.encounterid);
			$(".readonly-service-main").hide();
			$(".servicemain").hide();
		}
		else if(action === 'view'){
			$(".readonly-service-main").removeClass('hide');
			$(".readonly-service-main").show();
			$(".servicemain").hide();
			
			$('html, body').animate({
			        scrollTop: $(".readonly-service-main").parent().offset().top
			    }, 100);
			
			readonlyview(rowdetails);
		}
		else if(action === 'update'){
			//console.log("else if function for update reached");
			//console.log(rowdetails);
			resetform();
			$(".servicemain").removeClass("hide");
			//$(".readonly-service-main").removeClass("hide");
			$(".servicemain").show();
			$(".readonly-service-main").hide();
			
			$('html, body').animate({
			        scrollTop: $(".servicemain").parent().offset().top
			    }, 100);
			
			readonlyview(rowdetails);
			var delrow = $(this).parent().parent().parent();
			var btnval = {};
			global_row_to_delete = $(this).parent().parent().parent();
			//btnval['update'] = $(this).parent().parent().parent();
			//console.log($.type(btnval));
			$("#btnsave").val('update');
			console.log("btn value: "+$("#btnsave").val());
			update_view(rowdetails);
		}
		//$(".servicemain").removeClass("hide");	
		
		return false;
	});
	
	var update_view = function(data){
		//console.log(data);
		$(".viewonly > dd").empty().html('');
		var formid = data.formid;
		$("#id_form_id").val(formid);
		$("#id_encounter_id").val(data.encounterid);
		console.log($("#id_form_id").val());
		//$("#id_organisation_unit").hide();
		ben_area_id = data.areaid;
		bendateofbirth = data.bendateofbirth;
		handleAdverseCondVisibility();
		$("<dd>"+data.encounterdate+"</dd>").insertAfter($(".viewonly > #dateofservice"));
		$("<dd>"+data.orgunitname+"</dd>").insertAfter($(".viewonly > #orgunit"));
		$("<dd>"+data.bendisplayid+'-'+data.benfirstname+' '+data.bensurname+"</dd>").insertAfter($(".viewonly > #beneficiary"));
		
		$("#id_encounter_type").val(data.encountertypeid);
		$("#id_encounter_type").trigger("change");
		$("#id_encounter_location").val(data.areaid).combobox("refresh")
		
		
		$("#id_organisation_unit").hide();
		$("#orgunit_name_label").remove();
		$("#id_organisation_unit")
			.parent()
			.append('<p class="form-control-static" id="orgunit_name_label"> \
						<strong>'+data.orgunitname+'</strong> \
					</p>');
		$("#id_organisation_unit").val(data.orgunitid);
		
		
		$("#id_date_of_service").hide();
		$("#date_of_service_label").remove();
		$("#id_date_of_service")
			.parent()
			.append('<p class="form-control-static" id="date_of_service_label"> \
						<strong>'+data.encounterdate+'</strong> \
					</p>');
		$("#id_date_of_service").datepicker("setDate", data.encounterdate);
		
		$("#id_beneficiary_autocomplete").hide();
		$("#id_beneficiary_autocomplete").val(data.bendisplayid+'\t'+data.benfirstname+' '+data.bensurname);
		$("#beneficiary_info_label").remove();
		$("#id_beneficiary_autocomplete")
			.parent()
			.append('<p class="form-control-static" id="beneficiary_info_label"> \
						<strong>'+data.bendisplayid+'-'+data.benfirstname+' '+data.bensurname+'</strong> \
					</p>');
		$("#beneficiary_id").val(data.beneficiaryid);
		
		$("#services_provided").multiSelect('select', getarrayfromjsonkeys(data.servicesprovided));
		$("#referrals_made").multiSelect('select', getarrayfromjsonkeys(data.referralsmade));
		$("#referrals_completed").multiSelect('select', getarrayfromjsonkeys(data.referralscompleted));
		$("#adverse_conditions").multiSelect('select', getarrayfromjsonkeys(data.adverseconditions));
		
		
	};
	
	var readonlyview = function(data){
		//console.log(data);
		$(".viewonly > dd").html('');
		
		$("<dd>"+data.encounterdate+"</dd>").insertAfter($(".viewonly > #dateofservice"));
		$("<dd>"+data.orgunitname+"</dd>").insertAfter($(".viewonly > #orgunit"));
		$("<dd>"+data.bendisplayid+'-'+data.benfirstname+' '+data.bensurname+"</dd>").insertAfter($(".viewonly > #beneficiary"));
		$("<dd>"+data.encountertypetext+"</dd>").insertAfter($(".viewonly > #encountertype"));
		$("<dd>"+data.areaname+"</dd>").insertAfter($(".viewonly > #encounterlocation"));
		
		//$(".viewonly > #servicesprovided")
		$.each(valuesfromjsonobject(data.servicesprovided), function(key, value){
			$("<dd>"+value+"</dd>").insertAfter($(".viewonly > #servicesprovided"));
		});
		
		$.each(valuesfromjsonobject(data.referralsmade), function(key, value){
			$("<dd>"+value+"</dd>").insertAfter($(".viewonly > #referralsmade"));
		});
		
		$.each(valuesfromjsonobject(data.referralscompleted), function(key, value){
			$("<dd>"+value+"</dd>").insertAfter($(".viewonly > #referralscompleted"));
		});
		
		$.each(valuesfromjsonobject(data.adverseconditions), function(key, value){
			$("<dd>"+value+"</dd>").insertAfter($(".viewonly > #adverseconditions"));
		});
		
	};
	
	var valuesfromjsonobject = function(data){
		var toreturn = [];
		$.each(data, function(key, value){
			$.each(value, function(key2, value2){
				toreturn.push(value2);
			});
		});
		
		return toreturn;
	};
	
	var post_del = function(formid, benid, encounterid){
		var success = true;
		 $.ajax({
			  method: 'POST',
			  url: '/forms/delrecord/',
			  data: {return_type:'json', encounterid:encounterid, benid:benid, formid:formid, csrfmiddlewaretoken:$("input[name='csrfmiddlewaretoken']").val()},
			  datatype: "json",
			  async:false
			})
			.done(function(result){
				
				$.each($.parseJSON(result), function(key, value){
					if(key !== 'success')
					{
						success=false;
					}
				});
			});
		return success;	
			
	};

	$("#id_encounter_type").change(function(){
		var encounterlocation = $("#id_encounter_type").val();
		if(encounterlocation === 'EHOM')
		{
			$("#id_encounter_location").val(ben_area_id).combobox("refresh");
			$("#id_encounter_location_auto_complete").prop('readonly', true);
		}
		else{
			$("#id_encounter_location").val('').combobox("refresh");
			$("#id_encounter_location_auto_complete").prop('readonly', false);
		}
	});
	
	var jsonarraykeystostring = function(data){
		var toreturn = [];
		//console.log(data);
		$.each(data, function(key, value){
			//console.log(value);
			$.each(value, function(key2, value2){
				toreturn.push(key2);
			});
		});
		
		return toreturn.join(', ');
	};
	
	var getarrayfromjsonkeys = function(data){
		var toreturn = [];
		//console.log(data);
		$.each(data, function(key, value){
			//console.log(value);
			$.each(value, function(key2, value2){
				toreturn.push(key2);
			});
		});
		
		return toreturn;
	};
	
	

	$(".beneficiary_autocomplete").autocomplete({
		source: function(request, response){query_all_bens_source(request, response);},
		minLength:1,
		delay: 100,
		select: function(event, ui){
				event.preventDefault();
				$("#beneficiary_id").empty().val(ui.item.value[0]);
			}
		
	});
	
	 $(".beneficiary_autocomplete").on("autocompleteselect", function (event, ui) {
        var result = concatenatearray(ui.item.value, 1, 3, '\t');
        ben_area_id = ui.item.value[8];
        ben_area_display_text = ui.item.value[9];
        ben_control_source = [ben_area_id, ben_area_display_text];
        result = result +'\t'+ ben_area_display_text; 
        $("#beneficiary_id").val(ui.item.value[0]);
        $(".beneficiary_autocomplete").val(result);
        $("#id_encounter_type").val("EHOM");
        $("#id_encounter_type").trigger('change');
        bendateofbirth = ui.item.value[4];
        handleAdverseCondVisibility();
    });
	
	var customfieldreset = function(){
		$("#id_organisation_unit").show();
		$("#orgunit_name_label").remove();
		
		
		$("#id_date_of_service").show();
		$("#date_of_service_label").remove();
		
		$("#id_beneficiary_autocomplete").show();
		$("#beneficiary_info_label").remove();

	};

    var resetform = function(){
    	bendateofbirth = undefined;
    	customfieldreset();
    	clean_errors();
    	document.getElementById("formid").reset();
		 $('.selectmultiple').multiSelect('deselect_all');
		 $('.selectmultiple').multiSelect('refresh');
		 $("#id_form_id").val('');
		 $("#id_encounter_id").val('');
		 
    };
    
	 $("#id_encounter_location").combobox();
    
	 $("#btncancel").click(function(){
		 toastr.clear();
		 resetform();
		 $("#add_record").removeClass("disabled");
		 $(".servicemain").hide();
		 return false;
		 
	 });
	 
	 var multi_select_to_key_value_pair = function(selector){
		 var toreturn = {};
		 var results = $(selector).map(function(){
						 //var tmp = {};
						 toreturn[$(this).val()] = $(this).text();
						 //return tmp;
					 });
		 return toreturn;
	 };
	 $("#btnsave").click(function(e){
		 e.preventDefault();
		 handleAdverseCondVisibility();
		 toastr.clear();
		 clean_errors();
		 if($("#id_beneficiary_autocomplete").val().trim() === ''){
			 $("#beneficiary_id").val('');
			 $("#id_encounter_type").val('');
			 $("#id_encounter_type").trigger('change');
		 }
		 var dataString = $("#formid").serialize();
		 //console.log(dataString);
		 
		 
		 
		 var formid = -1;
		 var $form = $("#formid"),
		 url = $form.attr( "action" );
		 var success = false;
		 $.ajax({
			  type: 'POST',
			  url: url,
			  data: dataString+"&return_type=json",
			  datatype: "json",
			  async:false
			})
			.done(function(result){
				
				$.each($.parseJSON(result), function(key, value){
					if(key !== 'success')
					{
						console.log(key);
						insert_errors(key, value);
						//success=false;
					}
					else{
							success=true;
							var savebtnval_action = $("#btnsave").val();
							var message = 'new';
							 //console.log(savebtnval_action);
							 if(savebtnval_action === 'update'){
								 global_row_to_delete.hide();
								 message = 'update';
							}
							formid = value;
							var todisplayontable = [retriveformvalues(formid, message)];
							add_to_table(todisplayontable);
					}
				});
				
				
			})
		.fail(function(){
			insert_errors('__all__', ['An error occured while connecting to the server, please try saving again']);
			return false;
		});
		
		if(success)
		 {
			 toastr.success('You successfully saved the record', 'Basic service referral',{ timeOut: 500 });
			 
				 
			 resetform();
			 $("#add_record").removeClass("disabled");
			 $(".servicemain").hide();
		 }
		 else{
			 $("#overall-record-error").removeClass("hide");	
			 $("#overall-record-error").show();
			 $('html, body').animate({
			        scrollTop: $("#overall-record-error").offset().top
			    }, 500);
			 
			 toastr.error('You have errors on the page', 'Basic service referral',{ timeOut: 0, positionClass: "toast-bottom-right" });
		 }

		 
		 
	 });
	 
	 var retriveformvalues = function(formid, message){
		 
		var recordtoadd = {},
		
		tmpservprov = multi_select_to_key_value_pair("#services_provided option:selected"),
		tmprefsmade = multi_select_to_key_value_pair("#referrals_made option:selected"),
		tmprefscomp = multi_select_to_key_value_pair("#referrals_completed option:selected"),
		tmpadvconds = multi_select_to_key_value_pair("#adverse_conditions option:selected"),
		
		displayid_firstnamelastname_location = $(".beneficiary_autocomplete").val(),
		displayid_firstnamelastname_location_arry = displayid_firstnamelastname_location.split('\t'),
		
		firstnamelastname_arry = displayid_firstnamelastname_location_arry[1].split(' ');
		
		recordtoadd["workforceid"] = $("#id_workforce_system_id").val(),
			
		recordtoadd["servicesprovided"] = [tmpservprov],
		recordtoadd["referralscompleted"] = [tmprefscomp],
		recordtoadd["referralsmade"] = [tmprefsmade],
		recordtoadd["adverseconditions"] = [tmpadvconds],
		
		recordtoadd["formid"] = formid,
		recordtoadd["message"] = message,
		
		recordtoadd["beneficiaryid"] = $("#beneficiary_id").val(),
		recordtoadd["benfirstname"] = firstnamelastname_arry[0],
		recordtoadd["bensurname"] = firstnamelastname_arry[1],
		recordtoadd["bendisplayid"] = displayid_firstnamelastname_location_arry[0],
		
		recordtoadd["orgunitid"] = $("#id_organisation_unit option:selected").val(),
		recordtoadd["orgunitname"] = $("#id_organisation_unit option:selected").text(),
		recordtoadd["areaid"] = $("#id_encounter_location").val(),
		recordtoadd["areaname"] = $("#id_encounter_location_auto_complete").val(),
		
		recordtoadd["encountertypeid"] = $("#id_encounter_type option:selected").val(),
		recordtoadd["encountertypetext"] = $("#id_encounter_type option:selected").text(),
		recordtoadd["encounterdate"] = $("#id_date_of_service").val();
		
		//console.log(recordtoadd); 
		return recordtoadd;
	 };
	 
	 var clean_errors = function()
	 {
		 $('.help-block').remove();
		 $('div').removeClass("has-error");
		 $("#overall-record-error").hide();
		 $('.all-error').remove();
		 
	 };
	 
	 var insert_errors = function(key, value){
		 var div_prefix = '#div_id_';
		 var control_id_prefix = '#id_';
		 
		 var div_control_selector = div_prefix + key;
		 var control_id_selector = control_id_prefix + key;
		 
		 var error_message = value[0];
		 
		 $(div_control_selector).addClass('has-error');
		 $(control_id_selector)
		 	.parent()
		 	.append('<span id="error_1_id_'+value+'" class="help-block"><strong>'+error_message+'</strong></span>');
		 $("select > #"+key).parent()
		 	.append('<span id="error_1_id_'+value+'" class="help-block"><strong>'+error_message+'</strong></span>');
		 
		 if(key === '__all__'){
			 $("#overall-record-error > ul").append('<li class="ms-hover all-error">'+error_message+'</li>');
		 }
	 };
	  $("#add_record").click(function(e){
		 e.preventDefault();
		 handleAdverseCondVisibility();
		 resetform();
		 $("#btnsave").val("new");
		 $("#add_record").addClass("disabled");
		 $(".servicemain").removeClass("hide");	
		 $(".servicemain").show();
		 $(".readonly-service-main").hide();
		 
		 $('html, body').animate({
			        scrollTop: $(".servicemain").parent().offset().top
			    }, 100);
	 });
	  
	 $("#id_return_home").click(function(e){
		 e.preventDefault();
		 var navigatetourl = window.location.protocol+'//'+window.location.host+'/forms/forms_home';
		 window.location.replace(navigatetourl);
		 
	 });
	 
	 var onloadtorun = function(){
		var onloadformid = $("#onloadformid").val();
		if(onloadformid != "None")
		{
			searchrecords(onloadformid);
		}
	};

	onloadtorun();
	fill_orgs_control();

});