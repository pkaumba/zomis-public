$(document).ready(function () {
	$(".autocompletesubject").autocomplete({
        source: function (request, response) {
            $.getJSON("/forms/subject?search_token=" + $('#id_subject').val() + "&form_type=" + $('#id_form_type').val(), function (data) {
                response($.map(data, function (value, key) {
                    return {
                        label: concatenatearray(value),
                        value: value
                    };

                }));
            });
        },
        minLength: 1,
        delay: 100,
        select: function (event, ui) {
            event.preventDefault();
            $('#id_forms_pk_hidden').val(ui.item.value[0]);
        }
    });
	
    $("#id_add").on('click',function(){
		var subject_required = $('#id_forms_pk_hidden').val() != "" && $('#id_forms_pk_hidden').val() != null;
		var subject_not_required = $("#id_form_type").val() === 'FT2d' || $("#id_form_type").val() === 'FT1e';
    	if(subject_required || subject_not_required)
    	{
    		//http://stackoverflow.com/questions/1865837/whats-the-difference-between-window-location-and-window-location-replace
    		var red_url = "/forms/add_form?subject=" + $('#id_forms_pk_hidden').val() + "&form_type=" + $('#id_form_type').val();
	        //window.location = url;
	    	window.location.replace(red_url);
    	}
    	else
    	{
    		$('input[name="forms_pk_hidden"]')[0].setCustomValidity('Enter a valid subject');
    	}
    });
    
    $("#id_form_type").on('change', function(){
    	var formtype = $("#id_form_type").val();
    	$("#id_add").html('<i class="fa fa-plus-square"> Add</i>');
		//These two forms do not require a subject
		if(formtype === 'FT2d' || formtype === 'FT1e'){
			$("#div_id_community").hide();
		}
		else if(formtype === 'FT3e'){
			$("#id_add").html('<i class="fa fa-folder-open-o"> Open</i>');
			$("#div_id_community").show();
		}
		else{
			$("#div_id_community").show();
		}
    	$("#id_subject").val("");
    	$('#id_forms_pk_hidden').val("");
    	
    	if(formtype == "FT3d" || formtype == "FT3f")
    	{
    		$('.subject-text').text("Child:");
    		resetSubject();
    	}
    	else if(formtype == "FT2d" || formtype == "FT2c" || formtype == "FT3e")
    	{
    		$('.subject-text').text("Workforce member:");
    		$.ajax({
            dataType: "json",
            url: "/forms/subject?check_perms_wfc=" + formtype,
            contentType: "application/json; charset=utf-8",
            success: function (report_params) {
    				if(report_params != null && report_params != "")
    				{
    					var search_perm = get_value_from_dict(report_params,"has_search_perm");
    					if(search_perm == "self")
    					{
    						$("#id_forms_pk_hidden").val(get_value_from_dict(report_params,"wfc_pk"));
    						$('#id_subject').val(get_value_from_dict(report_params,"wfc_text"));
    						$('#id_subject').attr('disabled','disabled');
    					}
    					
    				}
                }
            });
    	}
    	else if(formtype == "FT1f")
    	{
    		$('.subject-text').text("Residential institution:");
    		resetSubject();
    	}
    	else
    	{
    		$('.subject-text').text("Subject:");
    		resetSubject();
    	}
    });
    
    function resetSubject()
    {
    	$('#id_forms_pk_hidden').val('');
    	$('#id_subject').val('');
		$('#id_subject').removeAttr('disabled');
    }
    
    
    function get_value_from_dict(dictToSearch,keyVal)
    {
    	var valueToRet;
    	$.each(dictToSearch, function (key, value) {
        	if(key==keyVal)
        	{
        		valueToRet = value;
        	}
        });
    	return valueToRet;
    }
    
    
    $( "#id_subject" ).on( "autocompleteselect", function( event, ui ) {
        var result = concatenatearray(ui.item.value);
        $( "#id_subject" ).val(result);
    } );
    
    concatenatearray = function(tmparray, startindex, separator)
    {
        var charseparator = separator? separator:'\t';
        var sindex = startindex? startindex:1;
        var result = [];
        for(i=sindex; i < tmparray.length; i++){
            if(tmparray[i] == null || tmparray[i] == '')
                continue;
           result.push(tmparray[i]);
        }
        
        return result.join(charseparator)
    }
    
    $('.deleterow').on('click', function()
	{
		var formid = $(this).attr("value");
		$("#id_confirm_delete").val(formid);
		$("#id_confirm_delete").attr("href","/forms/delete?form_id="+formid);
		$("#id_delete_confirm_dialog_box").click();
	});
    
		
	$(".gw-nextben").click(function () {
        var pagenumber = $(".gw-page").val();
        formpagination(++pagenumber);
    });
    $(".gw-prevben").click(function () {
        var pagenumber = $(".gw-page").val();
        formpagination(--pagenumber);
    });
    formpagination = function (pagenumber)
    {
        $(".totalrecords").empty().append("<i class='fa fa-spinner fa-spin'></i> Searching...");
        $(".searching").show();
        $(".totalrecords").hide();

        $.ajax({
            dataType: "json",
            url: "/beneficiary/formlists?searchterm=" + $("#searchformtxt").val() + "&pagenumber=" + pagenumber + "&itemsperpage=" + $(".gw-pageSizeben").val() + "&formtype=" + $("#id_form_type_search").val() + "&searchdate=" + $("#id_search_date").val(),
            success: function (results) {
                if (results.length == 0) {
                    //$('#benresults tbody').html("<tr><td colspan='9'>No results for search term supplied</td></tr>");
                }
                else {
                    $('#formSearchTable tbody').empty().html("");
                    $.each(results, function (index) {

                        if (results[index].pageinfo == 'pageinfo')
                        {
                            $(".countpages").empty().html(results[index].pagescount);
                            $(".totalrecords").empty().append("Found " + results[index].total_records + " matching form(s)");

                            //set the control number
                            $(".gw-page").val(results[index].pagenumber);
                            if (results[index].pagenumber >= results[index].pagescount)
                            {
                                if (!$(".gw-nextben").hasClass("disabled"))
                                {
                                    $(".gw-nextben").addClass("disabled");
                                }
                                if ($(".gw-prevben").hasClass("disabled"))
                                {
                                    $(".gw-prevben").removeClass("disabled");
                                }

                            }
                            if (results[index].pagescount == 1) {
                                if (!$(".gw-nextben").hasClass("disabled"))
                                {
                                    $(".gw-nextben").addClass("disabled");
                                }
                                if (!$(".gw-prevben").hasClass("disabled"))
                                {
                                    $(".gw-prevben").addClass("disabled");
                                }
                            }
                            if (results[index].pagenumber <= 1) {
                                if (!$(".gw-prevben").hasClass("disabled"))
                                {
                                    $(".gw-prevben").addClass("disabled");
                                }
                            }
                            if (results[index].pagenumber < results[index].pagescount) {
                                if ($(".gw-nextben").hasClass("disabled"))
                                {
                                    $(".gw-nextben").removeClass("disabled");
                                }
                            }
                            if (results[index].pagenumber < results[index].pagescount && results[index].pagenumber > 1) {
                                if ($(".gw-prevbent").hasClass("disabled"))
                                {
                                    $(".gw-prevben").removeClass("disabled");
                                }
                            }

                        }
                        else {
                            var action_html = "";
                            var row_html = "<tr>";
                            
                        	action_html = "";
                        	action_html += "<td>";
                        	action_html += "<span>";
                        	if (results[index].user_can_view == true && results[index].type_id != "FT3d" && results[index].type_id != "FT2c" && results[index].type_id != "FT3f")
                        	{
                        		action_html += "<a href='/forms/add_form/?subject="+results[index].subject_id+"'&form_type='FT3e' &onloadformid='"+results[index].form_id+"' class='btn btn-primary btn-xs viewrow'>";
                    		    if(results[index].type_id != "FT3e")
                    		    {
                    		    	action_html += "<i class='fa fa-edit'></i>&nbsp;View";
                    		    	
                    		    }
            		    		else
            		    		{
                    		    	action_html += "<i class='fa fa-folder-open'></i>&nbsp;Open";
                    		    }
                    			action_html += "</a> ";
                    		}
                    		if(results[index].user_can_edit == true)
                    		{
                    			if(results[index].type_id != "FT3e")
                    			{
                    				action_html += "<a href='/forms/add_form?form_id="+results[index].form_id+"' class='btn btn-primary btn-xs editrow'>";
                    				action_html += "<i class='fa fa-edit'></i>&nbsp;Edit";
                    				action_html += "</a> ";
                    				action_html += "<button value="+results[index].form_id+" class='btn btn-primary btn-xs deleterow'>";
                    				action_html += "<i class='fa fa-edit'></i>&nbsp;Delete";
                    				action_html += "</button>";
                    			}
                    		}
                       		action_html += "</span>";
                    		action_html += "</td>";
                        		
                            $("#formSearchTable > tbody").append(row_html +
                                    "<td>" + results[index].type + "</td> " +
                                    "<td>" + results[index].subject + "</td> " +
                                    "<td>" + results[index].location + "</td> " +
                                    "<td>" + results[index].date + "</td> " +
                                    "<td>" + results[index].end_date + "</td> " +
                                    "<td>" + results[index].date_last_updated + "</td> " +
                                    "<td>" + results[index].person_last_updated + "</td> " +
                                    action_html
                                    +"</tr>"
                                    );
                        }
                    });
                }
            },
            error: function () {
                $('#formSearchTable tbody').html("<tr><td span='8'>No results for search term supplied</td></tr>");
            }
        });
        $(".searching").fadeOut('slow', function () {
            $(".totalrecords").fadeIn('slow');
        });
    };
    $(".gw-pageSizeben").change(function () {
        $(".gw-pageSizeben").val($(".gw-pageSizeben").val());
        formpagination(1);
    });
    $("#searchbtnform").click(function () {
        formpagination(1);
    });

    $("#searchformtxt").keypress(function (event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            formpagination(1);
            return false;
        }
    });
            
    /**
	checkWorkforcePermission();
	function checkWorkforcePermission(){
        $.ajax({
                dataType: "json",
                url: "/forms/get_wfc_object?get_perms='True'",
                contentType: "application/json; charset=utf-8",
                async: false,
                success: function (perms) {
                	var has_edit_perm = false;
                    $.each(perms, function (key, val) {
                    	if(key != -1)
                    	{
                    		if(val == "auth.edit frm low sensitive own" || val == "auth.edit form sensitive own" || val == "auth.edit frm l sense all in org"
                    		   || val == "auth.edit frm h sense all in org"
                    				)
                    		{
                    			has_edit_perm = true;
                    		}
                        }
                    });
                    
                    if(!has_edit_perm)
                    {
                    	$('.editrow').hide();
                    }
                    else
                    {
                    	$('.editrow').show();
                    }
                    
                }
        });
	}**/
});