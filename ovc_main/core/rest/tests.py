'''
This class contains tests for download/upload functionality
'''
from django.test import TestCase
from ovc_main.models import *
from ovc_auth.models import *
from ovc_main.core.rest import download_upload_settings as dw_up_config
from serializers import *

class DownloadUploadSectionsCase(TestCase):
    def setUp(self):
        pass
        
    def test_download_section_ids_are_valid(self):
        """Test that all sections in 'download_upload_settings.download_sections' are valid"""
        for key, value in dw_up_config.download_sections.items():              
            self.assertTrue(key in dw_up_config.download_section_ids)
    
    def test_download_model_serialiser_names_are_valid(self):
        """Test that all models and serialisers in 'download_upload_settings.download_sections' are valid"""
        for key, value in dw_up_config.download_sections.items():              
            models = value['models']
            for model_info in models:

                ''' Testing the models '''
                model_name = model_info['model']
                if model_name == '':
                    ''' This section tests the custom serilisers '''
                else:
                    ''' This section tests the django serilisers '''
                    model_class = globals()[model_name]                    
                    obj1 = model_class.objects.all()
                    self.assertTrue(obj1 is not None, 'Cannot create model - ' + model_name)
                    ''' Testing the serialisers '''
                    serialiser_name = model_info['serializer']
                    serialiser_class = globals()[serialiser_name]                    
                    obj2 = serialiser_class()
                    self.assertTrue(obj2 is not None, 'Cannot create serialiser - ' + serialiser_name)
                
                    obj3 = serialiser_class(obj1, many=True)
                    self.assertTrue(obj3 is not None, 'Objects cannot be serialised with - ' + serialiser_name)
                
                
    def test_upload_model_serialiser_names_are_valid(self):
        """Test that all models and serialisers in 'download_upload_settings.upload_info' are valid"""
        for model_info in dw_up_config.upload_info:
            ''' Testing the models '''
            model_name = model_info['model']
            model_class = globals()[model_name]                    
            obj1 = model_class.objects.all()
            self.assertTrue(obj1 is not None, 'Cannot create model - '+model_name)
            ''' Testing the serialisers '''
            serialiser_name = model_info['serializer']
            serialiser_class = globals()[serialiser_name]                    
            obj2 = serialiser_class()
            self.assertTrue(obj2 is not None, 'Cannot create serialiser - ' + serialiser_name)  
            
            obj3 = serialiser_class(obj1, many=True)
            self.assertTrue(obj3 is not None, 'Objects cannot be serialised with - ' + serialiser_name)
            ''' Testing the parent models '''
            parent_model_name = model_info['parent_model']
            parent_serialiser_class = globals()[parent_model_name]                    
            obj3 = parent_serialiser_class()
            self.assertTrue(obj3 is not None, 'Cannot create model - '+model_name)
            
class SerialisersFieldNamesCase(TestCase):
    def setUp(self):
        pass
        
    def test_person_serialiser_fields(self):
        """ Test that the Person serialiser fields are in the model fields """
        for field_name in RegPersonSerializer.Meta.fields:
            self.assertTrue(field_name in RegPerson.__doc__,'RegPersonSerializer field name ('+ \
                field_name +') is not in RegPerson model. This field may have been removed or renamed.')
            
    def test_person_geo_serialiser_fields(self):
        """ Test that the Person Geo serialiser fields are in the model fields """
        for field_name in RegPersonsGeoSerializer.Meta.fields:
            self.assertTrue(field_name in RegPersonsGeo.__doc__,'RegPersonsGeoSerializer field name ('\
                + field_name +') is not in RegPersonsGeo model. This field may have been removed or renamed.')
   
    def test_person_gdclsu_serialiser_fields(self):
        """ Test that the Person Gdclsu serialiser fields are in the model fields """
        for field_name in RegPersonsGdclsuSerializer.Meta.fields:
            self.assertTrue(field_name in RegPersonsGdclsu.__doc__,'RegPersonsGdclsuSerializer field name ('\
                + field_name +') is not in RegPersonsGdclsu model. This field may have been removed or renamed.')
   
    def test_person_orgunit_serialiser_fields(self):
        """ Test that the Person Gdclsu serialiser fields are in the model fields """
        for field_name in RegPersonsOrgUnitsSerializer.Meta.fields:
            self.assertTrue(field_name in RegPersonsOrgUnits.__doc__,'RegPersonsOrgUnitsSerializer field name ('\
                + field_name +') is not in RegPersonsOrgUnits model. This field may have been removed or renamed.')
   
    def test_person_type_serialiser_fields(self):
        """ Test that the Person type serialiser fields are in the model fields """
        for field_name in RegPersonsTypesSerializer.Meta.fields:
            self.assertTrue(field_name in RegPersonsTypes.__doc__,'RegPersonsTypesSerializer field name ('\
                + field_name +') is not in RegPersonsTypes model. This field may have been removed or renamed.')
    
    def test_orgunit_serialiser_fields(self):
        """ Test that the RegOrg Unit serialiser fields are in the model fields """
        for field_name in RegOrgUnitSerializer.Meta.fields:
            self.assertTrue(field_name in RegOrgUnit.__doc__,'RegOrgUnitSerializer field name ('\
                + field_name +') is not in RegOrgUnit model. This field may have been removed or renamed.')
                                             
    def test_orgunit_geo_serialiser_fields(self):
        """ Test that the RegOrg Unit Geo serialiser fields are in the model fields """
        for field_name in RegOrgUnitGeoSerializer.Meta.fields:
            self.assertTrue(field_name in RegOrgUnitGeography.__doc__,'RegOrgUnitGeoSerializer field name ('\
                + field_name +') is not in RegOrgUnitGeography model. This field may have been removed or renamed.')
                                             
    def test_setup_lists_serialiser_fields(self):
        """ Test that the Setup list serialiser fields are in the model fields """
        for field_name in SetupListSerializer.Meta.fields:
            self.assertTrue(field_name in SetupList.__doc__,'SetupListSerializer field name ('\
                + field_name +') is not in SetupList model. This field may have been removed or renamed.')
                                      
    def test_setup_lists_geo_serialiser_fields(self):
        """ Test that the Setup Geography list serialiser fields are in the model fields """
        for field_name in SetupGeoSerializer.Meta.fields:
            self.assertTrue(field_name in SetupGeorgraphy.__doc__,'SetupGeoSerializer field name ('\
                + field_name +') is not in SetupGeorgraphy model. This field may have been removed or renamed.')
                           
    def test_app_user_serialiser_fields(self):
        """ Test that the AppUser serialiser fields are in the model fields """
        for field_name in UsersSerializer.Meta.fields:
            self.assertTrue(field_name in AppUser.__doc__,'UsersSerializer field name ('\
                + field_name +') is not in AppUser model. This field may have been removed or renamed.')
                           
    def test_list_questions_serialiser_fields(self):
        """ Test that the ListQuestions serialiser fields are in the model fields """
        for field_name in ListQuestionsSerializer.Meta.fields:
            self.assertTrue(field_name in ListQuestions.__doc__,'ListQuestionsSerializer field name ('\
                + field_name +') is not in ListQuestions model. This field may have been removed or renamed.')
                
    def test_list_answers_serialiser_fields(self):
        """ Test that the ListAnswers serialiser fields are in the model fields """
        for field_name in ListAnswersSerializer.Meta.fields:
            self.assertTrue(field_name in ListAnswers.__doc__,'ListAnswersSerializer field name ('\
                + field_name +') is not in ListAnswers model. This field may have been removed or renamed.')
                
    def test_forms_serialiser_fields(self):
        """ Test that the Forms serialiser fields are in the model fields """
        for field_name in FormsSerializer.Meta.fields:
            self.assertTrue(field_name in Forms.__doc__,'FormsSerializer field name ('\
                + field_name +') is not in Forms model. This field may have been removed or renamed.')

    def test_form_gen_answers_serialiser_fields(self):
        """ Test that the FormGenAnswers serialiser fields are in the model fields """
        for field_name in FormGenAnswersSerializer.Meta.fields:
            self.assertTrue(field_name in FormGenAnswers.__doc__,'FormGenAnswersSerializer field name ('\
                + field_name +') is not in FormGenAnswers model. This field may have been removed or renamed.')

    def test_form_gen_text_serialiser_fields(self):
        """ Test that the FormGenText serialiser fields are in the model fields """
        for field_name in FormGenTextSerializer.Meta.fields:
            self.assertTrue(field_name in FormGenText.__doc__,'FormGenTextSerializer field name ('\
                + field_name +') is not in FormGenText model. This field may have been removed or renamed.')

    def test_form_gen_numeric_serialiser_fields(self):
        """ Test that the FormGenNumeric serialiser fields are in the model fields """
        for field_name in FormGenNumSerializer.Meta.fields:
            self.assertTrue(field_name in FormGenNumeric.__doc__,'FormGenNumSerializer field name ('\
                + field_name +') is not in FormGenNumeric model. This field may have been removed or renamed.')      

    def test_form_csi_serialiser_fields(self):
        """ Test that the FormCsi serialiser fields are in the model fields """
        for field_name in FormCsiSerializer.Meta.fields:
            self.assertTrue(field_name in FormCsi.__doc__,'FormCsiSerializer field name ('\
                + field_name +') is not in FormCsi model. This field may have been removed or renamed.')

    def test_form_person_participation_serialiser_fields(self):
        """ Test that the FormPersonParticipation serialiser fields are in the model fields """
        for field_name in FormPersonPartSerializer.Meta.fields:
            self.assertTrue(field_name in FormPersonParticipation.__doc__,'FormPersonPartSerializer field name ('\
                + field_name +') is not in FormPersonParticipation model. This field may have been removed or renamed.')

    def test_form_community_interventions_serialiser_fields(self):
        """ Test that the FormCommInterventions serialiser fields are in the model fields """
        for field_name in FormCommIntervSerializer.Meta.fields:
            self.assertTrue(field_name in FormCommInterventions.__doc__,'FormCommIntervSerializer field name ('\
                + field_name +') is not in FormCommInterventions model. This field may have been removed or renamed.')
                 
    def test_form_orgunit_contributions_serialiser_fields(self):
        """ Test that the FormOrgUnitContributions serialiser fields are in the model fields """
        for field_name in FormOrgUnitContribSerializer.Meta.fields:
            self.assertTrue(field_name in FormOrgUnitContributions.__doc__,'FormOrgUnitContribSerializer field name ('\
                + field_name +') is not in FormOrgUnitContributions model. This field may have been removed or renamed.')
                 
    def test_form_finance_serialiser_fields(self):
        """ Test that the FormFinance serialiser fields are in the model fields """
        for field_name in FormFinanceSerializer.Meta.fields:
            self.assertTrue(field_name in FormFinance.__doc__,'FormFinanceSerializer field name ('\
                + field_name +') is not in FormFinance model. This field may have been removed or renamed.')
                      
    def test_form_res_children_serialiser_fields(self):
        """ Test that the FormResChildren serialiser fields are in the model fields """
        for field_name in FormResChildSerializer.Meta.fields:
            self.assertTrue(field_name in FormResChildren.__doc__,'FormResChildSerializer field name ('\
                + field_name +') is not in FormResChildren model. This field may have been removed or renamed.')
                      
    def test_form_res_workforce_serialiser_fields(self):
        """ Test that the FormResWorkforce serialiser fields are in the model fields """
        for field_name in FormResWfcSerializer.Meta.fields:
            self.assertTrue(field_name in FormResWorkforce.__doc__,'FormResWfcSerializer field name ('\
                + field_name +') is not in FormResWorkforce model. This field may have been removed or renamed.')
                                 
    def test_roles_detail_serialiser_fields(self):
        """ Test that the OVCRole serialiser fields are in the model fields """
        for field_name in RolesDetailSerializer.Meta.fields:
            self.assertTrue(field_name in OVCRole.__doc__,'RolesDetailSerializer field name ('\
                + field_name +') is not in OVCRole model. This field may have been removed or renamed.')
                                 
    def test_ovc_permission_serialiser_fields(self):
        """ Test that the OVCPermission serialiser fields are in the model fields """
        for field_name in PermsDetailSerializer.Meta.fields:
            self.assertTrue(field_name in OVCPermission.__doc__,'PermsDetailSerializer field name ('\
                + field_name +') is not in OVCPermission model. This field may have been removed or renamed.')
                                                           
    def test_roles_serialiser_fields(self):
        """ Test that the OVCUserRoleGeoOrg serialiser fields are in the model fields """
        for field_name in OVCUserRoleGeoOrgSerialiser.Meta.fields:
            self.assertTrue(field_name in OVCUserRoleGeoOrg.__doc__,'OVCUserRoleGeoOrgSerialiser field name ('\
                + field_name +') is not in OVCUserRoleGeoOrg model. This field may have been removed or renamed.')
                    