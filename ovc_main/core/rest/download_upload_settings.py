'''
    @lyokog This file should always be up to date with the most recent model names
    the corresponding serialisers in order for the download/upload functionality
    to work.
'''
import ovc_main.utils.lookup_field_dictionary as fields

download_section_ids = ['all', 'DSPR', 'DSOR', 'DSPA', 'DSSS', 'DSLT']
special_tables = ['role_perms','user_roles']

'''
This lists the different download sections and the associated ids, models and serilisers
'''       
download_sections = {
    'all': {
        'precedence':-1,
        'title':'All sections', 
        'models': []
        },
    'DSPR': {
        'title':'Person registry', 
        'precedence':4,
        'models': [
            {'id':'persons', 'model':'RegPerson','serializer':'RegPersonSerializer'},
            {'id':'persons_geo', 'model':'RegPersonsGeo','serializer':'RegPersonsGeoSerializer'},
            {'id':'persons_gdclsu', 'model':'RegPersonsGdclsu','serializer':'RegPersonsGdclsuSerializer'},
            {'id':'persons_orgs', 'model':'RegPersonsOrgUnits','serializer':'RegPersonsOrgUnitsSerializer'},
            {'id':'persons_cat', 'model':'RegPersonsTypes','serializer':'RegPersonsTypesSerializer'}
            ]
        },
    'DSOR': {
        'title': 'Organisational registry', 
        'precedence':3,
        'models': [
            {'id':'org_unit', 'model':'RegOrgUnit','serializer':'RegOrgUnitSerializer'},
            {'id':'org_unit_geo', 'model':'RegOrgUnitGeography','serializer':'RegOrgUnitGeoSerializer'}
            ]
        },
    'DSPA': {
        'title': "Person access", 
        'precedence':5,
        'models': [
            #''' Used for tables auth_user, auth_user_groups and auth_user_groups_geo_org respectively '''
            {'id':'users', 'model':'AppUser','serializer':'UsersSerializer'},
            {'id':'user_roles', 'model':'','serializer':'UserRolesSerialiser'},                          
            {'id':'user_role_geo_org', 'model':'OVCUserRoleGeoOrg','serializer':'OVCUserRoleGeoOrgSerialiser'}
            ]
        },
    'DSSS': {
        'title': 'Security setup', 
        'precedence':2,
        'models': [            
            {'id':'roles', 'model':'Group','serializer':'RolesSerializer'},
            {'id':'roles_detail', 'model':'OVCRole','serializer':'RolesDetailSerializer'},
            
            {'id':'perms', 'model':'Permission','serializer':'PermsSerializer'},
            {'id':'perms_detail', 'model':'OVCPermission','serializer':'PermsDetailSerializer'},

            {'id':'role_perms', 'model':'','serializer':'RolePermissionsSerialiser'}, 
            ]
        }, 
    'DSLT': {
        'title':'Lists', 
        'precedence':1,
        'models': [
            {'id':'list_gen', 'model':'SetupList','serializer':'SetupListSerializer'},
            {'id':'list_geo', 'model':'SetupGeorgraphy','serializer':'SetupGeoSerializer'},
            {'id':'list_questions', 'model':'ListQuestions','serializer':'ListQuestionsSerializer'},
            {'id':'list_answers', 'model':'ListAnswers','serializer':'ListAnswersSerializer'},
            ]
        }
    }

'''
This sections contains the id, models, serialisers, parent model and precedence of the tables to upload.
Precedence only helps us upload the Forms before any depending form_abc tables. 
1 is reserved for Forms, any Forms child table can have any precedence
'''
upload_info = [
        {'id':'fm_main', 'model':'Forms','serializer':'FormsSerializer','parent_model':'Forms', 'precedence':1},
        {'id':'fm_gen_ans', 'model':'FormGenAnswers','serializer':'FormGenAnswersSerializer','parent_model':'Forms', 'precedence':2},
        {'id':'fm_gen_txt', 'model':'FormGenText','serializer':'FormGenTextSerializer','parent_model':'Forms', 'precedence':3},
        {'id':'fm_gen_num', 'model':'FormGenNumeric','serializer':'FormGenNumSerializer','parent_model':'Forms', 'precedence':4},
        {'id':'fm_csi', 'model':'FormCsi','serializer':'FormCsiSerializer','parent_model':'Forms', 'precedence':5},
        {'id':'fm_psn_part', 'model':'FormPersonParticipation','serializer':'FormPersonPartSerializer','parent_model':'Forms', 'precedence':6},
        #{'id':'fm_com_int', 'model':'FormCommInterventions','serializer':'FormCommIntervSerializer','parent_model':'Forms', 'precedence':7},
        {'id':'fm_org_cont', 'model':'FormOrgUnitContributions','serializer':'FormOrgUnitContribSerializer','parent_model':'Forms', 'precedence':8},
        #{'id':'fm_fin', 'model':'FormFinance','serializer':'FormFinanceSerializer','parent_model':'Forms', 'precedence':9},
        {'id':'fm_res_chdrn', 'model':'FormResChildren','serializer':'FormResChildSerializer','parent_model':'Forms', 'precedence':10},
        {'id':'fm_res_wfc', 'model':'FormResWorkforce','serializer':'FormResWfcSerializer','parent_model':'Forms', 'precedence':11},
        {'id':'fm_core_adverse_conds', 'model':'CoreAdverseConditions','serializer':'CoreAdverseConditionsSerializer','parent_model':'Forms', 'precedence':12},
        {'id':'fm_core_encounters', 'model':'CoreEncounters','serializer':'CoreEncountersSerializer','parent_model':'Forms', 'precedence':13},
        {'id':'fm_core_services', 'model':'CoreServices','serializer':'CoreServicesSerializer','parent_model':'CoreEncounters', 'precedence':14}
    ]
    


