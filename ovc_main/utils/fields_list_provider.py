from ovc_main.models.audit_trail import RegPersonsAuditTrail, RegPersonsAuditTrail
from ovc_main.models import SetupList,RegPersonsGeo, SetupGeorgraphy, RegOrgUnit,\
        RegPerson, RegPersonsTypes, RegOrgUnitGeography, RegOrgUnitGdclsu, RegPersonsContact, \
        RegPersonsExternalIds, ListAnswers, AdminCaptureSites, CoreAdverseConditions, ListReports, ListReportsParameters, \
        CoreEncountersNotes, CoreEncounters, ReportsSets, ReportsSetsOrgUnits, Forms, CoreServices, ListQuestions, FormResChildren

from django.db.models import Q
import operator 
from ovc_main.utils.geo_location import get_communities_in_ward, get_geo_offsprings_by_id
from ovc_main.utils.general import list_has_key
from ovc_main.utils import lookup_field_dictionary as fdict, report_sql
from datetime import date
from django.db import connection
from ovc_auth.models import AppUser
from django.conf import settings as config
from collections import OrderedDict
from django.db.models import Max, Min
import datetime

initial_drop_item = [('','-----')]
alt_initial_drop_item = [('','All workforce/user types')]


#===========================Workforce Methods================================
def get_list_of_organisations(orgname,user=None, search_ids = False): 
    '''
    Returns the list of organisations by name or ID
    '''
    to_return = ''
    q_list = []
    if orgname:
        q_list.append(Q(org_unit_name__icontains=orgname.lower()))
        q_list.append(Q(org_unit_id_vis__icontains=orgname))
    if search_ids:
        to_return = tuple([(l.org_unit_id_vis, l.org_unit_name) for l in RegOrgUnit.objects.filter(reduce(operator.or_, q_list),is_void=False,date_closed=None)])
    else:
        to_return = tuple([(l.org_unit_id_vis, l.org_unit_name) for l in RegOrgUnit.objects.filter(org_unit_name__icontains=orgname,is_void=False,date_closed=None)])
    if user:
        if user.is_superuser and search_ids:
            to_return = tuple([(l.org_unit_id_vis, l.org_unit_name) for l in RegOrgUnit.objects.filter(reduce(operator.or_, q_list),is_void=False,date_closed=None)])  
        elif user.is_superuser and not search_ids:
            to_return = tuple([(l.org_unit_id_vis, l.org_unit_name) for l in RegOrgUnit.objects.filter(org_unit_name__icontains=orgname,is_void=False,date_closed=None)]) 
    return to_return

def get_list_of_organisations_by_ids(ids=[], user=None):
    if user and user.is_superuser:
        to_return =  tuple([(l.org_unit_id_vis, l.org_unit_name) for l in RegOrgUnit.objects.filter(is_void=False,date_closed=None)])
    else:
        to_return = tuple([(l.org_unit_id_vis, l.org_unit_name) for l in RegOrgUnit.objects.filter(id__in=ids,is_void=False,date_closed=None)])
    print 'our list', to_return
    return to_return

def get_list_of_workforce(workforce_name,user=None, workforce_only=False): 
    ''' This method only returns the list of all workforce members/users '''
    to_return = []
    q_list = []
    token = workforce_name
    if token:
        q_list.append(Q(first_name__icontains=token))
        q_list.append(Q(surname__icontains=token.lower()))
        q_list.append(Q(other_names__icontains=token.lower()))
        q_list.append(Q(workforce_id__icontains=token))
        q_list.append(Q(national_id__icontains=token))
        
        if workforce_only:
            workforce_users = RegPerson.objects.filter(reduce(operator.or_, q_list), regpersonstypes__person_type_id__in = get_Workforce_Type_as_list(), is_void=False)
            for wfc in workforce_users:
                if wfc.workforce_id:
                    to_return.append((wfc.pk, wfc.workforce_id, wfc.national_id, wfc.first_name + ' ' + wfc.surname, False))
            to_return = tuple(to_return)
        elif not workforce_only:
            to_return = tuple([(wfc.pk, wfc.workforce_id, wfc.national_id, wfc.first_name + ' ' + wfc.surname, False) for wfc in \
                                    RegPerson.objects.filter(reduce(operator.or_, q_list), regpersonstypes__person_type_id__in = get_Workforce_Type_as_list(), is_void=False)])
        

    return to_return

def get_sex_list(sex=None):
    return tuple(initial_drop_item + [(l.item_id, l.item_description) for l in SetupList.objects.filter(item_category=fdict.sex)])


def get_list_of_province_districts():
    optionstring = ''
    province_district = [(location.area_id, SetupGeorgraphy.objects.get(area_id=location.parent_area_id).area_name+'-%s' % location.area_name)for location in SetupGeorgraphy.objects.filter(area_type_id=fdict.geo_type_district_code)]
    
    for prov_dist in province_district:
        optionstring += '<option value="%s">%s</option>\n' % (prov_dist[0], prov_dist[1])   
    return optionstring

def get_Workforce_Type(search=None):
    workforce_type = None
    if search == None:
        workforce_type = initial_drop_item 
    else:
        workforce_type = alt_initial_drop_item
    return tuple(workforce_type + [(l.item_id, l.item_description) for l in SetupList.objects.filter(item_category= fdict.workforce_type)])

def get_Workforce_Type_as_list():
    all_wfc_types = []
    all_wfc_type_tpl=get_Workforce_Type(search=True)
    for kvp in all_wfc_type_tpl:
        if len(kvp) > 1:
            if kvp[0] == '':
                continue
            all_wfc_types.append(kvp[0])
    return all_wfc_types

def get_YesNo_Enums():
    '''
    Returns a tuple for Yes and No values with the_order and description. Going forward please use get_item_id_and_description_for_cat() instead especially if you will save the 
    value of the field
    '''
    choices = []
    return tuple(choices + [(l.the_order, l.item_description) for l in SetupList.objects.filter(item_category=fdict.gen_YesNo)])

def get_item_id_and_description_for_cat(add_initial = False, item_category = None,get_short_desc=False):
    '''
    This method should be used for list general lookups 
    Returns a tuple for a given category as item_id and description pairs.
    '''
    choices = []
    if add_initial:
        choices = initial_drop_item 
    return tuple(choices + [(l.item_id, l.item_description_short if get_short_desc else l.item_description) for l in SetupList.objects.filter(item_category = item_category)])   
    
    
def get_yes_no_index(yes_no_value):
    return tuple([(l.the_order) for l in SetupList.objects.filter(item_category=fdict.gen_YesNo, item_description = yes_no_value)])

def get_the_order_index_for_item(item_id, categoty):
    return tuple([(l.the_order) for l in SetupList.objects.filter(item_category=categoty, item_id = item_id)])

def get_description_for_item_id(item_id):
    return tuple([(l.item_description) for l in SetupList.objects.filter(item_id = item_id)])

def get_item_desc_for_order_and_category(the_order,category):
    return tuple([(l.item_description) for l in SetupList.objects.filter(the_order = the_order, item_category = category)])



#==============================End Workforce Methods===========================

def get_most_recent_record_date_for_person_id(person_id):
    recent_date = None
    persons = RegPerson.objects.filter(pk=person_id, is_void=False)
    reg_p = None
    for person in persons:
        reg_p = person
    if reg_p:
        audit_trails = RegPersonsAuditTrail.objects.filter(person_id=reg_p.pk)
        for audit_trail in audit_trails:
            if recent_date:
                if audit_trail.timestamp_modified:
                    if recent_date < audit_trail.timestamp_modified:
                        recent_date = audit_trail.timestamp_modified
            elif audit_trail.timestamp_modified:
                recent_date = audit_trail.timestamp_modified
    
    return recent_date

def get_org_list():
     return tuple(initial_drop_item + [(l.pk, l.org_unit_name) for l in RegOrgUnit.objects.all()])

def get_setup_field_id_from_description(lookuptext):
    return SetupList.objects.get(item_description=lookuptext).item_id

def get_setup_field_description_from_item_id(lookup_item_id):
    item_description = None
    try:
         item_description = SetupList.objects.get(item_id=lookup_item_id).item_description
    except Exception as e:
        print e
    return item_description


def get_list_of_organisation_types():
    org_drop_list = [('','All Types')]
    return tuple(org_drop_list + [(l.item_id, l.item_description) for l in SetupList.objects.filter(item_category='Organisational unit type')])

#===========================Beneficiary methods===================================
def get_ben_id_from_pk(person_id):
    return RegPerson.objects.get(pk=person_id).beneficiary_id

def adverse_conditions():
    adverse_conds = {l.item_id:l.item_description for l in SetupList.objects.filter(item_category=fdict.adverse_condition)}
    return adverse_conds

def get_list_adverse_cond(prefixid=False):
    adverse_cond = None
    choices = []
    prefix = ''
    try:
        if prefixid:
            adverse_cond = tuple(choices + [(l.item_id, l.item_id+'-'+l.item_description) for l in SetupList.objects.filter(item_category=fdict.adverse_condition).order_by('the_order')])
        else:
            adverse_cond = tuple(choices + [(l.item_id, l.item_description) for l in SetupList.objects.filter(item_category=fdict.adverse_condition).order_by('the_order')])
    except Exception as e:
        print e
        raise Exception('Setup list query failed to load adverse conditions')
    return adverse_cond

def get_list_of_residentials(res_search_key):
    print 'loading residential institutions'
    res_institutions = None
    toreturn = {}
    
    try:
        search_condition = []
        search_condition.append(Q(org_unit_name__icontains=res_search_key))
        search_condition.append(Q(org_unit_id_vis__icontains=res_search_key))
        
        res_institutions = RegOrgUnit.objects.filter(reduce(operator.or_, search_condition), org_unit_type_id = fdict.org_unit_type_res_Institution, is_void = False)
                    
        district_added = False
        ward_added = False
        cwac_added = False
        
        for res in res_institutions:
            org_area = RegOrgUnitGeography.objects.filter(org_unit=res.pk, date_delinked=None)
            
            for org_a in org_area:
                #if not district_added:
                if (fdict.geo_type_district_code == SetupGeorgraphy.objects.get(area_id=org_a.area_id).area_type_id):
                    if toreturn.has_key(res.pk):
                        toreturn[res.pk][1].append(org_a.area_id)
                        district_added = True
                    else:
                        toreturn[res.pk]=[res.org_unit_id_vis + ' ' + res.org_unit_name,[res.pk]]
                        toreturn[res.pk][1].append(org_a.area_id)
                        district_added = True
            
            for org_a in org_area:
                #if not ward_added:
                if (fdict.geo_type_ward_code == SetupGeorgraphy.objects.get(area_id=org_a.area_id).area_type_id): 
                    if toreturn.has_key(res.pk):
                        toreturn[res.pk][1].append(org_a.area_id)
                        ward_added = True
                    else:
                        toreturn[res.pk]=[res.org_unit_id_vis + ' ' + res.org_unit_name,[res.pk]]
                        toreturn[res.pk][1].append(org_a.area_id)
                        ward_added = True
            
            glu = None
            gdclsu = RegOrgUnitGdclsu.objects.filter(org_unit=res, is_void=False)
            for g in gdclsu:
                glu = g
            
            if glu:    
                org_reg = RegOrgUnit.objects.filter(pk=glu.gdclsu_id,is_void=False,is_gdclsu=True)
                            
                for org_a in org_reg:
                    #if not cwac_added:
                    if org_a.is_active:
                        if toreturn.has_key(res.pk):
                            toreturn[res.pk][1].append(int(org_a.pk))
                            cwac_added = True
                        else:
                            toreturn[res.pk]=[res.org_unit_id_vis + ' ' + res.org_unit_name,[res.pk]]
                            toreturn[res.pk][1].append(org_a.pk)
                            cwac_added = True
                    
    except Exception as e:
        print e
        raise Exception('Setup list query failed to load residential institutions')
    
    return toreturn

def get_location_from_unit_id(unit_id):
    search_condition = []
    tuple(initial_drop_item + [(area_id, area_name) for location in SetupGeorgraphy.objects.filter(parent_area_id=area_id)])
    
    
def get_org_geo_from_id(org_id):
    return RegOrgUnitGeography.objects.filter(org_unit=org_id)
    
def get_gdclsu_for_org(org_id):
    return RegOrgUnitGdclsu.objects.get(org_unit=org_id)   

def get_org_from_id(res_id):
    return RegOrgUnit.objects.get(org_unit_id_vis = res_id)

def get_item_desc_from_id(item_id):
    return SetupList.objects.get(item_id = item_id).item_description

def get_person_id(pers_id, get_string_id=False):
    to_return_ben = None
    if get_string_id:
        if RegPerson.objects.filter(pk=pers_id,is_void=False).count() == 1:
            to_return_ben = RegPerson.objects.get(pk=pers_id,is_void=False).beneficiary_id
        else:
            print 'something went wrong when loading beneficiaries in fields_list_provider.get_pk_id_from_beneficiary_id() method, found more than one beneficiary'         
    else:
        if RegPerson.objects.filter(beneficiary_id=pers_id,is_void=False).count() == 1:
            to_return_ben = RegPerson.objects.get(beneficiary_id=pers_id,is_void=False).pk
        else:
            print 'something went wrong when loading beneficiaries in fields_list_provider.get_pk_id_from_beneficiary_id() method, found more than one beneficiary'         
            
    return to_return_ben

def get_list_of_beneficiaries(beneficiary, person_type, get_wfc_only = False, wfc_type=None): 
    print 'loading guardians to control'   
    vals = None
    try: 
        search_condition = []
        search_condition.append(Q(first_name__icontains=beneficiary))
        search_condition.append(Q(surname__icontains=beneficiary))
        search_condition.append(Q(other_names__icontains=beneficiary))
        search_condition.append(Q(beneficiary_id__icontains=beneficiary))
        search_condition.append(Q(national_id__icontains=beneficiary)) 
        search_condition.append(Q(birth_reg_id__icontains=beneficiary)) 
        search_condition.append(Q(workforce_id__icontains=beneficiary))
        
        search_token = None
        if(person_type == 'guardian'):
            vals = tuple([(l.beneficiary_id,l.national_id, l.full_name) for l in RegPerson.objects.filter(reduce(operator.or_, search_condition), regpersonstypes__person_type_id__contains=fdict.child_guardian_guardian,date_of_death = None, is_void=False,regpersonstypes__is_void=False)]) 
        elif(person_type == 'workforce'):
            search_workforce = []
            if wfc_type:
                search_workforce.append(Q(regpersonstypes__person_type_id__contains=wfc_type))
            else:
                search_workforce.append(Q(regpersonstypes__person_type_id__contains=fdict.workforce_type_ngo_id))
                search_workforce.append(Q(regpersonstypes__person_type_id__contains=fdict.workforce_type_gov_id))
                search_workforce.append(Q(regpersonstypes__person_type_id__contains=fdict.workforce_type_vol_id))
            
            
            if get_wfc_only:
                q_list_execlude = []
                q_list_execlude.append(Q(workforce_id=None))
                q_list_execlude.append(Q(workforce_id=''))
                vals = tuple([(l.pk,l.workforce_id,l.national_id,l.full_name) for l in RegPerson.objects.filter(reduce(operator.or_, search_condition), reduce(operator.or_, search_workforce), is_void=False,regpersonstypes__is_void=False).exclude(reduce(operator.or_, q_list_execlude))])
                print 'searched wfc',vals
            else:
                vals = tuple([(l.pk,l.workforce_id,l.national_id,l.full_name) for l in RegPerson.objects.filter(reduce(operator.or_, search_condition), reduce(operator.or_, search_workforce),date_of_death = None, is_void=False,regpersonstypes__is_void=False)])
        else:
            return None
        
    except Exception as e:
        print e
        raise Exception('Couldnt load beneficiaries')
    
    return vals

def get_wfc_n_id_full_name_int(wfc_id):
    q_list_execlude = []
    q_list_execlude.append(Q(workforce_id=None))
    q_list_execlude.append(Q(workforce_id=''))
    wfc_objects = RegPerson.objects.filter(pk=wfc_id, is_void=False).exclude(reduce(operator.or_, q_list_execlude))
    if wfc_objects:
        print 'count obj',wfc_objects.count()
        if wfc_objects.count() == 1:
            wfc_obj = []
            wfc_obj.append(wfc_objects.get(pk=wfc_id).workforce_id)
            wfc_obj.append(wfc_objects.get(pk=wfc_id).national_id)
            wfc_obj.append(wfc_objects.get(pk=wfc_id).full_name)
            return wfc_obj
    return None

def get_wfc_n_id_full_name(wfc_id):
    q_list_execlude = []
    q_list_execlude.append(Q(workforce_id=None))
    q_list_execlude.append(Q(workforce_id=''))
    wfc_objects = RegPerson.objects.filter(workforce_id=wfc_id, is_void=False).exclude(reduce(operator.or_, q_list_execlude))
    if wfc_objects:
        print 'count obj',wfc_objects.count()
        if wfc_objects.count() == 1:
            wfc_obj = []
            wfc_obj.append(wfc_objects.get(workforce_id=wfc_id).workforce_id)
            wfc_obj.append(wfc_objects.get(workforce_id=wfc_id).national_id)
            wfc_obj.append(wfc_objects.get(workforce_id=wfc_id).full_name)
            return wfc_obj
    return None

def get_guardian_contacts(grd_id):
    
    guard_contacts = {}
    
    reg_person = RegPerson.objects.filter(beneficiary_id = grd_id, is_void=False)
    g_person = None
    for reg_p in reg_person:
        g_person = reg_p
    
    if g_person:
        reg_contacts = RegPersonsContact.objects.filter(person=g_person,is_void=False)
        
        for reg_contact in reg_contacts:
            contact_type = SetupList.objects.get(item_id=reg_contact.contact_detail_type_id).item_description
            guard_contacts[contact_type] = reg_contact.contact_detail

    return guard_contacts

def get_person_from_wfc_id(wfc_id):
    if RegPerson.objects.filter(workforce_id=wfc_id, is_void=False).count()>0:
        reg_person = RegPerson.objects.get(workforce_id=wfc_id, is_void=False)
    
    return reg_person

def get_person_type(person_id):
    reg_person = RegPerson.objects.get(pk=person_id)
    pers_type = None
    if RegPersonsTypes.objects.filter(person=reg_person,is_void=False).count()>0:
        pers_type=RegPersonsTypes.objects.get(person=reg_person,is_void=False)
        if SetupList.objects.filter(item_id=pers_type.person_type_id).count()>0:
            setup_list = SetupList.objects.get(item_id=pers_type.person_type_id)
            pers_type = setup_list.item_description
    
    return pers_type

def search_ben_ovc(token, user, id_perm_constraint=True, search_by_location=False, person_type = fdict.child_beneficiary_ovc):
    search_by_id_only = False
    if id_perm_constraint:
        if not user.has_perm('auth.ben lookup'):
            search_by_id_only = True
    return get_beneficiaries(token, person_type , search_by_id_only, search_by_location=search_by_location, search_dead_person = True)

def get_beneficiaries(ben_token, person_type, search_by_id_only=False, search_by_location = True, search_dead_person = False):
    '''
    Search for a beneficiary using a search token - ben_token and their type - person_type
    When search_by_id_only = True, the user can only search by Ids
    '''
    vals = []
    try:
        search_condition = []
        if search_by_id_only:
            if ben_token:
                for token in ben_token.split():
                    search_condition.append(Q(beneficiary_id__icontains=token))
                    search_condition.append(Q(national_id__icontains=token)) 
                    search_condition.append(Q(birth_reg_id__icontains=token))
        else:
            if ben_token:
                for token in ben_token.split():
                    search_condition.append(Q(first_name__icontains=token))
                    search_condition.append(Q(surname__icontains=token))
                    search_condition.append(Q(other_names__icontains=token))
                    search_condition.append(Q(beneficiary_id__icontains=token))
                    search_condition.append(Q(national_id__icontains=token)) 
                    search_condition.append(Q(birth_reg_id__icontains=token))       
        
        search_token = None
        if(person_type == fdict.child_guardian_guardian and ben_token):
            print 'searching Guardian'
            if search_dead_person:
                vals = RegPerson.objects.filter(reduce(operator.or_, search_condition), regpersonstypes__person_type_id=person_type, is_void=False)
            else:
                vals = RegPerson.objects.filter(reduce(operator.or_, search_condition), regpersonstypes__person_type_id=person_type, is_void=False, date_of_death = None)
            loc_results = None
            if search_by_location:
                loc_results = search_location(person_type,ben_token,vals)
            if loc_results:
                vals = loc_results
        elif(person_type == fdict.child_beneficiary_ovc and ben_token):
            print 'searching child'
            if search_dead_person:
                vals = RegPerson.objects.filter(reduce(operator.or_, search_condition), regpersonstypes__person_type_id=person_type, is_void=False)
            else:
                vals = RegPerson.objects.filter(reduce(operator.or_, search_condition), regpersonstypes__person_type_id=person_type, is_void=False, date_of_death = None)
            loc_results = None
            if search_by_location:
                loc_results = search_location(person_type,ben_token,vals)
            if loc_results:
                vals = loc_results
        elif ben_token and not person_type:
            print 'searching token'
            search_condition_2 = []
            search_condition_2.append(Q(regpersonstypes__person_type_id__contains=fdict.child_guardian_guardian))
            search_condition_2.append(Q(regpersonstypes__person_type_id__contains=fdict.child_beneficiary_ovc))
            print ''
            if search_dead_person:
                vals = RegPerson.objects.filter(reduce(operator.or_, search_condition), reduce(operator.or_, search_condition_2), is_void=False)
            else:
                vals = RegPerson.objects.filter(reduce(operator.or_, search_condition), reduce(operator.or_, search_condition_2), is_void=False, date_of_death = None)
            loc_results = None
            if search_by_location:
                loc_results = search_location(person_type,ben_token,vals)
            if loc_results:
                vals = loc_results
        elif person_type and not ben_token:
            print 'searching type'
            if search_dead_person:
                vals = RegPerson.objects.filter(regpersonstypes__person_type_id=person_type, is_void=False)
            else:
                vals = RegPerson.objects.filter(regpersonstypes__person_type_id=person_type, is_void=False, date_of_death = None)
            loc_results = None
            if search_by_location:
                loc_results = search_location(None,ben_token,vals)
            if loc_results:
                vals = loc_results
        else:
            print 'searching all'
            search_condition = []
            search_condition.append(Q(regpersonstypes__person_type_id__contains=fdict.child_guardian_guardian))
            search_condition.append(Q(regpersonstypes__person_type_id__contains=fdict.child_beneficiary_ovc))
            if search_dead_person:
                vals = RegPerson.objects.filter(reduce(operator.or_, search_condition), is_void=False)
            else:
                vals = RegPerson.objects.filter(reduce(operator.or_, search_condition), is_void=False, date_of_death = None)
    except Exception as e:
        print e
        raise Exception('Couldnt load beneficiaries')
        
    return vals 

def search_location(person_type,ben_token,vals):
    list_to_return = []
    for val in vals:
        list_to_return.append(val)
        
    org_ids = search_person_by_location_wrd(ben_token)
    org_ids_g = search_person_by_location_g(ben_token)
    if org_ids or org_ids_g:
        if person_type:
            if RegPerson.objects.filter(regpersonsgdclsu__gdclsu_id__in=org_ids_g,is_void=False,date_of_death=None, regpersonstypes__person_type_id=person_type).count() > 0:
                vals_found = RegPerson.objects.filter(regpersonsgdclsu__gdclsu_id__in=org_ids_g,is_void=False,date_of_death=None, regpersonstypes__person_type_id=person_type)
                for val in vals_found:
                    if val in list_to_return:
                        continue
                    list_to_return.append(val)
            
            if RegPerson.objects.filter(regpersonsgeo__area_id__in=org_ids,is_void=False,date_of_death=None, regpersonstypes__person_type_id=person_type).count()>0:
                vals_found = RegPerson.objects.filter(regpersonsgeo__area_id__in=org_ids,is_void=False,date_of_death=None, regpersonstypes__person_type_id=person_type)
                for val in vals_found:
                    if val in list_to_return:
                        continue
                    list_to_return.append(val)
        else:
            search_condition_loc = []
            search_condition_loc.append(Q(regpersonstypes__person_type_id__contains=fdict.child_guardian_guardian))
            search_condition_loc.append(Q(regpersonstypes__person_type_id__contains=fdict.child_beneficiary_ovc))
            if RegPerson.objects.filter(reduce(operator.or_, search_condition_loc), regpersonsgdclsu__gdclsu_id__in=org_ids_g,is_void=False,date_of_death=None).count() > 0:
                vals_found = RegPerson.objects.filter(reduce(operator.or_, search_condition_loc), regpersonsgdclsu__gdclsu_id__in=org_ids_g,is_void=False,date_of_death=None)
                for val in vals_found:
                    if val in list_to_return:
                        continue
                    list_to_return.append(val)
                    
            if RegPerson.objects.filter(reduce(operator.or_, search_condition_loc), regpersonsgeo__area_id__in=org_ids,is_void=False,date_of_death=None).count()>0:
                vals_found = RegPerson.objects.filter(reduce(operator.or_, search_condition_loc), regpersonsgeo__area_id__in=org_ids,is_void=False,date_of_death=None)
                for val in vals_found:
                    if val in list_to_return:
                        continue
                    list_to_return.append(val)
    return list_to_return

def search_person_by_location_wrd(tokens):
    org_ids = list()
    search_condition = []
    if tokens:
        search_condition.append(Q(area_name__icontains=tokens))
        
        if SetupGeorgraphy.objects.filter(reduce(operator.or_, search_condition)).count() > 0:
                for org in SetupGeorgraphy.objects.filter(reduce(operator.or_, search_condition)):
                    if org.area_id not in org_ids:
                        org_ids.append(org.area_id)
                        children = get_geo_offsprings_by_id(org.pk)
                        for child in children:
                            if child not in org_ids:
                                org_ids.append(child)
        
    return org_ids

def search_person_by_location_g(tokens):
    org_ids = list()
    if tokens:
        search_condition_two = []
        search_condition_two.append(Q(org_unit_name__icontains=tokens))
        search_condition_two.append(Q(org_unit_id_vis__icontains=tokens))
        if RegOrgUnit.objects.filter(reduce(operator.or_, search_condition_two),is_void = False).count() > 0:
                for org in RegOrgUnit.objects.filter(reduce(operator.or_, search_condition_two)):
                    if org.pk not in org_ids:
                        org_ids.append(org.pk)
    
    return org_ids

def get_children_ids(geoid, geoids=[]):
    geoids = [] + geoids
    children_ids = SetupGeorgraphy.objects.filter(parent_area_id=geoid).values_list('area_id', flat=True)
    if children_ids:
        for childid in children_ids:
            if childid in geoids:
                continue
            geoids.append(childid)
            get_children_ids(childid, geoids)
    return geoids   

def get_list_of_districts(first_entry=None):
    if not first_entry:
        first_entry = '-----'
    toret = tuple([('',first_entry)] + [(location.area_id, SetupGeorgraphy.objects.get(area_id=location.parent_area_id).area_name+'-%s' % location.area_name)for location in SetupGeorgraphy.objects.filter(area_type_id=fdict.geo_type_district_code)])
    return sorted(toret, key=lambda tup: tup[1])

def get_person_by_names(pk_id,f_name,l_name):
    to_return = None
    pk_pers=None
    if pk_id:
        pk_pers = int(pk_id)
        
    reg_persons = RegPerson.objects.filter(first_name__iexact=f_name,surname__iexact=l_name,is_void=False)
    reg_pers = None
    for reg_p in reg_persons:
        if reg_p.pk != pk_pers:
            reg_pers = reg_p
            
    if reg_pers:
        to_return = {'nrc':reg_pers.national_id,'name':reg_pers.full_name,'beneficiary_id':reg_pers.beneficiary_id,'workforce_id':reg_pers.workforce_id}
    return to_return

def get_list_of_wards(districtid = None, first_entry = None):
    to_return = None
    if not first_entry:
        first_entry = '-----'
    if districtid:
        to_return = [('',first_entry)]
        constituencies = SetupGeorgraphy.objects.filter(parent_area_id=districtid)
        if constituencies:
            for constituency in constituencies:
                wards = SetupGeorgraphy.objects.filter(area_type_id=fdict.geo_type_ward_code,parent_area_id=constituency.area_id)
                if wards:
                    for ward in wards:
                        to_return.append((ward.area_id, constituency.area_name+'-%s' % ward.area_name))
    else:    
        to_return = tuple([('',first_entry)] + [(location.area_id, SetupGeorgraphy.objects.get(area_id=location.parent_area_id).area_name+'-%s' % location.area_name)for location in SetupGeorgraphy.objects.filter(area_type_id=fdict.geo_type_ward_code)])

    return sorted(to_return, key=lambda tup: tup[1])

def get_list_of_cwacs(wards=None,all_orgs=False):
    to_return = None
    if wards:
        wards = [wardid for wardid, wardname in wards if wardid]
        communities = get_communities_in_ward(wards)
        communitiesfordisplay = [(com_id, '%s-%s' % (com_d_id, comm_name)) for com_id, com_d_id, comm_name in communities]
        return [('','-----')] + communitiesfordisplay
    elif all_orgs:
        to_return = tuple([('','-----')] + [(l.pk, l.org_unit_id_vis +'-%s' % l.org_unit_name)for l in RegOrgUnit.objects.filter(is_void=False)])
    else:
        to_return = tuple([('','-----')] + [(l.pk, l.org_unit_id_vis +'-%s' % l.org_unit_name)for l in RegOrgUnit.objects.filter(is_gdclsu=True, is_void=False)])
       
    return to_return

def get_district_from_child_id(token_area_id):
    not_district = True
    while not_district:
        if SetupGeorgraphy.objects.filter(area_id=token_area_id, area_type_id=fdict.geo_type_district_code).count() > 0:
             if(SetupGeorgraphy.objects.get(area_id=token_area_id, area_type_id=fdict.geo_type_district_code).area_type_id == fdict.geo_type_district_code):
                 not_ = False
                 tmp_area_id = SetupGeorgraphy.objects.get(area_id=token_area_id, area_type_id=fdict.geo_type_district_code)
                 
                 if tmp_area_id.parent_area_id:
                     token_area_id = tmp_area_id.parent_area_id
                 else:
                     break
        else:
            break
    
    try:
        loc = SetupGeorgraphy.objects.get(area_id=token_area_id)
        
        return  SetupGeorgraphy.objects.get(area_id=loc.parent_area_id).area_name+'-%s' % loc.area_name  
    
    except Exception as e:
        print e
        raise Exception('Couldnt load location')

def get_list_of_ben_types():
    return get_list_from_types_category(fdict.beneficiary_category)    
    
def get_list_from_types_category(item_type):
    org_drop_list = [('','All Types')]
    return tuple(org_drop_list + [(l.item_id, l.item_description) for l in SetupList.objects.filter(item_category=item_type)])
        

def get_list_children_from_parent_area_id(area_id):
    consti = []
    toreturn = initial_drop_item
    try:
        consti = SetupGeorgraphy.objects.filter(parent_area_id=area_id, area_type_id = fdict.geo_type_constituency_code )
        
        if consti.count() > 0:
            for cons in consti:
                toreturn.append([(location.area_id, ('%s - %s' + cons.area_name, location.area_name)) for location in SetupGeorgraphy.objects.filter(parent_area_id=cons.area_id, area_type_id = fdict.geo_type_ward_code)])
        
    except Exception as e:
        print e
        raise Exception('Loading list of wards failed')
 
    return toreturn

def province_district_control_list():
    optionstring = ''
    province_district = [(location.area_id, SetupGeorgraphy.objects.get(area_id=location.parent_area_id).area_name+'-%s' % location.area_name)for location in SetupGeorgraphy.objects.filter(area_type_id=fdict.geo_type_district_code)]
    
    for prov_dist in province_district:
        optionstring += '<option value="%s">%s</option>\n' % (prov_dist[0], prov_dist[1])
        
    return optionstring


def build_geo_control_option_list(selected_location, location, parent, setdisabled=False):
    optionstring = ''
    if selected_location and location.area_id in selected_location:
        optionstring = '<option value="%s" selected>%s-%s</option>\n' % (location.area_id, parent, location.area_name)
    else:
        if setdisabled and selected_location:
            optionstring = '<option value="%s" disabled>%s-%s</option>\n' % (location.area_id, parent, location.area_name)
        else:
            optionstring = '<option value="%s">%s-%s</option>\n' % (location.area_id, parent, location.area_name)
    return optionstring

def geo_control_select_list(geotype, selected_location=None, parentfilter=None, grandparentfilter=None,setdisabled=True):
    optionstring = ''
    geolisthasparent = False
    if SetupGeorgraphy.objects.filter(area_type_id=geotype).count() > 0:
        geolisthasparent = True
        
    if parentfilter:
        for location in SetupGeorgraphy.objects.filter(area_type_id=geotype, parent_area_id__in=parentfilter):
            parent  = '%s-' % SetupGeorgraphy.objects.get(area_id=location.parent_area_id).area_name
            optionstring += build_geo_control_option_list(selected_location, location, parent, setdisabled)
    elif grandparentfilter:
        parents =  [parent.area_id for parent in SetupGeorgraphy.objects.filter(parent_area_id__in=grandparentfilter)]
        for location in SetupGeorgraphy.objects.filter(area_type_id=geotype, parent_area_id__in=parents):
            parent  = '%s-' % SetupGeorgraphy.objects.get(area_id=location.parent_area_id).area_name
            optionstring += build_geo_control_option_list(selected_location, location, parent, setdisabled)
    else:
        for location in SetupGeorgraphy.objects.filter(area_type_id=geotype):
            parent = ''
            if geolisthasparent:
                parent  = '%s-' % SetupGeorgraphy.objects.get(area_id=location.parent_area_id).area_name
            optionstring += build_geo_control_option_list(selected_location, location, parent, setdisabled)
        
    return optionstring

def community_control_populate(selected_community=None, parent_filter=None):
    optionstring = ''
    if parent_filter:
        for communityid, communityorgid, communityname in get_communities_in_ward(parent_filter):
            if selected_community and communityid in selected_community:
                optionstring += '<option value="%s" selected>%s %s</option>\n' % (communityid, communityorgid, communityname)
            else:
                optionstring += '<option value="%s">%s %s</option>\n' % (communityid, communityorgid, communityname)
                '''if selected_community:
                    optionstring += '<option value="%s" disabled>%s %s</option>\n' % (communityid, communityorgid, communityname)
                else:
                    optionstring += '<option value="%s">%s %s</option>\n' % (communityid, communityorgid, communityname)'''
    return optionstring

def districts():
    districts = [(dst.area_id, dst.area_name) for dst in SetupGeorgraphy.objects.filter(area_type_id=fdict.geo_type_district_code)]
    return districts


def get_related_organisation(organisation_id):
    
    rel_orgs = RegOrgUnit.objects.filter(parent_org_unit_id=organisation_id).values_list('pk', flat=True)
    if not rel_orgs:
        return [organisation_id]
    return list(rel_orgs).append(organisation_id)

def get_user_related_organisation_workforce_id(workforceid):
    user = AppUser.objects.get(reg_person__pk=workforceid)
    return get_user_related_organisation(user)

def get_user_related_organisation(user, exclude_orgs=[], include_orgs=[]):
    related_orgs = []
    
    if user.is_superuser:
        related_orgs = [(org.pk, org.org_unit_name)for org in RegOrgUnit.objects.filter(is_void=False).exclude(pk__in=exclude_orgs) if org.is_active]
        return initial_drop_item + related_orgs
    
    from ovc_auth.models import OVCUserRoleGeoOrg
    orgs = [orgrole.org_unit.pk for orgrole in OVCUserRoleGeoOrg.objects.filter(user=user) if orgrole.org_unit and orgrole.org_unit.is_active]
    
    if not orgs:
        orgs = []
    user_orgs = [(org_role.org_unit.pk, org_role.org_unit.org_unit_name) for org_role in OVCUserRoleGeoOrg.objects.filter(user=user) if  org_role.org_unit and org_role.org_unit.is_active]
    related_orgs = [(org.pk, org.org_unit_name) for org in RegOrgUnit.objects.filter(parent_org_unit_id__in=orgs, is_void=False).exclude(pk__in=exclude_orgs) if org.is_active]
    if include_orgs:
        related_orgs += [(org.pk, org.org_unit_name) for org in RegOrgUnit.objects.filter(pk__in=include_orgs, is_void=False) if org.is_active]
    return initial_drop_item + list(set(user_orgs + related_orgs))

def calculate_age(birth_date):
    '''Adapted from http://stackoverflow.com/questions/2217488/age-from-birthdate-in-python'''
    tmp = birth_date
    if tmp:
        today = date.today()
        try: 
            birthday = tmp.replace(year=today.year)
        except ValueError: 
            birthday = tmp.replace(year=today.year, month=birth_date.month+1, day=1)
        if birthday > today:
            return today.year - tmp.year - 1
        else:
            return today.year - tmp.year
    return -1

def get_form_types_for_workforce(user):
    wfc_id = user.workforce_id
    wfc_id_pk = None
    options_form_type = "<option value=''>All types</option>\n"
    if RegPerson.objects.filter(workforce_id=wfc_id,is_void=False).count() == 1:
        wfc_id_pk = RegPerson.objects.get(workforce_id=wfc_id,is_void=False).pk
    
    #When implementing security, check the users permissions and check if they have view_ben permission to remove it from the forms
        
    form_types = SetupList.objects.filter(item_category='Form type')
    count = 0
    for form_type in form_types:
        can_user_add_form = user_can_add_form(user, form_type.item_id)
        if not can_user_add_form:
            continue
        count+=1
        options_form_type += "<option value='%s'>%s</option>\n" % (form_type.item_id, form_type.item_description)

    return options_form_type

def user_can_add_form(user, form_type):
    '''
    Implement logic to handle whether the user can add a given form type here
    '''
    if form_type:
        return {
                'FT3e': can_user_add_wfc_form,
                'FT3d': can_user_add_form_ovc_assessment,
                'FT2d': can_user_add_public_workforce_forms,
                'FT2c': can_user_add_wfc_form,
                'FT1g': dummy_func_returns_true,#disabled
                'FT1f': can_user_add_res_ins_form,
                'FT1c': dummy_func_returns_true,#disabled
                'FT1e': can_user_add_public_workforce_forms,
                'FT3f': can_user_add_child_in_conflict,
                }[form_type](user)
    return None

def can_user_add_wfc_form(user):
    if user.is_superuser:
        return True
    if user.has_perm('auth.enter frm high sensitive'):
        return True
    if user.has_perm('auth.enter frm high sensitive own') and (user.reg_person.workforce_id != '' and user.reg_person.workforce_id != None):
        return True
    return False

def can_view_form(user, form=None):
    if user.is_superuser:
        return True
    if user.has_perm('auth.enter frm high sensitive'):
        return True
    if user.has_perm('auth.enter frm high sensitive own') and (user.reg_person.workforce_id != '' and user.reg_person.workforce_id != None):
        return True
    return False

def can_user_add_res_ins_form(user):
    if user.is_superuser:
        return True
    if user.has_perm('auth.enter frm high sensitive'):
        return True
    if user.has_perm('auth.enter frm as soc welf'):
        return True
    if user.has_perm('enter frm high sensitive own') and has_parent_unit_res_ins(user.reg_person.pk):
        return True
    return False

def has_parent_unit_res_ins(wfc_id):
    orgs = RegPersonsOrgUnits.objects.filter(person_id = wfc_id)
    for org in orgs:
        if RegOrgUnit.objects.filter(pk=org.parent_org_unit_id).count() == 1:
            if RegOrgUnit.objects.get(pk=org.parent_org_unit_id).org_unit_type_id == fdict.org_unit_type_res_Institution:
                return True
    return False

def can_user_add_child_in_conflict(user):
    if user.is_superuser:
        return True
    if user.has_perm('auth.enter frm high sensitive'):
        return True
    if user.has_perm('auth.enter frm as soc welf'):
        return True
    return False

def can_user_add_form_ovc_assessment(user):
    if user.is_superuser:
        return True
    if user.has_perm('auth.enter frm high sensitive'):
        return True
    if user.has_perm('auth.enter frm high sensitive own') and (user.reg_person.workforce_id != '' and user.reg_person.workforce_id != None):
        return True
    return False

def can_user_add_public_workforce_forms(user):
    to_return = False
    if user.is_superuser:
        return True
    if user.has_perm('auth.enter frm low sensitive'):
        to_return = True
    return to_return

def dummy_func_returns_true(user=None):
    return True

def get_list_of_subjects(token, form_type): 
    print 'loading guardians to control'   
    vals = None
    
    try:
        search_condition = []
        search_condition.append(Q(first_name__icontains=token))
        search_condition.append(Q(surname__icontains=token))
        search_condition.append(Q(other_names__icontains=token))
        search_condition.append(Q(beneficiary_id__icontains=token))
        search_condition.append(Q(national_id__icontains=token)) 
        search_condition.append(Q(birth_reg_id__icontains=token))
        search_condition.append(Q(workforce_id__icontains=token))
        
        search_type = get_search_type(form_type)
        
        form_token = None
        if(search_type == 'beneficiary'):
            raw_vals = tuple([(l.pk,l.beneficiary_id, l.full_name) for l in RegPerson.objects.filter(reduce(operator.or_, search_condition),regpersonstypes__person_type_id__contains=fdict.child_beneficiary_ovc, is_void=False)]) 
            
            vals = []
            for pk_val,ben_id,full_name in raw_vals:
                steps = None
                if RegPersonsExternalIds.objects.filter(person_id=pk_val,identifier_type_id=fdict.steps_ovc_caregiver,is_void=False).count() == 1:
                    steps = RegPersonsExternalIds.objects.get(person_id=pk_val,identifier_type_id=fdict.steps_ovc_caregiver,is_void=False).identifier
                
                ward = None
                if RegPersonsGeo.objects.filter(person_id=pk_val, is_void=False).count() == 1:
                    ward = SetupGeorgraphy.objects.get(pk=RegPersonsGeo.objects.get(person_id=pk_val, is_void=False).area_id,is_void=False).area_name
                
                vals.append(tuple([pk_val,ben_id,steps,full_name,ward]))
            
        elif(search_type == 'workforce'):
            search_workforce = []
            search_workforce.append(Q(regpersonstypes__person_type_id__contains=fdict.workforce_type_ngo_id))
            search_workforce.append(Q(regpersonstypes__person_type_id__contains=fdict.workforce_type_gov_id))
            search_workforce.append(Q(regpersonstypes__person_type_id__contains=fdict.workforce_type_vol_id))
            q_list_execlude = []
            q_list_execlude.append(Q(workforce_id=None))
            q_list_execlude.append(Q(workforce_id=''))
            vals = tuple([(l.pk,l.workforce_id,l.national_id,l.full_name) for l in RegPerson.objects.filter(reduce(operator.or_, search_condition), reduce(operator.or_, search_workforce), is_void=False).exclude(reduce(operator.or_, q_list_execlude))])
        elif(search_type == 'cwac'):
            vals = tuple([(l.pk,l.org_unit_id_vis,l.org_unit_name) for l in RegOrgUnit.objects.filter(is_gdclsu=True,is_void=False)])
        elif(search_type == 'resd'):
            vals = tuple([(l.pk,l.org_unit_id_vis,l.org_unit_name) for l in RegOrgUnit.objects.filter(org_unit_type_id=fdict.org_type_residential_children,is_void=False)])
        elif(search_type == 'org'):
            vals = tuple([(l.pk,l.org_unit_id_vis,l.org_unit_name) for l in RegOrgUnit.objects.filter(~Q(org_unit_type_id=fdict.org_type_residential_children),is_gdclsu=False,is_void=False)])
        else:
            return None
        
    except Exception as e:
        print e
        raise Exception('Couldnt load beneficiaries')
    
    return vals

def get_search_type(form_type):
    if form_type:
        return {
                'FT3e': 'workforce',
                'FT3d': 'beneficiary',
                'FT2d': 'None',
                'FT2c': 'workforce',
                'FT1g': 'org',
                'FT1f': 'resd',
                'FT1c': 'cwac',
                'FT1e': 'None',
                'FT3f': 'beneficiary',
                }[form_type]
    return None

def get_capture_site_id():
    id = None
    if config.IS_CAPTURE_SITE:
        if len(AdminCaptureSites.objects.all()) > 0:
            model = AdminCaptureSites.objects.filter()[0]
            id = model.pk
    return id
            
def get_capture_site_name():
    name = '[Capture site not configured or configured as Server]'
    if config.IS_CAPTURE_SITE:
        if len(AdminCaptureSites.objects.all()) > 0:
            model = AdminCaptureSites.objects.filter()[0]
            name = model.capture_site_name
    return name

def get_wfc_pk(wfc_id):
    if RegPerson.objects.filter(workforce_id=wfc_id,is_void=False).count() == 1:
        return RegPerson.objects.get(workforce_id=wfc_id,is_void=False).pk
    elif RegPerson.objects.filter(workforce_id=wfc_id,is_void=False).count() > 1:
        print 'something went wrong on loading RegPerson, check fields_list_provider on line 698 and 699'
        
def get_answer_set(answer_set_ids = {}):
    '''
    Returns a dictionary of answer_set_id (key) to a tuple (value) of answer pk and answer text.
    param: answer_set_ids dictionary with the following items.
        key: answer_set_id is the answer_set_id for which we want to get answer details.
        value: add_initial determines whether we want to add the initial blank option.
    '''
    to_return = {}
    if answer_set_ids:
        for answer_set_id, add_initial in answer_set_ids.items():
            if add_initial:
                to_return[answer_set_id] = tuple(initial_drop_item + [(l.pk, l.answer) for l in ListAnswers.objects.filter(answer_set_id = answer_set_id,is_void=False).order_by('the_order')])
            else:
                to_return[answer_set_id] = tuple([(l.pk, l.answer) for l in ListAnswers.objects.filter(answer_set_id = answer_set_id,is_void=False).order_by('the_order')])
    return to_return

def get_wfc(wfc_id):
    wfc = None
    if RegPerson.objects.filter(pk=wfc_id, is_void=False).count()==1:
        reg_person = RegPerson.objects.get(pk=wfc_id, is_void=False)
        wfc = '%s, %s, %s, %s'%(reg_person.workforce_id,reg_person.national_id,reg_person.full_name,reg_person.pk) 
    elif RegPerson.objects.filter(pk=wfc_id, is_void=False).count()>1:
        print 'something went wrong on loading RegPerson, check fields_list_provider on method get_wfc()'
        
    return wfc

def get_adverse_conditions_for_form(form_id_pk, form_subject_id):
    adverse_conds_to_return = []
    adverse_conds = CoreAdverseConditions.objects.filter(form_id=form_id_pk,beneficiary_person_id=form_subject_id)
    for adverse_cond in adverse_conds:
        adverse_conds_to_return.append(adverse_cond.adverse_condition_id)
    
    return adverse_conds_to_return

def search_setup_list_by_category(category, searchtoken):
    '''
    category: what category of items to be retrieved
    searchtoken: of the retrieved categories, which ones match this search token
        both on item descriptions and item category
    '''
    condition = {
                 str:'item_category',
                 list:'item_category__in'
                 }
    
    search_condition = []
    st = searchtoken.strip().split(' ')
    for token in st:
        search_condition.append(Q(item_id__icontains=token))
        search_condition.append(Q(item_description__icontains=token))
    results = SetupList.objects.filter(reduce(operator.or_, search_condition), **{condition[type(category)]:category})

    if results:
        return [result.item_id for result in results]
    else:
        return []

def get_setup_list_by_category(category):
    '''
    category: field to filter by in setup list, can be a list
    '''
    condition = {
                 str:'item_category',
                 list:'item_category__in'
                 }
    if category:
            print 'MY DCTIONARY', condition[type(category)]
            return  SetupList.objects.filter(**{condition[type(category)]:category})

    return None
        
def get_setup_items_as_dict_by_category(category, prefixid=False):
    '''
    return a dictionary of the form
    {
        item_category1:[(itemid, item_category),....],
        item_category2:[(itemid, itemcategory), ...]
    }
    '''
    result = get_setup_list_by_category(category)
    print 'MYRESULT!', category
    toreturn = {}
    for setup_list_item in result:
        cat = setup_list_item.item_category
        itemid = setup_list_item.item_id
        itemdescription = setup_list_item.item_description
        if prefixid:
            itemdescription = itemid +'-'+ itemdescription
        if cat in toreturn:
            toreturn[cat].append((itemid, itemdescription))
        else:
            toreturn[cat] = [(itemid, itemdescription)]
    return toreturn
        
        
def get_list_of_report_types():
    option_report_type = ""
    #option_report_type = "<option value=''>-----</option>\n"
    report_types =  ListReports.objects.all()
    for report_type in report_types:
        option_report_type += "<option value='%s'>%s</option>\n" % (report_type.pk, report_type.report_title_short)
    return option_report_type

def get_report_params(rep_id, user):
    to_return_params = {}
    rep_params = ListReportsParameters.objects.filter(report_id = rep_id)
    cnt = 0
    for rep_param in rep_params:
        tmp_var = {}
        if rep_param.parameter == "area_id":
            tmp_var['filter'] = get_geo_filter_vals("all geo", user,rep_param.pk)
        elif rep_param.filter:
            tmp_var['filter'] = get_filter_vals(rep_param.filter, user,rep_param.pk)
        
        tmp_var['initially_visible'] = rep_param.initially_visible
        tmp_var['required'] = rep_param.required
        tmp_var['label'] = rep_param.label
        default_val = get_field_default_value(rep_param.pk)
        if default_val:
            tmp_var['default'] = default_val
        to_return_params[rep_param.parameter] = tmp_var
    
    return to_return_params

def get_field_default_value(field_id):
    default_value = None
    if field_id in [1,58,82,72,74,78,80,96,100,111,116]:
        default_value = get_forms_date_default("min")
    elif field_id in [3,59,83,73,75,79,81,97,101,112,117]:
        default_value = get_forms_date_default("max")
    elif field_id in [28,33,39,43,46]:
        default_value = get_service_date_default("min")
    elif field_id in [29,34,40,44,47]:
        default_value = get_service_date_default("max")
    elif field_id == 19:
        default_value = get_person_geo_date_default("min")
    elif field_id == 20:
        default_value = get_person_geo_date_default("max")
    elif field_id == 23:
        default_value = get_person_date_default("min")
    elif field_id == 24:
        default_value = get_person_date_default("max")
    elif field_id in [9,107]:
        default_value = get_encounter_date_default("min")
    elif field_id in [10,108]:
        default_value = get_encounter_date_default("max")
    elif field_id in [92,94]:
        default_value = get_res_children_date_default("min")
    elif field_id in [93,95]:
        default_value = get_res_children_date_default("max")
    elif field_id in [119,120,88]:
        default_value = get_todays_date_default()
        
    
    return default_value
def get_todays_date_default():
    return datetime.date.today().strftime('%d-%B-%Y')

def get_res_children_date_default(type_of_date):
    index = "date_left__%s"%(type_of_date)
    to_return = None
    to_return_val = FormResChildren.objects.all().aggregate(Min('date_left') if type_of_date=="min" else Max('date_left'))
    if to_return_val:
        to_return = to_return_val[index]
    
    if to_return:
        return datetime.datetime.strftime(to_return, '%d-%B-%Y')

def get_person_date_default(type_of_date):
    index = "date_of_death__%s"%(type_of_date)
    to_return = None
    to_return_val = RegPerson.objects.all().aggregate(Min('date_of_death') if type_of_date=="min" else Max('date_of_death'))
    if to_return_val:
        to_return = to_return_val[index]
    
    if to_return:
        return datetime.datetime.strftime(to_return, '%d-%B-%Y')

def get_person_geo_date_default(type_of_date):
    index = "date_linked__%s"%(type_of_date)
    to_return = None
    to_return_val = RegPersonsGeo.objects.all().aggregate(Min('date_linked') if type_of_date=="min" else Max('date_linked'))
    if to_return_val:
        to_return = to_return_val[index]
    
    if to_return:
        return datetime.datetime.strftime(to_return, '%d-%B-%Y')

def get_encounter_date_default(type_of_date):
    index = "encounter_date__%s"%(type_of_date)
    to_return = None
    to_return_val = CoreEncounters.objects.all().aggregate(Min('encounter_date') if type_of_date=="min" else Max('encounter_date'))
    if to_return_val:
        to_return = to_return_val[index]
    
    if to_return:
        return datetime.datetime.strftime(to_return, '%d-%B-%Y')

def get_service_date_default(type_of_date):
    index = "encounter_date__%s"%(type_of_date)
    to_return = None
    to_return_val = CoreServices.objects.all().aggregate(Min('encounter_date') if type_of_date=="min" else Max('encounter_date'))
    if to_return_val:
        to_return = to_return_val[index]
    
    if to_return:
        return datetime.datetime.strftime(to_return, '%d-%B-%Y')
    
def get_forms_date_default(type_of_date):
    index = "date_began__%s"%(type_of_date)
    to_return = None
    to_return_val = Forms.objects.all().aggregate(Min('date_began') if type_of_date=="min" else Max('date_began'))
    if to_return_val:
        to_return = to_return_val[index]
    
    if to_return:
        return datetime.datetime.strftime(to_return, '%d-%B-%Y')

    

def get_filter_vals(filter,user=None,field_id=None):
    if user != None and field_id==11:
        print 'filter', filter
        if not user.is_authenticated():
            to_return_filter = None
            to_ret_dict = {}
            filter = [fdict.geo_type_district_code,fdict.geo_type_province_code]
            if filter:
                to_return_filter = tuple([(l.item_id,l.item_description) for l in SetupList.objects.filter(item_id__in = filter)])
                
            for item_id, item_description in to_return_filter:
                to_ret_dict[item_id] = item_description
            return to_ret_dict
    elif field_id in [63, 70,113,118,121]:
        to_return_filter = None
        to_ret_dict = {}
        
        filter = [fdict.ANSWER_TYPE_MULTI_SELECT,fdict.ANSWER_TYPE_SINGLE_SELECT] if field_id in [63,113,118,121] else [fdict.ANSWER_TYPE_SINGLE_SELECT]
        if field_id == 118:
            form_t_id = "FT1f"  
        elif field_id == 113:
            form_t_id = "FT2c"
        elif field_id == 121:
            form_t_id = "FT3f"
        else:
            form_t_id = "FT3d"
        if filter:
            to_return_filter = tuple([(l.pk,l.question_text) for l in ListQuestions.objects.filter(form_type_id=form_t_id,answer_type_id__in = filter)])
            
        for item_id, item_description in to_return_filter:
            to_ret_dict[item_id] = item_description
        return to_ret_dict
    elif field_id == 110:
        to_return_filter = None
        to_ret_dict = {}
        filter = ["Service type","Referral made","Referral completed"]
        if filter:
            to_return_filter = tuple([(l.item_id,l.item_description) for l in SetupList.objects.filter(item_category__in = filter)])
            
        for item_id, item_description in to_return_filter:
            to_ret_dict[item_id] = item_description
            
        return to_ret_dict
    
        
    to_return_filter = None
    to_ret_dict = {}
    if filter:
        to_return_filter = tuple([(l.item_id,l.item_description) for l in SetupList.objects.filter(item_category__icontains = filter)])
        
    for item_id, item_description in to_return_filter:
        to_ret_dict[item_id] = item_description
        
    return to_ret_dict

def get_geo_filter_vals(f_val, user=None,field_id=None,rep_id_v=None):
    filter = []
    report_ids = ['3',]
    if f_val and (str(rep_id_v) in report_ids):
        if f_val == fdict.geo_type_district_code:
            filter = [fdict.geo_type_province_code,]
        elif f_val == fdict.geo_type_constituency_code:
            filter = [fdict.geo_type_province_code,fdict.geo_type_district_code]
        elif f_val == fdict.geo_type_ward_code:
            filter = [fdict.geo_type_constituency_code,fdict.geo_type_district_code]
    elif (f_val and (str(rep_id_v) == '6')) or str(field_id) == '109':
        filter = [fdict.geo_type_district_code,]
    elif (str(field_id) == '98'):
        filter = [fdict.geo_type_district_code,fdict.geo_type_province_code]
    else:
        filter = [f_val,]
    print 'filter',filter
    if user:
        fields_applied = ['6','13','27','41','45','50','87']       
        if not user.is_authenticated() and (str(field_id) in fields_applied):
            filter = None
            filter = [fdict.geo_type_district_code,fdict.geo_type_province_code]
    
    to_return_filter = None
    to_ret_dict = {}
    if filter:
        if "all geo" in filter:
            search_condition = [fdict.geo_type_district_code,fdict.geo_type_province_code,fdict.geo_type_constituency_code,fdict.geo_type_ward_code]
            to_return_filter = tuple([(l.pk, '%s %s'% (l.area_name,report_type(l.area_type_id))) for l in SetupGeorgraphy.objects.filter(area_type_id__in = search_condition)])
            print 'to_ret_filter', to_return_filter
        else:
            to_return_filter = tuple([(l.pk, '%s %s'% (l.area_name,report_type(l.area_type_id))) for l in SetupGeorgraphy.objects.filter(area_type_id__in = filter)])
    if to_return_filter:
        for item_id, item_description in to_return_filter:
            to_ret_dict[item_id] = item_description
        
    return to_ret_dict

def get_person_geo_location(person_id, geo_type='ward'):
    '''
    returns the warid, wardname-districtname
    TODO:
    return other area types
    '''
    geos = RegPersonsGeo.objects.filter(person__pk=person_id, is_void=False, date_delinked= None) 
    geo_ids = [geo.area_id for geo in geos]
    toreturn = None
    if geo_type:
        codes = {'ward': fdict.geo_type_ward_code}
        wards = SetupGeorgraphy.objects.filter(area_type_id=codes[geo_type], is_void=False, area_id__in=geo_ids)
        for ward in wards:
            toreturn = ward.area_id, ward.area_name +'-'+ SetupGeorgraphy.objects.get(area_id = ward.parent_area_id).area_name
            break
    return toreturn

def report_type(filter_type):
    if filter_type:
        return{
            fdict.geo_type_district_code:'district',
            fdict.geo_type_province_code:'province',
            fdict.geo_type_constituency_code:'constituency',
            fdict.geo_type_ward_code:'ward'
        }[filter_type]


def get_org_units(org_search_key):
        org_units = None
        to_return_org = {}
        search_condition = []
        search_condition.append(Q(org_unit_name__icontains=org_search_key))
        search_condition.append(Q(org_unit_id_vis__icontains=org_search_key))
        
        org_units = tuple([(l.pk, '%s %s'%(l.org_unit_id_vis, l.org_unit_name)) for l in RegOrgUnit.objects.filter(reduce(operator.or_, search_condition), is_void = False)])
            
        #for org_unit in org_units:
            #to_return_org[org_unit.pk] = '%s %s'%(org_unit.org_unit_id_vis, org_unit.org_unit_name)
            
        return org_units

def get_report_data(report_type,item_id,question,geo_area,org_unit,set_id,from_date,to_date,from_date_c,to_date_c):
    report_dict = OrderedDict()
    print 'all params',report_type,item_id,question,geo_area,org_unit,set_id,from_date,to_date,from_date_c,to_date_c
    cursor = connection.cursor()
    
    try:
        if report_type == report_sql.REPORTING_PERFORMANCE:
            if item_id and not org_unit and not set_id:
                print 'itm id'
                cursor.execute(report_sql.REPORTING_PERFORMANCE_ITEM_SQL,[from_date,to_date,item_id])
            elif org_unit and not item_id and not set_id:
                cursor.execute(report_sql.REPORTING_PERFORMANCE_ORG_UNIT_SQL,[from_date,to_date,org_unit])
            elif set_id and not item_id and not org_unit:
                cursor.execute(report_sql.REPORTING_PERFORMANCE_SET_SQL,[from_date,to_date,set_id])
            elif (org_unit and item_id) or (org_unit and set_id) or (set_id and item_id):
                print 'Not allowed to specify more than one of org unit type and org unit and org unit set'
            else:
                print 'else'
                cursor.execute(report_sql.REPORTING_PERFORMANCE_NOTHING_SPECIFIED_SQL,[from_date,to_date])
                print cursor.mogrify(report_sql.REPORTING_PERFORMANCE_NOTHING_SPECIFIED_SQL,[from_date,to_date])
        elif report_type == report_sql.OVC_DEMOGRAPHICS:
            if not geo_area and not org_unit:
                cursor.execute(report_sql.OVC_DEMOGRAPHICS_NOTHING_SPECIFIED_SQL)
            elif geo_area and not org_unit:
                area_type = None
                area_type = get_area_type(geo_area) # This method gets the type of geo_area, (e.g GDIS) and assigns it to the variable area_type
                if area_type == "GWRD":
                    cursor.execute(report_sql.OVC_DEMOGRAPHICS_AREA_ID_WARD_SPECIFIED_SQL, [geo_area])
                elif area_type == "GCON":
                    cursor.execute(report_sql.OVC_DEMOGRAPHICS_AREA_ID_CONST_SPECIFIED_SQL, [geo_area])
                elif area_type== "GDIS":
                    cursor.execute(report_sql.OVC_DEMOGRAPHICS_AREA_ID_DISTRICT_SPECIFIED_SQL, [geo_area])
                elif area_type == "GPRV":
                    cursor.execute(report_sql.OVC_DEMOGRAPHICS_AREA_ID_PROV_SPECIFIED_SQL, [geo_area])
            elif org_unit and not geo_area and not set_id:
                cursor.execute(report_sql.OVC_DEMOGRAPHICS_ORG_UNIT_SQL,[org_unit])
            elif set_id and not org_unit and not geo_area:
                cursor.execute(report_sql.OVC_DEMOGRAPHICS_SET_SQL,[set_id]) 
            else:
                print "Not allowed to select more than one of geographical, org unit and set criteria"
        elif report_type == report_sql.OVC_BY_GEO_AREA:
            area_type = None
            if geo_area:
                area_type = get_area_type(geo_area) # This method gets the type of geo_area, (e.g GDIS) and assigns it to the variable area_type

            if item_id == "GWRD":
                if area_type == "GCON":
                    cursor.execute(report_sql.OVC_BY_GEO_WARDS_IN_CONST_SQL,[to_date, to_date, to_date, to_date, from_date, to_date, geo_area])
                elif area_type == "GDIS":
                    cursor.execute(report_sql.OVC_BY_GEO_WARDS_IN_DIST_SQL,[to_date, to_date, to_date, to_date, from_date, to_date, geo_area])
            elif item_id == "GCON":
                if area_type == "GDIS":
                    cursor.execute(report_sql.OVC_BY_GEO_CONST_IN_DIST_SQL,[to_date, to_date, to_date, to_date, from_date, to_date, geo_area])
                elif area_type == "GPRV":
                    cursor.execute(report_sql.OVC_BY_GEO_CONST_IN_PROV_SQL,[to_date, to_date, to_date, to_date, from_date, to_date, geo_area])
            elif item_id =="GDIS":
                if area_type == "GPRV":
                    cursor.execute(report_sql.OVC_BY_GEO_DIST_IN_PROV_SQL,[to_date, to_date, to_date, to_date, from_date, to_date, geo_area])
                else:
                    cursor.execute(report_sql.OVC_BY_GEO_DIST_ALL_SQL,[to_date, to_date, to_date, to_date, from_date, to_date])
            elif item_id == "GPRV":
                cursor.execute(report_sql.OVC_BY_GEO_PROV_ALL_SQL,[to_date, to_date, to_date, to_date, from_date, to_date])            
            else:
                print 'Geographical area type is required'
                
        elif report_type == report_sql.OVC_ADVERSE_COND:
            if not geo_area and not org_unit and not set_id:
                cursor.execute(report_sql.OVC_ADVERSE_COND_NOTHING_SPECIFIED_SQL)
            elif geo_area and not org_unit and not set_id:
                area_type = None
                area_type = get_area_type(geo_area) # This method gets the type of geo_area, (e.g GDIS) and assigns it to the variable area_type
                if area_type == "GWRD":
                    cursor.execute(report_sql.OVC_ADVERSE_COND_GEO_WARD_SQL, [geo_area])
                elif area_type == "GCON":
                    cursor.execute(report_sql.OVC_ADVERSE_COND_GEO_CONST_SQL, [geo_area])
                elif area_type == "GDIS":
                    cursor.execute(report_sql.OVC_ADVERSE_COND_GEO_DIST_SQL, [geo_area])
                elif area_type == "GPRV":
                    cursor.execute(report_sql.OVC_ADVERSE_COND_GEO_PROV_SQL, [geo_area])
            elif org_unit and not geo_area and not set_id:
                cursor.execute(report_sql.OVC_ADVERSE_COND_ORG_UNIT_SQL,[org_unit])
            elif set_id and not org_unit and not geo_area:
                cursor.execute(report_sql.OVC_ADVERSE_COND_SET_SQL,[set_id])
            else:
                    print "Not allowed to select both geographical and org unit criteria"
        elif report_type == report_sql.OVC_EXP_ADVERSE_COND:
            if not geo_area and not org_unit:
                cursor.execute(report_sql.OVC_ADVERSE_COND_ONE_SQL,[item_id])
            elif geo_area and not org_unit and not set_id:
                area_type = None
                area_type = get_area_type(geo_area) # This method gets the type of geo_area, (e.g GDIS) and assigns it to the variable area_type
                if area_type == "GWRD":
                    cursor.execute(report_sql.OVC_ADVERSE_COND_ONE_GEO_WARD_SQL, [item_id, geo_area]) #ward
                elif area_type == "GCON":
                    cursor.execute(report_sql.OVC_ADVERSE_COND_ONE_GEO_CONST_SQL, [item_id, geo_area]) #constituency
                elif area_type == "GDIS":
                    cursor.execute(report_sql.OVC_ADVERSE_COND_ONE_GEO_DIST_SQL, [item_id, geo_area]) #district
                elif area_type == "GPRV":
                    cursor.execute(report_sql.OVC_ADVERSE_COND_ONE_GEO_PROV_SQL, [item_id, geo_area]) #province
            elif org_unit and not geo_area and not set_id:
                cursor.execute(report_sql.OVC_ADVERSE_COND_ONE_ORG_UNIT_SQL,[item_id, org_unit])
            elif set_id and not org_unit and not geo_area:
                cursor.execute(report_sql.OVC_ADVERSE_COND_ONE_SET_SQL,[item_id, set_id])   
            else:
                print "Not allowed to select more than one of geographical and org unit and set criteria"
        elif report_type == report_sql.OVC_MIGRATION:
            if item_id == "GWRD":
                cursor.execute(report_sql.OVC_MIGRATION_0_SQL,[to_date, to_date, from_date, to_date, geo_area, geo_area])
            elif item_id == "GCON":
                cursor.execute(report_sql.OVC_MIGRATION_1_SQL,[to_date, to_date, from_date, to_date])
            elif item_id == "GDIS":
                cursor.execute(report_sql.OVC_MIGRATION_2_SQL,[to_date, to_date, from_date, to_date])
            elif item_id == "GPRV":
                cursor.execute(report_sql.OVC_MIGRATION_3_SQL,[to_date, to_date, from_date, to_date])
                
        elif report_type == report_sql.GUARDIAN_DEATH:
            if not geo_area and not org_unit and not set_id:
                cursor.execute(report_sql.GUARDIAN_DEATH_SQL, [from_date, to_date])
            elif geo_area and not org_unit and not set_id:
                area_type = None
                area_type = get_area_type(geo_area) # This method gets the type of geo_area, (e.g GDIS) and assigns it to the variable area_type
                if area_type == "GWRD":
                    cursor.execute(report_sql.GUARDIAN_DEATH_GEO_0_SQL, [geo_area, to_date, to_date, from_date, to_date]) #ward
                elif area_type == "GCON":
                    cursor.execute(report_sql.GUARDIAN_DEATH_GEO_1_SQL, [to_date, to_date, from_date, to_date, geo_area]) #constituency
                elif area_type == "GDIS":
                    cursor.execute(report_sql.GUARDIAN_DEATH_GEO_2_SQL, [to_date, to_date, geo_area, from_date, to_date ]) #district
                elif area_type == "GPRV":
                    cursor.execute(report_sql.GUARDIAN_DEATH_GEO_3_SQL, [to_date, to_date, geo_area, from_date, to_date ]) #province
            elif org_unit and not geo_area and not set_id:        
                cursor.execute(report_sql.GUARDIAN_DEATH_ORG_UNIT_SQL, [from_date, to_date, org_unit])
            elif set_id and not geo_area and not org_unit:        
                cursor.execute(report_sql.GUARDIAN_DEATH_SET_SQL, [set_id, from_date, to_date])        
            else:
                print "Not allowed to select more than one of geographical and org unit and set criteria"
        elif report_type == report_sql.SERVICES_PROVIDED:
            if not geo_area and not org_unit and not set_id:
                cursor.execute(report_sql.SERVICES_PROVIDED_SQL, [from_date, to_date, to_date, to_date])
            elif geo_area and not org_unit and not set_id:
                area_type = None
                area_type = get_area_type(geo_area) # This method gets the type of geo_area, (e.g GDIS) and assigns it to the variable area_type
                if area_type == "GWRD":
                    cursor.execute(report_sql.SERVICES_PROVIDED_GEO_0_SQL, [from_date, to_date, geo_area, to_date, to_date]) #ward
                elif area_type == "GCON":
                    cursor.execute(report_sql.SERVICES_PROVIDED_GEO_1_SQL, [from_date, to_date, geo_area, to_date, to_date]) #constituency
                elif area_type == "GDIS":
                    cursor.execute(report_sql.SERVICES_PROVIDED_GEO_2_SQL, [geo_area, from_date, to_date, to_date, to_date]) #district
                elif area_type == "GPRV":
                    cursor.execute(report_sql.SERVICES_PROVIDED_GEO_3_SQL, [geo_area, from_date, to_date, to_date, to_date]) #province
            elif org_unit and not geo_area and not set_id:        
                cursor.execute(report_sql.SERVICES_PROVIDED_ORG_UNIT_SQL, [to_date,to_date, org_unit, from_date, to_date])
            elif set_id and not geo_area and not org_unit:        
                cursor.execute(report_sql.SERVICES_PROVIDED_SET_SQL, [from_date, to_date, set_id, to_date, to_date])        
            else:
                print "Not allowed to select more than one of geographical and org unit and set criteria"

        elif report_type == report_sql.BEN_RECV_SERVICE:
            if not geo_area and not org_unit and not set_id:
                cursor.execute(report_sql.SERVICE_PROVIDED_ONE_DEMOG_SQL, [from_date, to_date, item_id])
            elif geo_area and not org_unit and not set_id:
                area_type = None
                area_type = get_area_type(geo_area) # This method gets the type of geo_area, (e.g GDIS) and assigns it to the variable area_type
                if area_type == "GWRD":
                    cursor.execute(report_sql.SERVICE_PROVIDED_ONE_DEMOG_GEO_0_SQL, [from_date, to_date, item_id, geo_area]) #ward
                elif area_type == "GCON":
                    cursor.execute(report_sql.SERVICE_PROVIDED_ONE_DEMOG_GEO_1_SQL, [from_date, to_date, item_id, geo_area]) #constituency
                elif area_type == "GDIS":
                    cursor.execute(report_sql.SERVICE_PROVIDED_ONE_DEMOG_GEO_2_SQL, [geo_area, from_date, to_date, item_id]) #district
                elif area_type == "GPRV":
                    cursor.execute(report_sql.SERVICE_PROVIDED_ONE_DEMOG_GEO_3_SQL, [geo_area, from_date, to_date, item_id]) #province
            elif org_unit and not geo_area and not set_id:        
                cursor.execute(report_sql.SERVICE_PROVIDED_ONE_DEMOG_ORG_SQL, [from_date,to_date, item_id, org_unit])
            elif set_id and not geo_area and not org_unit:        
                cursor.execute(report_sql.SERVICE_PROVIDED_ONE_DEMOG_SET_SQL, [from_date, to_date, item_id, set_id])        
            else:
                print "Not allowed to select more than one of geographical and org unit and set criteria"
            
        elif report_type == report_sql.ORG_UNIT_PROVIDING_SERVICE:
            if not geo_area:
                cursor.execute(report_sql.SERVICE_PROVIDED_ONE_BY_ORG_SQL, [from_date, to_date, item_id, to_date, to_date])
            elif geo_area:
                area_type = None
                area_type = get_area_type(geo_area) # This method gets the type of geo_area, (e.g GDIS) and assigns it to the variable area_type
                if area_type == "GWRD":
                    cursor.execute(report_sql.SERVICE_PROVIDED_ONE_BY_ORG_GEO_0_SQL, [from_date, to_date, item_id, geo_area, to_date, to_date]) #ward
                elif area_type == "GCON":
                    cursor.execute(report_sql.SERVICE_PROVIDED_ONE_BY_ORG_GEO_1_SQL, [from_date, to_date, item_id, geo_area, to_date, to_date]) #constituency
                elif area_type == "GDIS":
                    cursor.execute(report_sql.SERVICE_PROVIDED_ONE_BY_ORG_GEO_2_SQL, [geo_area, from_date, to_date, item_id, to_date, to_date]) #district
                elif area_type == "GPRV":
                    cursor.execute(report_sql.SERVICE_PROVIDED_ONE_BY_ORG_GEO_3_SQL, [geo_area, from_date, to_date, item_id, to_date, to_date]) #province        
                    
            
        elif report_type == report_sql.WFC_BEN_RATIO_BY_ORG_UNIT:
            if not geo_area:
                cursor.execute(report_sql.WF_BEN_RATIO_BY_ORG_SQL, [from_date, to_date, from_date, to_date])
            elif geo_area:
                area_type = None
                area_type = get_area_type(geo_area) # This method gets the type of geo_area, (e.g GDIS) and assigns it to the variable area_type
                if area_type == "GWRD":
                    cursor.execute(report_sql.WF_BEN_RATIO_BY_ORG_GEO_0_SQL, [from_date, to_date, geo_area, from_date, to_date, geo_area]) #ward
                elif area_type == "GCON":
                    cursor.execute(report_sql.WF_BEN_RATIO_BY_ORG_GEO_1_SQL, [from_date, to_date, geo_area, from_date, to_date, geo_area]) #constituency
                elif area_type == "GDIS":
                    cursor.execute(report_sql.WF_BEN_RATIO_BY_ORG_GEO_2_SQL, [geo_area, from_date, to_date, geo_area, from_date, to_date]) #district
                elif area_type == "GPRV":
                    cursor.execute(report_sql.WF_BEN_RATIO_BY_ORG_GEO_3_SQL, [geo_area, from_date, to_date, geo_area, from_date, to_date]) #province        
            
            
        elif report_type == report_sql.REFFERALS_MADE:
            if not geo_area and not org_unit and not set_id:
                cursor.execute(report_sql.REFERRALS_SQL, [from_date, to_date, from_date])
            elif geo_area and not org_unit and not set_id:
                area_type = None
                area_type = get_area_type(geo_area) # This method gets the type of geo_area, (e.g GDIS) and assigns it to the variable area_type
                if area_type == "GWRD":
                    cursor.execute(report_sql.REFERRALS_GEO_0_SQL, [from_date, to_date, geo_area, from_date]) #ward
                elif area_type == "GCON":
                    cursor.execute(report_sql.REFERRALS_GEO_1_SQL, [from_date, to_date, geo_area, from_date]) #constituency
                elif area_type == "GDIS":
                    cursor.execute(report_sql.REFERRALS_GEO_2_SQL, [geo_area, from_date, to_date, from_date]) #district
                elif area_type == "GPRV":
                    cursor.execute(report_sql.REFERRALS_GEO_3_SQL, [geo_area, from_date, to_date, from_date]) #province
            elif org_unit and not geo_area and not set_id:        
                cursor.execute(report_sql.REFERRALS_ORG_SQL, [from_date,to_date, org_unit, from_date])
            elif set_id and not geo_area and not org_unit:        
                cursor.execute(report_sql.REFERRALS_SET_SQL, [from_date, to_date, set_id, from_date])        
            else:
                print "Not allowed to select more than one of geographical and org unit and set criteria"


        elif report_type == report_sql.OVC_ASSESSMENT:
            if not geo_area and not org_unit and not set_id:
                cursor.execute(report_sql.OVC_ASSESSMENT_SQL, [from_date, to_date, question])
            elif geo_area and not org_unit and not set_id:
                area_type = None
                area_type = get_area_type(geo_area) # This method gets the type of geo_area, (e.g GDIS) and assigns it to the variable area_type
                if area_type == "GWRD":
                    cursor.execute(report_sql.OVC_ASSESSMENT_GEO_0_SQL, [geo_area, to_date, to_date, from_date, to_date, question]) #ward
                elif area_type == "GCON":
                    cursor.execute(report_sql.OVC_ASSESSMENT_GEO_1_SQL, [to_date, to_date, geo_area, from_date, to_date, question]) #constituency
                elif area_type == "GDIS":
                    cursor.execute(report_sql.OVC_ASSESSMENT_GEO_2_SQL, [to_date, to_date, geo_area, from_date, to_date, question]) #district
                elif area_type == "GPRV":
                    cursor.execute(report_sql.OVC_ASSESSMENT_GEO_3_SQL, [to_date, to_date, geo_area, from_date, to_date, question]) #province
            elif org_unit and not geo_area and not set_id:        
                cursor.execute(report_sql.OVC_ASSESSMENT_ORG_SQL, [org_unit, from_date, to_date, question])
            elif set_id and not geo_area and not org_unit:        
                cursor.execute(report_sql.OVC_ASSESSMENT_SET_SQL, [set_id, from_date, to_date, question])        
            else:
                print "Not allowed to select more than one of geographical and org unit and set criteria"
        
        elif report_type == report_sql.OVC_ASSESSMENT_CSI:
            if not geo_area and not org_unit and not set_id:
                cursor.execute(report_sql.OVC_ASSESSMENT_CSI_SQL, [from_date, to_date])
            elif geo_area and not org_unit and not set_id:
                area_type = None
                area_type = get_area_type(geo_area) # This method gets the type of geo_area, (e.g GDIS) and assigns it to the variable area_type
                if area_type == "GWRD":
                    cursor.execute(report_sql.OVC_ASSESSMENT_CSI_GEO_0_SQL, [geo_area, to_date, to_date, from_date, to_date]) #ward
                elif area_type == "GCON":
                    cursor.execute(report_sql.OVC_ASSESSMENT_CSI_GEO_1_SQL, [to_date, to_date, from_date, to_date, geo_area]) #constituency
                elif area_type == "GDIS":
                    cursor.execute(report_sql.OVC_ASSESSMENT_CSI_GEO_2_SQL, [to_date, to_date, geo_area, from_date, to_date]) #district
                elif area_type == "GPRV":
                    cursor.execute(report_sql.OVC_ASSESSMENT_CSI_GEO_3_SQL, [to_date, to_date, geo_area, from_date, to_date]) #province
            elif org_unit and not geo_area and not set_id:        
                cursor.execute(report_sql.OVC_ASSESSMENT_CSI_ORG_SQL, [org_unit, from_date, to_date])
            elif set_id and not geo_area and not org_unit:        
                cursor.execute(report_sql.OVC_ASSESSMENT_CSI_SET_SQL, [set_id, from_date, to_date])        
            else:
                print "Not allowed to select more than one of geographical and org unit and set criteria"
              
        
        elif report_type == report_sql.OVC_ASSESSMENT_OVERTIME:
                cursor.execute(report_sql.OVC_ASSESSMENT_OVERTIME_SQL, [from_date, to_date, question, from_date_c, to_date_c, question])
        elif report_type == report_sql.OVC_ASSESSMENT_CSI_OVERTIME:
                cursor.execute(report_sql.OVC_ASSESSMENT_CSI_OVERTIME_SQL, [from_date, to_date, item_id, from_date_c, to_date_c, item_id])        
        
        elif report_type == report_sql.RESIDENTIAL_CHILD_STATUS_SUMMARY:
                cursor.execute(report_sql.RESIDENTIAL_CHILD_STATUS_SUMMARY_SQL, [to_date])
        elif report_type == report_sql.RESIDENTIAL_CHILD_STATUS_BY_INSTITUTION:
                cursor.execute(report_sql.RESIDENTIAL_CHILD_STATUS_BY_INSTITUTION_SQL, [to_date])        
        elif report_type == report_sql.RESIDENTIAL_DEMOGRAPHICS:
                if not org_unit:
                    cursor.execute(report_sql.RESIDENTIAL_DEMOG_SQL, [to_date, to_date])
                else:
                    cursor.execute(report_sql.RESIDENTIAL_DEMOG_ORG_SQL, [to_date, to_date, org_unit])
        elif report_type == report_sql.RESIDENTIAL_ADVERSE_COND:
                if not org_unit:
                    cursor.execute(report_sql.RESIDENTIAL_ADVERSE_COND_SQL, [])
                else:
                    cursor.execute(report_sql.RESIDENTIAL_ADVERSE_COND_ORG_SQL, [org_unit])
        elif report_type == report_sql.RESIDENTIAL_REINTEGRATION_DEMOG:
            cursor.execute(report_sql.RESIDENTIAL_REINTEGRATION_DEMOG_SQL, [from_date, to_date])
        elif report_type == report_sql.RESIDENTIAL_REINTEGRATION_BY_ORG:
            cursor.execute(report_sql.RESIDENTIAL_REINTEGRATION_BY_ORG_SQL, [from_date, to_date])
        elif report_type == report_sql.RESIDENTIAL_ASSESSMENT:
            cursor.execute(report_sql.RESIDENTIAL_ASSESSMENT_SQL, [from_date, to_date, question])
        
        elif report_type == report_sql.CICWL_ASSESSMENT:
            if not geo_area:
                cursor.execute(report_sql.CICWL_ASSESSMENT_SQL, [from_date, to_date, question])
            elif geo_area:
                area_type = None
                area_type = get_area_type(geo_area) # This method gets the type of geo_area, (e.g GDIS) and assigns it to the variable area_type
                if area_type == "GDIS":
                    cursor.execute(report_sql.CICWL_ASSESSMENT_GEO_2_SQL, [geo_area, from_date, to_date, question]) #district
                elif area_type == "GPRV":
                    cursor.execute(report_sql.CICWL_ASSESSMENT_GEO_3_SQL, [geo_area, from_date, to_date, question]) #province
                else:
                    # only districts and provinces can be selected
                    val = None #to prevent exception, python else must always have atleast a line of code
        elif report_type == report_sql.CICWL_DEMOGRAPHICS:
            cursor.execute(report_sql.CICWL_DEMOG_SQL, [from_date, to_date])
        
        elif report_type == report_sql.WORKFORCE_ACTIVE_BY_GEO:
            if not geo_area and not item_id:
                cursor.execute(report_sql.WORKFORCE_ACTIVE_BY_GEO2_SQL, [to_date, to_date, from_date, to_date])
            elif geo_area and not item_id:
                area_type = None
                area_type = get_area_type(geo_area) # This method gets the type of geo_area, (e.g GDIS) and assigns it to the variable area_type
                if area_type == "GDIS":
                    cursor.execute(report_sql.WORKFORCE_ACTIVE_BY_GEO0_SQL, [to_date, to_date, geo_area, from_date, to_date])
                else:
                    # only districts and provinces can be selected
                    val = None #to prevent exception, python else must always have atleast a line of code
            elif not geo_area and item_id:
                cursor.execute(report_sql.WORKFORCE_ACTIVE_BY_GEO2_ONE_SERVICE_SQL, [to_date, to_date, from_date, to_date, item_id])
            elif geo_area and item_id:
                area_type = None
                area_type = get_area_type(geo_area) # This method gets the type of geo_area, (e.g GDIS) and assigns it to the variable area_type
                if area_type == "GDIS":
                    cursor.execute(report_sql.WORKFORCE_ACTIVE_BY_GEO0_ONE_SERVICE_SQL, [to_date, to_date, geo_area, from_date, to_date, item_id])
                else:
                    # only districts and provinces can be selected        
                    val = None #to prevent exception, python else must always have atleast a line of code                    
        elif report_type == report_sql.WORKFORCE_ASSESSMENT:
            if not org_unit and not set_id:
                cursor.execute(report_sql.WORKFORCE_ASSESSMENT_SQL, [from_date, to_date, question])
            elif org_unit and not set_id:        
                cursor.execute(report_sql.WORKFORCE_ASSESSMENT_ORG_SQL, [to_date, to_date, from_date, to_date, question, org_unit])
            elif set_id and not org_unit:        
                cursor.execute(report_sql.WORKFORCE_ASSESSMENT_SET_SQL, [to_date, to_date, from_date, to_date, question, set_id])        
            else:
                print "Not allowed to select more than one of geographical and org unit and set criteria"
        report_dict = dictfetchall(cursor)
    except Exception as e:
        print e
    
    return report_dict
    
def get_area_type(geo_area):
    to_return_type = None
    if SetupGeorgraphy.objects.filter(area_id=geo_area).count() == 1:
        to_return_type = SetupGeorgraphy.objects.get(area_id=geo_area).area_type_id
    return to_return_type
    

def dictfetchall(cursor):
    print "Returns all rows from a cursor as a dict"
    to_return_dict = []
    desc = cursor.description
    if cursor.rowcount > 0:
        o_dict_rows = [
            OrderedDict(zip([str(col[0]) for col in desc], row))
            for row in cursor.fetchall()
        ]
        
        for row in o_dict_rows:
            o_dict_str = OrderedDict()
            for key,val in row.items():
                o_dict_str[key] = str(val)
                
            to_return_dict.append(o_dict_str)
    else:
        cols = [desc[0] for desc in cursor.description]
        dict_cols = OrderedDict()
        for col in cols:
            dict_cols[col] = ''
        to_return_dict.append(dict_cols)
        
    return to_return_dict

def get_district_for_ward(tmp_area_id):
    control_count = 0
    not_district = True
    while not_district:
        if SetupGeorgraphy.objects.filter(area_id=tmp_area_id).count() == 1:
            if SetupGeorgraphy.objects.filter(area_id=tmp_area_id, area_type_id=fdict.geo_type_district_code).count() == 1:
                not_district = False
                return SetupGeorgraphy.objects.get(area_id=tmp_area_id, area_type_id=fdict.geo_type_district_code).area_id
            else:
                tmp_area_id = SetupGeorgraphy.objects.get(area_id=tmp_area_id).parent_area_id
                control_count += 1
                if control_count == 5:
                    return

def get_person_object_from_user_id(user_id):
    if AppUser.objects.filter(pk=user_id).count() == 1:
        return AppUser.objects.get(pk=user_id).reg_person
    else:
        print 'this id is not there in AppUser'

def save_org_set(st_name,user_id):
    set_obj = ReportsSets.objects.create(set_name=st_name,user_id_created=user_id)
    return set_obj.pk

def delete_report_set(s_id):
    ReportsSetsOrgUnits.objects.filter(set_id=s_id).delete()
    ReportsSets.objects.filter(pk=s_id).delete()
    return True

def save_org_set_orgs(s_id,orgs):
    ReportsSetsOrgUnits.objects.filter(set_id=s_id).delete()
    for org in orgs:
        set_obj = ReportsSetsOrgUnits.objects.create(set_id=s_id,org_unit_id=org)
    return True

def get_orgs_for_set(s_id):
    if s_id == "org":
        orgs = {(l.pk,'%s - %s'%(l.org_unit_name,l.org_unit_id_vis)) for l in RegOrgUnit.objects.filter(is_void=False)}
        if orgs:
            return get_options_from_dict(orgs)
        else:
            return None
    elif s_id == "set":
        sets = {(l.pk,l.set_name) for l in ReportsSets.objects.all()}
        return get_options_from_dict(sets,True)
    
    sel_orgs = [(l.org_unit_id) for l in ReportsSetsOrgUnits.objects.filter(set_id=s_id)]
    return {'sel_orgs':sel_orgs}

def get_options_from_dict(dict_obj,init=False):
    opt_to_ret = ""
    dict_sorted = sorted(dict_obj, key=lambda tup: tup[0])
    opt_to_ret = ""
    if init:
        opt_to_ret = "<option value='%s'>%s</option>\n" % ("", "-----")
    for key,val in dict_sorted:
        opt_to_ret += "<option value='%s'>%s</option>\n" % (key, val)
    return opt_to_ret

def user_has_role(user,role):
    return user.groups.filter(name=role).exists()