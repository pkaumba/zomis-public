'''
Created on Sep 23, 2014

@author: MLumpa
'''
from ovc_main.models import RegPerson, RegPersonsWorkforceIds, RegPersonsBeneficiaryIds

organisation_id_prefix = 'U'
benficiary_id_prefix = 'B'
workforce_id_prefix = 'W'

def new_guid_32():
    import uuid
    return str(uuid.uuid1()).replace('-','')

def organisation_id_generator(modelid):
    uniqueid = '%05d' % modelid
    checkdigit = calculate_luhn(str(uniqueid))
    
    return organisation_id_prefix + str(uniqueid) + str(checkdigit)

def beneficiary_id_generator(reg_person):
    beneficiary_id = ''
    m_beneficiary,created = RegPersonsBeneficiaryIds.objects.get_or_create(person=reg_person)
    if created:
        uniqueid = '%07d' % m_beneficiary.pk
        checkdigit = calculate_luhn(str(uniqueid))
        
        beneficiary_id = benficiary_id_prefix + str(uniqueid) + str(checkdigit)

        m_beneficiary.beneficiary_id = beneficiary_id
        m_beneficiary.save()
    
    return beneficiary_id

def workforce_id_generator(reg_person):
    workforce_id = ''
    m_workforce_id,created = RegPersonsWorkforceIds.objects.get_or_create(person=reg_person)
    if created:
        uniqueid = '%06d' % m_workforce_id.pk
        checkdigit = calculate_luhn(str(uniqueid))

        workforce_id = workforce_id_prefix + str(uniqueid) + str(checkdigit)

        m_workforce_id.workforce_id = workforce_id
        m_workforce_id.save()
    
    return workforce_id

def luhn_checksum(card_number):
    '''
    http://en.wikipedia.org/wiki/Luhn_algorithm
    '''
    def digits_of(n):
        return [int(d) for d in str(n)]
    digits = digits_of(card_number)
    odd_digits = digits[-1::-2]
    even_digits = digits[-2::-2]
    checksum = 0
    checksum += sum(odd_digits)
    for d in even_digits:
        checksum += sum(digits_of(d*2))
    return checksum % 10
 
def is_luhn_valid(card_number):
    '''
    http://en.wikipedia.org/wiki/Luhn_algorithm
    '''
    return luhn_checksum(card_number) == 0
    

def calculate_luhn(partial_card_number):
    '''
    http://en.wikipedia.org/wiki/Luhn_algorithm
    '''
    check_digit = luhn_checksum(int(partial_card_number) * 10)
    return check_digit if check_digit == 0 else 10 - check_digit