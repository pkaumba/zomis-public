from django.conf import settings

label_new_workforce_title = 'Register new workforce member/user'
label_update_workforce_title = 'Update workforce member/user'

label_workforce_id = settings.APP_NAME+' workforce ID '
label_nrc= 'NRC '

label_workforce_name = 'Name '
label_workforce_type = 'Workforce/user type '
label_parent_unit = 'Primary parent org unit '
label_workforce_summary = 'Workforce member/user summary'


label_person_id = 'Person_id '
label_first_name = 'First name '
label_last_name = 'Surname '
label_other_names = 'Other name(s) '
label_sex = 'Sex '
label_date_of_birth = 'Date of birth '
label_workforce_member_type = 'Workforce member/user type '
label_man_number = 'MAN number '
label_ts_number = 'If teacher, TS number '
label_police_sign_number = 'If police officer, call sign number '
label_steps_number = 'Other ID '
# label_steps_number = 'STEPS OVC caregiver number '
# @Elaine This is still STEPS OVC caregiver ID in practise.  However ECR want mention of STEPS removed for political reaons.
label_services_to_children = 'Does this person provide services directly to children?'
label_org_unit = 'Organisational unit '
label_registration_assistant = 'Registration assistant for this org unit '
label_designated_mobile_number = 'Designated mobile number for sending and receiving SMSes '
label_other_mobile_number = 'Other mobile number '
label_email_address = 'Email address '
label_physical_address = 'Physical address '
label_districts = 'Districts'
label_wards = 'Wards'
label_communities = 'Communities'



label_option_update_workforce_user_details = 'Update workforce/user details'
label_option_died = 'Deceased'
label_option_never_existed = 'Workforce member/user never existed (was a registration mistake or a duplicate)'


label_workforce_type_change_date = 'Workforce/user type change date: '
label_parent_unit_change_date = 'Parent unit(s) change date:'
label_work_location_change_date = 'Work location(s) change date: '



label_button_add = 'Add'
label_button_remove = 'Remove'
label_button_cancel = 'Cancel'
label_button_save = 'Save' 
label_button_update = 'Update'

label_table_heading_org_unit = 'Org unit'
label_table_heading_org_unit_id = 'Org unit ID'
label_table_heading_primary_parent_org_unit = 'Primary parent org unit'
label_table_heading_registration_assistant = 'Registration assistant for this org unit'
label_table_heading_contact_details = 'Contact details'

error_message_nrc = "NRC number must be entered in the format 987654/32/1"
error_message_phone_number = "Phone number must be entered in the format: '+260977123456' or '0977123456'."
error_message_primary_parent_option = 'Please select an option for "Primary parent organisation unit"'
error_message_only_one_primary_parent = 'A primary parent unit has already been set for this person. Only one primary parent unit can be set per person.  If needed, you may remove the existing primary parent unit before adding another one.'
error_message_duplicate_org_unit = 'This organisational unit has already been added for this person.'
error_message_invalid_org_unit = 'Please search by typing in the box, and then select one of the matches from the drop down list. '
error_message_no_primary_unit = 'Please select at least one primary parent organisational unit for this person. No primary parent organisational unit selected.'
error_message_duplicate_nrc_first_part = 'This NRC is already linked to '
error_message_duplicate_nrc_last_part = '. You cannot register another person with this NRC.'
error_message_duplicate_designated_phone_first_part = 'This phone number is already linked to '
error_message_duplicate_designated_phone_last_part = '. You cannot register another person with this number.'
error_message_enter_nrc = 'As this person will not be assigned a workforce ID, please ensure that you enter an NRC for this person for identification.'
error_message_save_not_successful = 'Save not successful - please make corrections below'
error_message_change_date = "Date of change cannot be before date original record was saved"
error_message_no_reg_perm_for_primary_parent = "You do not have permission to register workforce members with this primary parent organisational unit"

warning_message_duplicate_dialog = 'There is already a workforce/user with the first name and surname you entered, are you sure you want to proceed?'
warning_message_deactivation_dialog = 'Are you sure this person never existed (ie this record is a mistake or a duplicate record)?'
warning_message_blank_date_of_death_dialog = 'Please enter the date of death'

label_section_personal_details = 'Personal details'
label_section_workforce_type = 'Workforce/user type'
label_section_parent_org_units = 'Parent organisational unit(s)'
label_section_where_work = 'Where workforce member/user works'

label_tooltip_other_names = 'Any other names which are either in official documents or are commonly used to refer to the person'
label_tooltip_date_of_birth = 'If unknown, put an estimate'
label_tooltip_designated_phone_number = 'This must be filled if the person is to interact with '+settings.APP_NAME+' via SMS'
label_tooltip_email_address = 'Either email address or designated mobile phone number (or both) must be filled if the user is to receive their password and interact directly with ' +settings.APP_NAME
