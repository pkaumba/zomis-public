HEADING_WORKFORCE_MEMBER_DETAILS = u"Workforce member details"
HEADING_SERVICE_REFERRAL_RECORDS_FOR_WORFORCE = u"Service/referral records for this workforce member"
HEADING_SERVICE_REFERRAL = u"Service/referral record"

LABEL_DATE_OF_SERVICE = u"Date of service"
LABEL_ORG_UNIT_PROVIDING_SERVICE = u"Organisational unit providing services"
LABEL_BENEFICIARY = u"Beneficiary"
LABEL_ENCOUNTER_TYPE = u"Encounter type"
LABEL_ENCOUNTER_LOCATION = u"Encounter location"
