from django.conf import settings

LABEL_HEAD_CHILD_DETAILS = 'Child details'
LABEL_ID = '%s ID: ' % settings.APP_NAME
LABEL_BIRTH_CERT = 'Birth cert number:'
LABEL_FIRST_NAME = 'First name:'
LABEL_SURNAME = 'Surname:'
LABEL_SEX = 'Sex:'
LABEL_DOB = 'Date of birth:'
LABEL_COMMUNITY = 'Community:'
LABEL_WARD = 'Ward:'

#SECTION A
LABEL_HEAD_SEC_A = 'Section A'
SOCIAL_WELFARE_MANAGING = 'Social welfare officer managing this case'
EDU_LEVEL_OF_CHILD = 'Educational level of child (Highest school grade reached)'
DATE_OF_ARREST = 'Date of arrest'
REFFERING_POLICE_STATION = 'Referring police station'
STATION_CALL_SIGN_NUMBER = 'Station call sign number'
POLICE_CASE_DOCKET_NUM = 'Police case docket number'
START_DATE = 'Date(s) of alleged offense'
END_DATE = 'To'
LOCATION_OF_OFFENSE_DIST = 'Geographical location of alleged offense - district'
LOCATION_OF_OFFENSE_WARD = 'Geographical location of alleged offense - ward'
TYPE_OF_OFFENSE = 'Type of alleged offense'
NOTES_ON_OFFENSE = 'Notes on alleged offense'
RECEIVED_BAIL = 'Received bail'
NOTES_ON_DETENTION_BAIL = 'Notes on detention and bail'
METHOD_OF_DISPOSAL_BY_POLICE = 'Method of disposal by police'

#SECTION B
LABEL_HEAD_SEC_B = 'Section B - Interviews and court stage'
SOCIAL_DATE = 'Date'
SOCIAL_WELFARE_OFFICER = 'Social welfare officer'
SOCIAL_WHO_WAS_INTERVIEWED = 'Who was interviewed'
SOCIAL_NOTES = 'Notes'
SOCIAL_RECOMMENDATIONS = 'Recommendations'

TH_SOCIAL_DATE = 'Date'
TH_SOCIAL_WELFARE_OFFICER = 'Workforce member name'
TH_SOCIAL_WHO_WAS_INTERVIEWED = 'Who was interviewed'
TH_SOCIAL_NOTES = 'Notes'
TH_SOCIAL_RECOMMENDATIONS = 'Recommendations'
TH_SOCIAL_EDIT = 'Edit'
TH_SOCIAL_REMOVE = 'Remove'
TH_SOCIAL_ADD_BUTTON = 'Add'

COURT_DATE = 'Date'
COURT_WELFARE_OFFICER = 'Social welfare officer'
COURT = 'Court'
NAME_OF_JUDGE = 'Name of judge / magristrate'
INVESTIGATIONS_ORDERED = 'Investigations ordered'
COURT_NOTES = 'Notes'
COURT_ADD_BUTTON = 'Add button'
COURT_LIST_OF_COURT_SESSIONS = 'List of court sessions'
COURT_CASE_NUM = 'Court case number'

TH_COURT_DATE = 'Date'
TH_COURT_WELFARE_OFFICER = 'Workforce member name'
TH_COURT = 'Court'
TH_NAME_OF_JUDGE = 'Name of judge or magristrate'
TH_INVESTIGATIONS_ORDERED = 'Investigations ordered'
TH_COURT_NOTES = 'Notes'
TH_COURT_EDIT = 'Edit'
TH_COURT_REMOVE = 'Remove'
TH_COURT_ADD_BUTTON = 'Add'

#SECTION C
LABEL_HEAD_SEC_C = 'Section C - outcome'
OUTCOME = 'Outcome'
NOTES_ON_OUTCOME = 'Notes on outcome'
DATE_CASE_DISPOSED = 'Date case disposed'
SAVE = 'Save'
CANCEL = 'Cancel'
UPDATE = 'Update'

REGEX_ERROR = 'The entered case docket number is invalid, please enter the valid case docket number in the format n/mm/yyyy where n is any number, and mm/yyyy is a month and year.'

