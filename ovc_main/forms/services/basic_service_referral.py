from django import forms
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Fieldset, \
    ButtonHolder, Submit, Button, HTML, MultiField, \
    Div, Field
from crispy_forms.bootstrap import FormActions, \
    AppendedText
from ovc_main.forms.services import \
    constants_basic_service_referral_form as consts
from ovc_main.utils import fields_list_provider, \
    lookup_field_dictionary, geo_location, \
    validators as validate_helper, workforce_utils
import datetime
from ovc_main.models import RegPerson,Forms, CoreEncounters


setup_list_item_category = [lookup_field_dictionary.REFERRAL_COMPLETED_CATEGORY_DB_TEXT,
                    lookup_field_dictionary.REFERRAL_MADE_CATEGORY_DB_TEXT,
                    lookup_field_dictionary.SERVICE_TYPE_CATEGORY_DB_TEXT,
                    lookup_field_dictionary.ENCOUNTER_TYPE_CATEGORY_DB_TEXT
                    ]

setup_category_dict = fields_list_provider.get_setup_items_as_dict_by_category(setup_list_item_category, True)


class BasicServiceReferralForm(forms.Form):
    workforce_system_id = forms.CharField(widget=forms.HiddenInput())
    organisation_unit = forms.ChoiceField(label=consts.LABEL_ORG_UNIT_PROVIDING_SERVICE)
    date_of_service = forms.DateField(widget=forms.DateInput(format='%d-%B-%Y', attrs={'class': 'datepicker'}),
                                                            input_formats=validate_helper.date_input_formats_tpl,
                                                             validators=[validate_helper.validate_general_date])
    beneficiary_autocomplete = forms.CharField(label = consts.LABEL_BENEFICIARY, widget=forms.TextInput(attrs={"class":"beneficiary_autocomplete"}))
    beneficiary_id = forms.CharField(widget=forms.HiddenInput(attrs={"id":"beneficiary_id"}))
    encounter_type = forms.ChoiceField(label=consts.LABEL_ENCOUNTER_TYPE)
    encounter_location = forms.ChoiceField(label=consts.LABEL_ENCOUNTER_LOCATION)
    
    form_type = forms.CharField(widget=forms.HiddenInput(), initial="FT3e")
    form_id = forms.CharField(widget=forms.HiddenInput(), required=False)
    encounter_id = forms.CharField(widget=forms.HiddenInput(), required=False)
    
    services_provided = forms.MultipleChoiceField(widget=forms.SelectMultiple(attrs={'id':'services_provided', 'class':'multiselect-searchable'}), required=False)
    referrals_made = forms.MultipleChoiceField(widget=forms.SelectMultiple(attrs={'id':'referrals_made', 'class':'multiselect-searchable'}), required=False)
    referrals_completed = forms.MultipleChoiceField(widget=forms.SelectMultiple(attrs={'id':'referrals_completed', 'class':'multiselect-searchable'}), required=False)
    new_adverse_condition = forms.MultipleChoiceField(widget=forms.SelectMultiple(attrs={'id':'adverse_conditions', 'class':'multiselect-searchable'}), required=False)
    
    def __init__(self, user=None, formfields=None):
        
        super(BasicServiceReferralForm, self).__init__(formfields)
        self.fields['encounter_type'].choices = fields_list_provider.initial_drop_item + setup_category_dict[lookup_field_dictionary.ENCOUNTER_TYPE_CATEGORY_DB_TEXT]
        self.fields['new_adverse_condition'].choices = fields_list_provider.get_list_adverse_cond(prefixid=True)
        self.fields['services_provided'].choices = setup_category_dict[lookup_field_dictionary.SERVICE_TYPE_CATEGORY_DB_TEXT]
        self.fields['referrals_made'].choices = setup_category_dict[lookup_field_dictionary.REFERRAL_MADE_CATEGORY_DB_TEXT]
        self.fields['referrals_completed'].choices = setup_category_dict[lookup_field_dictionary.REFERRAL_COMPLETED_CATEGORY_DB_TEXT]
        
        if formfields and formfields.has_key('workforce_system_id'):
            print 'our list of orgs', formfields.get('workforce_system_id'), workforce_utils.workforce_orgs_for_drop_list(formfields.get('workforce_system_id'))
            self.fields['organisation_unit'].choices = workforce_utils.workforce_orgs_for_drop_list(formfields.get('workforce_system_id'))
        self.fields['encounter_location'].choices = [('','')]+geo_location.ward_list()
        
        self.helper = FormHelper()
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-lg-3'
        self.helper.field_class = 'col-lg-6'
        self.helper.form_action = '/capture/add_form/'
        self.helper.form_id = 'formid'
        self.helper.layout = \
            Layout(
                   #==workforce memeber details==
                   Div(
                       #panel-heading
                       Div(
                           HTML(consts.HEADING_WORKFORCE_MEMBER_DETAILS),
                           HTML('<input type="hidden" id="id_subject" name="subject" value={{subject.id_int}} />'),
                           #HTML('<input type="hidden" id="id_form_type" name="form_type" value="FT3e" />'),
                           'form_type',
                           'form_id',
                           'encounter_id',
                           'workforce_system_id',
                           css_class='panel-heading'
                           ),
                       
                       #panel-body
                       Div(
                           HTML('{% include "ovc_main/general_ui/workforce_summary_info.html" %}'),
                           css_class='panel-body'
                           ),
                       css_class='panel panel-primary'
                       ),
                   
                   #==past service/referral records==
                   Div(
                       #panel-heading
                       Div(
                           HTML(consts.HEADING_SERVICE_REFERRAL_RECORDS_FOR_WORFORCE),
                           
                           css_class='panel-heading'
                           ),
                       
                       #panel-body
                       Div(
                           HTML('{% include "ovc_main/general_ui/basic_referral_workforce_records.html" %}'),
                           css_class='panel-body'
                           ),
                       css_class='panel panel-primary'
                       ),
                   
                   #==service/referral Record==
                   Div(#panel-heading
                       Div(
                           HTML(consts.HEADING_SERVICE_REFERRAL),
                           css_class='panel-heading'
                           ),
                       
                       #panel-body
                       Div(
                           HTML("""
                               <div class="alert alert-block alert-danger hide" id="overall-record-error">
                                   <ul>
                                       <li class="ms-hover">Basic service referral record not saved - see errors in read or try saving again.
                                        </li>
                                    </ul>
                                </div> 
                           """),
                           'organisation_unit',
                           'date_of_service',
                           
                           #preload first benefiaries served in the last 12 months
                           #then if no click in a few seconds, add to it clients
                           'beneficiary_id',
                           'beneficiary_autocomplete',
                           'encounter_type',
                           'encounter_location',
                           'services_provided',
                           'referrals_made',
                           'referrals_completed',
                           'new_adverse_condition',
                           HTML('<hr>'),
                           HTML(
                                """<nav>
                                      <ul class=\"pager\">
                                        <li><button id="btnsave" class="btn btn-success">Save</button></li>
                                        <li><button id="btncancel" class="btn btn-primary">Cancel</button></li>
                                      </ul>
                                    </nav>"""
                                ),
                           css_class='panel-body servicemain hide'
                           ),
                       Div(
                           HTML("""
                                    
                                    <div class="col-md-3"></div>
                                    <div class="col-md-6">
                                        <dl class="dl-horizontal custom viewonly">
                                              <dt id="dateofservice">Date of service</dt>
                                              
                                              <p><dt id="orgunit">Organisational unit providing services</dt>
                                              
                                              <p><dt id="beneficiary">Beneficiary</dt>
                                              
                                              <p><dt id="encountertype">Encounter type</dt>
                                              
                                              <p><dt id="encounterlocation">Encounter location</dt>
                                              
                                              <p><dt id="servicesprovided">Services provided</dt>
                                              
                                              <p><dt id="referralsmade">Referrals made</dt>
                                              
                                              <p><dt id="referralscompleted">Referrals completed</dt>
                                              
                                              <p><dt id="adverseconditions">New adverse conditions</dt>
                                              
                                        </dl>
                                    </div>
                                    <div class="col-md-3"></div>
                                    
                                    
                                
                                """
                                ),
                           css_class='panel-body readonly-service-main hide'    
                        ),
                       css_class='panel panel-primary'
                       ),
                   
                   #==SaveCancelButtons==
                   Div(
                        HTML(
                                """<nav>
                                      <ul class=\"pager\">
                                        <li><button id="id_return_home" class="btn btn-danger">Back to Forms Home</button></li>
                                      </ul>
                                    </nav>"""
                                )
                       )
                   
                   )
            
    def clean_date_of_service(self):
        date_of_service = self.cleaned_data.get('date_of_service')
        if not date_of_service:
            raise forms.ValidationError("Date of service cannot be empty")
        if date_of_service > datetime.datetime.now().date():
            raise forms.ValidationError("Date of service cannot be empty")
        return date_of_service
    
    def clean(self):
        cleaned_data = super(BasicServiceReferralForm, self).clean()
        
        organisation_unit = cleaned_data.get('organisation_unit')
        workforce_system_id = cleaned_data.get('workforce_system_id')
        beneficiary_id = cleaned_data.get('beneficiary_id')
        
        if not beneficiary_id:
            beneficiary_id_msg = u"Beneficiary is a required field."
            #the error message has to be shown on the autocomplet form field
            self._errors["beneficiary_autocomplete"] = self.error_class([beneficiary_id_msg])
        
        date_of_service = cleaned_data.get('date_of_service')
        
        isadult = True;
        
        if beneficiary_id and date_of_service:
            try:
                ben = RegPerson.objects.get(pk=beneficiary_id)
                dateofbirth = ben.date_of_birth
                dateofdeath = ben.date_of_death
                
                if dateofbirth >= date_of_service:
                    date_of_service_msg = u"Date of service cannot be before beneficiary's date of birth"
                    self._errors["date_of_service"] = self.error_class([date_of_service_msg])
                
                if dateofdeath and dateofdeath < date_of_service:
                    date_of_service_msg_ben_died = u"Date of service cannot be after beneficiary's date of death"
                    self._errors["date_of_service"] = self.error_class([date_of_service_msg_ben_died])
                    
                    beneficiary_id_msg_ben_dead = u"The selected beneficiary is recorded as dead on the selected date of service"
                    self._errors["beneficiary_autocomplete"] = self.error_class([beneficiary_id_msg_ben_dead])
                if dateofbirth and date_of_service:
                    from ovc_main.utils import general
                    age = general.calculate_age(dateofbirth, date_of_service)
                    if age < 18:
                        isadult = False
            except Exception as e:
                print e
                beneficiary_id_msg = u"The beneficiary selected does not exist. Choose another one."
                self._errors["beneficiary_autocomplete"] = self.error_class([beneficiary_id_msg])
        
        formid = cleaned_data.get('form_id')
        if not formid and date_of_service and organisation_unit and workforce_system_id:
            forms = Forms.objects.filter(is_void=False, date_filled_paper=date_of_service,\
                                         form_type_id='FT3e', \
                                         form_subject_id=workforce_system_id)
            form_ids = [form.pk for form in forms]
            if form_ids:
                encounters = CoreEncounters.objects.filter(form_id__in=form_ids, beneficiary_person_id=beneficiary_id)
                if encounters:          
                    self._errors["__all__"] = self.error_class(["A record with the selected date of service, beneficiary and organisational unit was already entered."])
        
        services_provided = cleaned_data.get('services_provided')
        referrals_made = cleaned_data.get('referrals_made')
        referrals_completed = cleaned_data.get('referrals_completed')
        new_adverse_condition = cleaned_data.get('new_adverse_condition')
        
        if (services_provided == referrals_completed == new_adverse_condition == referrals_made) and not services_provided:
            self._errors["__all__"] = self.error_class(["Select at least one of: Services provided; %s Referrals made; and/or Referrals completed." % "Adverse conditions;" if not isadult else ""])
            self._errors["services_provided"] = self.error_class([''])
            self._errors["referrals_made"] = self.error_class([''])
            self._errors["referrals_completed"] = self.error_class([''])
            self._errors["new_adverse_condition"] = self.error_class([''])
        return cleaned_data
            
            