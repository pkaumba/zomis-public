from django import forms
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Fieldset,\
    ButtonHolder, Submit, Button, HTML, MultiField, Div, Field
    
from crispy_forms.bootstrap import TabHolder, Tab, InlineRadios
from ovc_main.forms.services import constants_residential_institution_form as \
        form_label
from functools import partial
DateInput = partial(forms.DateInput, {'class': 'datepicker'})
from crispy_forms.bootstrap import FormActions, AppendedText
from ovc_main.utils import validators as validate_helper
from ovc_main.utils import fields_list_provider as db_fieldchoices, lookup_field_dictionary as db_constants
from ovc_main.models import RegOrgUnit, RegPerson
'''
Call get_answer_set() with the dictionary of answer_set_ids and add empty option boolean that you will need for all the questions on the page.
In the control itself access the choice list by index as shown below

choices = form_db_lookups[9]
'''
answer_set_ids_add_empty_list = {8:True, 9:False, 10:True, 11:True, 12:False, 13:False, 14:True, 25:True, 26:True, 27:True, 28:True, 29:True, 30:True, 31:True, 32:True, 33:False}
form_db_lookups = db_fieldchoices.get_answer_set(answer_set_ids = answer_set_ids_add_empty_list)

class ResidentialInstitutionForm(forms.Form):
    ''' General fields '''
    form_type = forms.CharField(required = False,widget=forms.HiddenInput)
    subject = forms.CharField(required = False,widget=forms.HiddenInput)
    ward_id  = forms.CharField(required = False,widget=forms.HiddenInput)
    form_id = forms.CharField(required = False,widget=forms.HiddenInput)
    ''' Section A '''
    date_of_form = forms.DateField(widget=DateInput(), input_formats=validate_helper.date_input_formats_tpl, label = form_label.DATE_OF_FORM,validators=[validate_helper.validate_general_date])
    Q032 = forms.ChoiceField(label=form_label.REPORTED_BY, choices=form_db_lookups[8])
    user_workforce_filled_form = forms.CharField(label=form_label.WORKFORCE_OR_USER_FILLING_FORM, required=False)
    user_workforce_filled_form_hidden = forms.CharField(widget=forms.TextInput(attrs={'type':'number','min':'0'}), label='', required=False)
    person_filled_form_text = forms.CharField(label='', required = False)
    ''' Section B '''
    Q033 = forms.MultipleChoiceField(label=form_label.RESIDENTIAL_INSTITUTION_TYPE, widget = forms.CheckboxSelectMultiple(), choices = form_db_lookups[9])
    Q115 = forms.ChoiceField(label=form_label.WHO_OWNS_THE_BUILDING, choices = form_db_lookups[25])
    Q034 = forms.CharField(max_length=255, label=form_label.MISSION_OF_INSTITUTUION, required = False)
    Q035 = forms.CharField(widget=forms.TextInput(attrs={'min':'0'}), label=form_label.MINIMUM_AGE_OF_ENROLLMENT, required = False)
    Q036 = forms.CharField(widget=forms.TextInput(attrs={'min':'0'}), label=form_label.MAXIMUM_AGE_OF_ENROLLMENT, required = False)
    Q037 = forms.CharField(widget=forms.TextInput(attrs={'min':'1'}), label=form_label.INSTITUTUIONAL_CAPACITY, required = False)
    ''' Section C '''
    child = forms.CharField(label=form_label.CHILD, required=False)
    child_id = forms.CharField(label='', required = False)
    child_date_of_birth = forms.DateField(widget=DateInput(), input_formats=validate_helper.date_input_formats_tpl, label = '', required = False)
    child_date_of_death = forms.DateField(widget=DateInput(), input_formats=validate_helper.date_input_formats_tpl, label = '', required = False)
    new_adverse_condition =  forms.MultipleChoiceField(choices=db_fieldchoices.get_list_adverse_cond(), \
                                    widget=forms.SelectMultiple(attrs={'class':'multiselect-searchable'}), label='.', required = False)
    current_residential_status = forms.ChoiceField(label=form_label.CURRENT_RESIDENTIAL_STATUS, \
            choices = db_fieldchoices.get_item_id_and_description_for_cat(add_initial = True,\
            item_category = db_constants.category_residential_status), required=False)
    family_status = forms.ChoiceField(label=form_label.FAMILY_STATUS, choices = db_fieldchoices.get_item_id_and_description_for_cat(add_initial = True, item_category = db_constants.category_family_status), required = False)
    has_court_committal_order = forms.ChoiceField(label=form_label.HAS_COURT_COMMITTAL_ORDER, choices = db_fieldchoices.get_item_id_and_description_for_cat(add_initial = True, item_category = db_constants.gen_YesNo), required = False)
    date_first_admitted = forms.DateField(widget=DateInput(), input_formats=validate_helper.date_input_formats_tpl, label = form_label.DATE_FIRST_ADMITTED, required = False, validators=[validate_helper.validate_general_date])
    date_left = forms.DateField(widget=DateInput(), input_formats=validate_helper.date_input_formats_tpl, label = form_label.DATE_LEFT, required = False, validators=[validate_helper.validate_general_date])
    child_status_data = forms.CharField(label='',required=False)
    ''' Section D '''
    workforce_roles = forms.ChoiceField(choices=db_fieldchoices.get_item_id_and_description_for_cat(add_initial = True, item_category = db_constants.category_role_in_residential), required = False)
    workforce_employment_status = forms.ChoiceField(choices=db_fieldchoices.get_item_id_and_description_for_cat(add_initial = True, item_category = db_constants.category_employment_status), required = False) 
    workforce_data_hidden = forms.CharField(required = False)
    workforce_data = forms.CharField(label='',required=False)
    ''' Section E '''
    '''Water and hygiene'''
    Q039 = forms.ChoiceField( label=form_label.WATER_AND_HYGENE, choices=form_db_lookups[10], required = False)
    Q116 = forms.ChoiceField(label=form_label.HYGIENE_STANDARD_IN_INSTITUTION, choices=form_db_lookups[26], required = False)
    Q117 = forms.ChoiceField(label=form_label.IS_WATER_SOURCE_IN_INSTITUTION, choices=form_db_lookups[14], required = False)
    Q118 = forms.ChoiceField(label=form_label.PRIMARY_WATER_SOURCE, choices=form_db_lookups[27], required = False)
    Q119 = forms.CharField(widget=forms.TextInput(attrs={'min':'0'}), label=form_label.HOW_FAR_IS_WATER_SOURCE, required = False)
    Q120 = forms.ChoiceField(label=form_label.DOES_WATER_SOURCE_SERVICE_INSTITUTION_DAILY, choices=form_db_lookups[14], required = False)   
    Q040 = forms.ChoiceField(label=form_label.TOILET_FACILITIES, choices=form_db_lookups[10], required = False)
    Q121 = forms.CharField(widget=forms.TextInput(attrs={'min':'0'}), label=form_label.HOW_MANY_TOILETS_FOR_BOYS, required = False)
    Q122 = forms.CharField(widget=forms.TextInput(attrs={'min':'0'}), label=form_label.HOW_MANY_TOILETS_FOR_GIRLS, required = False)
    Q123 = forms.CharField(widget=forms.TextInput(attrs={'min':'0'}), label=form_label.HOW_MANY_TOILETS_TOTAL, required = False)
    Q124 = forms.ChoiceField(label=form_label.DO_BOYS_AND_GIRLS_SHARE_SAME_TOILETS, choices=form_db_lookups[28], required = False)
    Q125 = forms.ChoiceField(label=form_label.TYPES_OF_TOILETS_CHILDREN_USE, choices=form_db_lookups[29], required = False)
    Q126 = forms.CharField(widget=forms.TextInput(attrs={'min':'0'}), label=form_label.HOW_MANY_BATHROOMS_FOR_BOYS, required = False)
    Q127 = forms.CharField(widget=forms.TextInput(attrs={'min':'0'}), label=form_label.HOW_MANY_BATHROOMS_FOR_GIRLS, required = False)
    Q128 = forms.CharField(widget=forms.TextInput(attrs={'min':'0'}), label=form_label.HOW_MANY_BATHROOMS_TOTAL, required = False)
    Q129 = forms.ChoiceField(label=form_label.DO_BOYS_AND_GIRLS_SHARE_SAME_BATHROOMS, choices=form_db_lookups[28], required = False)
    Q130 = forms.ChoiceField(label=form_label.TYPES_OF_BATHROOMS_CHILDREN_USE, choices=form_db_lookups[30], required = False)
    Q131 = forms.CharField(max_length=255, label=form_label.NOTES_ON_WATER_HYGIENE_SANITATION, required = False)
    '''Food and nutrition'''
    Q038 = forms.ChoiceField(label=form_label.COOKING_AND_DINING_FACILITIES, choices = form_db_lookups[10], required = False)
    Q053 = forms.ChoiceField(label=form_label.DIETARY_CHART, choices=form_db_lookups[11], required = False)
    Q054 = forms.ChoiceField(label=form_label.VARIETY_IN_DIET, choices=form_db_lookups[11], required = False)
    Q055 = forms.ChoiceField(label=form_label.UNDER_FIVE_CARDS_FOR_CHILDREN_5_AND_BELOW, choices=form_db_lookups[11], required = False)
    Q056 = forms.CharField(widget=forms.TextInput(attrs={'min':'0'}),label=form_label.NUMBER_OF_MEALS_PER_DAY, required = False)
    '''Sleeping facilities'''
    Q132 = forms.ChoiceField(label=form_label.SLEEPING_FACILITIES, choices = form_db_lookups[10], required = False)
    Q133 = forms.ChoiceField(label=form_label.SEPARATE_SLEEPING_FACILITIES_FOR_BOYS_AND_GIRLS, choices=form_db_lookups[28], required = False)
    Q134 = forms.CharField(widget=forms.TextInput(attrs={'min':'0.0'}),label=form_label.NUMBER_OF_CHILDREN_PER_BED, required = False)
    Q135 = forms.CharField(widget=forms.TextInput(attrs={'min':'0'}),label=form_label.HOW_MANY_SLEEP_ON_FLOOR_MATS, required = False)
    Q136 = forms.ChoiceField(label=form_label.MOSQUITO_NETS_ON_BEDS_AND_WINDOWS, choices=form_db_lookups[14], required = False)
    Q137 = forms.CharField(max_length=255, label=form_label.NOTES_ON_SLEEPING_FACILITIES, required = False)
    '''Health and safety'''
    Q042 = forms.ChoiceField(label=form_label.HOME_SECUTITY, choices=form_db_lookups[10], required = False)
    Q043 = forms.ChoiceField(label=form_label.FIRE_SAFETY, choices=form_db_lookups[10], required = False)
    Q044 = forms.ChoiceField(label=form_label.VENTILATION, choices=form_db_lookups[10], required = False)
    Q058 = forms.ChoiceField(label=form_label.CHILD_PROTECTION_POLICY, choices=form_db_lookups[11], required = False)
    Q138 = forms.ChoiceField(label=form_label.FIRST_AID_BOX_AVAILABLE, choices=form_db_lookups[14], required = False)
    Q139 = forms.ChoiceField(label=form_label.QUALITY_OF_FIRST_AID_BOX_CONTENTS, choices=form_db_lookups[10], required = False)
    Q140 = forms.ChoiceField(label=form_label.SICK_BAY_PRESENT, choices=form_db_lookups[14], required = False)
    Q141 = forms.ChoiceField(label=form_label.TRAINED_PERSONNEL_IN_SICK_BAY, choices=form_db_lookups[14], required = False)
    Q142 = forms.ChoiceField(label=form_label.SICK_BAY_CONDITION, choices=form_db_lookups[10], required = False)
    Q143 = forms.ChoiceField(label=form_label.CHILDREN_IN_NEED_OF_MEDICAL_TX_TAKEN_TO, choices=form_db_lookups[31], required = False)
    Q144 = forms.CharField(widget=forms.TextInput(attrs={'min':'0'}),label=form_label.DISTANCE_TO_NEAREST_MEDICAL_FACILITY, required = False)
    Q145 = forms.ChoiceField(label=form_label.HOW_ARE_CHILDREN_TRANSPORTED_TO_MED_FACILITY, choices=form_db_lookups[32], required = False)
    Q146 = forms.CharField(max_length=255, label=form_label.NOTES_ON_SLEEPING_SAFETY, required = False)
    '''Recreation facilities'''
    Q041 = forms.ChoiceField(label=form_label.RECREATION_FACILITIES, choices=form_db_lookups[10], required = False)
    Q046 = forms.ChoiceField(label=form_label.MENTAL_GAMES, choices=form_db_lookups[11], required = False)
    Q047 = forms.ChoiceField(label=form_label.PHYSICAL_GAMES, choices=form_db_lookups[11], required = False)
    Q048 = forms.ChoiceField(label=form_label.INDOOR_GAMES, choices=form_db_lookups[11], required = False)
    Q049 = forms.ChoiceField(label=form_label.OUTDOOR_GAMES, choices=form_db_lookups[11], required = False)
    Q147 = forms.ChoiceField(label=form_label.ARE_GAMES_AGE_APPROPRIATE, choices=form_db_lookups[14], required = False)
    Q050 = forms.CharField(max_length=255, label=form_label.NOTES_ON_RECREATIONAL_ACTIVITIES_FACILITIES, required = False)
    '''Education and rehabilitation'''    
    Q148 = forms.MultipleChoiceField(label=form_label.FORMAL_EDUCATION_OFFERED, widget = forms.CheckboxSelectMultiple(), choices=form_db_lookups[33], required = False)
    Q149 = forms.CharField(widget=forms.TextInput(attrs={'min':'0'}),label=form_label.HIGHEST_GRADE_OFFERED, required = False)
    Q051 = forms.MultipleChoiceField(label=form_label.EDUCATIONAL_REHABILITATION_PROGRAMES_OFFERED, widget = forms.CheckboxSelectMultiple(), choices=form_db_lookups[12], required = False)#, choices = db_fieldchoices.get_answer_set(3)
    Q052 = forms.CharField(max_length=255, label=form_label.NOTES_ON_EDUCATIONAL_REHABILITATION_PROGRAMMES, required = False)
    
    #Q045 = forms.CharField(label=form_label.NOTES_ON_INFRASTRUCTURE, required = False)
    ''' Section F '''
    Q057 = forms.ChoiceField(label=form_label.WORK_PLAN, choices=form_db_lookups[11], required = False)
    Q059 = forms.ChoiceField(label=form_label.ADMINISTRATIVE_RECORDS, choices=form_db_lookups[11], required = False)
    Q060 = forms.ChoiceField(label=form_label.BANK_ACCOUNT, choices=form_db_lookups[11], required = False)
    Q061 = forms.CharField(max_length=255, label=form_label.NOTES_ON_ADMINISTRATION, required = False)
    Q062 = forms.MultipleChoiceField(label=form_label.SOURCES_OF_INCOME, widget = forms.CheckboxSelectMultiple(), choices=form_db_lookups[13], required = False)
    Q063 = forms.CharField(max_length=255, label=form_label.NOTES_ON_SOURCES_OF_INCOME, required = False)    
    ''' Section G '''
    Q064 = forms.CharField(max_length=255, label=form_label.GENERAL_REMARKS, required = False)
    Q065 = forms.CharField(max_length=255, label=form_label.INSPECTOR_RECOMMENDATIONS, required = False)
    Q066 = forms.ChoiceField(label=form_label.ADHERENCE_TO_MINIMUM_STANDARDS, choices=form_db_lookups[14], required = False)
    
    
    def __init__(self, user, *args, **kwargs):
        self.user = user
        super(ResidentialInstitutionForm, self).__init__(*args, **kwargs)
        
        '''
        if 'subject_id' not in args:
            org_system_id = kwargs[1]['subject_id']
            Q032 = forms.ChoiceField(required=True, label=form_label.REPORTED_BY, choices=cleanup_reported_by_options(org_system_id))
        '''
        
        self.helper = FormHelper()
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-md-3'
        self.helper.field_class = 'col-md-6'
        self.helper.form_action = '/capture/add_form/'
        
        self.helper.layout = \
            Layout(
               Div(
                   Div(
                       HTML(form_label.HEADING_ORGANISATION_DETAILS),
                       css_class='panel-heading'
                       ),
                   Div(
                       HTML('{% include "ovc_main/general_ui/organisation_info.html" %}'),
                       css_class='panel-body'
                       ),
                   css_class='panel panel-primary'
                   ),
                Div(
                    Div(
                        HTML(form_label.HEADING_SECTION_A),
                        css_class='panel-heading'
                        ),
                    Div(
                        HTML('<p/>'),
                        'date_of_form',
                        'Q032',
                        'ward_id',
                        'form_id',
                        HTML(""" 
                        <input id="id_subject" name="subject" type="hidden" value="{{subject.org_system_id}}">
                        <input id="id_form_type" name="form_type" type="hidden" value=\"""" + \
                            db_constants.form_type_residential_institution + """\">
                        """),
                        Field('user_workforce_filled_form', style="display:inline", css_class="residential_institute_autocompletewfc form-inline", id="id_user_workforce_filled_form"),
                        Field('user_workforce_filled_form_hidden', style="display:None", css_class="form-inline", id="id_user_workforce_filled_form_hidden"),
                        Field('person_filled_form_text', style="display:None", css_class="form-inline", id="id_person_filled_form_text"),
                        HTML('<input type = \'hidden\' id = \'id_org_system\' value={{ subject.org_id }}>'),
                        css_class='panel panel-body'
                    ),
                    css_class='panel panel-primary',
                ),
                TabHolder(
                        Tab('Section B',
                            section_b,
                            css_class='row',
                        ),
                        Tab('Section C',
                           section_c,
                           css_class='row',
                        ),
                         Tab('Section D',
                            section_d,
                            css_class='row',
                        ),
                        Tab('Section E F G',
                            section_e,
                            section_f,
                            section_g,
                            css_class='row',
                        )
                    ),
                Div(
                    ButtonHolder(
                        HTML("<Button type=\"submit\" onclick=\"updateFinale(event)\" name=\"submit\" style=\"margin-right:4px\" class=\"btn btn-primary\"><i class=\"fa fa-floppy-o\"></i> "+form_label.LABEL_BUTTON_SUBMIT+"</Button>"),
                        HTML("""<a type=\"button\" href=\"/capture/forms_home/\"class=\"btn btn-success\" ><i class="fa fa-times-circle-o"></i> """+form_label.LABEL_BUTTON_CANCEL+"""</a>"""),
                    ),
                    style='margin-left:41%'
                    ),
            )
    
    '''
    Server side validations for this page are handled here
    '''
    def clean(self):      
        cleaned_data = super(ResidentialInstitutionForm, self).clean()
        '''
        person filled form is a mandatory field
        '''
        number_fields = ["Q035","Q036","Q037","Q119","Q121","Q122","Q123","Q126","Q127","Q128","Q056","Q135","Q144","Q149", "Q134"]
        for field in number_fields:
            if cleaned_data.get(field):
                print field,'field'
                val = cleaned_data.get(field)
                ''' Q134 is a decimal and all other numeric fields are integer '''
                if field == 'Q134':
                    from decimal import Decimal
                    try:
                        val_decimal = Decimal(val)
                        if val_decimal < 0.0:
                            self._errors[field] = self.error_class([form_label.ERROR_MESSAGE_ENTER_NUMBER])
                        c= val_decimal % 1
                        if not c >= 0.0:
                            self._errors[field] = self.error_class([form_label.ERROR_MESSAGE_ENTER_NUMBER])
                    except InvalidOperation as ex:
                        self._errors[field] = self.error_class([form_label.ERROR_MESSAGE_ENTER_NUMBER])
                else:
                    if not val.isdigit():
                        self._errors[field] = self.error_class([form_label.ERROR_MESSAGE_ENTER_NUMBER])

        if cleaned_data.get('user_workforce_filled_form_hidden') != None:
            user_workforce_filled_form_hidden_val = cleaned_data.get('user_workforce_filled_form_hidden')
            if not user_workforce_filled_form_hidden_val:
                self._errors["user_workforce_filled_form"] = self.error_class([form_label.ERROR_MESSAGE_PERSON_FILLED_PAPER_REQUIRED])
        '''
        Cannot be in the future.  Cannot be before the selected org unit's "date became 
        operational" (if any) and cannot be after selected org unit's "date closed" (if any)
        '''
        date_of_form_val = None
        org_id_val = None
        if cleaned_data.get('date_of_form') != None:
            date_of_form_val = cleaned_data.get('date_of_form')
        if cleaned_data.get('subject') != None:
            org_id_val = cleaned_data.get('subject')
        if org_id_val and date_of_form_val:
            if len(RegOrgUnit.objects.filter(pk=org_id_val, is_void=False)) > 0:
                m_org = RegOrgUnit.objects.get(pk=org_id_val, is_void=False)
                if m_org.date_operational:
                    if date_of_form_val < m_org.date_operational:
                        self._errors["date_of_form"] = self.error_class([form_label.ERROR_MESSAGE_DATE_LESS_THAN_DATE_OPERATIONAL])
                if m_org.date_closed:
                    if date_of_form_val > m_org.date_closed:
                        self._errors["date_of_form"] = self.error_class([form_label.ERROR_MESSAGE_DATE_GREATER_THAN_DATE_CLOSED])                   
        '''
        minimum_age_of_enrollment: Number between 0 and 17
        maximum_age_of_enrollment: Number between 0 and 80
        institutional_capacity: Number greater than 0
        '''
        minimum_age_of_enrollment_val = ''
        maximum_age_of_enrollment_val = ''

        if cleaned_data.get('Q035'):
            minimum_age_of_enrollment_val = cleaned_data.get('Q035')
            if str(minimum_age_of_enrollment_val).isdigit():
                if not (int(minimum_age_of_enrollment_val) >= 0 and int(minimum_age_of_enrollment_val) <= 17):
                    self._errors["Q035"] = self.error_class([form_label.ERROR_MESSAGE_MINIMUM_AGE_RANGE]) 
        
        if cleaned_data.get('Q036'):
            #print 'Maximum fimo'
            maximum_age_of_enrollment_val = cleaned_data.get('Q036')
            if str(maximum_age_of_enrollment_val).isdigit():
                if int(maximum_age_of_enrollment_val) < 0 or int(maximum_age_of_enrollment_val) > 80:
                    self._errors["Q036"] = self.error_class([form_label.ERROR_MESSAGE_MAXIMUM_AGE_RANGE])
        
        if cleaned_data.get('Q037'):
            institutional_capacity_val = cleaned_data.get('Q037')
            if str(institutional_capacity_val).isdigit():
                if int(institutional_capacity_val) < 0:
                    self._errors["Q037"] = self.error_class([form_label.ERROR_MESSAGE_INSTITUTIONAL_CAPACITY])

        if str(minimum_age_of_enrollment_val).isdigit() and maximum_age_of_enrollment_val.isdigit():
            if int(minimum_age_of_enrollment_val) > int(maximum_age_of_enrollment_val):
                self._errors["Q035"] = self.error_class([form_label.ERROR_MESSAGE_MINIMUM_AGE_GT_MAXIMUM_AGE]) 

        if self._errors:
            self._errors["__all__"] = self.error_class([u""+form_label.ERROR_MESSAGE_SAVE_NOT_SUCCESSFUL])
     
        return self.cleaned_data

    def cleanup_reported_by_options(self, org_id_int):
        '''
        Report by institution is only available for selection if 
            a) the logged in user has this institution as a parent unit or 
            b) the logged in user has a data entry permission 1051.  
        "Report by external inspector" is only availabe for selection 
            a) if the logged in user has inspector permission 1053 or
            b) if the logged in user has a data entry permission 1051.   
        '''
        all_choices = form_db_lookups[8]
        choices_to_return = []
        
        if self.user.is_super_user:
            return all_choices
        m_person = self.user.reg_person
        org_is_parent_unit = False
        if len(RegPersonsOrgUnits.objects.filter(person=m_person, primary=True, \
            parent_org_unit_id = org_id_int, date_delinked=None, is_void=False)) > 0:
            org_is_parent_unit = True
        if self.user.has_perm('enter frm high sensitive') == False or org_is_parent_unit == False:
            tmp_choices = None
            for choice in all_choices:
                if not choice[0] == db_constants.frm_res_inst_report_by_type_institution_option_id:
                    choices_to_return.append(item)
        return tuple(choices_to_return)
    
'''
The layouts for sections b to g are defined here
'''

section_b = Layout(
    Div(
        Div(
            HTML(form_label.HEADING_SECTION_B),
            css_class='panel-heading'
            ),
        Div(
            HTML('<p/>'),
            'Q033',
            'Q115',
            'Q034',
            'Q035',
            'Q036',
            'Q037',
            Div(
                HTML("<br><a type=\"button\" onclick=\"activateTab('section-c')\" class=\"btn btn-info\">"+form_label.LABEL_BUTTON_NEXT_TAB_SEC_B+"</a><br><br>"),
                style='margin-left:41%',
                ),
            css_class='panel panel-body'
        ),
        css_class='panel panel-primary',
    ),
)

section_c = Layout(
    Div(
        Div(
            HTML(form_label.HEADING_SECTION_C),
            css_class='panel-heading'
            ),
        Div(
            HTML('<p/>'),
            Field('child', style="display:inline", css_class="residential_institute_autocompletechild form-inline", id="id_child_autocomplete"),
            Div(
                Field('child_id', css_class="form-inline", id="id_child_id"),
                Field('child_date_of_birth', style="display:None", css_class="form-inline", id="id_child_date_of_birth"),
                Field('child_date_of_death', style="display:None", css_class="form-inline", id="id_child_date_of_death"),
                HTML("""
                    <input id="id_beneficiary_id" class="beneficiary_id_hidden">
                    <input id="id_child_name" class="child_name_hidden">  
                    <input id="id_recent_date_admitted" class="recent_date_admitted">
                    <input id="id_recent_date_left" class="recent_date_left">
                    """),
                    style="display:None",
                    ),
            'new_adverse_condition',
            HTML('<p/>'),
            'current_residential_status',
            'family_status',
            'has_court_committal_order',
            'date_first_admitted',
            Div(
                'date_left',
                style="display:None",
                id="div_id_date_left"
                ),
            Field(Div('child_status_data', style='display:None'), css_class='form-inline mandatory'),
            HTML("<label id=\"valid_data\" style=\"color:red;margin-left:50px;display:text\"><strong>  </strong></label>"),
            Div(
                HTML("<br><a type=\"button\" onclick=\"addChildStatus()\" class=\"btn btn-info\"><i class=\"fa fa-plus\"></i>"+form_label.LABEL_BUTTON_ADD+"</a><br><br>"),
                style='margin-left:41%',
                ),
            Div(
                HTML("""
                <table id="childStatusTable"  class="table table-hover table-condensed table-bordered">
                    <thead>
                    <tr>
                        <th style="width:1%;display:None"></th>
                        <th>""" + form_label.BENEFICIARY_ID_BEN_TBL_HEADER + """</th>
                        <th>""" + form_label.NAME_BEN_TBL_HEADER + """</th>
                        <th>""" + form_label.NEW_ADVERSE_CONDITION_BEN_TBL_HEADER + """</th>
                        <th style="width:40%">""" + form_label.CHILD_STATUS_DETAILS_BEN_TBL_HEADER + """</th>
                        <th style="width:1%;display:None"></th>
                        <th style="width:1%;display:None"></th>
                        <th style="width:1%;display:None"></th>
                        <th>""" + form_label.DATE_FIRST_ADMITTED_BEN_TBL_HEADER + """</th>
                        <th>""" + form_label.DATE_LEFT_BEN_TBL_HEADER + """</th>
                        <th>"""+form_label.LABEL_BUTTON_REMOVE+"""</th>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
                
                """), 
                ),
            Div(
                HTML("<br><a type=\"button\" onclick=\"activateTab('section-d')\" class=\"btn btn-info\">"+form_label.LABEL_BUTTON_NEXT_TAB_SEC_C+"</a><br><br>"),
                style='margin-left:41%',
                ),
            css_class='panel panel-body'
            ),
        css_class='panel panel-primary',
    ),
)

section_d = Layout(
    Div(
        Div(
            HTML(form_label.HEADING_SECTION_D),
            css_class='panel-heading'
            ),
        Div(
            HTML('<p/>'),
            Field(Div('workforce_roles', style='display:None'), css_class='form-inline mandatory'),
            Field(Div('workforce_employment_status', style='display:None'), css_class='form-inline mandatory'),
            #Field(Div('workforce_assessment_filled_out', style='display:None'), css_class='form-inline mandatory'),
            Field(Div('workforce_data', style='display:None'), css_class='form-inline mandatory'),
            Div(
                HTML("""
                <table id="workforceStatusTable"  class="table table-hover table-condensed table-bordered">
                    <thead>
                    <tr>
                        <th>""" + form_label.WORKFORCE_ID_WORKFORCE_TBL_HEADER + """</th>
                        <th>""" + form_label.NAME_WORKFORCE_TBL_HEADER + """</th>
                        <th>""" + form_label.POSITION_OR_ROLE_TBL_HEADER + """</th>
                        <th>""" + form_label.EMPLOYMENT_STATUS_TBL_HEADER + """</th>
                    </tr>
                    </thead>
                    <tbody>
                    
                    </tbody>
                </table>"""), 
                ),
            Div(
                HTML("<br><a type=\"button\" onclick=\"activateTab('section-e-f-g')\" class=\"btn btn-info\">"+form_label.LABEL_BUTTON_NEXT_TAB_SEC_D+"</a><br><br>"),
                style='margin-left:41%',
                ),
            css_class='panel panel-body'
        ),
        css_class='panel panel-primary',
    ),
)

section_e = Layout(
    Div(
        Div(
            HTML(form_label.HEADING_SECTION_E),
            css_class='panel-heading'
            ),
        Div(
            HTML('<p/>'),
            #Water and hygiene
            'Q039',
            'Q116',
            'Q117',
            'Q118',
            'Q119',
            'Q120',
            'Q040',
            'Q121',
            'Q122',
            'Q123',
            'Q124',
            'Q125',
            'Q126',
            'Q127',
            'Q128',
            'Q129',
            'Q130',
            'Q131',
            #Food and nutrition
            'Q038',
            'Q053',
            'Q054',
            'Q055',
            'Q056',
            #Sleeping facilities
            'Q132',
            'Q133',
            'Q134',
            'Q135',
            'Q136',
            'Q137',
            #Health and safety
            'Q042',
            'Q043',
            'Q044',
            'Q058',
            'Q138',
            'Q139',
            'Q140',
            'Q141',
            'Q142',
            'Q143',
            'Q144',
            'Q145',
            'Q146',
            #Recreation facilities
            'Q041',
            'Q046',
            'Q047',
            'Q048',
            'Q049',
            'Q147',
            'Q050',
            #Education and rehabilitation
            'Q148',
            'Q149',
            'Q051',
            'Q052',
            css_class='panel panel-body'
        ),
        css_class='panel panel-primary',
    ),
)

section_f = Layout(
    Div(
        Div(
            HTML(form_label.HEADING_SECTION_F),
            css_class='panel-heading'
            ),
        Div(
            HTML('<p/>'),
            'Q057',
            #InlineRadios('Q058'),
            'Q059',
            'Q060',
            'Q061',
            'Q062',
            'Q063',
            css_class='panel panel-body'
        ),
        css_class='panel panel-primary',
    ),
)

section_g = Layout(
    Div(
        Div(
            HTML(form_label.HEADING_SECTION_G),
            css_class='panel-heading'
            ),
        Div(
            HTML('<p/>'),
            'Q064',
            Div(
                Field('Q065', css_class="form-inline", id="id_Q065"),
                Field('Q066', css_class="form-inline", id="id_Q066"),
                style="display:None",
                id="div_id_Q065_066"
                ),
            css_class='panel panel-body'
        ),
        css_class='panel panel-primary',
    ),
)

