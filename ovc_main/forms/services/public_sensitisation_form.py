from django import forms
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Fieldset,\
    ButtonHolder, Submit, Button, HTML, MultiField, Div, Field
    
from functools import partial
DateInput = partial(forms.DateInput, {'class': 'datepicker'})
from crispy_forms.bootstrap import FormActions, AppendedText
from ovc_main.utils import validators as validate_helper
from ovc_main.utils import fields_list_provider as db_fieldchoices, lookup_field_dictionary as db_constants
import constants_public_sensitisation as form_label
from ovc_main.models import Forms
'''
Call get_answer_set() with the dictionary of answer_set_ids and add empty option boolean that you will need for all the questions on the page.
In the control itself access the choice list by index as shown below

choices = form_db_lookups[9]
'''
answer_set_ids_add_empty_list = {8:True, 9:False, 10:False, 11:False, 12:False, 13:False, 14:True, 19:True, 20:True, 21:False}
form_db_lookups = db_fieldchoices.get_answer_set(answer_set_ids = answer_set_ids_add_empty_list)

class PublicSensitisationForm(forms.Form):
    ''' General fields '''
    form_type = forms.CharField(required = False,widget=forms.HiddenInput)
    form_id = forms.CharField(required = False,widget=forms.HiddenInput)
    ''' Section A '''
    date_event_started = forms.DateField(widget=DateInput(), input_formats=validate_helper.date_input_formats_tpl, label = form_label.DATE_EVENT_STARTED,validators=[validate_helper.validate_general_date])
    date_event_ended = forms.DateField(widget=DateInput(), input_formats=validate_helper.date_input_formats_tpl, label = form_label.DATE_EVENT_ENDED,validators=[validate_helper.validate_general_date])
    duration_of_event = forms.DecimalField(decimal_places=1, min_value=0, widget=forms.NumberInput(attrs={'decimal_places':'1'}), required=False, label=form_label.DURATION_OF_EVENT)
    duration_units = forms.ChoiceField(label='.', choices=(('', '-----'),(1, 'Hours'), (2, 'Days')), required=False)
    Q086 = forms.CharField(required = False, widget=forms.HiddenInput)
    Q087 = forms.CharField(required = False, widget=forms.HiddenInput)
    event_title = forms.CharField(max_length=255, label = form_label.EVENT_TITLE)
    Q089 = forms.ChoiceField(label = form_label.GEOGRAPHICAL_FOCUS, choices = form_db_lookups[19], required=False)
    district = forms.ChoiceField(label = form_label.DISTRICT, choices = db_fieldchoices.get_list_of_districts(), required=False)
    ward = forms.ChoiceField(label = form_label.WARD, choices = db_fieldchoices.get_list_of_wards(), required=False)
    Q090 = forms.ChoiceField(label = form_label.TYPE_OF_EVENT, choices=form_db_lookups[20])
    ''' Section B '''
    Q091 = forms.MultipleChoiceField(choices=form_db_lookups[21], \
                                    widget=forms.SelectMultiple(attrs={'class':'multiselect-searchable'}), label=form_label.TOPICS_COVERED)
    Q092 = forms.CharField(max_length=255, label = form_label.EVENT_DESCRIPTION, required=False)
    ''' Section C '''
    organisational_unit = forms.CharField(label=form_label.ORGANISATIONAL_UNIT, required=False) 
    #organisational_unit_id = forms.CharField(label='', required = False)
    organisation_contribution = forms.MultipleChoiceField(label='.', widget = forms.CheckboxSelectMultiple(), choices = db_fieldchoices.get_item_id_and_description_for_cat(item_category='Contribution'), required = False)
    org_contributions_data = forms.CharField(label='',required=False)
    
    ''' Section D '''
    beneficiary = forms.CharField(label=form_label.BENEFICIARY, required=False)
    participation_level = forms.ChoiceField(label = form_label.PARTICIPATION_LEVEL, required=False, choices = db_fieldchoices.get_item_id_and_description_for_cat(item_category = 'Participation level', add_initial=True))
    beneficiary_data = forms.CharField(label='',required=False)

    Q093 = forms.DecimalField(decimal_places=0, min_value=0, widget=forms.TextInput(attrs={'type':'number'}), label='', required=False)
    Q094 = forms.DecimalField(decimal_places=0, min_value=0, widget=forms.TextInput(attrs={'type':'number'}), label='', required=False)
    Q095 = forms.DecimalField(decimal_places=0, min_value=0, widget=forms.TextInput(attrs={'type':'number'}), label='', required=False)
    Q096 = forms.DecimalField(decimal_places=0, min_value=0, widget=forms.TextInput(attrs={'type':'number'}), label='', required=False)
    Q097 = forms.DecimalField(decimal_places=0, min_value=0, widget=forms.TextInput(attrs={'type':'number'}), label='', required=False)

    def __init__(self, user, *args, **kwargs):
        self.is_update_page = False
        self.form_id_int = ''
        if len(args) > 0 and args[0]: 
            if 'form_id' in args[0]:  
                self.form_id_int = args[0]['form_id']
                if self.form_id_int:
                    self.is_update_page = True

        super(PublicSensitisationForm, self).__init__(*args, **kwargs)
        
        self.helper = FormHelper()
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-md-3'
        self.helper.field_class = 'col-md-6'
        self.helper.form_action = '/capture/add_form/'
        
        self.helper.layout = \
            Layout(
                Div(
                    Div(
                        HTML(form_label.SECTION_A),
                        css_class='panel-heading'
                        ),
                    Div(
                        HTML('<p/>'),
                        'form_id',
                        'date_event_started',
                        'date_event_ended',
                        Div(
                            Field('duration_of_event', template='ovc_main/custom_duration_control.html', style = ''),
                            Field('duration_units', template='ovc_main/custom_duration_units_control.html', style = ''),
                            css_class="form-group",
                            ), 
                        #'duration_of_event',
                        #'duration_units',
                        'Q086',
                        'Q087',
                        'event_title',
                        'Q089',
                        Field(Div('district', style='display:None', id='div_id_district_parent'), css_class='form-inline mandatory'),
                        Field(Div('ward', style='display:None', id='div_id_ward_parent'), css_class='form-inline mandatory'),
                        'Q090',
                        HTML(""" 
                        <input id="id_form_type" name="form_type" type="hidden" value=\"""" + \
                            db_constants.form_type_public_sensitization + """\">
                        """),
                        css_class='panel panel-body'
                    ),
                    css_class='panel panel-primary',
                ),
                Div(
                    Div(
                        HTML(form_label.SECTION_B),
                        css_class='panel-heading'
                        ),
                    Div(
                        HTML('<p/>'),
                        'Q091',
                        'Q092',
                        css_class='panel panel-body'
                    ),
                    css_class='panel panel-primary',
                ),
                Div(
                    Div(
                        HTML(form_label.SECTION_C),
                        css_class='panel-heading'
                        ),
                    Div(
                        HTML('<p/>'),
                        Field('organisational_unit', style="display:inline", css_class="organisation_autocomplete form-inline", id="id_organisational_unit"),
                        HTML("""    <input id="id_org_unit_id" class="organisation_hidden" type="hidden">
                            <input id="id_org_unit_name" class="organisation_hidden" type="hidden">  
                            """),
                        #Field('organisational_unit_id', style="display:None", css_class="form-inline", id="organisational_unit_id"),
                        'organisation_contribution',
                        Field(Div('org_contributions_data', style='display:None'), css_class='form-inline mandatory'),
                        HTML("<label id=\"valid_orgs_data\" style=\"color:red;margin-left:50px;display:text\"><strong>  </strong></label>"),
                        Div(
                            HTML("<br><a type=\"button\" onclick=\"addOrgsData()\" class=\"btn btn-info\"><i class=\"fa fa-plus\"></i>"+form_label.LABEL_BUTTON_ADD+"</a><br><br>"),
                            style='margin-left:41%',
                            ),
                        Div(
                            HTML("""
                            <table id="organisationContributionsTable"  class="table table-hover table-condensed table-bordered">
                                <thead>
                                <tr>
                                    <th>""" + form_label.TBL_HEADER_ORG_UNIT + """</th>
                                    <th>""" + form_label.TBL_HEADER_ORG_UNIT_ID + """</th>
                                    <th>""" + form_label.TBL_HEADER_CONTRIBUTIONS + """</th>
                                    <th style="display: None">""" + form_label.TBL_HEADER_CONTRIBUTIONS_ID + """</th>
                                    <th>""" + form_label.LABEL_BUTTON_REMOVE + """</th>
                                </tr>
                                </thead>
                                <tbody>
                    
                                </tbody>
                            </table>"""), 
                            ),
                        css_class='panel panel-body'
                    ),
                    css_class='panel panel-primary',
                ),
                Div(
                    Div(
                        HTML(form_label.SECTION_D),
                        css_class='panel-heading'
                        ),
                    Div(
                        HTML('<p/>'),
                        Field('beneficiary', style="display:inline", css_class="beneficiary_autocomplete form-inline", id="id_beneficiary"),
                        HTML("""    <input id="person_id" class="beneficiary_hidden" type="hidden">
                                    <input id="beneficiary_id" class="beneficiary_hidden" type="hidden">  
                                    <input id="birth_reg_id" class="beneficiary_hidden" type="hidden">
                                    <input id="national_id" class="beneficiary_hidden" type="hidden">  
                                    <input id="beneficiary_name" class="beneficiary_hidden" type="hidden">
                                    <input id="age" class="workforce_hidden" type="hidden">  
                                    <input id="sex" class="workforce_hidden" type="hidden">
                                    <input id="id_age" class="workforce_hidden" type="hidden">  
                                    <input id="id_sex" class="workforce_hidden" type="hidden">
                                    """),
                        'participation_level',
                        Field(Div('beneficiary_data', style='display:None'), css_class='form-inline mandatory'),
                        HTML("<label id=\"valid_beneficiary_data\" style=\"color:red;margin-left:50px;display:None\"><strong>  </strong></label>"),
                        Div(
                            HTML("<br><a type=\"button\" onclick=\"addBeneficiaryData()\" class=\"btn btn-info\"><i class=\"fa fa-plus\"></i>"+form_label.LABEL_BUTTON_ADD+"</a><br><br>"),
                            style='margin-left:41%',
                            ),
                        Div(
                            HTML("""
                            <table id="beneficiaryParticipationTable"  class="table table-hover table-condensed table-bordered">
                                <thead>
                                <tr>
                                    <th style="display: None">""" + form_label.TBL_HEADER_PERSON_ID + """</th>
                                    <th>""" + form_label.TBL_HEADER_BENEFICIARY_ID + """</th>
                                    <th>""" + form_label.TBL_HEADER_BENEFICIARY_BIRTH_CERT_OR_NRC + """</th>
                                    <th style="display: None">""" + form_label.TBL_HEADER_BENEFICIARY_BIRTH_REG_ID + """</th>
                                    <th style="display: None">""" + form_label.TBL_HEADER_BENEFICIARY_NATIONAL_ID + """</th>
                                    <th>""" + form_label.TBL_HEADER_BENEFICIARY_NAME + """</th>
                                    <th>""" + form_label.TBL_HEADER_PARTICIPATION_LEVEL + """</th>
                                    <th style="display: None">""" + form_label.TBL_HEADER_PARTICIPATION_LEVEL_ID + """</th>
                                    <th style="display: None">""" + form_label.TBL_HEADER_PARTICIPATION_SEX + """</th>
                                    <th style="display: None">""" + form_label.TBL_HEADER_PARTICIPATION_AGE + """</th>
                                    <th>""" + form_label.LABEL_BUTTON_REMOVE + """</th>
                                </tr>
                                </thead>
                                <tbody>
                    
                                </tbody>
                            </table>"""), 
                            ),
                        Div(
                            HTML('<p/>'),
                            HTML("""
                                <label for="id_estimate_of_people_reached" class="control-label col-md-3">
				                    Estimate of people reached:
			                    </label>
                                <div class = "controls col-md-6">
                                    <table id="organisationContributionTable"  class="table table-hover table-condensed ">
                                        <thead>
                                        <tr>
                                            <th></th>
                                            <th>""" + form_label.TBL_HEADER_MALE + """</th>
                                            <th>""" + form_label.TBL_HEADER_FEMALE + """</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            <tr> 
                                                <td>""" + form_label.TBL_HEADER_ADULTS + """</td>
                                                <td>"""), 
                                                    Field('Q093', style="display:text", css_class="form-inline counts", id="id_Q093"),
                                                HTML("""</td>
                                                <td>"""), 
                                                    Field('Q094', style="display:text", css_class="form-inline counts", id="id_Q094"),
                                                HTML("""</td>
                                            </tr>
                                            <tr> 
                                                <td>""" + form_label.TBL_HEADER_CHILDREN + """</td>
                                                <td>"""), 
                                                    Field('Q095', style="display:text", css_class="form-inline counts", id="id_Q095"),
                                                HTML("""</td>
                                                <td>"""), 
                                                   Field('Q096', style="display:text", css_class="form-inline counts", id="id_Q096"),
                                                HTML("""</td>
                                            </tr>
                                            <tr> 
                                            <!--td  colspan="3"></td-->
                                            </tr>
                                            <tr> 
                                                <td>""" + form_label.TBL_HEADER_TOTAL + """</td>
                                                <td colspan="2">"""), 
                                                    'Q097',
                                                HTML("""</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                 </div>"""), 
                            ),
                        css_class='panel panel-body'
                    ),
                    css_class='panel panel-primary',
                ),
                Div(
                    ButtonHolder(
                        HTML("<Button type=\"submit\" onclick=\"updateFinale(event)\" name=\"submit\" style=\"margin-right:4px\" class=\"btn btn-primary\"><i class=\"fa fa-floppy-o\"></i> "+form_label.LABEL_BUTTON_SUBMIT+"</Button>"),
                        HTML("""<a type=\"button\" href=\"/capture/forms_home/\"class=\"btn btn-success\" ><i class="fa fa-times-circle-o"></i> """+form_label.LABEL_BUTTON_CANCEL+"""</a>"""),
                    ),
                    style='margin-left:41%'
                    ),
            )
    
    '''
    Server side validations for this page are handled here
    '''
    def clean(self):      
        cleaned_data = super(PublicSensitisationForm, self).clean()     

        date_event_started = None
        date_event_ended = None
        event_title = None
        duration_of_event = None
        duration_units = None

        if cleaned_data.get('duration_of_event'):
            duration_of_event = cleaned_data.get('duration_of_event')

        if cleaned_data.get('duration_units'):
            duration_units = cleaned_data.get('duration_units')

        if duration_of_event and not duration_units:
            self._errors["duration_units"] = self.error_class([form_label.ERROR_MESSAGE_NO_DURATION_UNITS])
        if duration_units and not duration_of_event:
            self._errors["duration_units"] = self.error_class([form_label.ERROR_MESSAGE_NO_DURATION])

        if cleaned_data.get('date_event_started'):
            date_event_started = cleaned_data.get('date_event_started')
        if cleaned_data.get('date_event_ended'):
            date_event_ended = cleaned_data.get('date_event_ended')

        if date_event_started and date_event_ended:
            if date_event_started > date_event_ended: 
                self._errors["date_event_ended"] = self.error_class([form_label.ERROR_MESSAGE_DATE_ENDED_GT_DATE_STARTED])

        if cleaned_data.get('event_title'):
            event_title = cleaned_data.get('event_title')
            m_form = Forms.objects.filter(form_title=event_title, is_void=False)
            dups_detected = False
            if self.form_id_int:
                for form in m_form:
                    if form.pk == int(self.form_id_int):
                        dups_detected = False
                    else:
                        dups_detected = True
                        break
            else:
                if len(m_form) > 0:
                    dups_detected = True
            if dups_detected:
                self._errors["event_title"] = self.error_class([form_label.ERROR_MESSAGE_DUPLICATE_EVENT_TITLE]) 
        if (cleaned_data.get('org_contributions_data') != None):
            value = cleaned_data.get('org_contributions_data')
            if not value or value == "{}":
                self._errors["organisational_unit"] = self.error_class([form_label.ERROR_MESSAGE_ENTER_ATLEAST_ONE_ORG_CONTRIBUTION])
        
        male_adults = 0
        female_adults = 0
        male_children = 0
        female_children = 0
        total = 0
        if (cleaned_data.get('Q093') != None):
            male_adults = int(cleaned_data.get('Q093'))
        if (cleaned_data.get('Q094') != None):
            female_adults = int(cleaned_data.get('Q094'))
        if (cleaned_data.get('Q095') != None):
            male_children = int(cleaned_data.get('Q095'))
        if (cleaned_data.get('Q096') != None):
            female_children = int(cleaned_data.get('Q096'))
        if (cleaned_data.get('Q097') != None):
            total = int(cleaned_data.get('Q097'))
        cal_sum = male_adults + female_adults + male_children + female_children
        if total < cal_sum:
            self._errors["Q097"] = self.error_class([form_label.ERROR_MESSAGE_TOTAL_LT_CALCULATED_SUM])

        if self._errors:
            self._errors["__all__"] = self.error_class([u""+form_label.ERROR_MESSAGE_SAVE_NOT_SUCCESSFUL])
          
        return self.cleaned_data
'''
duration_controls= Layout(
    Div(
        HTML("""
            <label for="id_estimate_of_people_reached" class="control-label col-md-3">
				Duration of training: 
			</label>
            <div class = "row">"""), 
                    Field('duration_of_event', 
                            style="display:text", 
                            css_class="form-inline counts form-control col-md-5", 
                            id="id_duration_of_event"),
                    Field('duration_units', 
                            style="display:text", 
                            css_class="form-inline counts form-control col-md-4", 
                            id="id_duration_units"),
        ),
)
'''