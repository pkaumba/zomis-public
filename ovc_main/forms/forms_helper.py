from ovc_main.models import ListQuestions, ListAnswers, FormGenAnswers, FormGenNumeric, \
        FormGenText, RegPersonsGeo, Forms, CoreAdverseConditions, SetupList, RegPerson, \
        SetupGeorgraphy, FormCsi, RegOrgUnit, FormResChildren, FormResWorkforce, \
        AdminUploadForms, RegPersonsOrgUnits, CoreEncounters, CoreServices, \
        FormOrgUnitContributions, FormPersonParticipation, FormGenDates, CoreEncountersNotes
from ovc_main.utils import lookup_field_dictionary as db_text, idgenerator, \
        fields_list_provider as field_provider, validators as val, geo_location
import datetime
import time
from ovc_auth.models import AppUser
from django.db import transaction
import json
from django.conf import settings
from django.db.models import Q
import operator 
import traceback

class Form:
    form_pk = None,
    form_guid = None,
    form_type_id = None,
    form_subject_id = None,
    form_area_id = None,
    form_title = None,
    date_began = None,
    date_ended = None,
    date_filled_paper = None,
    person_id_filled_paper = None,
    wfc_id_hidden = None,
    org_unit_id_filled_paper = None,
    capture_site_id = None,
    timestamp_created = None,
    user_id_created = None,
    timestamp_updated = None,
    user_id_updated = None,
    questions = None
    
    def __init__(
                    self, 
                    form_pk,
                    form_guid,
                    form_type_id,
                    form_subject_id,
                    form_area_id,
                    form_title,
                    date_began,
                    date_ended,
                    date_filled_paper,
                    person_id_filled_paper,
                    wfc_id_hidden,
                    org_unit_id_filled_paper,
                    capture_site_id,
                    timestamp_created,
                    user_id_created,
                    timestamp_updated,
                    user_id_updated,
                    questions 
                ):
                self.form_pk = form_pk
                self.form_guid = form_guid
                self.form_type_id = form_type_id
                self.form_subject_id = form_subject_id
                self.form_area_id = form_area_id
                self.form_title = form_title
                self.date_began = date_began
                self.date_ended = date_ended
                self.date_filled_paper = date_filled_paper
                self.person_id_filled_paper = person_id_filled_paper
                self.wfc_id_hidden = wfc_id_hidden
                self.org_unit_id_filled_paper = org_unit_id_filled_paper
                self.capture_site_id = capture_site_id
                self.timestamp_created = timestamp_created
                self.user_id_created = user_id_created
                self.timestamp_updated = timestamp_updated
                self.user_id_updated = user_id_updated
                self.questions = questions
    
    
class Question:
    question_id = None
    question_text = None
    question_code = None
    posible_answers = None
    answer_type = None
    answer_set_id = None
    answers = None
    
    def __init__(self, question_id, question_text, question_code, posible_answers, answer_type, answer_set_id, answers):
            self.question_id = question_id
            self.question_text = question_text
            self.question_code = question_code
            self.posible_answers = posible_answers
            self.answer_type = answer_type
            self.answer_set_id = answer_set_id
            self.answers = answers
            
class Answer:
    answer_id = None
    answer = None
    
    def __init__(self, answer_id, answer):
            self.answer_id = answer_id
            self.answer = answer
            
logged_in_user = None


@transaction.commit_on_success
def save_form(cleaned_data, user, form_t)  :
    print '''
    This is the entry point into the save. All methods in relation to saving are called from here.
    '''
    form_id = False
    if cleaned_data.has_key('form_id'):
        form_id = cleaned_data['form_id']
    
    if form_id:
        void_old_form(form_id)
    print 'cleaned_data',cleaned_data
    global logged_in_user
    logged_in_user = user
    try:
        form_save_method = {'FT3e': save_basic_service_referral}
        if form_t in form_save_method:
            form = form_save_method[form_t](cleaned_data, user, form_id)
        else:
            form = general_save_form(form_t, cleaned_data, user, form_id)
        save_to_admin_upload(form.form_pk)
    except Exception as e:
        traceback.print_exc()
        raise e
    return form.form_pk
def save_basic_service_referral(cleaned_data, user, formid=None):
    
    form_obj = create_form_object(cleaned_data,'FT3e')
    form_obj.user_id_created = user.pk
    form = save_form_basic_data_basic_service_referral(form_obj, formid)
    
    beneficiary_id = cleaned_data['beneficiary_id']
    workforce_id = cleaned_data['workforce_system_id']

    encounterid = cleaned_data['encounter_id'] if cleaned_data.has_key('encounter_id') else None

    encounter_type_id = cleaned_data['encounter_type']
    save_core_encounter(workforce_id, beneficiary_id, form.date_filled_paper, 
                        form.org_unit_id_filled_paper, form.form_area_id,  
                        encounter_type_id, formid=form.form_pk, encounterid=encounterid)
    print type(cleaned_data)
    adverseconditions = dict_value_or_empty_list('new_adverse_condition', cleaned_data)
    save_all_adverse_conditions({beneficiary_id:adverseconditions}, form)
    
    services_provided  = dict_value_or_empty_list('services_provided', cleaned_data)
    referrals_made = dict_value_or_empty_list('referrals_made', cleaned_data)
    referrals_completed = dict_value_or_empty_list('referrals_completed', cleaned_data)
    
    all_services = services_provided + referrals_made + referrals_completed
    
    save_services(form, workforce_id, beneficiary_id, form.date_filled_paper, all_services)
    save_to_admin_upload(form.form_pk) 
    return form

def dict_value_or_empty_list(key, dictionary):
    if settings.DEBUG:
        print 'adverse conditions dictionary', dictionary
    return dictionary[key] if key in dictionary else []


def search_basic_service_referral_records(searchtoken, user, workforceid=None, datefilter=None, dateattr=None):

    childlist = field_provider.search_ben_ovc(searchtoken, user, person_type=None)
    childlistids = [child.pk for child in childlist]
    print childlistids
    setup_list_item_category = [db_text.REFERRAL_COMPLETED_CATEGORY_DB_TEXT,
                                db_text.REFERRAL_MADE_CATEGORY_DB_TEXT,
                                db_text.SERVICE_TYPE_CATEGORY_DB_TEXT,
                                db_text.ENCOUNTER_TYPE_CATEGORY_DB_TEXT,
                                db_text.adverse_condition
                                ]
    
    matching_item_ids = field_provider.search_setup_list_by_category(setup_list_item_category, searchtoken)
    adverseovc = CoreAdverseConditions.objects.filter(adverse_condition_id__in=matching_item_ids)
    for adv in adverseovc:
        if adv.beneficiary_person_id in childlistids:
            continue
        childlistids.append(adv.beneficiary_person_id)
        
    serviceovc = CoreServices.objects.filter(core_item_id__in=matching_item_ids)
    for serv in serviceovc:
        if serv.beneficiary_person_id in childlistids:
            continue
        childlistids.append(serv.beneficiary_person_id)
    
    toreturn = []

    if childlistids or searchtoken.strip() == '':
        conditions = core_encounter_query_conditions(datefilter=datefilter, dateattr=dateattr, workforceid=workforceid, beneficiaryids=childlistids)     
        encounters = CoreEncounters.objects.filter(**conditions)
        print encounters
        toreturn = get_service_records_from_encounters(encounters)
    print toreturn
    return toreturn

def core_encounter_query_conditions(datefilter=None, dateattr=None, workforceid=None, form_id=None, beneficiaryids=None, encountertype=['EWRK', 'EELS', 'EHOM']):
    conditions = {}
    if datefilter:
        if dateattr == 'onafter':
            conditions['encounter_date__gte'] = datefilter
        else:
            conditions['encounter_date'] = datefilter
    
    if workforceid and str(workforceid).isdigit():
        conditions['workforce_person_id'] = int(workforceid)
            
    if beneficiaryids:
        conditions['beneficiary_person_id__in'] =  beneficiaryids
        
    if form_id:
        conditions['form_id'] = form_id
    
    if encountertype:
        conditions['encounter_type_id__in'] = encountertype if not isinstance(encountertype, str) else [encountertype]
    
    return conditions

def get_service_records_by_form_id(formid):
    toreturn = []
    if formid and str(formid).isdigit():
        conditions = core_encounter_query_conditions(form_id=formid)     
        encounters = CoreEncounters.objects.filter(**conditions)
        toreturn = get_service_records_from_encounters(encounters)
    
    return toreturn

def get_service_records_from_encounters(encounters):
    toreturn = []
    #encounters = CoreEncounters.objects.filter(form_id=form_id)
    encounter_form_ids = [e.form_id for e in encounters]
    valid_form_ids = []
    if encounter_form_ids:
        valid_form_ids = [tmpform.pk for tmpform in Forms.objects.filter(pk__in=encounter_form_ids, is_void=False)]
    for encounter in encounters:
        '''
        1. if there is an sms_id but the form_id is null or empty, we want to record that encounter
        2. if the form id exists, and is in the list of valid form_ids, we want to record that encounter
        '''
        print 'in get service record'
        if encounter.form_id and encounter.form_id not in valid_form_ids:
            continue
        print 'we have continued in get service record'
        record = get_service_referral_record(encounter)
        if record:
            toreturn.append(record)
    #print toreturn
    return toreturn

def ben_matching_service_search_token(searchtoken):
    pass

def ben_matching_adverse_search_token(searchtoken):
    pass

def delete_service_referral_record(formid, beneficiaryid, user, encounterid):
    
    success = False
    querycondition = {}
    try:
        if formid:
            querycondition['form_id'] = formid
        elif encounterid:
            encounter = CoreEncounters.objects.get(pk=encounterid)
            if encounter.sms_id and not encounter.form_id:
                querycondition['sms_id'] = encounter.sms_id
        if querycondition:
            querycondition['beneficiary_person_id'] = beneficiaryid
                
        CoreAdverseConditions.objects.filter(**querycondition).delete()
        CoreServices.objects.filter(**querycondition).delete()
        
        if CoreAdverseConditions.objects.filter(**querycondition).count() == 0 and CoreServices.objects.filter(**querycondition).count() == 0:
            CoreEncounters.objects.filter(**querycondition).delete()
            Forms.objects.filter(pk=formid, is_void=False).update(is_void=True)
        
        update_form_timestamp(formid)
        save_to_admin_upload(formid)
        success = True
    except:
        success = False
    return success

def update_form_timestamp(formid):
    
    Forms.objects.filter(id=formid).update(timestamp_updated=datetime.datetime.now())
    
def get_service_referral_record(encounter):
    if not encounter:
        return None
        
    form = Forms.objects.get(pk = encounter.form_id, is_void = False) if Forms.objects.filter(pk = encounter.form_id, is_void=False).count()==1 else None
    
    if settings.DEBUG:
        if form:
            print form.__dict__
            print encounter.__dict__
        
    coreservices = get_form_core_service_items(form, encounter)
    
    querycondition = {}
    
    if form:
        querycondition['form_id'] = encounter.form_id
    elif encounter and encounter.sms_id and not encounter.form_id:
        querycondition['sms_id'] = encounter.sms_id
    adverse_conditions = CoreAdverseConditions.objects.filter(is_void=False, **querycondition).values_list('adverse_condition_id',flat=True)
    print 'adverse_conditions', adverse_conditions
    services_record = group_items_by_service_type(list(coreservices)+list(adverse_conditions)+[encounter.encounter_type_id])
    
    
    
    tmpservicerecord = {
                            'workforceid': encounter.workforce_person.pk,
                            'beneficiaryid': encounter.beneficiary_person.pk,
                            'benfirstname': encounter.beneficiary_person.first_name,
                            'bensurname': encounter.beneficiary_person.surname,
                            'bendisplayid': encounter.beneficiary_person.beneficiary_id,
                            'bendateofbirth': encounter.beneficiary_person.date_of_birth.strftime(val.date_input_format),
                            #form.timestamp_created.strftime(val.date_input_format)
                            'encounterdate': encounter.encounter_date.strftime(val.date_input_format),
                            'orgunitid': encounter.org_unit_id,
                            'orgunitname': geo_location.get_org_unit_name(encounter.org_unit_id),
                            'areaid': encounter.area_id,
                            'areaname': geo_location.get_geo_name(encounter.area_id),
                            'encountertypeid': encounter.encounter_type_id,
                            'encountertypetext': services_record[db_text.ENCOUNTER_TYPE_CATEGORY_DB_TEXT][0][encounter.encounter_type_id],
                            'formid': encounter.form_id,
                            
                            'servicesprovided': services_record[db_text.SERVICE_TYPE_CATEGORY_DB_TEXT],
                            'referralsmade':services_record[db_text.REFERRAL_MADE_CATEGORY_DB_TEXT],
                            'referralscompleted':services_record[db_text.REFERRAL_COMPLETED_CATEGORY_DB_TEXT],
                            
                            'adverseconditions': services_record[db_text.adverse_condition], 
                            
                            'encounterid': encounter.pk
                      }
    
    return tmpservicerecord

def group_items_by_service_type(coreservices):
    
    setup_list_item_category = [db_text.REFERRAL_COMPLETED_CATEGORY_DB_TEXT,
                                db_text.REFERRAL_MADE_CATEGORY_DB_TEXT,
                                db_text.SERVICE_TYPE_CATEGORY_DB_TEXT,
                                db_text.ENCOUNTER_TYPE_CATEGORY_DB_TEXT,
                                db_text.adverse_condition
                                ]
    services_record = {
                db_text.REFERRAL_COMPLETED_CATEGORY_DB_TEXT:[],
                db_text.REFERRAL_MADE_CATEGORY_DB_TEXT:[],
                db_text.SERVICE_TYPE_CATEGORY_DB_TEXT:[],
                db_text.ENCOUNTER_TYPE_CATEGORY_DB_TEXT:[],
                db_text.adverse_condition:[]
                }
    
    #itemcategory:[(itemid, item descriptopm), ]
    setup_category_dict = field_provider.get_setup_items_as_dict_by_category(setup_list_item_category)


    for servicetype, servicedetails in setup_category_dict.items():
        for itemid, itemdescription in servicedetails:
            if itemid in coreservices:
                services_record[servicetype].append({itemid:itemdescription})
    
    return services_record

def get_form_core_service_items(form, encounter=None):
    
    coreservices = None
    if settings.DEBUG:
        if form:
            print form.form_subject_id
            print form.date_filled_paper
    
    if form:
        coreservices = CoreServices.objects.filter(workforce_person__pk = form.form_subject_id,
                                beneficiary_person__pk = encounter.beneficiary_person_id,
                                encounter_date = form.date_began,
                                form_id=form.pk).values_list('core_item_id', flat=True)
        
    elif encounter and encounter.sms_id and not encounter.form_id:
        coreservices = CoreServices.objects.filter(workforce_person__pk = encounter.workforce_person_id,
                                beneficiary_person__pk = encounter.beneficiary_person_id,
                                encounter_date = encounter.encounter_date,
                                sms_id=encounter.sms_id).values_list('core_item_id', flat=True)
    
    
    

    if settings.DEBUG:
        print 'coreservices', coreservices
        
    if coreservices:
        return coreservices
    return []


def get_ben_coreencounter_record(formid, benid, encounterid=None):
    if formid:
        encounter = CoreEncounters.objects.get(form_id = formid, beneficiary_person__pk=benid) if CoreEncounters.objects.filter(form_id = formid, beneficiary_person__pk=benid).count() == 1 else None
    else:
        encounter = CoreEncounters.objects.get(pk=encounterid, beneficiary_person__pk=benid) if CoreEncounters.objects.filter(pk=encounterid, beneficiary_person__pk=benid).count() == 1 else None
    return encounter

def save_core_encounter(workforceid, beneficiaryid, encounterdate, organisationunit, 
                        areaid, encountertype, smsid=None, formid=None, encounterid=None):
    
    if settings.DEBUG:
        print "Saving to Core encounters"
    
    coreencounter = None
    
    if CoreEncounters.objects.filter(workforce_person = RegPerson.objects.get(pk=workforceid),
                                                  beneficiary_person=RegPerson.objects.get(pk=beneficiaryid),
                                                  encounter_date = encounterdate, **core_encounter_query_conditions()).count() > 0:
        coreencounter = CoreEncounters.objects.filter(workforce_person = RegPerson.objects.get(pk=workforceid),
                                                  beneficiary_person=RegPerson.objects.get(pk=beneficiaryid),
                                                  encounter_date = encounterdate, **core_encounter_query_conditions()).update(org_unit_id = organisationunit,
                                                          area_id = areaid,
                                                          encounter_type_id=encountertype,
                                                          sms_id = smsid,
                                                          form_id = formid)
    elif encounterid and str(encounterid).isdigit() and CoreEncounters.objects.filter(pk=int(encounterid), form_id__isnull=True).count() > 0:
        coreencounter = CoreEncounters.objects.filter(pk=int(encounterid), form_id__isnull=True, **core_encounter_query_conditions()).update(org_unit_id = organisationunit,
                                                          area_id = areaid,
                                                          encounter_type_id=encountertype,
                                                          sms_id = smsid,
                                                          form_id = formid)
    
    else:
        coreencounter = CoreEncounters.objects.create(workforce_person = RegPerson.objects.get(pk=workforceid),
                                                      beneficiary_person=RegPerson.objects.get(pk=beneficiaryid),
                                                      encounter_date = encounterdate,
                                                      org_unit_id = organisationunit,
                                                      area_id = areaid,
                                                      encounter_type_id=encountertype,
                                                      sms_id = smsid,
                                                      form_id = formid)
    return coreencounter


def save_services(form, workforceid, beneficiaryid, date_of_service, services=[]):
    from ovc_main.models import CoreServices
    
    services_models = []
    workforce = RegPerson.objects.get(pk=workforceid)
    beneficiary = RegPerson.objects.get(pk=beneficiaryid)
    
    CoreServices.objects.filter(workforce_person = workforce,
                                            beneficiary_person = beneficiary,
                                            encounter_date=date_of_service).delete()
    
    for service in services:
        services_models.append(CoreServices(workforce_person = workforce,
                                            beneficiary_person = beneficiary,
                                            encounter_date=date_of_service,
                                            core_item_id=service,
                                            form_id=form.form_pk))
                               
    if services_models:
        if settings.DEBUG:
            print "saving to core services"
        CoreServices.objects.bulk_create(services_models)
    
def general_save_form(form_t, cleaned_data, logged_in_user, form_id):
    questions = create_questions_object_from_data(form_t)
    questions_with_answers = get_answers_for_questions(questions,cleaned_data)
    form_obj = create_form_object(cleaned_data,form_t)
    form_obj.questions = questions_with_answers
    form_obj.user_id_created = logged_in_user.pk
    
    form = None
    if form_id:
        #In update mode
        form = update_form_basic_data(form_obj, form_id)
        delete_form_data(form_id)
    else:
        form = save_form_basic_data(form_obj)
    
    if form:
        save_custom_fields(cleaned_data,form_t,form)
        save_questions(form)
        
    return form

def save_to_admin_upload(form_id):
    '''
    Save to the tbl_admin_uploads if the current site is a capture site
    '''
    if settings.IS_CAPTURE_SITE:
        try:
            if len(AdminUploadForms.objects.filter(form_id=form_id, timestamp_uploaded=None)) < 1:
                AdminUploadForms.objects.create(form_id=form_id)
        except Exception as e:
            print e
            raise e
    else:
        return
    
def void_old_form(form_id):
    if Forms.objects.filter(pk=form_id).count() == 1:
        form = Forms.objects.filter(pk=form_id).update(is_void=True)
        save_to_admin_upload(form_id)
        
    elif Forms.objects.filter(pk=form_id).count() > 1:
        print 'something went wrong when loading form from file forms_helper, method void_old_form()'

def save_custom_fields(cleaned_data,form_type,form):
    if form_type:
        return {
                'FT3e': dummy_method,
                'FT3d': save_custom_ovc_assessment,
                'FT2d': save_custom_workforce_training,
                'FT2c': dummy_method,
                'FT1g': dummy_method,
                'FT1f': save_custom_res_institution,
                'FT1c': dummy_method,
                'FT1e': save_custom_public_sensitisation,
                'FT3f': save_custom_child_in_conflict_law,
                }[form_type](cleaned_data,form)
    return None

def dummy_method(cleaned_data,form):
    d = None

def save_custom_public_sensitisation(cleaned_data,form): 
    org_data = None
    beneficiary_data = None
    '''
    {"1":{"org_unit_name":"MCD-MCH","org_unit_id":"U000018","org_contribution":" - ","org_contribution_id":"CTOR - CTRT","":" "},
    "2":{"org_unit_name":"Mwandi HQ","org_unit_id":"U000026","org_contribution":"","org_contribution_id":"CTOR","":" "}}
    '''
    if cleaned_data.has_key('org_contributions_data'):
        org_data = cleaned_data['org_contributions_data']
        if org_data:
            org_contrib_data = json.loads(org_data)
            for key, value in org_contrib_data.items():
                org_unit_id = value['org_unit_id']
                org_id_int = None
                if len(RegOrgUnit.objects.filter(org_unit_id_vis=org_unit_id, is_void=False)) > 0:
                    org_id_int = RegOrgUnit.objects.filter(org_unit_id_vis=org_unit_id, is_void=False)[0].pk
                org_contribution_ids = value['org_contribution_id'].split(' - ')
                if org_id_int and org_contribution_ids:
                    for contribution in org_contribution_ids:
                        FormOrgUnitContributions.objects.create(form_id = form.form_pk, org_unit_id = org_id_int, contribution_id = contribution)
    '''
    {"1":{"person_id":"2","beneficiary_id":"B00000018","birth_reg_id":"PRO/123456/2004","national_id":"","participation_level_id":"PTFL","":" "},
    "2":{"person_id":"4","beneficiary_id":"B00000026","birth_reg_id":"","national_id":"295311/82/2","participation_level_id":"PTPT","":" "}}
    '''
    if cleaned_data.has_key('beneficiary_data'):
        beneficiary_data = cleaned_data['beneficiary_data']
        if beneficiary_data:
            participation_data = json.loads(beneficiary_data)
            for key, value in participation_data.items():
                beneficiary_id = value['beneficiary_id']
                beneficiary_int = None
                if len(RegPerson.objects.filter(beneficiary_id=beneficiary_id, is_void=False)) > 0:
                    beneficiary_int = RegPerson.objects.filter(beneficiary_id=beneficiary_id, is_void=False)[0].pk
                participation_level_id = value['participation_level_id']
                
                if beneficiary_int and participation_level_id:
                    FormPersonParticipation.objects.create(form_id = form.form_pk, workforce_or_beneficiary_id=beneficiary_int, participation_level_id=participation_level_id)

def save_custom_child_in_conflict_law(cleaned_data,form):
    social_data = None
    if cleaned_data.has_key('social_data_hidden'):
        social_data = cleaned_data['social_data_hidden']
    social_data_json = json.loads(social_data)
    for key, social_data_str in social_data_json.items():
        save_encounter(social_data_str,form,'social')
        
    court_data = None
    if cleaned_data.has_key('court_data_hidden'):
        court_data = cleaned_data['court_data_hidden']
    court_data_json = json.loads(court_data)
    for key, court_data_str in court_data_json.items():
        save_encounter(court_data_str,form,'court')
        
    
def save_encounter(social_data_str,form,enc_source):
    if social_data_str and form:
        welfare_officer = social_data_str['p_id']
        encounter_date = datetime.datetime.strptime(social_data_str['enc_date'], val.date_input_format) 
        try:
            encounter = CoreEncounters.objects.create(
                                            workforce_person_id = welfare_officer,
                                            beneficiary_person_id = form.form_subject_id,
                                            encounter_date = encounter_date,
                                            org_unit_id = form.org_unit_id_filled_paper,
                                            area_id = form.form_area_id,
                                            encounter_type_id = db_text.ENCOUNTER_TYPE_ID_SOCIAL if enc_source == 'social' else social_data_str['enc_type'],
                                            sms_id = None,
                                            form_id = form.form_pk
                                          )
            if enc_source == 'social':
                save_encounter_notes(encounter,social_data_str,form,social_data_str['enc_int'],db_text.ENCOUNTER_TYPE_WHO_WAS_INTERVIEWED,enc_source)
                save_encounter_notes(encounter,social_data_str,form,social_data_str['enc_notes'],db_text.ENCOUNTER_NOTES,enc_source)
                save_encounter_notes(encounter,social_data_str,form,social_data_str['enc_recom'],db_text.ENCOUNTER_RECOMMENDATIONS,enc_source)
            elif enc_source == 'court':
                save_encounter_notes(encounter,social_data_str,form,social_data_str['enc_jm'],db_text.ENCOUNTER_TYPE_JM,enc_source)
                save_encounter_notes(encounter,social_data_str,form,social_data_str['enc_io'],db_text.ENCOUNTER_TYPE_IO,enc_source)
                save_encounter_notes(encounter,social_data_str,form,social_data_str['enc_notes'],db_text.ENCOUNTER_NOTES,enc_source)
        except Exception as e:
            print e
            raise e

def save_encounter_notes(enctr,social_data_str,form,note,n_type_id,enc_source):
    if social_data_str and form:
        CoreEncountersNotes.objects.create(
                                            encounter = enctr,
                                            form_id = form.form_pk,
                                            workforce_person_id = social_data_str['p_id'],
                                            beneficiary_person_id = form.form_subject_id,
                                            encounter_date = datetime.datetime.strptime(social_data_str['enc_date'], val.date_input_format),
                                            note_type_id = n_type_id,
                                            note = note
                                           )

def get_encounters_text_for_display(frm_id,enc_type):
    to_return = {}
    encounters = None
    if enc_type == db_text.ENCOUNTER_TYPE_ID_SOCIAL:
        encounters = CoreEncounters.objects.filter(form_id = frm_id,encounter_type_id=enc_type)
    else:
        q_list = []
        q_list.append(Q(encounter_type_id=db_text.ENCOUNTER_TYPE_COURT_HIGH))
        q_list.append(Q(encounter_type_id=db_text.ENCOUNTER_TYPE_COURT_SUB))
        encounters = CoreEncounters.objects.filter(reduce(operator.or_, q_list),form_id = frm_id)
        
    if encounters:
        count = 1
        for encountr in encounters:
            enc_data = {}
            if encountr.encounter_type_id == db_text.ENCOUNTER_TYPE_ID_SOCIAL:
                enc_data['p_id'] = encountr.workforce_person_id
                e_date = datetime.datetime.strptime(str(encountr.encounter_date), '%Y-%m-%d')
                enc_data['enc_date'] = e_date.strftime('%d-%B-%Y')
                enc_data['wfc_name'] = get_person_name(encountr.workforce_person_id)
                enc_notes = CoreEncountersNotes.objects.filter(encounter = encountr) 
                for enc_note in enc_notes:
                    if enc_note.note_type_id == db_text.ENCOUNTER_TYPE_WHO_WAS_INTERVIEWED:
                        enc_data['enc_int'] = enc_note.note
                    elif enc_note.note_type_id == db_text.ENCOUNTER_NOTES:
                        enc_data['enc_notes'] = enc_note.note
                    elif enc_note.note_type_id == db_text.ENCOUNTER_RECOMMENDATIONS:
                        enc_data['enc_recom'] = enc_note.note
            else:
                enc_data['p_id'] = encountr.workforce_person_id
                enc_data['enc_type'] = encountr.encounter_type_id
                e_date = datetime.datetime.strptime(str(encountr.encounter_date), '%Y-%m-%d')
                enc_data['enc_date'] = e_date.strftime('%d-%B-%Y')
                enc_data['wfc_name'] = get_person_name(encountr.workforce_person_id)
                enc_data['enc_desc'] = get_setup_item_short_description(encountr.encounter_type_id)
                enc_notes = CoreEncountersNotes.objects.filter(encounter = encountr) 
                for enc_note in enc_notes:
                    if enc_note.note_type_id == db_text.ENCOUNTER_TYPE_JM:
                        enc_data['enc_jm'] = enc_note.note
                    elif enc_note.note_type_id == db_text.ENCOUNTER_TYPE_IO:
                        enc_data['enc_io'] = enc_note.note
                    elif enc_note.note_type_id == db_text.ENCOUNTER_NOTES:
                        enc_data['enc_notes'] = enc_note.note

            to_return[count] = enc_data
            count = count + 1
    return json.dumps(to_return)

def save_custom_workforce_training(cleaned_data,form): 
    org_data = None
    workforce_data = None

    if cleaned_data.has_key('org_contributions_data'):
        org_data = cleaned_data['org_contributions_data']
        org_contrib_data = json.loads(org_data)
        for key, value in org_contrib_data.items():
            org_unit_id = value['org_unit_id']
            org_id_int = None
            if len(RegOrgUnit.objects.filter(org_unit_id_vis=org_unit_id, is_void=False)) > 0:
                org_id_int = RegOrgUnit.objects.filter(org_unit_id_vis=org_unit_id, is_void=False)[0].pk
            org_contribution_ids = value['org_contribution_id'].split(' - ')
            if org_id_int and org_contribution_ids:
                for contribution in org_contribution_ids:
                    FormOrgUnitContributions.objects.create(form_id = form.form_pk, org_unit_id = org_id_int, contribution_id = contribution)

    if cleaned_data.has_key('workforce_member_data'):
        workforce_data = cleaned_data['workforce_member_data']
        participation_data = json.loads(workforce_data)
        for key, value in participation_data.items():
            workforce_id = value['workforce_id']
            workforce_nrc = value['workforce_nrc']
            workforce_int = None
            if workforce_id:
                if len(RegPerson.objects.filter(workforce_id=workforce_id, is_void=False)) > 0:
                    workforce_int = RegPerson.objects.filter(workforce_id=workforce_id, is_void=False)[0].pk
            participation_level_id = value['participation_level_id']
            
            if workforce_int and participation_level_id:
                FormPersonParticipation.objects.create(form_id = form.form_pk, workforce_or_beneficiary_id=workforce_int, participation_level_id=participation_level_id)
    
def save_custom_res_institution(cleaned_data, form):
    '''
    This function saves the child status, workforce status and adverse conditions 
    data from the Residential Institution Form
    '''
    adverse_cond = None
    child_status_data = None
    workforce_data = None
    
    ''' Get the institution id '''
    org_unit_id = None
    if form.form_subject_id:
        if len(RegOrgUnit.objects.filter(pk=form.form_subject_id, is_void = False, date_closed = None)) > 0:
            m_org = RegOrgUnit.objects.filter(pk=form.form_subject_id, is_void = False, date_closed = None)[0]
            org_unit_id = m_org.pk
    if cleaned_data.has_key('child_status_data'):
        child_status_data = cleaned_data['child_status_data']
        child_and_adverse_conditions = get_adverse_conditionss(child_status_data)
    if child_and_adverse_conditions:  
        save_all_adverse_conditions(child_and_adverse_conditions, form)
    if cleaned_data.has_key('workforce_data'):
        workforce_data = cleaned_data['workforce_data']
    if workforce_data:
        save_res_workforce(workforce_data, form, org_unit_id)
    if cleaned_data.has_key('child_status_data'):
        child_status_data = cleaned_data['child_status_data']
    if child_status_data:
        save_res_children(child_status_data, form, org_unit_id)

def save_res_workforce(workforce_data, form, org_unit_id):
    '''
    Saves the workforce data in a json string in this format
    {"1":{"workforce_id":"W0000125","workforce_name":"Nancy Bombwe","workforce_role":"RWSP","workforce_employment_status":"EMVL"}}
   
    '''
    to_return = True
    if workforce_data:
        workforce_status_data = json.loads(workforce_data)
        for row_id, workforce_status in workforce_status_data.items():
            try:
                workforce_id = workforce_status['workforce_id']
                institution_id_int = org_unit_id
                position_id = workforce_status['workforce_role']
                full_part_time_id = workforce_status['workforce_employment_status']
                if position_id or full_part_time_id:
                    workforce_id_int = None
                    if len(RegPerson.objects.filter(workforce_id = workforce_id, is_void = False)) > 0:
                        person = RegPerson.objects.filter(workforce_id = workforce_id, is_void = False)[0]
                        workforce_id_int = person.pk
                    m_res_workforce = FormResWorkforce.objects.create(form_id = form.form_pk,
                                                                workforce_id = workforce_id_int,
                                                                institution_id = institution_id_int,
                                                                position_id = position_id,
                                                                full_part_time_id = full_part_time_id
                                                                )
            except Exception as e:
                print e
                to_return = False
            
    return to_return

def save_res_children(child_status_data, form, org_unit_id):
    '''
    Save data received in this format into the ResChildren table
    {
    "1":{"child_id_int":"3","beneficiary_id":"B00000026","adverse_conditions":"PHVP PILL ",
    "current_residential_status":"RSRI","family_status":"","has_court_committal_order":"",
    "date_first_admitted":"3-March-2015","date_left":"3-March-2015"},
    "2":{...},
    "3":{...},
    } 
    '''
    to_return = True
    if child_status_data:
        print 'Saving child in form id: ',form.form_pk
        child_status_data = json.loads(child_status_data)
        for row_id, child_status in child_status_data.items():
            child_id = int(child_status['child_id_int'])
            current_residential_status = child_status['current_residential_status']
            family_status = child_status['family_status']
            has_court_committal_order = child_status['has_court_committal_order']
            date_first_admitted_str = child_status['date_first_admitted']
            date_left_str = child_status['date_left']
            date_first_admitted = None
            date_left = None
            
            if date_first_admitted_str:
                date_first_admitted = datetime.datetime.strptime(date_first_admitted_str, val.date_input_format)
            if date_left_str:
                date_left = datetime.datetime.strptime(date_left_str, val.date_input_format)
            try:
                m_res_child = FormResChildren.objects.create(form_id = form.form_pk,
                                                            child_person_id = child_id,
                                                            institution_id = org_unit_id,
                                                            residential_status_id = current_residential_status,
                                                            court_committal_id = has_court_committal_order,
                                                            family_status_id = family_status,
                                                            date_admitted = date_first_admitted,
                                                            date_left = date_left
                                                            )
            except Exception as e:
                print e
                to_return = False
            
    return to_return

def get_adverse_conditionss(child_status_data):
    '''
    Converts this string
    {
    "1":{"child_id_int":"3","beneficiary_id":"B00000026","adverse_conditions":"PHVP PILL ",
    "current_residential_status":"RSRI","family_status":"","has_court_committal_order":"",
    "date_first_admitted":"3-March-2015","date_left":"3-March-2015"},
    "2":{...},
    "3":{...},
    } 
    into a dictionary that looks like 
    {child_id1:[PHVP, PILL, PCIL], child_id2: [PHVP, PILL, PCIL],...}
    '''
    to_return = {}
    if child_status_data:
        child_status_data = json.loads(child_status_data)
        for row_id, child_status in child_status_data.items():
            child_id = child_status['child_id_int']
            adverse_string = child_status['adverse_conditions']
            adverse_list = adverse_string.split(' ')
            to_return[child_id] = adverse_list
    return to_return
    
def save_custom_ovc_assessment(cleaned_data,form):
    adverse_cond = None
    if cleaned_data.has_key('adverse_cond'):
        adverse_cond = cleaned_data['adverse_cond']
    if adverse_cond:
        adverse_conditions = {form.form_subject_id:adverse_cond}
        save_all_adverse_conditions(adverse_conditions, form)
    save_csi(cleaned_data, form.form_pk)

def save_csi(cleaned_data,form_id):
    csi_fields = SetupList.objects.filter(item_category='CSI domain',is_void=False)
    for csi_field in csi_fields:
        if csi_field.item_id:
            score = None
            if cleaned_data.has_key(csi_field.item_id):
                score = cleaned_data[csi_field.item_id]
            observation = None
            score_detail_key = '%s_det'%csi_field.item_id
            if cleaned_data.has_key(score_detail_key):
                observation = cleaned_data[score_detail_key]
            
            if score or observation:
                save_csi_values(csi_field.item_id, score, observation, form_id)
       
def save_csi_values(domain, score, observations, form_id):
    score = None if not score or score == '' else score#ensure to pass null to the score int value if a string has been passed to score instead
    FormCsi.objects.create(form_id=form_id,domain_id=domain,score=score,observations=observations)
    
def load_csi_for_form_id(frm_id, formfields):
    csi_fields = FormCsi.objects.filter(form_id=frm_id)
    for csi_field in csi_fields:
        formfields[csi_field.domain_id] = csi_field.score
        score_detail_key = '%s_det'%csi_field.domain_id
        formfields[score_detail_key] = csi_field.observations
        
    return formfields

def save_all_adverse_conditions(child_and_adverse_conditions=None, form = None):
    '''
    Saves adverse conditions. Expects a dictionary in the form 
    {child_id1:[PHVP, PILL, PCIL], child_id2: [PHVP, PILL, PCIL],...}
    '''
    if settings.DEBUG:
        print 'child adverse dictionary', child_and_adverse_conditions
    if child_and_adverse_conditions:
        for child_id, adverse_conditions in child_and_adverse_conditions.items():
            CoreAdverseConditions.objects.filter(beneficiary_person_id = child_id, is_void=False).update(is_void=True)
            for adverse_condition in adverse_conditions:
                if adverse_condition:
                    '''
                    If the adverse condition is already recorded for the child
                    and has no form_id , add the form_id to the record.  
                    '''
                    CoreAdverseConditions.objects.create(
                                                         beneficiary_person_id = child_id,
                                                         adverse_condition_id = adverse_condition,
                                                         is_void=False,
                                                         sms_id = None,
                                                         form_id = form.form_pk
                                                         )
                    if settings.DEBUG:
                        print 'child adverse dictionary', child_and_adverse_conditions
                        print "CoreAdverseConditions created"
                                             
def create_form_object(cleaned_data,form_type):
    form_guid = idgenerator.new_guid_32()
    if form_type:
        return {
                'FT3e': create_form_basic_service_referral,
                'FT3d': create_form_ovc_assessment,
                'FT2d': create_form_workforce_training,
                'FT2c': create_form_wfc_assessment,
                'FT1g': None,
                'FT1f': create_form_residential_institution,
                'FT1c': None,
                'FT1e': create_form_public_sensitisation,
                'FT3f': create_form_child_in_conflict_with_the_law,
                }[form_type](cleaned_data, form_guid)
    return None

def create_form_public_sensitisation(cleaned_data, form_guid):
    form_type_id = None
    if cleaned_data.has_key('form_type'):
        form_type_id = cleaned_data['form_type']
        
    date_event_started = None
    if cleaned_data.has_key('date_event_started'):
        date_event_started = cleaned_data['date_event_started']
    
    date_event_ended = None
    if cleaned_data.has_key('date_event_ended'):
        date_event_ended = cleaned_data['date_event_ended']
    
    event_title = None
    if cleaned_data.has_key('event_title'):
        event_title = cleaned_data['event_title']

    area_id = None
    if cleaned_data.has_key('ward'):
        area_id = cleaned_data['ward'] if cleaned_data['ward'] else None

    if not area_id:
        if cleaned_data.has_key('district'):
            area_id = cleaned_data['district'] if cleaned_data['district'] else None

    time_stamp_created = datetime.datetime.now().date()

    form_to_return = Form (
                    form_pk = None,
                    form_guid = form_guid,
                    form_type_id = form_type_id,
                    form_subject_id = None,
                    form_area_id = area_id,
                    form_title = event_title,
                    date_began = date_event_started,
                    date_ended = date_event_ended,
                    date_filled_paper = None,
                    person_id_filled_paper = None,
                    org_unit_id_filled_paper = None,
                    capture_site_id = field_provider.get_capture_site_id(),
                    timestamp_created = time_stamp_created,
                    user_id_created = None,
                    timestamp_updated = None,
                    user_id_updated = None,
                    questions = None,
                    wfc_id_hidden = None
                 )
    return form_to_return

def create_form_workforce_training(cleaned_data, form_guid):
    form_type_id = None
    if cleaned_data.has_key('form_type'):
        form_type_id = cleaned_data['form_type']
        
    date_training_started = None
    if cleaned_data.has_key('date_training_started'):
        date_training_started = cleaned_data['date_training_started']
    
    date_training_ended = None
    if cleaned_data.has_key('date_training_ended'):
        date_training_ended = cleaned_data['date_training_ended']
    
    training_title = None
    if cleaned_data.has_key('training_title'):
        training_title = cleaned_data['training_title']

    area_id = None
    if cleaned_data.has_key('where_training_took_place_ward'):
        area_id = cleaned_data['where_training_took_place_ward'] if cleaned_data['where_training_took_place_ward'] else None

    time_stamp_created = datetime.datetime.now().date()
    print date_training_ended,'date_training_ended'
    form_to_return = Form (
                    form_pk = None,
                    form_guid = form_guid,
                    form_type_id = form_type_id,
                    form_subject_id = None,
                    form_area_id = area_id,
                    form_title = training_title,
                    date_began = date_training_started,
                    date_ended = date_training_ended,
                    date_filled_paper = None,
                    person_id_filled_paper = None,
                    org_unit_id_filled_paper = None,
                    capture_site_id = field_provider.get_capture_site_id(),
                    timestamp_created = time_stamp_created,
                    user_id_created = None,
                    timestamp_updated = None,
                    user_id_updated = None,
                    questions = None,
                    wfc_id_hidden = None
                 )
    return form_to_return

def create_form_residential_institution(cleaned_data, form_guid):
    

    form_type_id = None
    if cleaned_data.has_key('form_type'):
        form_type_id = cleaned_data['form_type']
        
    form_subject_id = None
    if cleaned_data.has_key('subject'):
        form_subject_id = cleaned_data['subject']
        
    person_id_field_paper = None
    if cleaned_data.has_key('user_workforce_filled_form_hidden'):
        person_id_field_paper = cleaned_data['user_workforce_filled_form_hidden']
    
    date_began = None
    if cleaned_data.has_key('date_of_form'):
        date_began = cleaned_data['date_of_form']
    
    time_stamp_created = datetime.datetime.now().date()
    
    area_id = None
    if cleaned_data.has_key('ward_id'):
        area_id = cleaned_data['ward_id']
        
    form_to_return = Form (
                    form_pk = None,
                    form_guid = form_guid,
                    form_type_id = form_type_id,
                    form_subject_id = form_subject_id,
                    form_area_id = area_id,
                    form_title = None,
                    date_began = date_began,
                    date_ended = None,
                    date_filled_paper = None,
                    person_id_filled_paper = person_id_field_paper,
                    org_unit_id_filled_paper = None,
                    capture_site_id = field_provider.get_capture_site_id(),
                    timestamp_created = time_stamp_created,
                    user_id_created = None,
                    timestamp_updated = None,
                    user_id_updated = None,
                    questions = None,
                    wfc_id_hidden = None
                 )
    return form_to_return

def create_form_basic_service_referral(cleaneddata, form_guid):
    #[u'1/'], u'referrals_made': [u'RMPL'], u'organisation_unit': [u'1'], 
    #u'referrals_completed': [u'SCBN'], u'services_provided': [u'RCCT'], 
    #u'date_of_service': [u'2-April-2015'], u'encounter_type': [u'EHOM'], 
    #u'encounter_location': [u'263'], u'new_adverse_condition': [u'PCOL'], 
    #u'form_type': [u'FT3e'], 
    #u'csrfmiddlewaretoken': [u'kR7qeuHgfgCpKsOssSXNfaAmXhITKj6J'], 
    #u'beneficiary_autocomplete': [u'B00000018\tPRO/123457/2010\tchild Lusaka\tChawama-Chawama'], 
    #u'subject': [u'1/']}>
    date_of_service = cleaneddata['date_of_service']
    ward_encounter_location = cleaneddata['encounter_location']
    subject_id = cleaneddata['workforce_system_id']
    time_stamp_created = datetime.datetime.now().date()
    form_type = cleaneddata['form_type']
    organisation_unit_id = cleaneddata['organisation_unit']
    
    form_to_return = Form (
                    form_pk = None,
                    form_guid = form_guid,
                    form_type_id = form_type,
                    form_subject_id = subject_id,
                    form_area_id = int(ward_encounter_location),
                    form_title = None,
                    date_began = date_of_service,
                    date_ended = None,
                    date_filled_paper = date_of_service,
                    person_id_filled_paper = subject_id,
                    wfc_id_hidden = None,
                    org_unit_id_filled_paper = organisation_unit_id,
                    capture_site_id = field_provider.get_capture_site_id(),
                    timestamp_created = time_stamp_created,
                    user_id_created = None,
                    timestamp_updated = None,
                    user_id_updated = None,
                    questions = None
                 )
    return form_to_return
def create_form_ovc_assessment(cleaned_data, form_guid):
    
    form_subject_id = None
    if cleaned_data.has_key('subject'):
        form_subject_id = cleaned_data['subject']
        
    person_id_field_paper = None
    wfc = None
    if cleaned_data.has_key('wfc_id_hidden'):
        person_id_field_paper = cleaned_data['wfc_id_hidden']
    
    org_unit_id_field_paper = None
    if cleaned_data.has_key('org_assessing_child'):
        org_unit_id_field_paper = cleaned_data['org_assessing_child']
    
    date_began = None
    if cleaned_data.has_key('date_assessment_comp'):
        date_began = cleaned_data['date_assessment_comp']
        
    form_type_id = None
    if cleaned_data.has_key('form_type'):
        form_type_id = cleaned_data['form_type']
    
    area_ward_id = RegPersonsGeo.objects.get(person_id=form_subject_id,is_void=False).area_id if RegPersonsGeo.objects.filter(person_id=form_subject_id,is_void=False).count() == 1 else None
    
    time_stamp_created = datetime.datetime.now().date()
        
    form_to_return = Form (
                    form_pk = None,
                    form_guid = form_guid,
                    form_type_id = form_type_id,
                    form_subject_id = form_subject_id,
                    form_area_id = area_ward_id,
                    form_title = None,
                    date_began = date_began,
                    date_ended = None,
                    date_filled_paper = None,
                    person_id_filled_paper = person_id_field_paper,
                    wfc_id_hidden = person_id_field_paper,
                    org_unit_id_filled_paper = org_unit_id_field_paper,
                    capture_site_id = field_provider.get_capture_site_id(),
                    timestamp_created = time_stamp_created,
                    user_id_created = None,
                    timestamp_updated = None,
                    user_id_updated = None,
                    questions = None
                 )
    return form_to_return

def create_form_child_in_conflict_with_the_law(cleaned_data,form_guid):
    form_guid = idgenerator.new_guid_32()
    
    form_subject_id = None
    if cleaned_data.has_key('subject'):
        form_subject_id = cleaned_data['subject']
        
    person_id_filled_paper = None
    wfc = None
    if cleaned_data.has_key('wfc_id_hidden'):
        person_id_filled_paper = cleaned_data['wfc_id_hidden']
    
    date_began = None
    if cleaned_data.has_key('Q099'):
        date_began = cleaned_data['Q099']
    
    form_area_id = None
    if cleaned_data.has_key('ward'):
        form_area_id = cleaned_data['ward']
        
    org_unit_id_filled_paper = None
    if cleaned_data.has_key('org_assessing_child'):
        org_unit_id_filled_paper = cleaned_data['org_assessing_child']
    
    date_ended = None
    if cleaned_data.has_key('Q113'):
        date_ended = cleaned_data['Q113']
        
    form_type_id = None
    if cleaned_data.has_key('form_type'):
        form_type_id = cleaned_data['form_type']
            
    time_stamp_created = datetime.datetime.now().date()
        
    form_to_return = Form (
                    form_pk = None,
                    form_guid = form_guid,
                    form_type_id = form_type_id,
                    form_subject_id = form_subject_id,
                    form_area_id = form_area_id,
                    form_title = None,
                    date_began = date_began,
                    date_ended = None,
                    date_filled_paper = None,
                    person_id_filled_paper = person_id_filled_paper,
                    wfc_id_hidden = person_id_filled_paper,
                    org_unit_id_filled_paper = org_unit_id_filled_paper,
                    capture_site_id = field_provider.get_capture_site_id(),
                    timestamp_created = time_stamp_created,
                    user_id_created = None,
                    timestamp_updated = None,
                    user_id_updated = None,
                    questions = None
                 )
    return form_to_return

def create_form_wfc_assessment(cleaned_data,form_guid):
    form_guid = idgenerator.new_guid_32()
    
    form_subject_id = None
    if cleaned_data.has_key('subject'):
        form_subject_id = cleaned_data['subject']
        
    person_id_field_paper = None
    wfc = None
    if cleaned_data.has_key('wfc_id_hidden'):
        person_id_field_paper = cleaned_data['wfc_id_hidden']
    
    org_unit_id_field_paper = None
    if cleaned_data.has_key('org_assessing_wfc'):
        org_unit_id_field_paper = cleaned_data['org_assessing_wfc']
    
    print 'org_unit_id_field_paper',org_unit_id_field_paper
    
    date_began = None
    if cleaned_data.has_key('date_assessment_comp'):
        date_began = cleaned_data['date_assessment_comp']
        
    form_type_id = None
    if cleaned_data.has_key('form_type'):
        form_type_id = cleaned_data['form_type']
        
    capture_site_id = None
    
    time_stamp_created = datetime.datetime.now().date()
        
    form_to_return = Form (
                    form_pk = None,
                    form_guid = form_guid,
                    form_type_id = form_type_id,
                    form_subject_id = form_subject_id,
                    form_area_id = None,
                    form_title = None,
                    date_began = date_began,
                    date_ended = None,
                    date_filled_paper = None,
                    person_id_filled_paper = person_id_field_paper,
                    wfc_id_hidden = person_id_field_paper,
                    org_unit_id_filled_paper = org_unit_id_field_paper,
                    capture_site_id = field_provider.get_capture_site_id(),
                    timestamp_created = time_stamp_created,
                    user_id_created = None,
                    timestamp_updated = None,
                    user_id_updated = None,
                    questions = None
                 )
    return form_to_return

def update_form_basic_data(form, frm_id):
    try:
        if frm_id:
            Forms.objects.filter(pk = frm_id).update(
                                                        form_type_id = form.form_type_id,
                                                        form_subject_id = form.form_subject_id,
                                                        form_area_id = form.form_area_id,
                                                        form_title = form.form_title,
                                                        date_began = form.date_began,
                                                        date_ended = form.date_ended,
                                                        date_filled_paper = form.date_filled_paper,
                                                        person_id_filled_paper = form.person_id_filled_paper,
                                                        org_unit_id_filled_paper = form.org_unit_id_filled_paper,
                                                        capture_site_id = field_provider.get_capture_site_id(),
                                                        timestamp_updated = form.timestamp_created,
                                                        user_id_updated = form.user_id_created,
                                                        is_void = False
                                                    )
            form.form_pk = frm_id
    except Exception as e:
        print e
    return form


def save_form_basic_data_basic_service_referral(form, formid=None, encounterid=None):
    
    created_form_model = None
    try:
        if formid and Forms.objects.filter(pk=formid).count() > 0:
                
                Forms.objects.filter(pk=formid).update(
                                                        form_area_id = form.form_area_id,
                                                        capture_site_id = field_provider.get_capture_site_id(),
                                                        timestamp_updated = form.timestamp_updated,
                                                        org_unit_id_filled_paper = form.org_unit_id_filled_paper,
                                                        user_id_updated = form.user_id_created,
                                                        is_void = False
                                                    )
                created_form_model = Forms.objects.get(pk=formid, is_void=False)
                form.form_pk = created_form_model.pk
                form.form_guid = created_form_model.form_guid
        else:
            form = save_form_basic_data(form)
             
    except Exception as e:
        print "saving exiting basic referral form failed."
        raise e

    return form

def save_form_basic_data(form):
    try:
        
        created_form_model = Forms.objects.create(
                                            form_guid = form.form_guid,
                                            form_type_id = form.form_type_id,
                                            form_subject_id = form.form_subject_id,
                                            form_area_id = form.form_area_id,
                                            form_title = form.form_title,
                                            date_began = form.date_began,
                                            date_ended = form.date_ended,
                                            date_filled_paper = form.date_filled_paper,
                                            person_id_filled_paper = form.person_id_filled_paper,
                                            org_unit_id_filled_paper = form.org_unit_id_filled_paper,
                                            capture_site_id = field_provider.get_capture_site_id(),
                                            timestamp_created = form.timestamp_created,
                                            user_id_created = form.user_id_created,
                                            timestamp_updated = form.timestamp_updated,
                                            user_id_updated = None,
                                            is_void = False
                                            )
        form.form_pk = created_form_model.pk
    except Exception as e:
        print "saving basic form data failed"
        print e
        raise e
    return form
    

def create_questions_object_from_data(form_t):
    '''
    This method is for getting the questions in object forms. The questions are populated with all the needed data like the answer object
    '''
    questions_db = ListQuestions.objects.filter(form_type_id=form_t,is_void=False)
    questions = []
    for question in questions_db:
        questions.append(Question(
                                     question_id = question.pk,
                                     question_text = question.question_text,
                                     question_code = question.question_code,
                                     posible_answers = None,
                                     answer_type = question.answer_type_id,
                                     answer_set_id = question.answer_set_id,
                                     answers = None
                                 )
                         )
    return questions

def get_answers_for_questions(questions,cleaned_data):
    '''
    This method is for attaching the selected answers and the possible answers for each question to the question object
    '''
    questions_to_return = []
    for question in questions:
        possible_answers = get_valid_answers(question.answer_set_id)
        selected_answers = get_selected_answers(question.question_code,cleaned_data,True if question.answer_type == db_text.ANSWER_TYPE_MULTI_SELECT else None,question.answer_set_id, True if question.answer_type == db_text.ANSWER_TYPE_SINGLE_SELECT else None )
        question.posible_answers = possible_answers
        question.answers = selected_answers
        questions_to_return.append(question)
    return questions_to_return
    
def get_valid_answers(answer_set_id,answer_id=None):
    '''
    This method gets the answers that are acceptable according to the available answers in the database
    '''
    answers = []
    answers_to_return = []
    if answer_set_id:
        if answer_id:
            answers = ListAnswers.objects.filter(pk=answer_id,answer_set_id=answer_set_id)
        else:
            answers = ListAnswers.objects.filter(answer_set_id=answer_set_id)
        
        for answer in answers:
            answers_to_return.append(get_answer(answer.pk,answer.answer))
    return answers_to_return

def get_selected_answers(question_code,cleaned_data, get_multi_values=False, answer_set_id=None, single_select=None):
    '''
    This method is for getting the selected/entered answers from the form
    '''
    answers_to_return = []
    entered_answers = None
    if cleaned_data.has_key(question_code):
        entered_answers = cleaned_data[question_code]
    
    if get_multi_values:
        for answer_val in entered_answers:
            answers_to_return = answers_to_return + get_valid_answers(answer_set_id,answer_val)
    elif single_select:
        if entered_answers:
            if ListAnswers.objects.filter(pk=int(entered_answers)).count() == 1:
                answer = ListAnswers.objects.get(pk=int(entered_answers))
                if answer:
                    answers_to_return.append(get_answer(answer.pk,answer.answer))
            else:
                print 'found more than 1 entered answer for a single select field, check method get_selected_answers() in forms_helper file'
    else:
        if entered_answers:
            answers_to_return.append(get_answer(1,entered_answers))
            
    return answers_to_return

def get_answer(ans_id,ans_val):
    '''
    This method gets the answer object
    '''
    return Answer(
                   answer_id = ans_id,
                   answer = ans_val
                  )
    
def save_questions(form):
    for question in form.questions:
        if question.answers:
            save_question_by_type(question.answer_type,question,form)
    
        
def save_question_by_type(answer_type, question, form):
    '''
    This is a dictionary method used to call the appropriate save method based on the type of answer expected from a question
    '''
    if answer_type:
        return {
                db_text.ANSWER_TYPE_MULTI_SELECT: save_select_question,
                db_text.ANSWER_TYPE_NUMERIC: save_numeric_question,
                db_text.ANSWER_TYPE_DECIMAL: save_numeric_question,
                db_text.ANSWER_TYPE_TEXT: save_text_question,
                db_text.ANSWER_TYPE_SINGLE_SELECT: save_select_question,
                db_text.ANSWER_TYPE_DATE: save_date_question
                }[answer_type](question, form)
    return None 

def save_select_question(question, form):
    '''
    used for saving multi-select and single select answers
    '''
    
        
    for answer in question.answers:
        if answer.answer_id:
            if question.question_id == 13:
                print 'answer for 13', answer.answer_id
            try:
                FormGenAnswers.objects.create(form_id=form.form_pk,question_id=question.question_id,answer_id=answer.answer_id)
            except Exception as e:
                print e
                raise e
    
def save_numeric_question(question, form):
    '''
    used for saving numeric answers
    '''
    for answer in question.answers:
        if answer.answer:
            FormGenNumeric.objects.create(form_id=form.form_pk,question_id=question.question_id,answer=answer.answer)
    
def save_text_question(question, form):
    '''
    used for saving text answers
    '''
    for answer in question.answers:
        if answer.answer:
            FormGenText.objects.create(form_id=form.form_pk,question_id=question.question_id,answer_text=answer.answer)

def save_date_question(question, form):
    '''
    used for saving date answers
    '''
    for answer in question.answers:
        if answer.answer:
            FormGenDates.objects.create(form_id=form.form_pk,question_id=question.question_id,answer_date=answer.answer)
            

def Load_form(form_id, get_json = False, user= None,searchtokens=None,search_date=None,form_type=None):
    print 'passed parameters', form_id, get_json, user,searchtokens,search_date,form_type
    
    forms = []
    form_to_return = []
    if form_id == 'all' or form_id == 'search':
        if searchtokens:
            print 'here now', form_id,len(searchtokens),searchtokens[0]
            if form_id == 'all':# or (len(searchtokens) == 1 and not searchtokens[0]):
                forms = Forms.objects.filter(is_void = False)
            elif form_id == 'search':
                all_forms = Forms.objects.filter(is_void = False)
                forms = search_forms(all_forms,search_date,form_type,searchtokens)
        else:
            forms = Forms.objects.filter(is_void = False)
            
    elif form_id:
        if Forms.objects.filter(pk = form_id, is_void = False).count()==1:
            form = Forms.objects.get(pk = form_id, is_void = False)
            form_object = get_form_from_db_obj(form)
            return form_object
        elif Forms.objects.filter(pk = form_id, is_void = False).count()>1:
            print 'something went terribly worng when loading a form in file forms_helper, method Load_form, there appears to be dublicates in the database for forms'
        
    if forms:
        form_to_return += get_form_from_db_object(forms)
        
    if get_json:
        return get_json_form(form_to_return, user)
        
    return form_to_return   

def search_forms(all_forms,search_date,form_type,searchtokens):
    print 'searching...'
    forms_to_return = []
    search_by_token = token_entered(searchtokens)
    for form in all_forms:
        subject_match = False
        area_match = False
        form_type_match = False
        date_match = False
        if search_by_token:
            if form.form_area_id:
                if SetupGeorgraphy.objects.filter(pk=form.form_area_id).count() == 1:
                    for searchtoken in searchtokens:
                        community = SetupGeorgraphy.objects.get(pk=SetupGeorgraphy.objects.get(pk=form.form_area_id).parent_area_id)
                        district = SetupGeorgraphy.objects.filter(pk=community.parent_area_id, area_name__icontains=searchtoken)
                        if SetupGeorgraphy.objects.filter(pk=form.form_area_id,area_name__icontains=searchtoken).count() > 0 or district.count() > 0:
                            area_match = True
        else:
            area_match = True
        
        if search_by_token:
            if form.form_subject_id:
                for searchtoken in searchtokens:
                    q_list = []
                    q_list.append(Q(first_name__icontains=searchtoken.lower()))
                    q_list.append(Q(other_names__icontains=searchtoken.lower()))
                    q_list.append(Q(surname__icontains=searchtoken.lower()))
                    q_list.append(Q(beneficiary_id__icontains=searchtoken.lower()))
                    q_list.append(Q(workforce_id__icontains=searchtoken.lower()))
                    if RegPerson.objects.filter(reduce(operator.or_, q_list),pk=form.form_subject_id,is_void=False).count() > 0:
                        subject_match = True
        else:
            subject_match = True  
        
        if form_type:
            if form.form_type_id == form_type:
                form_type_match = True
        else:
            form_type_match = True
        
        if search_date:
            date_search_date = datetime.datetime.strptime(str(search_date), '%d-%B-%Y')
            date_date_began = datetime.datetime.strptime(str(form.date_began), '%Y-%m-%d')
            if date_date_began == date_search_date:
                date_match = True
        else:
            date_match = True
        
        print 'matches',subject_match , area_match , form_type_match , date_match
        
        if (subject_match or area_match) and form_type_match and date_match:
            forms_to_return.append(form)
        
    return forms_to_return

def token_entered(searchtokens):
    for searchtoken in searchtokens:
        if searchtoken:
            return True
    return False

def get_json_form(form_to_return, user):
    forms = []
    form_types_tuple = get_form_type_tuple()
    for form in form_to_return:
        timestamp = ''
        if form.timestamp_updated:
            timestamp = form.timestamp_updated.strftime(val.date_input_format)  
        elif form.timestamp_created: 
            timestamp = form.timestamp_created.strftime(val.date_input_format)
        
        subj = get_subject_name_id(form.form_subject_id,form.form_type_id)
        
        forms.append(
                         {
                            'form_id': form.form_pk,
                            'type': get_form_type_name_from_tuple(form_types_tuple,form.form_type_id),
                            'subject': form.form_title if subj is None or subj == "" else subj,
                            'location': get_ward_district_name(form.form_area_id),
                            'date': form.date_began.strftime(val.date_input_format) if form.date_began else '',
                            'end_date': form.date_ended.strftime(val.date_input_format) if form.date_ended else '',
                            'date_last_updated': timestamp,
                            'person_last_updated': get_user_updated_name(form.user_id_created if not form.timestamp_updated else form.user_id_updated),
                            'user_can_view': can_user_view_form(form.form_type_id, user, form),
                            'user_can_edit': can_user_edit_form(form.form_type_id, user, form),
                            'type_id': form.form_type_id,
                            'subject_id':form.form_subject_id,
                        }
                    )
    return forms

def can_user_view_form(form_type, user, form):
    '''
    Implement logic to handle whether the user can view a given form type here
    '''
    if form_type:
        return {
                'FT3e': field_provider.can_view_form,
                'FT3d': dummy_func_returns_true,
                'FT2d': can_user_view_public_workforce_forms,
                'FT2c': dummy_func_returns_true,
                'FT1g': dummy_func_returns_true,
                'FT1f': dummy_func_returns_true,
                'FT1c': dummy_func_returns_true,
                'FT1e': can_user_view_public_workforce_forms,
                'FT3f': dummy_func_returns_true,
                }[form_type](user, form)
    return None

def can_user_add_form(form_type, user, subject):
    '''
    Implement logic to handle whether the user can add a given form type here
    '''
    if form_type:
        return {
                'FT3e': dummy_func_returns_true,
                'FT3d': can_user_add_form_gen,
                'FT2d': can_user_add_workforce_training,#can_user_edit_public_workforce_forms,
                'FT2c': can_user_add_form_gen,
                'FT1g': dummy_func_returns_true,
                'FT1f': can_user_add_residential_institution,
                'FT1c': dummy_func_returns_true,
                'FT1e': can_user_add_public_senstization,#can_user_edit_public_workforce_forms,
                'FT3f': can_user_add_form_gen,
                }[form_type](user, subject)
    return None

def can_user_add_public_senstization(user,subject):
    if user.is_superuser:
        return True
    if user.has_perm('auth.enter frm low sensitive'):
        return True
    return False

def can_user_add_residential_institution(user,subject):
    if user.is_superuser:
        return True
    if user.has_perm('auth.enter frm high sensitive own') or user.has_perm('auth.enter frm high sensitive') or user.has_perm('auth.enter frm as soc welf'):
        return True
    return False

def can_user_add_workforce_training(user,subject):
    if user.is_superuser:
        return True
    if user.has_perm('auth.edit frm low sensitive own'):
        return True
    return False

def can_user_add_form_gen(user,subject):
    if user.is_superuser:
        return True
    if user.has_perm('auth.enter frm high sensitive own') or user.has_perm('auth.enter frm high sensitive'):
        return True
    return False

def can_user_edit_form(form_type, user, form):
    '''
    Implement logic to handle whether the user can edit a given form type here
    '''
    if form_type:
        return {
                'FT3e': can_user_edit_form_with_data_supervisor,
                'FT3d': can_user_edit_form_generic,
                'FT2d': can_user_edit_workforce_training_and_pub_sense,
                'FT2c': can_user_edit_form_generic,
                'FT1g': can_user_edit_form_with_data_supervisor,
                'FT1f': can_user_edit_form_generic,
                'FT1c': can_user_edit_form_with_data_supervisor,
                'FT1e': can_user_edit_workforce_training_and_pub_sense,
                'FT3f': can_user_edit_form_generic,
                }[form_type](user, form)
    return None

def can_user_edit_workforce_training_and_pub_sense(user, form):
    print 'user perms',user.has_perm('auth.edit frm low sensitive own'),user_and_person_filled_paper_same_user(user, form.user_id_created)
    if user.is_superuser:
        return True
    if user.has_perm('auth.edit frm low sensitive own') and user_and_person_filled_paper_same_user(user, form.user_id_created):
        return True
    if user.has_perm('auth.edit frm l sense all in org') and user_and_person_filled_paper_same_org(user, form.wfc_id_hidden):
        return True
    return False

def can_user_edit_form_generic(user,form):
    if user.is_superuser:
        return True
    if user.has_perm('auth.edit form sensitive own') and user_and_person_filled_paper_same_user(user, form.user_id_created):
        return True
    if user.has_perm('auth.edit frm l sense all in org') and user_and_person_filled_paper_same_org(user, form.wfc_id_hidden):
        return True
    return False

def user_and_person_filled_paper_same_user(user,user_id_created):
    if user_id_created:
        if user.id:
            if str(user.id) == str(user_id_created):
                return True
    return False

def user_and_person_filled_paper_same(user,person_filled_paper_id):
    if person_filled_paper_id:
        m_person = user.reg_person
        if m_person:
            if m_person.pk == person_filled_paper_id:
                return True

def can_user_edit_form_with_data_supervisor(user,form):
    if user.is_superuser:
        return True
    if user_and_person_filled_paper_same_org(user, form.wfc_id_hidden):
        return True
    if field_provider.user_has_role(user,"Data supervisor"):
        return True
    return False

def can_user_edit_form_gen(user, subject):
    if user.has_perm('auth.edit frm low sensitive own') or user.has_perm('auth.edit form sensitive own') \
        or user.has_perm('auth.edit frm l sense all in org') or can_user_edit_public_workforce_forms(user, form):
        return True
    

def can_user_add_res_institution(user, subject):
    to_return = False
    org_id_int = subject
    #print user.has_perm('auth.enter frm as soc welf'),'auth.enter frm as soc welf'
    #print user.has_perm('auth.enter frm high sensitive'),'auth.enter frm high sensitive'
    #print org_is_primary_parent(user, org_id_int),'org_is_primary_parent(user, org_id_int)'
    if user.has_perm('auth.enter frm as soc welf') or user.has_perm('auth.enter frm high sensitive') \
        or org_is_primary_parent(user, org_id_int):
        to_return = True
    return to_return

def can_user_view_public_workforce_forms(user, form):
    to_return = False
    if user.is_superuser:
        return True
    if user.has_perm('auth.View form - low sensitivity'):
        to_return = True
    return to_return

def can_user_edit_public_workforce_forms(user, form):
    to_return = False
    user_filled_form = False
    if user.is_superuser:
        return True
    person_filled_form_id = form.person_id_filled_paper
    if person_filled_form_id:
        m_person = user.reg_person
        if m_person.pk == person_filled_form_id:
            user_filled_form = True

    if (user.has_perm('auth.Edit form - low sensitivity - uploaded by self') and person_filled_form) or \
            (user.has_perm('auth.Edit form - low sensitivity - uploaded by anyone in org unit') \
        and user_and_person_filled_paper_same_org(user, person_filled_form_id)):
        to_return = True
    return to_return

def user_and_person_filled_paper_same_org(user, person_filled_form_id):
    toReturn = False
    #person_filled_form = RegPerson.objects.get(pk = person_filled_form_id, is_void = False) #Commented out because its not being used and its throwing an exception
    person_filled_form_orgs_list = set(RegPersonsOrgUnits.objects.filter(person_id=person_filled_form_id).values_list('parent_org_unit_id', flat=True))
    user_orgs_list = set(RegPersonsOrgUnits.objects.filter(person_id=user.reg_person.pk).values_list('parent_org_unit_id', flat=True))

    for org_id in user_orgs_list:
        if org_id in person_filled_form_orgs_list:
            return True
    return toReturn

def can_user_edit_res_institution(user, form):
    to_return = False
    org_id_int = form.form_subject_id
    #print user.has_perm('auth.enter frm as soc welf'),'auth.enter frm as soc welf'
    #print user.has_perm('auth.enter frm high sensitive'),'auth.enter frm high sensitive'
    #print org_is_primary_parent(user, org_id_int),'org_is_primary_parent(user, org_id_int)'
    if user.has_perm('auth.enter frm as soc welf') or user.has_perm('auth.enter frm high sensitive') \
        or org_is_primary_parent(user, org_id_int):
        to_return = True
    return to_return

def org_is_primary_parent(user, org_id_int):
    if user.is_superuser:
        return True
    to_return = False
    if user.reg_person:
        if len(RegPersonsOrgUnits.objects.filter(person_id = user.reg_person.pk, parent_org_unit_id=int(org_id_int), date_delinked=None, is_void=False)) > 0:
            to_return = True   
    return to_return


def dummy_func_returns_true(user=None, subject=None):
    return True

def get_ward_district_name(ward_id):
    community = None
    district = None
    ward = None
    ward_district_string = ''
    
    if SetupGeorgraphy.objects.filter(pk=ward_id,is_void=False).count() == 1:
        ward = SetupGeorgraphy.objects.get(pk=ward_id,is_void=False)
    if ward and SetupGeorgraphy.objects.filter(pk=ward.parent_area_id,is_void=False).count() == 1:
        community = SetupGeorgraphy.objects.get(pk=ward.parent_area_id,is_void=False)
    if community and SetupGeorgraphy.objects.filter(pk=community.parent_area_id,is_void=False).count() == 1:
        district = SetupGeorgraphy.objects.get(pk=community.parent_area_id,is_void=False)
    if ward and district:
           ward_district_string = '%s - %s'%(ward.area_name, district.area_name)
    
    return ward_district_string

def get_user_updated_name(user_id):
    user_to_return = ''
    if AppUser.objects.filter(pk=user_id).count() == 1:
        app_user = AppUser.objects.get(pk=user_id)
        if app_user.is_superuser:
            user_to_return = 'Super User'
        else:
            user_to_return = app_user.reg_person.full_name
    else:
        print 'something went wrong when loading logged in user from file forms_helper, method get_user_updated_name()'
    return user_to_return

def get_subject_name_id(subject_id,form_type_id):
    subject_to_return = ''
    if form_type_id == 'FT3e': 
        subject_to_return = get_person_name_id('wfc',subject_id)
    elif form_type_id == 'FT3d':
        subject_to_return = get_person_name_id('ben',subject_id)
    elif form_type_id == 'FT2d':
        subject_to_return = None
    elif form_type_id == 'FT2c':
        subject_to_return = get_person_name_id('wfc',subject_id)
    elif form_type_id == 'FT1g':
        subject_to_return = get_org_name_id(subject_id)
    elif form_type_id == 'FT1f':
        subject_to_return = get_org_name_id(subject_id)
    elif form_type_id == 'FT1c':
        subject_to_return = get_org_name_id(subject_id)
    elif form_type_id == 'FT1e':
        subject_to_return = ''
    elif form_type_id == 'FT3f':
        subject_to_return = get_person_name_id('ben',subject_id)
    return subject_to_return
    
 
def get_person_name_id(person_type,person_id):
    person_to_return=''
    person = None
    if person_id:
        if RegPerson.objects.filter(pk=person_id,is_void=False).count() == 1:
            person = RegPerson.objects.get(pk=person_id,is_void=False)
        else:
            print 'something went wrong when loading person from file forms_helper, method get_person_name_id()'
        
    if person_type == 'wfc':
        if person:
            person_to_return = '%s - %s'%(person.workforce_id if person.workforce_id else person.national_id, person.full_name)
    elif person_type == 'ben':
        if person:
            person_to_return = '%s - %s'%(person.beneficiary_id if person.beneficiary_id else person.national_id, person.full_name)
            
    return person_to_return

def get_person_name(pk_id):
    if RegPerson.objects.filter(pk=pk_id).count() == 1:
        return RegPerson.objects.get(pk=pk_id).full_name
    else:
        return None

def get_setup_item_short_description(encounter_id):
    if SetupList.objects.filter(item_id=encounter_id).count() == 1:
        return SetupList.objects.get(item_id=encounter_id).item_description_short
    else:
        return None

def get_org_name_id(org_id):
    org_to_return = ''
    if RegOrgUnit.objects.filter(pk=org_id).count() == 1:
        org = RegOrgUnit.objects.get(pk=org_id)
        org_to_return = '%s - %s'%(org.org_unit_id_vis,org.org_unit_name)
    else:
        print 'something went wrong when loading org from file forms_helper, method get_org_name_id()'
    return org_to_return
    

def get_form_type_name_from_tuple(form_types,form_type_id):
    for form_t_id, form_description in form_types:
        if form_t_id == form_type_id:
            return form_description
    return ''

def get_form_type_tuple():
    return tuple([(l.item_id,l.item_description) for l in SetupList.objects.filter(item_category='Form type')])
        
def get_form_from_db_object(forms):
    forms_to_return = []
    for form in forms:
        form_object = get_form_from_db_obj(form)
        forms_to_return.append(form_object)
        
    return forms_to_return

def get_form_from_db_obj(form):
    form_object = None
    if form:
        questions = create_questions_object_from_data(form.form_type_id)
        wfc_filled = None
        if form.person_id_filled_paper:
            if RegPerson.objects.filter(pk=form.person_id_filled_paper, is_void=False).count() == 1:
                person = RegPerson.objects.get(pk=form.person_id_filled_paper, is_void=False)
                wfc_filled = person.workforce_id
                wfc_filled += "\t %s"%person.national_id if person.national_id else ""
                wfc_filled += "\t %s"%person.full_name if person.full_name else ""
        
        form_object = Form (
                                        form_pk = form.pk,
                                        form_guid = form.form_guid,
                                        form_type_id = form.form_type_id,
                                        form_subject_id = form.form_subject_id,
                                        form_area_id = form.form_area_id,
                                        form_title = form.form_title,
                                        date_began = form.date_began,
                                        date_ended = form.date_ended,
                                        date_filled_paper = form.date_filled_paper,
                                        person_id_filled_paper = wfc_filled,
                                        wfc_id_hidden = form.person_id_filled_paper,
                                        org_unit_id_filled_paper = form.org_unit_id_filled_paper,
                                        capture_site_id = form.capture_site_id,
                                        timestamp_created = form.timestamp_created,
                                        user_id_created = form.user_id_created,
                                        timestamp_updated = form.timestamp_updated,
                                        user_id_updated = form.user_id_updated,
                                        questions = questions
                                     )
        questions_with_answers = get_answers_for_questions_from_db(form_object)
        form_object.questions = questions_with_answers
    return form_object
        
def get_answers_for_questions_from_db(form):
    questions_to_return = []
    for question in form.questions:
        possible_answers = get_valid_answers(question.answer_set_id)
        selected_answers = get_selected_answers_from_db_and_object(possible_answers, form.form_pk, question)
        question.answers = selected_answers
        questions_to_return.append(question)
        
    return questions_to_return

def get_selected_answers_from_db_and_object(possible_answers, form_id, question):
    answer_db_object = []
    #Search for answer in the possible answers and return it if its in the database as entered in the current form
    if possible_answers and (question.answer_type == db_text.ANSWER_TYPE_MULTI_SELECT or question.answer_type == db_text.ANSWER_TYPE_SINGLE_SELECT):
        for answer in possible_answers:
            for ans in FormGenAnswers.objects.filter(form_id=form_id,question_id=question.question_id):
                if answer.answer_id == ans.answer_id:
                    answer_db_object.append(answer)
                    continue
                
    elif question.answer_type == db_text.ANSWER_TYPE_TEXT:
        if FormGenText.objects.filter(form_id=form_id,question_id=question.question_id).count() == 1:
            answer = FormGenText.objects.get(form_id=form_id,question_id=question.question_id)
            answer_db_object.append(get_answer(1,answer.answer_text))
        elif FormGenText.objects.filter(form_id=form_id,question_id=question.question_id).count() > 1:
            print 'An error occurred loaded more than one answer for a single question id: %s'% question.question_id
    
    elif question.answer_type == db_text.ANSWER_TYPE_DATE:
        if FormGenDates.objects.filter(form_id=form_id,question_id=question.question_id).count() == 1:
            answer = FormGenDates.objects.get(form_id=form_id,question_id=question.question_id)
            answer_db_object.append(get_answer(1,answer.answer_date))
        elif FormGenDates.objects.filter(form_id=form_id,question_id=question.question_id).count() > 1:
            print 'An error occurred loaded more than one answer for a single question id: %s'% question.question_id
        
    elif question.answer_type == db_text.ANSWER_TYPE_NUMERIC or question.answer_type == db_text.ANSWER_TYPE_DECIMAL:
        if FormGenNumeric.objects.filter(form_id=form_id,question_id=question.question_id).count() == 1:
            answer = FormGenNumeric.objects.get(form_id=form_id,question_id=question.question_id)
            if question.answer_type == db_text.ANSWER_TYPE_NUMERIC:
                answer_db_object.append(get_answer(1,int(answer.answer)))
            elif question.answer_type == db_text.ANSWER_TYPE_DECIMAL:
                answer_db_object.append(get_answer(1,answer.answer))
        elif FormGenNumeric.objects.filter(form_id=form_id,question_id=question.question_id).count() > 1:
            print 'An error occurred loaded more than one answer for a single question id: %s'% question.question_id
    
    return answer_db_object

@transaction.commit_on_success
def delete_form(form_id):
    try:
        delete_form_data(form_id)
        void_old_form(form_id)
        save_to_admin_upload(form_id)
    except Exception as e:
        print e
    
def delete_form_data(form_id):
    delete_questions(form_id)
    form_type = get_form_type(form_id)
    delete_custom_form_questions(form_type,form_id)
    
def get_form_type(frm_id):
    if Forms.objects.filter(pk=frm_id).count() == 1:
        return Forms.objects.get(pk=frm_id).form_type_id
    elif Forms.objects.filter(pk=frm_id).count() > 1:
        print 'An error occurred while loading form. Loaded more than one Forms object for a single pk_id:'% frm_id
    
def delete_custom_form_questions(form_type,form_id):
    if form_type:
        return {
                'FT3e': None,
                'FT3d': delete_ovc_assess_custom_questions,
                'FT2d': delete_workforce_training_custom_questions,
                'FT2c': form_with_no_custom,
                'FT1g': None,
                'FT1f': delete_residential_custom_questions,
                'FT1c': None,
                'FT1e': delete_public_sensitisation_custom_questions,
                'FT3f': delete_custom_child_in_conflict_law,
                }[form_type](form_id)
    return None

def form_with_no_custom(form_id):
    Nothing_saved = None
    
def delete_custom_child_in_conflict_law(frm_id):
    try:
        CoreEncounters.objects.filter(form_id=frm_id).delete()
        CoreEncountersNotes.objects.filter(form_id=frm_id).delete()
    except Exception as e:
        print e
        raise e
        

def delete_public_sensitisation_custom_questions(form_id):
    try:
        FormPersonParticipation.objects.filter(form_id=form_id).delete()
        FormOrgUnitContributions.objects.filter(form_id=form_id).delete()
    except Exception as e:
        print e
        raise e

def delete_workforce_training_custom_questions(form_id):
    try:
        FormPersonParticipation.objects.filter(form_id=form_id).delete()
        FormOrgUnitContributions.objects.filter(form_id=form_id).delete()
    except Exception as e:
        print e
        raise e

def delete_residential_custom_questions(form_id):
    try:
        FormResChildren.objects.filter(form_id=form_id).delete()
        FormResWorkforce.objects.filter(form_id=form_id).delete()
        CoreAdverseConditions.objects.filter(form_id=form_id).update(is_void=True)
    except Exception as e:
        print e
        raise e
    
def delete_ovc_assess_custom_questions(frm_id):
    #delete adverse conditions
    CoreAdverseConditions.objects.filter(form_id=frm_id).update(is_void=True)
    
    #delete csi
    FormCsi.objects.filter(form_id=frm_id).delete()

def delete_wfc_assess_custom_questions(frm_id):
    #delete adverse conditions
    CoreAdverseConditions.objects.filter(form_id=frm_id).update(is_void=True)
    
    #delete csi
    FormCsi.objects.filter(form_id=frm_id).delete()
    
    
def delete_questions(frm_id):
    FormGenAnswers.objects.filter(form_id=frm_id).delete()
    FormGenText.objects.filter(form_id=frm_id).delete()
    FormGenNumeric.objects.filter(form_id=frm_id).delete()
    FormGenDates.objects.filter(form_id=frm_id).delete()
    
def get_child_status_data(form_id):
    m_res_children = FormResChildren.objects.filter(form_id = form_id)    
    return list(m_res_children)
        
def get_child_status_json(form_id):
    '''
    Returns a dictionary with the child status data that is used to populate the
    table in Section C of the Residential Institution Form
    '''
    to_return = {}
    res_children_models = get_child_status_data(form_id)
    if res_children_models:
        count = 1
        for child_data in res_children_models:
            child_status_data = {}
            child_id_int = child_data.child_person_id
            beneficiary_id = ''
            child_name = ''
            adverse_conditions = ''
            details = ''
            residential_status_id = ''
            family_status_id = ''
            court_committal_id = ''
            if child_id_int:
                ''' Get child details '''
                if len(RegPerson.objects.filter(pk=child_id_int, is_void=False)) > 0:
                    m_child = RegPerson.objects.filter(pk=child_id_int, is_void=False)[0]
                    beneficiary_id = m_child.beneficiary_id
                    child_name = m_child.first_name + ' ' + m_child.surname
            ''' Get adverse conditions '''
            if len(CoreAdverseConditions.objects.filter(form_id=form_id, beneficiary_person_id=child_id_int, is_void=False)) > 0:
                adverse_models = CoreAdverseConditions.objects.filter(form_id=form_id, beneficiary_person_id=child_id_int, is_void=False)
                for model in list(adverse_models):
                    adverse_conditions = adverse_conditions + ' ' + model.adverse_condition_id
                    adverse_conditions.lstrip(' ')
            ''' construct the details text '''
            residential_status_id = child_data.residential_status_id
            family_status_id = child_data.family_status_id
            court_committal_id = child_data.court_committal_id
            
            resedential_status_text = None
            if residential_status_id:
                resedential_status_text = field_provider.get_description_for_item_id(residential_status_id)[0]
            family_status_text = None
            if family_status_id:
                family_status_text = field_provider.get_description_for_item_id(family_status_id)[0]
            court_committal_text = None
            if court_committal_id:
                court_committal_text = field_provider.get_description_for_item_id(court_committal_id)[0]

            if resedential_status_text:
                details = resedential_status_text
            if family_status_text:
                details = details + ' - ' + family_status_text
            if court_committal_text:
                details = details + ' - ' + court_committal_text
            details.lstrip(' - ').rstrip(' - ')
                    
            child_status_data['child_id_int'] = child_id_int
            child_status_data['beneficiary_id'] = beneficiary_id
            child_status_data['child_name'] = child_name
            child_status_data['adverse_conditions'] = adverse_conditions
            child_status_data['details'] = details
            child_status_data['current_residential_status'] = residential_status_id
            child_status_data['family_status'] = family_status_id
            child_status_data['has_court_committal_order'] = court_committal_id
            child_status_data['date_first_admitted'] = child_data.date_admitted.strftime(val.date_input_format) if child_data.date_admitted else ''
            child_status_data['date_left'] = child_data.date_left.strftime(val.date_input_format) if child_data.date_left else ''
            
            to_return[count] = child_status_data
            count = count + 1
    return json.dumps(to_return)

def get_organisation_contributions_json(form_id):
    '''
    Returns a dictionary with the organisations data that is used to populate the
    table in Section C of the Workforce Training and Public Sensitisation Forms
    '''
    to_return = {}
    org_ids_list = set(FormOrgUnitContributions.objects.filter(form_id=form_id).values_list('org_unit_id', flat=True))
    if org_ids_list:
        count = 1
        for org_id in org_ids_list:
            org_contrib_data = {}
            org_id_int = org_id
            org_id_vis = None
            onrg_name = None


            if len(RegOrgUnit.objects.filter(pk=org_id_int, is_void=False)) > 0:
                m_org = RegOrgUnit.objects.filter(pk=org_id_int, is_void=False)[0]
                org_id_vis = m_org.org_unit_id_vis
                onrg_name = m_org.org_unit_name

            m_org_contribs = list(FormOrgUnitContributions.objects.filter(form_id=form_id, org_unit_id=org_id))
            org_contributions = ''
            org_contribution_ids = ''
            for m_org_contrib in m_org_contribs:    
                contribution_id = m_org_contrib.contribution_id
                contibution_name = field_provider.get_item_desc_from_id(item_id=contribution_id)
                org_contribution_ids = org_contribution_ids + ' - ' + contribution_id if org_contribution_ids else contribution_id
                org_contributions = org_contributions + ' - ' + contibution_name if org_contributions else contibution_name
            org_contrib_data['org_unit_name'] = onrg_name
            org_contrib_data['org_unit_id'] = org_id_vis
            org_contrib_data['org_contribution'] = org_contributions
            org_contrib_data['org_contribution_id'] = org_contribution_ids
            
            to_return[count] = org_contrib_data
            count = count + 1
    return json.dumps(to_return)

def get_beneficiary_status_json(form_id):
    '''
    Returns a dictionary with the beneficiary data that is used to populate the
    table in Section D of the Public Sensitisation Form
    '''
    to_return = {}
    m_ben_participation = list(FormPersonParticipation.objects.filter(form_id=form_id))
    if m_ben_participation:
        count = 1
        for m_paticipation in m_ben_participation:
            m_paticipation_data = {}
            beneficiary_id_int = int(m_paticipation.workforce_or_beneficiary_id)
            beneficiary_id_vis = None
            beneficiary_nrc = None
            beneficiary_name = None

            if len(RegPerson.objects.filter(pk=beneficiary_id_int, is_void=False)) > 0:
                m_person = RegPerson.objects.filter(pk=beneficiary_id_int, is_void=False)[0]
                beneficiary_id_vis = m_person.beneficiary_id
                beneficiary_nrc = m_person.national_id
                beneficiary_birth_reg_id = m_person.birth_reg_id
                beneficiary_name = m_person.first_name + ' ' + m_person.surname
                ben_birth_cert_or_nrc = None
                if beneficiary_nrc:
                    ben_birth_cert_or_nrc = beneficiary_nrc
                elif beneficiary_birth_reg_id:
                    ben_birth_cert_or_nrc = beneficiary_birth_reg_id

            participation_level_id = m_paticipation.participation_level_id
            participation_level = field_provider.get_item_desc_from_id(item_id=participation_level_id)

            m_paticipation_data['person_id'] = m_person.pk
            m_paticipation_data['beneficiary_id'] = beneficiary_id_vis
            m_paticipation_data['ben_birth_cert_or_nrc'] = ben_birth_cert_or_nrc
            m_paticipation_data['birth_reg_id'] = beneficiary_birth_reg_id
            m_paticipation_data['national_id'] = beneficiary_nrc
            m_paticipation_data['beneficiary_name'] = beneficiary_name            
            m_paticipation_data['participation_level'] = participation_level
            m_paticipation_data['participation_level_id'] = participation_level_id
            
            to_return[count] = m_paticipation_data
            count = count + 1
    return json.dumps(to_return)

def get_workforce_status_json(form_id):
    '''
    Returns a dictionary with the workforce data that is used to populate the
    table in Section D of the Workforce Training Form
    '''
    to_return = {}
    m_wfc_participation = list(FormPersonParticipation.objects.filter(form_id=form_id))
    if m_wfc_participation:
        count = 1
        for m_paticipation in m_wfc_participation:
            m_paticipation_data = {}
            workforce_id_int = int(m_paticipation.workforce_or_beneficiary_id)
            workforce_id_vis = None
            workforce_nrc = None
            workforce_name = None

            if len(RegPerson.objects.filter(pk=workforce_id_int, is_void=False)) > 0:
                m_person = RegPerson.objects.filter(pk=workforce_id_int, is_void=False)[0]
                workforce_id_vis = m_person.workforce_id
                workforce_nrc = m_person.national_id
                workforce_name = m_person.first_name + ' ' + m_person.surname

            participation_level_id = m_paticipation.participation_level_id
            participation_level = field_provider.get_item_desc_from_id(item_id=participation_level_id)

            m_paticipation_data['workforce_id'] = workforce_id_vis
            m_paticipation_data['workforce_nrc'] = workforce_nrc
            m_paticipation_data['workforce_name'] = workforce_name
            m_paticipation_data['participation_level'] = participation_level
            m_paticipation_data['participation_level_id'] = participation_level_id
            
            to_return[count] = m_paticipation_data
            count = count + 1
    return json.dumps(to_return)

def get_recent_child_date_admitted_left(child_models):
    to_return = {}
    '''
    Loops through a list of RegPerson models and returns a dictionary of the 
    most recent date_admitted and date_left for a given child
    '''
    for m_child in child_models:
        try:
            date_admitted = ''
            date_left = ''
            child_details = {}
            if len(FormResChildren.objects.filter(child_person_id = m_child.pk)) > 0:
                m_res_children = FormResChildren.objects.filter(child_person_id = \
                    m_child.pk).exclude(date_admitted=None).order_by('-form')
                if len(m_res_children) > 0:
                    m_res_child = m_res_children[0]
                    if m_res_child:  
                        if m_res_child.date_admitted:
                            date_admitted = m_res_child.date_admitted.strftime(val.date_input_format)
            
                m_res_children = FormResChildren.objects.filter(child_person_id = \
                    m_child.pk).exclude(date_left=None).order_by('-form')
                if len(m_res_children) > 0:
                    m_res_child = m_res_children[0]
                    if m_res_child:
                        if m_res_child.date_left:
                            date_left = m_res_child.date_left.strftime(val.date_input_format)
            
            child_details['date_admitted'] = date_admitted
            child_details['date_left'] = date_left
            
            to_return[m_child.pk] = child_details
                    
        except Exception as e:
            print e
            raise e
    return to_return


