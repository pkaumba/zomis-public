from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from ovc_main.utils.capture.capture_utililty import get_upload_queue, \
    get_downloads_queue, get_initial_data, stop_download
from django.http import HttpResponse, HttpResponseRedirect
import json

'''
@glyoko 29-06-2015
This class contains all the views that are used by the capture application. These 
include starting the upload and download, loading and refreshing the admin page.
'''
@login_required(login_url='/')
def stop_download_upload(request):
    download_info = []
    if request.method == 'GET':
        task_id = request.GET['task_id']
        download_info = stop_download(task_id = task_id, return_json=True)
    return HttpResponse(json.dumps((download_info)))

@login_required(login_url='/')
def capture_admin(request):      
    initial_data = get_initial_data(task_id='')
    return render(request, 'ovc_main/capture_administration.html', initial_data)

@login_required(login_url='/')
def refresh_capture_site(request):
    print 'Refresh capture view'
    initial_data = []
    if request.method == 'GET':
        task_id = request.GET['task']
        initial_data = get_initial_data(task_id = task_id, return_json=True)
    return HttpResponse(json.dumps((initial_data)))

@login_required(login_url='/')
def start_upload(request):
    print 'Start upload'
    upload_data = []
    if request.method == 'GET':                
        from rest_client import upload
        task = upload.delay()
        
        upload_data = get_upload_queue(task.task_id, return_json=True)     
    return HttpResponse(json.dumps((upload_data)))
    
@login_required(login_url='/')
def start_download(request):
    print 'Start download'
    download_data = []
    if request.method == 'GET':
        section = request.GET['section']
        password = request.GET['password']
        username = request.user.get_username()
        params = {'section':section, 'username':username, 'password':password}
        from rest_client import download
        task = download.delay(params) 
        
        download_data = get_downloads_queue(task.task_id, return_json=True)
    return HttpResponse(json.dumps((download_data)))

