from rest_framework.decorators import api_view, parser_classes
from ovc_main.core.rest.serializers import *
from rest_framework.parsers import JSONParser
from django.contrib.auth.decorators import login_required
from rest_framework import status
from rest_framework.response import Response
from ovc_main.core.rest import download_upload_settings as dp_settings
import uuid
from datetime import datetime
from ovc_main.models.audit_trail import RegPersonsAuditTrail, RegOrgUnitsAuditTrail
import traceback

'''
@lyokog 29/06/2015
This contains the views that receive requests from the capture sites and processes them. The two 
types of requests are upload and download
'''

@api_view(['POST'])
@parser_classes((JSONParser,))
@transaction.commit_on_success
def process_upload(request, format=None):
    '''
    Handles requests to upload data from the capture application
    '''
    if request.method == 'POST':
        serializer = None
        type = request.data['type']
        form_guid = request.data['form_guid']
        serialiser_class_str = None
        model_str = None
        parent_model_class_str = None
        parent_model_class = None
        for item in dp_settings.upload_info:
            if item['id'] == type:
                model_str = item['model']
                serialiser_class_str = item['serializer']
                parent_model_class_str = item['parent_model']
                parent_model_class = globals()[parent_model_class_str]
                break
        processing_parent = model_str == parent_model_class_str
        try:
            if serialiser_class_str:
                our_data = request.data['data']              
                model_class = globals()[model_str]
                ''' Delete existing entries with same form_id '''
                if processing_parent:
                    ''' 
                    This will delete all the tables associated to Forms via
                    foreign keys except CoreEncounters, CoreServices and CoreAdverseConditions
                    '''
                    model_class.objects.filter(form_guid = form_guid).delete() 
                ''' Delete special cases. TODO handle this differently and more generically in future '''
                if model_str == 'CoreEncounters' or model_str == 'CoreAdverseConditions':
                    delete_core_tables(parent_model_class, model_class, form_guid)
                if model_str == 'CoreServices':
                    delete_core_services(parent_model_class, model_class, form_guid) 
                for item in our_data:
                    serialiser_class = globals()[serialiser_class_str]
                    serializer = serialiser_class(data=item)
                    if not serializer:
                        print 'serializer cannot be initialised'
                    if serializer.is_valid():  
                        validated_data = serializer.validated_data
                        serializer.save(validated_data,form_guid,parent_model_class)
                        print 'Save successful'
                    else:
                        print 'serializer is invalid'
                        print serializer.errors,'serializer.errors'                        
        except Exception as e:
            traceback.print_exc()
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    return Response(serializer.data, status=status.HTTP_201_CREATED)

def delete_core_services(parent_model_class, model_class, form_guid):
    to_return = False
    try:
        if len(Forms.objects.filter(form_guid = form_guid)) == 0:
            return
        form = Forms.objects.filter(form_guid = form_guid)[0]
        parent_models = list(parent_model_class.objects.filter(form_id = form.pk))
        for m_core in parent_models:
            model_class.objects.filter(core_item_id = m_core.encounter_type_id).delete()
        to_return = True
    except Exception as e:
        traceback.print_exc()
        raise e
    return to_return

def delete_core_tables(parent_model_class, model_class, form_guid):
    to_return = False
    try:
        if len(parent_model_class.objects.filter(form_guid = form_guid)) == 0:
            return
        m_parent = parent_model_class.objects.filter(form_guid = form_guid)[0]
        model_class.objects.filter(form_id = m_parent.pk).delete() 
    except Exception as e:
        traceback.print_exc()
        raise e
    return to_return

@api_view(['GET'])
def download(request, format=None):
    """
    Handles the requests to download tables from the server.
    
    """
    print 'Downloading items'
    payload = {'request_id':'', 'data':[], 'model_pk_name':''}
    #from models import *
    try:
        if request.method == 'GET':
            model_id = request.GET['type']
            capture_site_id = request.GET['capture_site_id']
            request_id = request.GET['request_id']
            section_id = request.GET['section_id']
            is_last_request = request.GET['is_last_request']
            m_download = None
            number_records = 0
            if not request_id:
                request_id = str(uuid.uuid1())
                m_download = log_download(m_download=None, capture_site_id=capture_site_id, section_id=section_id, timestamp_started=datetime.now(), request_id=request_id)
            else:
                m_download = get_admin_download(request_id)
                number_records = m_download.number_records
            model_class_str = ''
            serialiser_class_str = ''
            go_on = True
            for v in dp_settings.download_sections.values():
                if not go_on:
                    break
                our_models = v['models']
                for model_info in our_models:
                    if model_info['id'] == model_id:
                        model_class_str = model_info['model']
                        serialiser_class_str = model_info['serializer']
                        go_on = False
                        break
            serialiser_class = globals()[serialiser_class_str]  
            payload = {'request_id':str(request_id), 'data':[], 'model_pk_name':''}
            if model_class_str: 
                print ''' process model serialisers '''
                model_class = globals()[model_class_str]
                objects = get_eligible_objects(capture_site_id=capture_site_id, section_id=section_id,model_class=model_class,model_class_str=model_class_str,request_id=request_id, serialiser_class_str=serialiser_class_str)
                serializer = serialiser_class(objects, many=True)
                payload = {'request_id':str(request_id), 'data':serializer.data, 'model_pk_name':model_class._meta.pk.name}
                number_records = number_records + len(objects)
            else:
                print ''' process custom serialisers '''
                serializer = serialiser_class()
                serializer.set_data_items()
                payload = {'request_id':str(request_id), 'data':json.dumps(serializer.data), 'model_pk_name':'national_id'}
                number_records = number_records + len(serializer.data)
            log_download(m_download=get_admin_download(request_id), is_last_request=is_last_request, number_records=number_records, request_id = request_id)    
    except Exception as e:
        traceback.print_exc()
        raise e
    return Response(payload)


def get_eligible_objects(capture_site_id, section_id, model_class, model_class_str, request_id, serialiser_class_str):
    '''
    This method returns the objects to be returned to capture site for download.
    It checks the most recent download date for the section in order to return 
    only recent downloads
    '''

    objects = None
    try:
        ''' 1900-01-01: set default date in case this is the first time '''
        latest_time_stamp_comp = '1900-01-01 00:00:00.874000'
        if len(AdminDownload.objects.filter(capture_site_id=capture_site_id, section_id = section_id, success = True).exclude(request_id=request_id)) > 0:
            m_download = AdminDownload.objects.filter(capture_site_id=capture_site_id, section_id = section_id, success = True).exclude(request_id=request_id).latest('timestamp_completed')
            if m_download:
                latest_time_stamp_comp = m_download.timestamp_completed
    
        print latest_time_stamp_comp,'latest_time_stamp_comp'
        ids = []
        if section_id == 'DSPR':
            m_audits = RegPersonsAuditTrail.objects.filter(timestamp_modified__gt=latest_time_stamp_comp)
            if m_audits:
                for m_audit in m_audits:
                    ids.append(m_audit.person_id)
            if model_class_str == 'RegPerson':
                objects = model_class.objects.filter(pk__in=ids)
            else:
                objects = model_class.objects.filter(person_id__in=ids)
        
        if section_id == 'DSOR':
            m_audits = RegOrgUnitsAuditTrail.objects.filter(timestamp_modified__gt=latest_time_stamp_comp)
            if m_audits:
                for m_audit in m_audits:
                    ids.append(m_audit.org_unit_id)
            if model_class_str == 'RegOrgUnit':
                objects = model_class.objects.filter(pk__in=ids)
            else:
                objects = model_class.objects.filter(org_unit_id__in=ids)
            
        if section_id == 'DSPA':
            print ids,'ids'
            org_unit_id_int = get_org_id(capture_site_id)
            ovc_user_org = OVCUserRoleGeoOrg.objects.filter(org_unit_id = org_unit_id_int)
        
            for item in ovc_user_org:
                group = Group.objects.get(pk=item.group_id)
                group_perms = group.permissions.values_list('id',flat=True)
                if 1029 in group_perms:#auth.log in capture site
                    ids.append(item.user.pk)
            if model_class_str == 'AppUser':
                objects = model_class.objects.filter(pk__in=ids, timestamp_updated__gt=latest_time_stamp_comp)
            elif model_class_str == 'OVCUserRoleGeoOrg':
                objects = model_class.objects.filter(user_id__in=ids, timestamp_modified__gt=latest_time_stamp_comp)
            print len(objects), 'User objects'
        if section_id == 'DSSS':
            if model_class_str == 'Group':
                ids = []
                changed_groups = OVCRole.objects.filter(timestamp_modified__gt=latest_time_stamp_comp)
                for ovc_role in changed_groups:
                    ids.append(ovc_role.group_ptr_id)
                objects = model_class.objects.filter(pk__in=ids)
            elif model_class_str == 'Permission':
                ids = []
                changed_perms = OVCPermission.objects.filter(timestamp_modified__gt=latest_time_stamp_comp)
                for ovc_perm in changed_perms:
                    ids.append(ovc_perm.pk)
                objects = model_class.objects.filter(pk__in=ids)
            else:
                objects = model_class.objects.filter(timestamp_modified__gt=latest_time_stamp_comp)
        
        if section_id == 'DSLT':
            print model_class_str,'model_class_str'
            if model_class_str == 'SetupGeorgraphy':
                objects = model_class.objects.filter(timestamp_updated__gt=latest_time_stamp_comp)
            else:
                objects = model_class.objects.filter(timestamp_modified__gt=latest_time_stamp_comp)
    except Exception as e:
        traceback.print_exc()
        raise e
    return objects

def get_org_id(capture_site_id):
    org_id = 0
    if len(AdminCaptureSites.objects.filter(pk=capture_site_id))>0:
        m_capture_site = AdminCaptureSites.objects.get(pk=capture_site_id)
        org_id = m_capture_site.org_unit_id
    '''if org_unit_id:
        if len(RegOrgUnit.objects.filter(org_unit_id_vis = org_unit_id, is_void=False, date_closed = None)) == 1:
            m_org = RegOrgUnit.objects.get(org_unit_id_vis = org_unit_id, is_void=False, date_closed = None)
            org_id = m_org.pk'''
    return org_id
    
def get_admin_download(request_guid):
    ''' Returns the download record given a request ID '''
    m_download = None
    try:
        count = len(AdminDownload.objects.filter(request_id=request_guid))
        print count,'count'
        if count > 0:
            m_download = AdminDownload.objects.filter(request_id=request_guid)[0]
    except Exception as e:
        traceback.print_exc()
        raise e
    return m_download
    
def log_download(m_download=None, capture_site_id=None, section_id=None, timestamp_started=None, timestamp_completed=None, number_records=None, request_id=None, success=None, is_last_request=False):
    """
    This method handles logging dowloads on the server side.
    """
    try:
        if not m_download:
            ''' If there is no log entry for downloading a section, create a new one '''
            print 'Not m_download'
            m_download = AdminDownload(
                                        capture_site_id=capture_site_id,
                                        section_id=section_id,
                                        timestamp_started = timestamp_started,
                                        #timestamp_completed = timestamp_completed,#datetime.datetime.now(),
                                        number_records = number_records,
                                        request_id = request_id,
                                        success = False
                                        )
            m_download.save()
            m_d = get_admin_download(request_id)
            print m_d.request_id,'m_download.request__id'
        else:
            ''' 
            If the request guid already exists on the server, retrieve it and update the number of records,
            success and timestamp_completed. Note that if a section being downloaded has 0- records at the
            end of the download, we delete it to prevent the table from being too blotted 
            '''
            timestamp_completed = None
            success = False
            m_temp = get_admin_download(request_id)
            if is_last_request == 'True':
                timestamp_completed = datetime.now()
                success = True
            elif is_last_request == 'False':
                timestamp_completed = None
                success = False
            if number_records == 0 and success == True:
                m_temp.delete()
                return m_temp
            if m_temp:
                m_temp.timestamp_completed = timestamp_completed
                m_temp.number_records = number_records
                m_temp.success = success
                m_temp.save()        
            return m_temp

    except Exception as e:
        traceback.print_exc()
        raise e
'''
A simple implementation of an api for exposing the Lists general table
'''
@api_view(['GET'])
def data_dictionary(request, format='JSON'):
    """
    Get lists general
    
    """
    print 'Getting list general'
    if request.method == 'GET':
        #serialiser_class = SetupListSerialiser()
        print ''' process model serialisers '''
        objects = SetupList.objects.all()
        serializer = SetupListSerializer(objects, many=True)
        payload = {'data':serializer.data}
    return Response(payload)
