import json
from django.http import HttpResponse
from ovc_main.reporting import reports_common
from django.shortcuts import render, redirect

def report_counts(request):
    dashsummary = reports_common.dashboard_reg_summary()
    return  HttpResponse(json.dumps(dashsummary))

def dashboard_children(request):
    dashsummary = reports_common.dashboard_children_section()
    return  HttpResponse(json.dumps(dashsummary))

def test(request):
    print 'test post in test multifield'
    if request.POST:
        print request.POST
    return render(request, 'ovc_main/utils_temps/multifieldpost.html')