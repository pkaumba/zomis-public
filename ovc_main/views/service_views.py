from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, redirect
import datetime
import json
from ovc_main.forms.forms_helper import search_basic_service_referral_records, \
    delete_service_referral_record, get_service_records_by_form_id
from ovc_main.forms.services import basic_service_referral, residential_institution_form,\
    workforce_training_form, public_sensitisation_form
    
from ovc_main.utils import fields_list_provider, workforce_utils

from django.contrib.auth.decorators import login_required


def basic_service_referral_vw(request):
    form = basic_service_referral.BasicServiceReferralForm()
    
    return render(request, 'ovc_main/Forms/basic_service_referral_form.html', 
            {'form': form})
    
def delete_form(request):
    
    if request.POST:
        beneficiaryid = request.POST.get('benid', None)
        formid = request.POST.get('formid', None)
        encounterid = request.POST.get('encounterid', None)
        
        if beneficiaryid and beneficiaryid.isdigit():
            beneficiaryid = int(beneficiaryid)
            
        if formid and formid.isdigit():
            formid = int(formid)
            encounterid = None
        else:
            formid = None
            encounterid = int(encounterid)
            
        delete_service_referral_record(formid, beneficiaryid, request.user, encounterid)
        
    return HttpResponse(json.dumps({'success':'success'}))    
            
            
def forms_search(request):
    
    results = ''
    
    if request.GET:
        print 'forms-search:', request.GET
        form_type = request.GET.get('formtype',None)
        what = request.GET.get('what', None)
        searchtoken = request.GET.get('token', None)
        subject = request.GET.get('subject', None)
        datetosearch = request.GET.get('dateofsearch', None)
        dateattr = request.GET.get('dateattr', None)
        form_id = request.GET.get('form_id', None)
        datetouse = None
        if datetosearch:
            try:
                datetouse = datetime.datetime.strptime(datetosearch, "%d-%B-%Y").date()
            except:
                pass
        if form_type and what and subject:
            whattosearch = {'FT3e':{
                                    'past-records':search_basic_service_referral_records,
                                    'byformid':get_service_records_by_form_id
                                    }
                            }
            #print searchtoken, request.user, subject, datetouse, dateattr
            if what == 'byformid':
                results = whattosearch[form_type][what](form_id)
            else:
                results = whattosearch[form_type][what](searchtoken, request.user, 
                                                    workforceid=subject, datefilter=datetouse, 
                                                    dateattr=dateattr)
            
    return HttpResponse(json.dumps(results))

@login_required(login_url='/')
def beneficiary_search_result(request):
    if request.method == 'GET':
        to_return = {}
        token = request.GET['search_token']
        user = request.user
        
        child_list = fields_list_provider.search_ben_ovc(token, user, person_type=None)
    
        from ovc_main.views.vw_utils import gen_vw_utils as util
        formatted_ben_list = util.format_bens_for_display(child_list)
        
        return HttpResponse(json.dumps(formatted_ben_list))
def residential_institution_vw(request):
    form = residential_institution_form.ResidentialInstitutionForm()
    
    return render(request, 'ovc_main/Forms/residential_institution.html', 
            {'form': form})

def workforce_training_vw(request):
    form = workforce_training_form.WorkforceTrainingForm()

    return render(request, 'ovc_main/Forms/workforce_training.html', 
            {'form': form})

def public_sensitisation_vw(request):
    form = public_sensitisation_form.PublicSensitisationForm()

    return render(request, 'ovc_main/Forms/public_sensitisation.html', 
            {'form': form})