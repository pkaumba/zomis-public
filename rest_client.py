from __future__ import absolute_import
import os
from celery import Celery
from django.conf import settings
import requests
from rest_framework import status
import json
from OVC_IMS.celery_app import app
from ovc_main.models import *
from ovc_main.core.rest.serializers import *
import datetime
from ovc_main.core.rest import download_upload_settings as dp_settings
from ovc_main.utils.fields_list_provider import get_capture_site_id
from celery.result import AsyncResult
import celery.states
from django.db.models import Max
from django.db import transaction
import traceback

download_task_type = 'download' 
upload_task_type = 'upload'

@app.task(serializer='json', bind=True)
@transaction.commit_on_success
def upload(self):
    '''
    This method uploads forms from a given capture site    
    '''
    try:
        task_id = self.request.id
        if task_id:
            log_task_id(task_id, upload_task_type, completed = False)

        server_url = settings.CAPTURE_SITE_URLS['upload_server_url']
        if len(AdminUploadForms.objects.filter(timestamp_uploaded=None)) > 0:
            to_upload = AdminUploadForms.objects.filter(timestamp_uploaded = None)
            from ovc_main.utils.capture.capture_utililty import get_upload_precedence
            upload_details = get_upload_precedence()
            #expectation is that the dictionary has sorted keys
            upload_precedence_values = upload_details.keys()
            for record in to_upload:
                for precedence in upload_precedence_values:
                    #TODO Check if has key
                    item = upload_details[precedence]
                    model_str = item['model']
                    parent_model_str = item['parent_model']
                    serializer_str = item['serializer']
                    upload_form(model_str, parent_model_str, serializer_str, record,\
                        item, server_url)
                                            
                    
            if task_id:
                log_task_id(task_id, upload_task_type,completed = True)
    except Exception as e:
        traceback.print_exc()
        raise e
                
def upload_form(model_str, parent_model_str, serializer_str, record, item, server_url):  
    try:
        model_class = globals()[model_str]
        serializer_class = globals()[serializer_str]
        p_model = None
        m_record = None
        uploading_parent = model_str == parent_model_str
        form_guid_to_send = ''
        if uploading_parent:
            if len(model_class.objects.filter(pk = int(record.form_id))) == 0:
                return
        else:
            parent_model_class = globals()[parent_model_str]
            ''' Special case for CoreServices because the parent is not Forms table but CoreEncounters '''
            if model_str == 'CoreServices':
                if len(parent_model_class.objects.filter(form_id = record.form_id)) > 0:
                    p_model = parent_model_class.objects.filter(form_id = record.form_id)[0]
                    form = None
                    if len(Forms.objects.filter(pk = record.form_id)) == 0:
                        return
                    form_guid_to_send = Forms.objects.filter(pk = record.form_id)[0].form_guid
                    if len(model_class.objects.filter(workforce_person = p_model.workforce_person,\
                                beneficiary_person = p_model.beneficiary_person,\
                                encounter_date = p_model.encounter_date)) == 0:
                        return
            else:
                if len(parent_model_class.objects.filter(pk = record.form_id)) > 0:
                    p_model = parent_model_class.objects.get(pk = record.form_id)
                    if len(model_class.objects.filter(form_id = p_model.pk)) == 0:
                        return
                    form_guid_to_send = p_model.form_guid
        if uploading_parent:
            m_record = model_class.objects.filter(pk = record.form_id)
            if len(m_record)>0:
                form_guid_to_send = m_record[0].form_guid
        else:
            if model_str == 'CoreServices':
                if not p_model:
                    return
                m_record = model_class.objects.filter(workforce_person = p_model.workforce_person,\
                                beneficiary_person = p_model.beneficiary_person,\
                                encounter_date = p_model.encounter_date)
            else:
                m_record = model_class.objects.filter(form_id = p_model.pk)                      

        serializer = serializer_class(m_record, many = True)
        payload = {'type':item['id'], 'form_guid':form_guid_to_send, 'data':serializer.data}
        req = requests.post(server_url, headers = {'content-type': 'application/json'}, data = json.dumps(payload))
        if req.status_code == status.HTTP_201_CREATED:
            record.timestamp_uploaded = datetime.datetime.now()
            record.save()
            #print 'Upload successful'
    except Exception as e:
        traceback.print_exc()
        raise e

def update_upload_status(update_id):
    '''
    Logs the uploads on the capture site into the AdminUploadForms table
    '''
    rec = AdminUploadForms.objects.get(form_id=update_id)
    rec.timestamp_uploaded = datetime.datetime.now()
    rec.save()

@app.task(serializer='json', bind=True)
@transaction.commit_on_success
def download(self, params):
    '''
    This method downloads tables found in download_upload_settings based on their precedence and last download time in order
    to keep them in sync with the server
    '''
    section_to_download = params['section']
    list_codes_all = dp_settings.download_sections.keys()
    task_id = self.request.id
    if task_id:
        log_task_id(task_id, download_task_type,completed = False)
    if section_to_download:
        from ovc_main.utils.capture.capture_utililty import get_download_precedence

        sec_ids_precedence = get_download_precedence()
        download_precedence_values = sec_ids_precedence.keys()
        for precedence in download_precedence_values:
            download_section(sec_ids_precedence[precedence], params['username'], params['password'])
    if task_id:
        log_task_id(task_id, download_task_type,completed = True)

def download_section(section_id, username, password):
    '''if not section_id == 'DSPA':
        return'''
    server_url = settings.CAPTURE_SITE_URLS['download_server_url']
    models_to_download = dp_settings.download_sections[section_id]['models']
    ''' Logging fields '''
    capture_site_id = get_capture_site_id()        

    num_of_records_success = 0
    num_of_records_fail = 0
    success = None
    if models_to_download:
        request_guid = ''
        m_download = update_download_status(m_download = None, capture_site_id = capture_site_id, section_id = section_id)
        is_last_request = False
        count = 0
        for model_info in models_to_download:
            count = count + 1
            if count == len(models_to_download):
                is_last_request = True
            get_param = model_info['id']
            serialiser_str = model_info['serializer']
            model_str = model_info['model']
            #print serialiser_str
            try:
                payload = {'type': get_param, 'request_id':request_guid, 'capture_site_id':capture_site_id,'section_id':section_id, 'is_last_request':is_last_request}
                req = requests.get(server_url, auth=(username, password), params = payload)
                #print req,'req'
                object_list = []
                #print 'There'
                object_list = req.json()
                #print object_list,'object_list'
            except Exception as e:
                print e
                continue
            if not object_list:
                print 'NULL object_list'
                continue
            if not object_list.has_key('data'):
                print 'No data in object_list'
                continue
            if object_list['request_id']:
                request_guid = object_list['request_id']
            serialiser_class = globals()[serialiser_str]
            model_pk_name = object_list['model_pk_name']
            #print 11111111111111111
            #print serialiser_str,'serialiser_str'
            if not model_str:
                #load json for custom serialisers
                #print ''' Save custom serialisers '''
                object_list['data'] = json.loads(object_list['data'])
                num_of_records_success, num_of_records_fail = save_custom_serialisers(object_list['data'], \
                                serialiser_str, serialiser_class, model_pk_name, section_id,\
                                get_param, num_of_records_success, num_of_records_fail)
            else:
                '''if model_str == 'UsersSerializer':
                    
                    #print '******************************'
                    #print object_list['data']
                    #print '******************************'''
                for item in object_list['data']:
                    #print item, 'item'
                    #print ''' Save generic serilisers '''
                    num_of_records_success, num_of_records_fail = save_generic_models(item, \
                                serialiser_str, serialiser_class, model_pk_name, section_id,\
                                get_param, num_of_records_success, num_of_records_fail)
                    

        #final logging
        update_download_status(m_download = m_download, num_of_records_success = num_of_records_success, num_of_records_fail = num_of_records_fail)

def save_custom_serialisers(items, serialiser_str, serialiser_class, model_pk_name, \
                section_id, get_param, num_of_records_success, num_of_records_fail):
    #print '************************************'
    #print 'Saving custom fimo'
    try:
        serializer = serialiser_class(data=items)
        serializer.save()
        num_of_records_success = num_of_records_success + len(items)
        #print 'save successful'
    except Exception as e:
        print e
        #print 'SAVE NOT SUCCESSFUL!.'
        #print item,'item'
        num_of_records_fail = num_of_records_fail  + len(items)
        #print serializer.errors,'Errors'
    #print '************************************'
    return num_of_records_success, num_of_records_fail
        
    
def save_generic_models(item, serialiser_str, serialiser_class, model_pk_name, section_id,\
        get_param, num_of_records_success, num_of_records_fail):
    serializer = serialiser_class(data=item)
    id = None
    '''
    We do this because we the id is the only way to access the corresponding Group/Permission
    '''
    if serialiser_str == 'RolesDetailSerializer' or serialiser_str == 'PermsDetailSerializer':
        id = int(item['id'])
    else:
        id = int(item[model_pk_name])
    is_valid = False
    '''
    This is necessary so that we can handle custom validations for special models that require unique values in certain columns
    '''
    if serialiser_str == 'UsersSerializer' or serialiser_str == 'RolesSerializer' or serialiser_str == 'RolesDetailSerializer' or serialiser_str == 'PermsSerializer' or serialiser_str == 'PermsDetailSerializer':
        is_valid = serializer.is_valid(id)
        #print is_valid,'is_valid'
    else:
        is_valid = serializer.is_valid()
    if is_valid:
        validated_data = serializer.validated_data
        serializer.save(validated_data,id)
        #if serialiser_str == 'PermsDetailSerializer':
        #    print validated_data
        '''
        We do this in order to log only unique person/organisation entities
        '''
        if section_id == 'DSPR' and not get_param == 'persons':
            return num_of_records_success, num_of_records_fail
        if section_id == 'DSOR' and not get_param == 'org_unit':
            return num_of_records_success, num_of_records_fail
        num_of_records_success = num_of_records_success + 1
        #print 'save successful'
    else:
        print 'SAVE NOT SUCCESSFUL!.'
        print item,'item'
        num_of_records_fail = num_of_records_fail + 1
        print serializer.errors,'Errors'
    return num_of_records_success, num_of_records_fail
                    
def update_download_status(m_download=None, capture_site_id=None, section_id=None, num_of_records_success=None, num_of_records_fail=None):
    '''
    Logs the downloads on the capture site into the AdminDownload table
    '''
    if not m_download:
        try:
            m_download = AdminDownload(
                                    capture_site_id=capture_site_id, 
                                    section_id=section_id, 
                                    timestamp_started = datetime.datetime.now(),
                                    )
            m_download.save()
        except Exception as e:
            print e
            raise Exception("Unable to log download for section ",section_id)
    else:
        if num_of_records_success > 0:
            m_temp = AdminDownload.objects.get(pk=m_download.pk)
            m_temp.timestamp_completed = datetime.datetime.now()
            m_temp.number_records = num_of_records_success
            m_temp.success = True
            m_temp.save()
        if num_of_records_fail > 0:
            m_download_fails = AdminDownload(
                                            capture_site_id=m_download.capture_site_id,
                                            section_id=m_download.section_id,
                                            timestamp_started = m_download.timestamp_started,
                                            timestamp_completed = datetime.datetime.now(),
                                            number_records = num_of_records_fail,
                                            success = False
                                            )
            m_download_fails.save()
        if num_of_records_success == 0 and num_of_records_fail == 0:
            m_temp = AdminDownload.objects.get(pk=m_download.pk)
            m_temp.delete()

    return m_download

def log_task_id(task_id, task_type, completed):
    '''
    Logs a task to our model that tracks the tasks that are run from the capture sites
    '''
    task_entry = None
    if completed:
        if CaptureTaskTracker.objects.filter(task_id = task_id, operation = task_type, completed=False).count() == 1:
            task_entry = CaptureTaskTracker.objects.get(task_id = task_id, operation = task_type, completed=False, cancelled = False)
            task_entry.completed = True
            task_entry.timestamp_completed = datetime.datetime.now()
            task_entry.save()
            if task_entry:
                return True
            else:
                return False
    else:
        if CaptureTaskTracker.objects.filter(task_id = task_id, operation = task_type, completed=False).count() == 0:
            task_entry = CaptureTaskTracker.objects.create(task_id = task_id, operation = task_type, completed=False, cancelled = False)
            if task_entry:
                return True
            else:
                return False
       
def get_task_status(task_id):
    '''
    Returns the celery status of a task
    '''
    result = AsyncResult(task_id) 
    status = result.status 
    
    return status
 
def get_recent_task_id():
    '''
    Returns the most recent task id recorded in the CaptureTaskTracker based solely on the serial pk
    '''
    task_id = None
    capture_task_pk = CaptureTaskTracker.objects.filter(completed=False, cancelled=False).aggregate(Max('id'))['id__max']
    if capture_task_pk:
        m_capture = CaptureTaskTracker.objects.get(pk = capture_task_pk)
        task_id = m_capture.task_id                
    return task_id
        
def get_task_type(task_id):
    '''
    Returns the type of a given task i.e. download or upload
    '''
    task_type = None
    if CaptureTaskTracker.objects.filter(task_id = task_id).count() == 1:
        task_entry = CaptureTaskTracker.objects.get(task_id = task_id)
        task_type = task_entry.operation
    return task_type

def get_task_complete_status(task_id):
    '''
    Returns the completed status from the CaptureTaskTracker model for a given task ID
    '''
    completed = None
    if CaptureTaskTracker.objects.filter(task_id = task_id).count() == 1:
        task_entry = CaptureTaskTracker.objects.get(task_id = task_id)
        completed = task_entry.completed
    return completed

def get_recent_running_task_info():
    '''
    Returns the task_id, it's status and the task_operation type that is download or upload
    '''
    task_id = None
    status = None
    task_operation = None
    completed = None
    
    task_id = get_recent_task_id()
    if task_id:
        status = get_task_status(task_id)
        task_operation = get_task_type(task_id)
        print 'Status: ', status
        print 'Task_operation: ', task_operation
    if not status == celery.states.PENDING:
        return None, None, None#, None
    return task_id, status, task_operation#,completed

def get_task_info(task_id):
    '''
    This returns the task_id, status and type of task(download or upload) for a given task_id
    '''
    
    status = None
    task_operation = None
    
    if task_id:
        status = get_task_status(task_id)
        task_operation = get_task_type(task_id)
    return task_id, status, task_operation