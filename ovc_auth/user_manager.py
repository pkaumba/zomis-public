from ovc_auth.models import OVCRole, AppUser, OVCUserRoleGeoOrg
from ovc_main.models import RegPerson
import datetime

default_password = 'default'

def activate_user(user):
    print 'user is activaate'
    if user and not user.is_active:
        user.is_active = True
        user.save()

def deactivate_user(user):
    print 'user is deactivate'
    if user and user.is_active:
        user.is_active = False
        user.save()
        
def delete_user(user):
    if user:
        user.delete()
        
def create_user_from_workforce_id(workforceid):
    workforce_model = RegPerson.objects.get(pk=workforceid)
    return create_user_from_workforce_model(workforce_model)

def create_user_from_workforce_model(workforce_model, password=None):
    workforce_id = workforce_model.workforce_id
    national_id = workforce_model.national_id
    first_name = workforce_model.first_name
    last_name = workforce_model.surname
    #print workforce_id,'workforce_id'
    #print national_id,'national_id'
    if not national_id:
        national_id = None
        
    user = None
    if not password:
        password=default_password
    '''
    1. new user, no nrc, has workforce id: doesn't exists in app user
    2. new user, has nrc, has workforce id: doesn't exists in app user
    3. new user, has nrc,no workforce id: doesnt exist in app user
    4. new user, no nrc,no workforce id: doesnt exist in app user
    
    4. old user, no nrc, has workforce id: exists in app user
    5. old user, has nrc, has workforce id:exists in app user
    6. old user, has nrc,no workforce id: exist in app user
    6. old user, no nrc,no workforce id: exist in app user
    '''
    
    if AppUser.objects.filter(workforce_id = workforce_id).count() > 0:
        user = AppUser.objects.get(workforce_id=workforce_id)
        user.reg_person =  workforce_model
    else:
        try:
            user = AppUser.objects.get(reg_person__pk=workforce_model.pk)
        except AppUser.DoesNotExist:
            pass
        if user is None:
            if workforce_id:
                user = AppUser.objects.create_user(workforce_id=workforce_id, national_id=national_id, 
                                                   first_name = first_name, last_name=last_name, password=password)
            else:
                user = AppUser.objects.create_user(workforce_id=str(workforce_model.pk), national_id=national_id, 
                                                  first_name = first_name, last_name=last_name, password=password)
    
    user.reg_person = workforce_model
    user.last_login = datetime.datetime.min
    user.is_active = True
    user.save()
    return user

def allocate_standard_log_in(workforce_model):

    password = generate_password()
    print 'allocate role'
    user =  get_or_create_user_from_model(workforce_model, password) 
    print user, 'in allocate standard logged in'  
    _assign_standard_login_role(user)
    
    return user, password
def _assign_standard_login_role(user):
    assign_role(user=user, rolename='Standard logged in')
    
def get_or_create_user_from_model(workforce_model, password=None):
    
    workforce_id = workforce_model.workforce_id
    national_id = workforce_model.national_id
    user = None
    try:
        if workforce_id:
            user = AppUser.objects.get(workforce_id=workforce_id)
        elif national_id:
            user = AppUser.objects.get(national_id=national_id)
        elif workforce_model:
            user = AppUser.objects.get(reg_person__pk=workforce_model.pk)
        if password:
            set_password(user, password)
    except AppUser.DoesNotExist:
        user = create_user_from_workforce_model(workforce_model, password)
    return user

def allocate_registration_assistant_role(workforce_model, organisation_id=None, password=None):
    
    user =  get_or_create_user_from_model(workforce_model, password)      
    _assign_registration_assistant_role(user, organisation_id)
    
    return user
    
def _assign_registration_assistant_role(user, organisation_id):
    assign_role(user=user, rolename='Registration assistant', organisationid=organisation_id)
    
def assign_role(user, rolename, organisationid=None, area_id=None):
    try:
        role = OVCRole.objects.get(group_name=rolename)
        role.user_set.add(user)
        if organisationid or area_id:
            org=None
            area= None
            from ovc_main.models import RegOrgUnit, RegPersonsGeo
            if organisationid:
                
                org = RegOrgUnit.objects.get(pk=organisationid)
            if area_id:
                area = RegPersonsGeo.objects.get(area_id__exact=area_id, person=user.reg_person)
            createdresult = OVCUserRoleGeoOrg.objects.get_or_create(user=user, group=role, org_unit=org, area=area)
            
    except Exception as e:
        print e
        raise Exception('Role %s does not exist %s' % (rolename,e))
    
def get_user_group(user):
    return user.groups.all()

def get_ovc_user_roles(user):
    return OVCRole.objects.filter(group_ptr__in=user.groups.all())

def get_user_role_geo_org(user):
    ovc_roles_with_geo_and_org_info = OVCUserRoleGeoOrg.objects.filter(user=user,void=False)
    #ovc_roles_with_geo_and_org_info = OVCUserRoleGeoOrg.objects.filter(group__in=user.groups.all())
    return ovc_roles_with_geo_and_org_info

def reset_ovc_user_role_geo_org(user):
    OVCUserRoleGeoOrg.objects.filter(user=user).delete()
    
def remove_roles_in_geo(user, area_to_delete_ids=None, preserve_area_list=None):
    groups = []
    if area_to_delete_ids:
        groups += [role_geo.group for role_geo in OVCUserRoleGeoOrg.objects.filter(user=user, area__area_id__in=area_to_delete_ids)]
        OVCUserRoleGeoOrg.objects.filter(user=user, area__area_id__in=area_to_delete_ids).delete()
    if preserve_area_list:
        groups += [role_geo.group for role_geo in OVCUserRoleGeoOrg.objects.filter(user=user).exclude(area__area_id__in=preserve_area_list)]
        OVCUserRoleGeoOrg.objects.filter(user=user).exclude(area__area_id__in=preserve_area_list).delete()

    if groups:
        for group in groups:
            remove_role(user, groups)

    
def remove_role(user, group, organisation_id=None, area_id=None):
    if organisation_id or area_id:
        OVCUserRoleGeoOrg.objects.filter(user=user, group=group, org_unit_id__pk=organisation_id, area__area_id=area_id).delete()
    
    #if this role belongs to this user to multple locations or organisations
    #dont delete the role from the user.
    if OVCUserRoleGeoOrg.objects.filter(user=user, group=group).count() > 0:
        pass
    else:
        user.groups.remove(group)

def generate_password(password_length=6):
    import string
    import random
    return ''.join(random.SystemRandom().choice(string.letters + string.digits) for _ in range(password_length))
 
def reset_password(user):
    password = generate_password()
    user.set_password(password)
    user.save()
    return password
    
def set_password(user, new_password):
    user.set_password(new_password)
    user.save()
