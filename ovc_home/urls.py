'''
Created on Aug 30, 2014

@author: MLumpa
'''
from django.conf.urls import patterns, url

from ovc_home import views

urlpatterns = patterns('',
    url(r'^$', views.ovc_login, name='ovc_login'),
    url(r'^home_dashboard', views.home_dashboard, name='home_dashboard'),
)