$(document).ready(
        function () {
            //BEGIN PLUGINS SELECT2
            //$("#id_date_of_birth").onSelect(function(date,inst) {
            //	alert(date);
            //});

            //$(".table_row_clickable").click(function(){
            $(".table").on("click", ".table_row_clickable", function () {

                window.location = $(this).attr("href");
            });

            hidelocationcontrols = function (initialrun)
            {

                if (typeof ($("#id_organisation_type").val()) == 'undefined')
                {
                    on_org_type_change();
                    $("#ward_location, #community_location, #district_location").show();
                }
                else {
                    on_org_type_change();
                    //alert(initialrun);
                    if (initialrun == false)
                    {
                        $("#districts").multiSelect("deselect_all");
                        $("#districts").multiSelect("refresh");

                    }

                    $("#ward_location, #community_location, #district_location").hide();
                    $("#div_id_legal_registration_type").hide();
                    $("#div_id_legal_registratiom_number").hide();
                }

            };
            handlelocationhidingandshowing = function (initialrun)
            {
                //alert(initialrun);
                if (initialrun != true)
                {
                    initialrun = false;
                }
                hidelocationcontrols(initialrun);
                switch ($("#id_organisation_type").val()) {
                    case 'TNRS'://residential institution
                    case 'TNCB':
                    case 'TNCM':
                    case 'TNND':
                    case 'TNGS':
                        $("#district_location").show();
                        $("#ward_location").show()
                        $("#community_location").show();
                        break;
                    case 'TNCW'://cwac
                        $("#district_location").show();
                        $("#ward_location").show()
                        break;
                    case 'TNGD':
                    case 'TNCD':
                    case 'TNNM':
                    case 'TNCP':
                    case 'TNGP':
                        $("#district_location").show();
                        break;
                }

                switch ($("#id_organisation_type").val()) {
                    case 'TNCB':
                        //case 'TNCW':
                        //case 'TNCM':
                    case 'TNRS'://residential institution
                    case 'TNND':
                    case 'TNNM':
                    case 'TNNH':
                        $("#div_id_legal_registration_type").show();
                        $("#div_id_legal_registratiom_number").show();
                        break;
                }
            };



            on_org_type_change = function () {
                var ismultiselect_district = true;
                var ismultiselect_ward = true;
                var ismultiselect_community = true;

                switch ($("#id_organisation_type").val()) {
                    case 'TNRS'://residential institution
                    case 'TNCB':
                    case 'TNCW'://cwac
                    case 'TNCD':
                    case 'TNGD':
                        ismultiselect_district = false;
                        ismultiselect_ward = false;
                        ismultiselect_community = false;
                        break;
                    case 'TNCM':
                    case 'TNGS':
                    case 'TNND':
                        //alert("some false");
                        ismultiselect_district = false;
                        ismultiselect_ward = true;
                        ismultiselect_community = true;
                    case 'TNNM':
                    case 'TNCP':
                    case 'TNGP':
                        ismultiselect_district = true;
                        ismultiselect_ward = false;
                        ismultiselect_community = false;
                        break;
                }


                $('#wards').multiSelect({
                	selectableHeader: "<input type='text' class='form-control search-input' autocomplete='off' placeholder='Search items...' style='margin-bottom:5px;'>",
           		 	selectionHeader: "<input type='text' class='form-control search-input2' autocomplete='off' style='visibility:hidden;margin-bottom:5px;'>",

	           		 afterInit: function(ms){
	           			 //console.log("initialising in int");
	         		    var that = this,
	         		        $selectableSearch = that.$selectableUl.prev(),
	         		        $selectionSearch = that.$selectionUl.prev(),
	         		        selectableSearchString = '#'+that.$container.attr('id')+' .ms-elem-selectable:not(.ms-selected)',
	         		        selectionSearchString = '#'+that.$container.attr('id')+' .ms-elem-selection.ms-selected';
	         		
	         		    
	         		    that.qs1 = $selectableSearch.quicksearch(selectableSearchString,
	         		    		{
	         		    			'onBefore': function () {
	         		    				that.qs1.cache();
	         		    	    	}
	         		    		}		
	         		    ).on('keydown', function(e){
	         		      if (e.which === 40){
	         		        that.$selectableUl.focus();
	         		        return false;
	         		      }
	         		   });
	         		    
	         		  },
                    afterSelect: function (values) {
                    	//console.log("in after select of wards");
                    	//this.qs1.cache();
                        //$('#wards').attr('disabled', 'disabled');
                        //$("#wards").multiSelect("refresh");
                        $("#wards > option").each(function () {
                            if (values != this.value) {
                                if ($("#wards").attr('ismultiselect') != 'True') {
                                    switch ($("#id_organisation_type").val()) {
                                        case 'TNRS'://residential institution
                                        case 'TNCB':
                                        case 'TNCW'://cwac
                                        case 'TNGD':
                                            ismultiselect_district = false;
                                            ismultiselect_ward = false;
                                            ismultiselect_community = false;
                                            break;
                                        case 'TNCM':
                                        case 'TNGS':
                                        case 'TNND':
                                        case 'TNCD':
                                        case 'TNCM':
                                            //alert("some false");
                                            ismultiselect_district = false;
                                            ismultiselect_ward = true;
                                            ismultiselect_community = true;
                                            break;
                                        case 'TNCP':
                                        case 'TNGP':
                                        case 'TNNM':
                                            ismultiselect_district = true;
                                            ismultiselect_ward = false;
                                            ismultiselect_community = false;
                                            break;
                                    }
                                    if (ismultiselect_ward != true)
                                    {
                                        $(this).attr('disabled', 'disabled');
                                    }
                                    //alert(this.text + ' ' + this.value);
                                }
                            }
                        });
                        $("#wards").multiSelect("refresh");
                        $.ajax({
                            dataType: "json",
                            url: "/organisation/communities?ward_id=" + values,
                            contentType: "application/json; charset=utf-8",
                            //data: {"districtid" : values},
                            success: function (communities) {
                                var items = [];
                                $.each(communities, function (key, val) {
                                    $('#communities').multiSelect('addOption', {value: key, text: val});

                                });

                            }
                        });

                    },
                    afterDeselect: function (values) {
                    	//this.qs1.cache();
                        $("#wards > option").each(function () {

                            $(this).removeAttr('disabled')
                            //alert(this.text + ' ' + this.value);

                        });
                        $("#wards").multiSelect("refresh");
                        $.ajax({
                            dataType: "json",
                            url: "/organisation/communities?ward_id=" + values,
                            contentType: "application/json; charset=utf-8",
                            //data: {"districtid" : values},
                            success: function (communities) {
                                var items = [];
                                $.each(communities, function (key, val) {
                                    $("#communities option[value=" + key + "]").remove();
                                    $("#communities").multiSelect("refresh");
                                    //alert("Deselect value: "+key);
                                });
                            }
                        });
                        //$("#destination option[value='1']").remove();
                    }
                });
                $('#districts').multiSelect({
                	selectableHeader: "<input type='text' class='form-control search-input' autocomplete='off' placeholder='Search items...' style='margin-bottom:5px;'>",
           		 	selectionHeader: "<input type='text' class='form-control search-input2' autocomplete='off' style='visibility:hidden;margin-bottom:5px;'>",

	           		 afterInit: function(ms){
	         		    var that = this,
	         		        $selectableSearch = that.$selectableUl.prev(),
	         		        $selectionSearch = that.$selectionUl.prev(),
	         		        selectableSearchString = '#'+that.$container.attr('id')+' .ms-elem-selectable:not(.ms-selected)',
	         		        selectionSearchString = '#'+that.$container.attr('id')+' .ms-elem-selection.ms-selected';
	         		
	         		    
	         		    that.qs1 = $selectableSearch.quicksearch(selectableSearchString,
	         		    		{
		     		    			'onBefore': function () {
		     		    				that.qs1.cache();
		     		    	    	}
		     		    		}
	         		    ).on('keydown', function(e){
	         		      if (e.which === 40){
	         		        that.$selectableUl.focus();
	         		        return false;
	         		      }
	         		   });
	         		    
	         		  },
           		 	
           		 	
                    afterSelect: function (values) {
                    	//this.qs1.cache();
                        $("#districts > option").each(function () {
                            if (values != this.value) {
                                if ($("#districts").attr('ismultiselect') != 'True') {
                                    //alert(ismultiselect_district);
                                    switch ($("#id_organisation_type").val()) {
                                        case 'TNRS'://residential institution
                                        case 'TNCB':
                                        case 'TNCW'://cwac
                                        case 'TNGD':
                                            ismultiselect_district = false;
                                            ismultiselect_ward = false;
                                            ismultiselect_community = false;
                                            break;
                                        case 'TNCM':
                                        case 'TNGS':
                                        case 'TNND':
                                        case 'TNCD':
                                        case 'TNCM':
                                            //alert("some false");
                                            ismultiselect_district = false;
                                            ismultiselect_ward = true;
                                            ismultiselect_community = true;
                                            break;
                                        case 'TNNM':
                                        case 'TNCP':
                                        case 'TNGP':
                                            ismultiselect_district = true;
                                            ismultiselect_ward = false;
                                            ismultiselect_community = false;
                                            break;
                                    }
                                    if (ismultiselect_district != true)
                                    {

                                        $(this).attr('disabled', 'disabled');
                                    }
                                }
                                //alert(this.text + ' ' + this.value);
                            }
                        });
                        $("#districts").multiSelect("refresh");
                        $.ajax({
                            dataType: "json",
                            url: "/organisation/wards?district_id=" + values,
                            contentType: "application/json; charset=utf-8",
                            //data: {"districtid" : values},
                            success: function (wards) {
                                var items = [];
                                $.each(wards, function (key, val) {
                                    $('#wards').multiSelect('addOption', {value: key, text: val});

                                    //alert("Deselect value: "+key);
                                });
                            }
                        });
                        
                        //$.getJSON( "/organisation/wards", {"districtid" : values}, function( data ) {
                        //	  var items = [];
                        //	  $.each( data, function( key, val ) {
                        //		  $('#wards').multiSelect('addOption', { value: key, text: val});
                        //		  //alert("Deselect value: "+key);
                        //	  });
                        //	});
                    },
                    afterDeselect: function (values) {
                    	//this.qs1.cache();
                        $("#districts > option").each(function () {

                            $(this).removeAttr('disabled')
                            //alert(this.text + ' ' + this.value);

                        });
                        $("#districts").multiSelect("refresh");
                        $.ajax({
                            dataType: "json",
                            url: "/organisation/wards?district_id=" + values,
                            contentType: "application/json; charset=utf-8",
                            //data: {"districtid" : values},
                            success: function (wards) {
                                var items = [];
                                $.each(wards, function (key, val) {
                                    $("#wards option[value=" + key + "]").remove();
                                    $("#wards").multiSelect("refresh");
                                    deselectcommunities(key);
                                    //alert("Deselect value: "+key);
                                });
                            }
                        });
                        //$("#destination option[value='1']").remove();
                    }
                });
                $('#communities').multiSelect({
                	selectableHeader: "<input type='text' class='form-control search-input' autocomplete='off' placeholder='Search items...' style='margin-bottom:5px;'>",
           		 	selectionHeader: "<input type='text' class='form-control search-input2' autocomplete='off' style='visibility:hidden;margin-bottom:5px;'>",

	           		 afterInit: function(ms){
	         		    var that = this,
	         		        $selectableSearch = that.$selectableUl.prev(),
	         		        $selectionSearch = that.$selectionUl.prev(),
	         		        selectableSearchString = '#'+that.$container.attr('id')+' .ms-elem-selectable:not(.ms-selected)',
	         		        selectionSearchString = '#'+that.$container.attr('id')+' .ms-elem-selection.ms-selected';
	         		
	         		    
	         		    that.qs1 = $selectableSearch.quicksearch(selectableSearchString,
	         		    		{'onBefore': function () {
	         		    			that.qs1.cache();
	         		    	    	}
	         		    		}	
	         		    ).on('keydown', function(e){
	         		      if (e.which === 40){
	         		        that.$selectableUl.focus();
	         		        return false;
	         		      }
	         		   });
	         		    
	         		  },
                    afterSelect: function (values) {
                    	//this.qs1.cache();
                        $("#communities > option").each(function () {
                            if (values != this.value) {
                                if ($("#communities").attr('ismultiselect') != 'True') {
                                    //alert(ismultiselect_district);
                                    switch ($("#id_organisation_type").val()) {
                                        case 'TNRS'://residential institution
                                        case 'TNCB':
                                        case 'TNCW'://cwac
                                        case 'TNGD':
                                            ismultiselect_district = false;
                                            ismultiselect_ward = false;
                                            ismultiselect_community = false;
                                            break;
                                        case 'TNCM':
                                        case 'TNGS':
                                        case 'TNND':
                                        case 'TNCD':
                                        case 'TNCM':
                                            //alert("some false");
                                            ismultiselect_district = false;
                                            ismultiselect_ward = true;
                                            ismultiselect_community = true;
                                            break;
                                        case 'TNNM':
                                        case 'TNCP':
                                        case 'TNGP':
                                            ismultiselect_district = true;
                                            ismultiselect_ward = false;
                                            ismultiselect_community = false;
                                            break;
                                    }
                                    if (ismultiselect_community != true)
                                    {

                                        $(this).attr('disabled', 'disabled');
                                    }
                                }
                                //alert(this.text + ' ' + this.value);
                            }
                        });
                        $("#communities").multiSelect("refresh");
                    },
                    afterDeselect: function (values) {
                    	//this.qs1.cache();
                        $("#communities > option").each(function () {

                            $(this).removeAttr('disabled')
                            //alert(this.text + ' ' + this.value);

                        });
                        $("#communities").multiSelect("refresh");
                    }
                });
            };

            $("#id_organisation_type").change(handlelocationhidingandshowing);
            handlelocationhidingandshowing.apply($("#id_organisation_type"), [true]);


            $("#searchbtn").click(function () {
                //alert($("#searchorgtxt").val());
                $.ajax({
                    dataType: "html",
                    url: "/organisation/orglists?searchterm=" + $("#searchorgtxt").val(),
                    contentType: "application/json; charset=utf-8",
                    //data: {"districtid" : values},
                    success: function (results) {
                        if (results.length == 0) {
                            $('#orgresults tbody').html("<tr><td colspan='5'>No results for search term supplied</td></tr>");
                        }
                        else {
                            $('#orgresults tbody').empty().append(results);
                        }
                    },
                    error: function () {
                        $('#orgresults tbody').html("<tr><td span='5'>No results for search term supplied</td></tr>");
                        //alert($("#searchorgtxt").val())
                    }
                });
            });

            $(".gw-next").click(function () {
                var pagenumber = $(".gw-page").val();
                orgpagination(++pagenumber);
            });
            $(".gw-prev").click(function () {
                var pagenumber = $(".gw-page").val();
                orgpagination(--pagenumber);
            });
            orgpagination = function (pagenumber)
            {
                $(".totalrecords").empty().append("<i class='fa fa-spinner fa-spin'></i> Searching...");
                $(".searching").show();
                $(".totalrecords").hide();
                $.ajax({
                    dataType: "json",
                    url: "/organisation/orglists?searchterm=" + $("#searchorgtxt").val() + "&pagenumber=" + pagenumber + "&itemsperpage=" + $(".gw-pageSize").val() + "&orgtype=" + $("#orgtype").val() + "&search_all=" + $(".search_closed_org").is(':checked'),
                    success: function (results) {
                        if (results.length == 0) {
                            $('#orgresults tbody').html("<tr><td colspan='5'>No results for search term supplied</td></tr>");
                        }
                        else {
                            $('#orgresults tbody').empty().html("");
                            $.each(results, function (index) {

                                if (results[index].pageinfo == 'pageinfo')
                                {
                                    $(".countpages").empty().html(results[index].pagescount);
                                    $(".totalrecords").empty().append("Found " + results[index].total_records + " matching organisation(s)");

                                    //set the control number
                                    $(".gw-page").val(results[index].pagenumber);
                                    if (results[index].pagenumber >= results[index].pagescount)
                                    {
                                        if (!$(".gw-next").hasClass("disabled"))
                                        {
                                            $(".gw-next").addClass("disabled");
                                        }
                                        if ($(".gw-prev").hasClass("disabled"))
                                        {
                                            $(".gw-prev").removeClass("disabled");
                                        }

                                    }
                                    if (results[index].pagescount == 1) {
                                        if (!$(".gw-next").hasClass("disabled"))
                                        {
                                            $(".gw-next").addClass("disabled");
                                        }
                                        if (!$(".gw-prev").hasClass("disabled"))
                                        {
                                            $(".gw-prev").addClass("disabled");
                                        }
                                    }
                                    if (results[index].pagenumber <= 1) {
                                        if (!$(".gw-prev").hasClass("disabled"))
                                        {
                                            $(".gw-prev").addClass("disabled");
                                        }
                                    }
                                    if (results[index].pagenumber < results[index].pagescount) {
                                        if ($(".gw-next").hasClass("disabled"))
                                        {
                                            $(".gw-next").removeClass("disabled");
                                        }
                                    }
                                    if (results[index].pagenumber < results[index].pagescount && results[index].pagenumber > 1) {
                                        if ($(".gw-prevt").hasClass("disabled"))
                                        {
                                            $(".gw-prev").removeClass("disabled");
                                        }
                                    }

                                }
                                else {

                                    var tmp_span = "<span class='badge label-danger'>closed</span>";
                                    var is_active = false;
                                    if (results[index].is_active === true)
                                    {
                                        tmp_span = "";
                                    }
                                    var action_html = "";
                                    var row_html = "<tr>";
                                    if (!results[index].is_capture_app) {
                                        row_html = "<tr class='table_row_clickable' " +
                                                "href='/organisation/view_org?organisation_id=" + results[index].org_system_id + "' style='cursor:pointer'> ";
                                    }
                                    if (!results[index].is_capture_app) {
                                        action_html = "<td><a href='/organisation/update_org?organisation_id=" + results[index].org_system_id + "' class='btn btn-primary btn-xs'><i class='fa fa-edit'></i>&nbsp;Update</a></td> </tr>"
                                    }

                                    $("#orgresults > tbody").append(row_html +
                                            "<td>" + results[index].org_id + " " + tmp_span + "</td> " +
                                            "<td>" + results[index].org_name + "</td> " +
                                            "<td>" + results[index].unit_type_name + "</td> " +
                                            "<td>" + results[index].parent_organisation_name + "</td> " +
                                            "<td>" + results[index].location + "</td> " +
                                            action_html);
                                }
                            });
                        }
                    },
                    error: function () {
                        $('#orgresults tbody').html("<tr><td span='5'>No results for search term supplied</td></tr>");
                        //alert($("#searchorgtxt").val())
                    }
                });

                $(".searching").fadeOut('slow', function () {
                    $(".totalrecords").fadeIn('slow');
                });

                //$(".totalrecords").show();
            };
            $(".gw-pageSize").change(function () {
                $(".gw-pageSize").val($(".gw-pageSize").val());
                orgpagination(1);
            });
            $("#searchbtn2").click(function () {
                orgpagination(1);
            });

            $("#searchorgtxt").keypress(function (event) {
                var keycode = (event.keyCode ? event.keyCode : event.which);
                if (keycode == '13') {
                    orgpagination(1);
                    return false;
                }
            });
            
            

        

            //BEGIN PLUGINS MULTI SELECT

            deselectcommunities = function (wardid)
            {

                //alert(wardid)
                $.ajax({
                    dataType: "json",
                    url: "/organisation/communities?ward_id=" + wardid,
                    contentType: "application/json; charset=utf-8",
                    //data: {"districtid" : values},
                    success: function (communities) {
                        var items = [];
                        $.each(communities, function (key, val) {
                            $("#communities option[value=" + key + "]").remove();
                            $("#communities").multiSelect("refresh");
                            //alert("Deselect value: "+key);
                        });
                    }
                });
            };


            $('#workforce_ward').multiSelect();
            $('#workforce_district').multiSelect({
                afterSelect: function (values) {
                    $.ajax({
                        dataType: "json",
                        url: "/organisation/wards?district_id=" + values,
                        contentType: "application/json; charset=utf-8",
                        //data: {"districtid" : values},
                        success: function (wards) {
                            var items = [];
                            $.each(wards, function (key, val) {
                                $('#workforce_ward').multiSelect('addOption', {value: key, text: val});
                                //alert("Deselect value: "+key);
                            });
                        }
                    });

                    //$.getJSON( "/organisation/wards", {"districtid" : values}, function( data ) {
                    //	  var items = [];
                    //	  $.each( data, function( key, val ) {
                    //		  $('#wards').multiSelect('addOption', { value: key, text: val});
                    //		  //alert("Deselect value: "+key);
                    //	  });
                    //	});
                },
                afterDeselect: function (values) {
                    $.ajax({
                        dataType: "json",
                        url: "/organisation/wards?district_id=" + values,
                        contentType: "application/json; charset=utf-8",
                        //data: {"districtid" : values},
                        success: function (wards) {
                            var items = [];
                            $.each(wards, function (key, val) {
                                $("#workforce_ward option[value=" + key + "]").remove();
                                $("#workforce_ward").multiSelect("refresh");
                                //alert("Deselect value: "+key);
                            });
                        }
                    });
                    //$("#destination option[value='1']").remove();
                }
            });
            $('#workforce_community').multiSelect();

            $(".autocompleteovc").autocomplete({
                source: function (request, response) {
                    $.getJSON("/workforce/orgautocomp?org_name=" + request.term, function (data) {
                        response($.map(data, function (value, key) {
                            return {
                                label: concatenatearray(value, 0, 1, '\t'),
                                value: value
                            };

                        }));
                    });
                },
                minLength: 1,
                delay: 100,
                select: function (event, ui) {
                    event.preventDefault();
                    // feed hidden org name field
                    $('#org_unit_name_hidden').val(ui.item.value[1]);
                    // feed org id hidden field
                    $('#org_unit_id_hidden').val(ui.item.value[0]);
                    // feed eligible for primary parent hidden field
                    $('#eligible_primary_parent_hidden').val(ui.item.value[2]);

                }
            });

            $("#id_org_unit").on("autocompleteselect", function (event, ui) {
                var result = concatenatearray(ui.item.value, 0, 1, '\t');
                $("#id_org_unit").val(result);
            });

            concatenatearray = function (tmparray, startindex, lastindex, separator)
            {
                var charseparator = separator ? separator : '\t';
                var sindex = startindex ? startindex : 0;
                var eindex = lastindex < tmparray.length ? lastindex : tmparray.length - 1;
                var result = [];
                for (i = sindex; i <= eindex; i++) {
                    if (tmparray[i] === null || tmparray[i] === '')
                        continue;
                    result.push(tmparray[i]);
                }

                return result.join(charseparator)
            }

            $(".autocompleteben").autocomplete({
                source: function (request, response) {
                    $.getJSON("/beneficiary/ben_autocomplete?ben_name=" + request.term, function (data) {
                        response($.map(data, function (value, key) {
                            return {
                                label: value,
                                value: value
                            };

                        }));
                    });
                },
                minLength: 1,
                delay: 100,
                select: function (event, ui) {
                    //feed nrc hidden field
                    $('#ben_wfc_name_national_id').val(ui.item.value[1]);
                    // feed hidden org name field
                    $('#ben_name_hidden').val(ui.item.value[2]);
                    // feed org id hidden field
                    $('#ben_id_hidden').val(ui.item.value[0]);

                }
            });

            $(".autocompletewfc").autocomplete({
                source: function (request, response) {
                    $.getJSON("/beneficiary/ben_wfc_autocomplete?wfc_name=" + request.term, function (data) {
                        response($.map(data, function (value, key) {
                            return {
                                label: value[1] + ' - ' + value[2] + ' - ' + value[3],
                                value: value
                            };

                        }));
                    });
                },
                minLength: 1,
                delay: 100,
                select: function (event, ui) {
                    //feed nrc hidden field
                    $('#ben_wfc_name_national_id').val(ui.item.value[3]);
                    // feed hidden workforce field
                    $('#ben_wfc_name_hidden').val(ui.item.value[2]);
                    // feed workforce id hidden field
                    $('#ben_wfc_id_hidden').val(ui.item.value[1]);

                }
            });

            $(".autocompleteRes").autocomplete({
                source: function (request, response) {
                    $.getJSON("/beneficiary/ben_res_autocomplete?ins_name=" + request.term, function (data) {
                        response($.map(data, function (value, key) {
                            return {
                                label: value[0],
                                value: value
                            };

                        }));
                    });
                },
                minLength: 1,
                delay: 100,
                select: function (event, ui) {
                    $('#id_ben_res_name_hidden').val(ui.item.value[0]);
                    var vals = ui.item.value[1];
                    $('#id_res_id_hidden').val(vals[0]);

                    $('#id_ben_district').val('' + vals[1] + '')
                    handledistrictchange(vals[2]);
                    handlewardchange(vals[3], vals[2]);
                    event.preventDefault();
                }
            });
            
            $("#id_residential_institution").on( "autocompleteselect", function( event, ui ) {
		        var result = ui.item.value[0];
		        $( "#id_residential_institution" ).val(result);
		    });

            handledistrictchange = function (selval) {
                $.ajax({
                    dataType: "json",
                    url: "/beneficiary/wards?district_id=" + $("#id_ben_district").val(),
                    contentType: "application/json; charset=utf-8",
                    success: function (wards) {
                        $("#id_ben_ward").empty();
                        $("#id_community").empty();
                        $('<option>').val(0).text('-----').appendTo("#id_ben_ward");
                        $.each(wards, function (key, val) {
                            $('<option>').val(key).text(val).appendTo("#id_ben_ward");
                            if (selval != null)
                            {
                                if ($('#id_ben_ward').val() != selval)
                                {
                                    $('#id_ben_ward').val('' + selval + '');
                                }
                            }
                        });
                    }
                });
            };

            handlewardchange = function (selval, wardval) {
                searchval = $("#id_ben_ward").val();
                if (wardval != null)
                {
                    searchval = wardval;
                }
                $.ajax({
                    dataType: "json",
                    url: "/beneficiary/communities?ward_id=" + searchval,
                    contentType: "application/json; charset=utf-8",
                    success: function (wards) {
                        $("#id_community").empty();
                        $('<option>').val(0).text('-----').appendTo("#id_community");
                        $.each(wards, function (key, val) {
                            $('<option>').val(key).text(val).appendTo("#id_community");
                            if (selval != null)
                            {
                                if ($('#id_community').val() != selval)
                                {
                                    $('#id_community').val('' + selval + '');
                                }
                            }
                        });
                    }
                });
            };


            $("#id_ben_district").change(handledistrictchange);

            $("#id_ben_ward").change(handlewardchange);

            $('#id_workforce_type').change(function () {
                if ($('#id_workforce_type').val() == "TWVL") {
                    $('#div_id_steps_ovc_number').show();
                    $('#div_id_sign_number').hide();
                    $('#div_id_ts_number').hide();
                    $('#div_id_man_number').hide();
                } else if ($('#id_workforce_type').val() == "TWGE") {
                    $('#div_id_steps_ovc_number').hide();
                    $('#div_id_sign_number').show();
                    $('#div_id_ts_number').show();
                    $('#div_id_man_number').show();
                } else if ($('#id_workforce_type').val() == "TWNE") {
                    $('#div_id_steps_ovc_number').hide();
                    $('#div_id_sign_number').hide();
                    $('#div_id_ts_number').hide();
                    $('#div_id_man_number').hide();
                } else {
                    $('#div_id_steps_ovc_number').hide();
                    $('#div_id_sign_number').hide();
                    $('#div_id_ts_number').hide();
                    $('#div_id_man_number').hide();
                }
                clearControls();
            });
            function clearControls() {
                $('#id_steps_ovc_number').attr('value', '');
                $('#id_ts_number').attr('value', '');
                $('#id_sign_number').attr('value', '');
                $('#id_man_number').attr('value', '');
            }

            if ($('#id_workforce_type').val() == "TWVL") {
                $('#div_id_steps_ovc_number').show();
                $('#div_id_sign_number').hide();
                $('#div_id_ts_number').hide();
                $('#div_id_man_number').hide();
            } else if ($('#id_workforce_type').val() == "TWGE") {
                $('#div_id_steps_ovc_number').hide();
                $('#div_id_sign_number').show();
                $('#div_id_ts_number').show();
                $('#div_id_man_number').show();
            } else if ($('#id_workforce_type').val() == "TWNE") {
                $('#div_id_steps_ovc_number').hide();
                $('#div_id_sign_number').hide();
                $('#div_id_ts_number').hide();
                $('#div_id_man_number').hide();
            } else {
                $('#div_id_steps_ovc_number').hide();
                $('#div_id_sign_number').hide();
                $('#div_id_ts_number').hide();
                $('#div_id_man_number').hide();
            }

            populateBenTable();

            function populateBenTable() {
                var benData = $("#id_guardians_hidden").val();
                if (benData != null && benData != "" && benData != undefined)
                {
                    var splitBens = (benData).split('#');
                    if (splitBens != null)
                    {
                        for (index = 0; index < splitBens.length; ++index) {
                            splitBenData = (splitBens[index]).split(',');
                            if (splitBenData[1] == null || splitBenData[1] == "")
                            {
                                continue;
                            }

                            var guard_id = '"' + splitBenData[1] + '"';
                            if ($("#id_is_update").val() == "Yes")
                            {
                                $("#guardianTable").find('tbody')
                                        .append($('<tr>')
                                                .append($('<td id="1">')
                                                        .append($('<label>')
                                                                .text(splitBenData[1])
                                                                )
                                                        )
                                                .append($('<td id="2">')
                                                        .append($('<label>')
                                                                .text(splitBenData[2])
                                                                )
                                                        )
                                                .append($('<td id="3">')
                                                        .append($('<label>')
                                                                .text(splitBenData[3])
                                                                )
                                                        )
                                                .append($("<td id='4' > </td>"))
                                                .append($("<td id='5'> <button onclick='myDeleteFunction()' class='removebutton'><i class='fa fa-trash fa-2'></i></button></td>"))
                                                );
                            }
                            else
                            {
                                $("#guardianTable").find('tbody')
                                        .append($('<tr>')
                                                .append($('<td id="1">')
                                                        .append($('<label>')
                                                                .text(splitBenData[1])
                                                                )
                                                        )
                                                .append($('<td id="2">')
                                                        .append($('<label>')
                                                                .text(splitBenData[2])
                                                                )
                                                        )
                                                .append($('<td id="3">')
                                                        .append($('<label>')
                                                                .text(splitBenData[3])
                                                                )
                                                        )
                                                .append($("<td id='4' > <button type='button' onclick='useContacts(" + guard_id + ")' class='use-address'><i class='fa fa-check-square-o'></i></button></td>"))
                                                .append($("<td id='5'> <button onclick='myDeleteFunction()' class='removebutton'><i class='fa fa-trash fa-2'></i></button></td>"))
                                                );
                            }

                        }
                    }
                }
            }

            populateOrgTable();
            function populateOrgTable() {
                if ($("#id_org_data_hidden").length > 0) {
                    var hiddenText = $("#id_org_data_hidden").val();
                    if (hiddenText == '')
                        return;
                    $("#orgTable > tbody").html("");
					var org_data_items = JSON.parse(hiddenText);
					$.each(org_data_items, function (index) {
						var org_id = org_data_items[index].org_unit_id;
						var org_name = org_data_items[index].org_unit_name;
						var primary_org_unit = org_data_items[index].primary_org;
						var wra = org_data_items[index].reg_assistant;

                        //alert(index);
                        if (org_data_items[index] == "{}") {
                            return;
                        }

                        var hide = $("#id_reg_assistant_col1").is(":hidden");
                        var hidden = 'text';
                        if (hide) {
                            alert(hide);
                            hidden = '';
                        }
                        $("#orgTable").find('tbody')
                                .append($('<tr>')
                                        .append($('<td id="0">')
                                                .append($('<label>')
                                                        .text('')
                                                        .css('width', '1%')
                                                        )
                                                )
                                        .append($('<td id="org_unit_name">')
                                                .append($('<label>')
                                                        .text(org_name)
                                                        )
                                                )
                                        .append($('<td id="org_unit_id">')
                                                .append($('<label>')
                                                        .text(org_id)
                                                        )
                                                )
                                        .append($('<td id="primary_org">')
                                                .append($('<label>')
                                                        .text(primary_org_unit)
                                                        )
                                                )
                                        .append($('<td id="reg_assistant">')
                                                .append($('<label>')
                                                        .text(wra)
                                                        .css("visibility", hidden)
                                                        )
                                                )
                                        .append($("<td id='5'> <button onclick='myDeleteFunction()' class='removebutton'><i class='fa fa-trash fa-2'></i></button></td>"))
                                        );
                    });
                }
            }
            
            $("#searchwfctxt").keypress(function (event) {
                var keycode = (event.keyCode ? event.keyCode : event.which);
                if (keycode == '13') {
                	wfcpagination(1);
                    return false;
                }
            });

            $("#wfcsearchbtn").click(function () {
                wfcpagination(1);
            });
            var today = new Date();
            var month = new Array();
            month[0] = "January";
            month[1] = "February";
            month[2] = "March";
            month[3] = "April";
            month[4] = "May";
            month[5] = "June";
            month[6] = "July";
            month[7] = "August";
            month[8] = "September";
            month[9] = "October";
            month[10] = "November";
            month[11] = "December";
            var currentMonth = month[today.getMonth()];
            var formattedDate = (today.getDate() + '-' + currentMonth + '-' + today.getFullYear());


            $("#id_workforce_type_change_date").val(formattedDate);
            $("#id_parent_org_change_date").val(formattedDate);
            $("#id_work_locations_change_date").val(formattedDate);

            var myDate = new Date();
            var validDate = (myDate.getMonth() + 1) + '/' + myDate.getDate() + '/' + myDate.getFullYear();

            //$( ".datepicker" ).datepicker( "option", "dateFormat", 'd-MM-yy' );
            //$( ".datepicker" ).datepicker( "option", "changeMonth", true );
            //$( ".datepicker" ).datepicker( "option", "changeYear", true );

            wfcpagination = function (pagenumber)
            {
                $(".totalrecords").empty().append("<i class='fa fa-spinner fa-spin'></i> Searching...");
                $(".searching").show();
                $(".totalrecords").hide();

                $.ajax({
                    dataType: "json",
                    url: "/workforce/wfclists?searchterm=" + $("#searchwfctxt").val() + "&pagenumber=" + pagenumber + "&itemsperpage=" + $(".gw-pageSize").val() + "&wfctype=" + $("#wfctype").val(),
                    success: function (results) {
                        if (results.length == 0) {
                            $('#wfcresults tbody').html("<tr><td colspan='12'>No results for search term supplied</td></tr>");
                        }
                        else {
                            $('#wfcresults tbody').empty().html("");
                            $.each(results, function (index) {

                                if (results[index].pageinfo == 'pageinfo')
                                {
                                    $(".countpages").empty().html(results[index].pagescount);
                                    $(".totalrecords").empty().append("Found " + results[index].total_records + " matching member(s)");

                                    //set the control number
                                    $(".gw-page").val(results[index].pagenumber);
                                    if (results[index].pagenumber >= results[index].pagescount)
                                    {
                                        if (!$(".gw-nextben").hasClass("disabled"))
                                        {
                                            $(".gw-nextben").addClass("disabled");
                                        }
                                        if ($(".gw-prevben").hasClass("disabled"))
                                        {
                                            $(".gw-prevben").removeClass("disabled");
                                        }

                                    }
                                    if (results[index].pagescount == 1) {
                                        if (!$(".gw-nextben").hasClass("disabled"))
                                        {
                                            $(".gw-nextben").addClass("disabled");
                                        }
                                        if (!$(".gw-prevben").hasClass("disabled"))
                                        {
                                            $(".gw-prevben").addClass("disabled");
                                        }
                                    }
                                    if (results[index].pagenumber <= 1) {
                                        if (!$(".gw-prevben").hasClass("disabled"))
                                        {
                                            $(".gw-prevben").addClass("disabled");
                                        }
                                    }
                                    if (results[index].pagenumber < results[index].pagescount) {
                                        if ($(".gw-nextben").hasClass("disabled"))
                                        {
                                            $(".gw-nextben").removeClass("disabled");
                                        }
                                    }
                                    if (results[index].pagenumber < results[index].pagescount && results[index].pagenumber > 1) {
                                        if ($(".gw-prevbent").hasClass("disabled"))
                                        {
                                            $(".gw-prevben").removeClass("disabled");
                                        }
                                    }

                                }
                                else {
                                    var action_html = "";
                                    var row_html = "<tr>";
                                    if (!results[index].is_capture_app) {
                                        row_html = "<tr class='" + results[index].user_can_view + "' " +
                                                "href='/workforce/view_wfc?person_id_int=" + results[index].id_int + "' style='" + results[index].user_can_view_cursor + "'> "
                                    }
                                    if (!results[index].is_capture_app) {
                                        action_html = "<td><span style=\"display:" + results[index].user_can_edit + "\"><a href='/workforce/update_wfc?person_id=" + results[index].id_int + "' class=\"btn btn-primary btn-xs\"><i class=\"fa fa-edit\"></i>&nbsp;Update</a></span>&nbsp;<span style=\"display:" + results[index].user_can_assign_role + "\"><a href='/allocate_role?workforceid=" + results[index].id_int + "' class=\"btn btn-warning btn-xs\"><i class=\"fa fa-lock\"></i>&nbsp;Allocate role</a></span></td> </tr>";
                                    }
                                    $("#wfcresults > tbody").append(row_html +
                                            "<td>" + results[index].user_id + "</td> " +
                                            "<td>" + results[index].name + "</td> " +
                                            "<td>" + results[index].person_type + "</td> " +
                                            "<td>" + results[index].primary_org_unit_name + "</td> " +
                                            "<td>" + results[index].location + "</td> " +
                                            action_html
                                            );
                                }
                            });
                        }
                    },
                    error: function () {
                        $('#wfcresults tbody').html("<tr><td span='12'>No results for search term supplied</td></tr>");
                        //alert($("#searchorgtxt").val())
                    }
                });
                $(".searching").fadeOut('slow', function () {
                    $(".totalrecords").fadeIn('slow');
                });


                //$(".totalrecords").show();
            };

        });
