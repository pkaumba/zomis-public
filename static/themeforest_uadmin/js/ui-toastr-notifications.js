$(function () {
    var i = -1;
    var toastCount = 0;
    var $toastlast;
    
    function getErrorMessage (person){
    	var message = "";
    	$.each(person, function (key, val) {
    											message = message + "["+val+"] , ";
                                           });
    	message = message + "is already in this registry. Do you want to save?";
    	return message
    }
    
    
    var getMessage = function (person) {
    	var message = getErrorMessage (person);       
        var msgs = 	 '<div>'+
                    		message +
		             '</div>&nbsp;'+
		             '<div>'+
		             	'<button type="button" id="okBtn"  class="btn btn-primary">'+
		             		'Yes'+
		             	'</button>'+
		             	'<button type="button" id="surpriseBtn" class="btn" style="margin: 0 8px 0 8px">'+
		             		'No'+
		             	'</button>'+
		             '</div>';

        return msgs;
    };
    
    Name_Exists_Message = function (person) {
    	
        var shortCutFunction = 'warning';//$("#toastTypeGroup input:radio:checked").val();
        var msg = '';
        //var title = '';
        //var $showDuration = 0;
        //var $hideDuration = 0;
        //var $timeOut = 0;
        //var $extendedTimeOut = 0;
        //var $showEasing = 'swing';
        //var $hideEasing = 'linear';
        //var $showMethod = 'fadeIn';
        //var $hideMethod = 'fadeOut';
        var toastIndex = toastCount++;
        
        /*
        toastr.options = {
            closeButton: $('#closeButton').prop('checked'),
            debug: $('#debugInfo').prop('checked'),
            positionClass: $('#positionGroup input:radio:checked').val() || 'toast-top-right',
            onclick: null
        };
		
        if ($('#addBehaviorOnToastClick').prop('checked')) {
            toastr.options.onclick = function () {
                alert('You can perform some custom action after a toast goes away');
            };
        }
        
        if ($showDuration > 0) {
            toastr.options.showDuration = $showDuration;
        }

        if ($hideDuration > 0) {
            toastr.options.hideDuration = $hideDuration;
        }

        if ($extendedTimeOut) {
            toastr.options.extendedTimeOut = $extendedTimeOut;
        }
        */
        toastr.options.timeOut = 0;
        /*
        if ($showEasing.length) {
            toastr.options.showEasing = $showEasing;
        }

        

        if ($showMethod.length) {
            toastr.options.showMethod = $showMethod;
        }

        if ($hideMethod.length) {
            toastr.options.hideMethod = $hideMethod;
        }
        */
        if (!msg) {
            msg = getMessage(person);
        }

        $("#toastrOptions").text("Command: toastr["
                + shortCutFunction
                + "](\""
                + msg
                + (title ? "\", \"" + title : '')
                + "\")\n\ntoastr.options = "
                + JSON.stringify(toastr.options, null, 2)
        );

        var $toast = toastr[shortCutFunction](msg); // Wire up an event handler to a button in the toast, if it exists
        $toastlast = $toast;
        if ($toast.find('#okBtn').length) {
            $toast.delegate('#okBtn', 'click', function () {
            	$toast.remove();
            	return true;
            });
        }
        if ($toast.find('#surpriseBtn').length) {
            $toast.delegate('#surpriseBtn', 'click', function () {
            	$( "#id_err_msg" ).val(getErrorMessage (person));
            	return false;
            });
        }
    }
    
    $('#showtoast').click(function () {
        var shortCutFunction = $("#toastTypeGroup input:radio:checked").val();
        var msg = $('#message').val();
        var title = $('#title').val() || '';
        var $showDuration = $('#showDuration');
        var $hideDuration = $('#hideDuration');
        var $timeOut = $('#timeOut');
        var $extendedTimeOut = $('#extendedTimeOut');
        var $showEasing = $('#showEasing');
        var $hideEasing = $('#hideEasing');
        var $showMethod = $('#showMethod');
        var $hideMethod = $('#hideMethod');
        var toastIndex = toastCount++;

        toastr.options = {
            closeButton: $('#closeButton').prop('checked'),
            debug: $('#debugInfo').prop('checked'),
            positionClass: $('#positionGroup input:radio:checked').val() || 'toast-top-right',
            onclick: null
        };

        if ($('#addBehaviorOnToastClick').prop('checked')) {
            toastr.options.onclick = function () {
                alert('You can perform some custom action after a toast goes away');
            };
        }

        if ($showDuration.val().length) {
            toastr.options.showDuration = $showDuration.val();
        }

        if ($hideDuration.val().length) {
            toastr.options.hideDuration = $hideDuration.val();
        }

        if ($timeOut.val().length) {
            toastr.options.timeOut = $timeOut.val();
        }

        if ($extendedTimeOut.val().length) {
            toastr.options.extendedTimeOut = $extendedTimeOut.val();
        }

        if ($showEasing.val().length) {
            toastr.options.showEasing = $showEasing.val();
        }

        if ($hideEasing.val().length) {
            toastr.options.hideEasing = $hideEasing.val();
        }

        if ($showMethod.val().length) {
            toastr.options.showMethod = $showMethod.val();
        }

        if ($hideMethod.val().length) {
            toastr.options.hideMethod = $hideMethod.val();
        }

        if (!msg) {
            msg = getMessage();
        }

        $("#toastrOptions").text("Command: toastr["
                + shortCutFunction
                + "](\""
                + msg
                + (title ? "\", \"" + title : '')
                + "\")\n\ntoastr.options = "
                + JSON.stringify(toastr.options, null, 2)
        );

        var $toast = toastr[shortCutFunction](msg, title); // Wire up an event handler to a button in the toast, if it exists
        $toastlast = $toast;
        if ($toast.find('#okBtn').length) {
            $toast.delegate('#okBtn', 'click', function () {
                alert('you clicked me. i was toast #' + toastIndex + '. goodbye!');
                $toast.remove();
            });
        }
        if ($toast.find('#surpriseBtn').length) {
            $toast.delegate('#surpriseBtn', 'click', function () {
                alert('Surprise! you clicked me. i was toast #' + toastIndex + '. You could perform an action here.');
            });
        }
    });
    function getLastToast(){
        return $toastlast;
    }
    $('#clearlasttoast').click(function () {
        toastr.clear(getLastToast());
    });
    $('#cleartoasts').click(function () {
        toastr.clear();
    });
});